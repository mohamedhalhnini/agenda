<?php

/* InstructionBundle:Client:layout.html.twig */
class __TwigTemplate_42d4fb8076b29ea6a6651e06b2cb739b9d18cec93d01b069fd0fbf588b4a48a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'meta' => array($this, 'block_meta'),
            'title' => array($this, 'block_title'),
            'stylesheet' => array($this, 'block_stylesheet'),
            'mesScript' => array($this, 'block_mesScript'),
            'favicon' => array($this, 'block_favicon'),
            'js_head' => array($this, 'block_js_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>

<!--[if IEMobile 7]><html class=\"no-js iem7 oldie\"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class=\"no-js ie7 oldie\" lang=\"en\"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class=\"no-js ie8 oldie\" lang=\"en\"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class=\"no-js ie9\" lang=\"en\"><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)]><!-->
<html class=\"no-js\" lang=\"en\">
<!--<![endif]-->

<head>
    ";
        // line 12
        $this->displayBlock('meta', $context, $blocks);
        // line 35
        echo "
    <title>";
        // line 36
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    ";
        // line 38
        $this->displayBlock('stylesheet', $context, $blocks);
        // line 65
        echo "
    ";
        // line 66
        $this->displayBlock('mesScript', $context, $blocks);
        // line 67
        echo "
    ";
        // line 68
        $this->displayBlock('favicon', $context, $blocks);
        // line 106
        echo "    ";
        $this->displayBlock('js_head', $context, $blocks);
        // line 109
        echo "</head>


  


       
<body>

            <div class=\"columns\">
                <div class=\"twelve-columns twelve-columns-tablet twelve-columns-mobile\">
                   <center>
                    ";
        // line 121
        $this->displayBlock('content', $context, $blocks);
        // line 125
        echo "              </center>
                </div>
            </div>
</body>

 

 
 

</html>";
    }

    // line 12
    public function block_meta($context, array $blocks = array())
    {
        // line 13
        echo "        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">

        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">

        <!-- http://davidbcalhoun.com/2010/viewport-metatag -->
        <meta name=\"HandheldFriendly\" content=\"True\">
        <meta name=\"MobileOptimized\" content=\"320\">
        <!-- Microsoft clear type rendering -->
        <meta http-equiv=\"cleartype\" content=\"on\">

        <!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
        <meta name=\"application-name\" content=\"Developr Admin Skin\">
        <meta name=\"msapplication-tooltip\" content=\"Cross-platform admin template.\">
        <meta name=\"msapplication-starturl\" content=\"http://www.display-inline.fr/demo/developr\">
        <!-- These custom tasks are examples, you need to edit them to show actual pages -->
        <meta name=\"msapplication-task\" content=\"name=Agenda;action-uri=http://www.display-inline.fr/demo/developr/agenda.html;icon-uri=http://www.display-inline.fr/demo/developr/";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/favicon.ico"), "html", null, true);
        echo "\">
        <meta name=\"msapplication-task\" content=\"name=My profile;action-uri=http://www.display-inline.fr/demo/developr/profile.html;icon-uri=http://www.display-inline.fr/demo/developr/";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/favicon.ico"), "html", null, true);
        echo "\">
        <!-- http://www.kylejlarson.com/blog/2012/iphone-5-web-design/ and http://darkforge.blogspot.fr/2010/05/customize-android-browser-scaling-with.html -->
        <meta name=\"viewport\" content=\"user-scalable=0, initial-scale=1.0, target-densitydpi=115\">
    ";
    }

    // line 36
    public function block_title($context, array $blocks = array())
    {
        echo "I-Agenda ";
    }

    // line 38
    public function block_stylesheet($context, array $blocks = array())
    {
        // line 39
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/DataTables/jquery.dataTables.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/glDatePicker/developr.fixed.css?v=1"), "html", null, true);
        echo "\">

        <!-- For all browsers -->
        <link rel=\"stylesheet\" href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/reset.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/style.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/colors.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"print\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/print.css?v=1"), "html", null, true);
        echo "\">
        <!-- For progressively larger displays -->
        <link rel=\"stylesheet\" media=\"only all and (min-width: 480px)\" href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/480.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"only all and (min-width: 768px)\" href=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/768.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"only all and (min-width: 992px)\" href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/992.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"only all and (min-width: 1200px)\" href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/1200.css?v=1"), "html", null, true);
        echo "\">
        <!-- For Retina displays -->
        <link rel=\"stylesheet\" media=\"only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)\" href=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/2x.css?v=1"), "html", null, true);
        echo "\">
        <!-- Webfonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>

        <!-- Additional styles -->
        <link rel=\"stylesheet\" href=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/agenda.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/dashboard.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/form.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/modal.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/progress-slider.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/switches.css?v=1"), "html", null, true);
        echo "\">
    ";
    }

    // line 66
    public function block_mesScript($context, array $blocks = array())
    {
    }

    // line 68
    public function block_favicon($context, array $blocks = array())
    {
        // line 69
        echo "        <!-- For Modern Browsers -->
        <link rel=\"shortcut icon\" href=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/favicon.png"), "html", null, true);
        echo "\">
        <!-- For everything else -->
        <link rel=\"shortcut icon\" href=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/favicon.ico"), "html", null, true);
        echo "\">
         <link rel=\"stylesheet\" href=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/table.css?v=1"), "html", null, true);
        echo "\"> ";
        // line 74
        echo "
        <!-- iOS web-app metas -->
        <meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
        <meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">

        <!-- iPhone ICON -->
        <link rel=\"apple-touch-icon\" href=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/apple-touch-icon-ipad-retina.png"), "html", null, true);
        echo "\" sizes=\"144x144\">

        <!-- iPhone SPLASHSCREEN (320x460) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/iphone.png"), "html", null, true);
        echo "\" media=\"(device-width: 320px)\">
        <!-- iPhone (Retina) SPLASHSCREEN (640x960) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/iphone-retina.png"), "html", null, true);
        echo "\" media=\"(device-width: 320px) and (-webkit-device-pixel-ratio: 2)\">
        <!-- iPhone 5 SPLASHSCREEN (640×1096) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/iphone5.png"), "html", null, true);
        echo "\" media=\"(device-height: 568px) and (-webkit-device-pixel-ratio: 2)\">
        <!-- iPad (portrait) SPLASHSCREEN (748x1024) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/ipad-portrait.png"), "html", null, true);
        echo "\" media=\"(device-width: 768px) and (orientation: portrait)\">
        <!-- iPad (landscape) SPLASHSCREEN (768x1004) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/ipad-landscape.png"), "html", null, true);
        echo "\" media=\"(dimg/favicons/apple-touch-icon.png')}}\" sizes=\"57x57\">
        <!-- iPad ICON -->
        <link rel=\"apple-touch-icon\" href=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/apple-touch-icon-ipad.png"), "html", null, true);
        echo "\" sizes=\"72x72\">
        <!-- iPhone (Retina) ICON -->
        <link rel=\"apple-touch-icon\" href=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/apple-touch-icon-retina.png"), "html", null, true);
        echo "\" sizes=\"114x114\">
        <!-- iPad (Retina) ICON -->
        <link rel=\"apple-touch-icon\" href=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/evice-width: 768px) and (orientation: landscape"), "html", null, true);
        echo "\">
        <!-- iPad (Retina, portrait) SPLASHSCREEN (2048x1496) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/ipad-portrait-retina.png"), "html", null, true);
        echo "\" media=\"(device-width: 1536px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2)\">
        <!-- iPad (Retina, landscape) SPLASHSCREEN (1536x2008) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/ipad-landscape-retina.png"), "html", null, true);
        echo "\" media=\"(device-width: 1536px)  and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2)\">
        <!-- JavaScript at bottom except for Modernizr -->


    ";
    }

    // line 106
    public function block_js_head($context, array $blocks = array())
    {
        // line 107
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/modernizr.custom.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    // line 121
    public function block_content($context, array $blocks = array())
    {
        // line 122
        echo "

                    ";
    }

    public function getTemplateName()
    {
        return "InstructionBundle:Client:layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  320 => 122,  317 => 121,  310 => 107,  307 => 106,  298 => 101,  293 => 99,  288 => 97,  283 => 95,  278 => 93,  273 => 91,  268 => 89,  263 => 87,  258 => 85,  253 => 83,  247 => 80,  239 => 74,  236 => 73,  232 => 72,  227 => 70,  224 => 69,  221 => 68,  216 => 66,  210 => 63,  206 => 62,  202 => 61,  198 => 60,  194 => 59,  190 => 58,  182 => 53,  177 => 51,  173 => 50,  169 => 49,  165 => 48,  160 => 46,  156 => 45,  152 => 44,  148 => 43,  142 => 40,  137 => 39,  134 => 38,  128 => 36,  120 => 31,  116 => 30,  97 => 13,  94 => 12,  80 => 125,  78 => 121,  64 => 109,  61 => 106,  59 => 68,  56 => 67,  54 => 66,  51 => 65,  49 => 38,  44 => 36,  41 => 35,  39 => 12,  26 => 1,);
    }
}
