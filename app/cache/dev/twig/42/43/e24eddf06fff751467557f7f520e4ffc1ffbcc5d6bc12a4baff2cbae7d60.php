<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_4243e24eddf06fff751467557f7f520e4ffc1ffbcc5d6bc12a4baff2cbae7d60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 6
        echo "
<form action=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\" id=\"form-login\" class=\"input-wrapper blue-gradient glossy\" title=\"Connexion\">
    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />
<ul class=\"inputs black-input large\">
    <li><span class=\"icon-user mid-margin-right\"></span>
";
        // line 12
        echo "    <input type=\"text\" placeholder=\"Identifiant\" id=\"username\" class=\"input-unstyled\" name=\"_username\" value=\"";
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\"   />
    </li>
    <li><span class=\"icon-lock mid-margin-right\"></span>
";
        // line 16
        echo "    <input type=\"password\" id=\"password\" name=\"_password\"   class=\"input-unstyled\" placeholder=\"Mot de passe\" />
    </li>
</ul>
     <p class=\"button-height\">
    <button type=\"submit\" class=\"button anthracite-active  float-right\" id=\"_submit\" name=\"_submit\"  >";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "</button>    
    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"1\" checked=\"checked\" class=\"switch tiny mid-margin-right with-tooltip\" title=\"Enable auto-login\" />
    <label for=\"remember_me\">Se souvenir de moi.";
        // line 22
        echo "</label>
   
     </p>
</form>
   

\t<!-- Template functions -->
\t<!-- Scripts -->
\t<script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/jquery-1.10.2.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/setup.js"), "html", null, true);
        echo "\"></script>

\t<!-- Template functions -->
\t<script src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.input.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.message.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.notify.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.tooltip.js"), "html", null, true);
        echo "\"></script>


    <script type=\"text/javascript\">

    ";
        // line 42
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 43
            echo "        \$(document).ready(function()
\t\t{
\t\t// What about a notification?
\t\tnotify('Erreur', '";
            // line 46
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "', {
\t\t\tautoClose: true,
\t\t\tdelay: 500,
\t\t\ticon: '";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/demo/icon.png"), "html", null, true);
            echo "'
\t\t});
                });
        ";
        }
        // line 52
        echo " 
    </script>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 52,  115 => 49,  109 => 46,  104 => 43,  102 => 42,  94 => 37,  90 => 36,  86 => 35,  82 => 34,  76 => 31,  72 => 30,  62 => 22,  57 => 20,  51 => 16,  44 => 12,  38 => 8,  34 => 7,  31 => 6,  28 => 5,  11 => 1,);
    }
}
