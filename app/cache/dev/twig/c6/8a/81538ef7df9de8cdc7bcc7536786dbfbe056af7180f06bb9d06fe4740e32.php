<?php

/* FOSUserBundle:Resetting:reset_content.html.twig */
class __TwigTemplate_c68a81538ef7df9de8cdc7bcc7536786dbfbe056af7180f06bb9d06fe4740e32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<form action=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_resetting_reset", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))), "html", null, true);
        echo "\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " method=\"POST\"class=\"input-wrapper orange-gradient glossy\" title=\"Mot de passe oublié?\">
";
        // line 8
        echo "<ul class=\"inputs black-input large\">
    ";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
 </ul>   
    <div>
        <button type=\"submit\" class=\"button glossy full-width\" >Envoyer un nouveau mot de passe</button>

    </div>
</form>
\t";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 9,  28 => 8,  22 => 3,  19 => 2,);
    }
}
