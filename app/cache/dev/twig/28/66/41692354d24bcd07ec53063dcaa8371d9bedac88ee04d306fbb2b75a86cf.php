<?php

/* InstructionBundle:Instruction:listPermanente.html.twig */
class __TwigTemplate_286641692354d24bcd07ec53063dcaa8371d9bedac88ee04d306fbb2b75a86cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<table id=\"sorting-advanced\" class=\"table responsive-table responsive-table-on dataTable\" aria-describedby=\"sorting-advanced_info\">
    <thead>
        <tr role=\"row\">
            <th scope=\"col\" class=\"sorting_disabled\" role=\"columnheader\" rowspan=\"1\" colspan=\"1\" aria-label=\"\" style=\"width: 12px;\">
                <input type=\"checkbox\" name=\"check-all\" id=\"check-all\" value=\"1\"></th>
            <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Id</th>
            <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Titre</th>
            <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Priorite</th>
            <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Client</th>
            <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">PhraseUseuelle</th>
                ";
        // line 12
        if (((isset($context["archived"]) ? $context["archived"] : $this->getContext($context, "archived")) == 1)) {
            echo " 
                <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\"> date d'archivation</th>
                ";
        }
        // line 15
        echo "           <th scope=\"col\" width=\"60\" class=\"align-center sorting_disabled\" role=\"columnheader\" rowspan=\"1\" colspan=\"1\" aria-label=\"Actions\" style=\"width: 100px;\">Actions</th>

        </tr>
    </thead>

    <tfoot>
        ";
        // line 24
        echo "    </tfoot>
    <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\">
        ";
        // line 26
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["permanentInstructions"]) ? $context["permanentInstructions"] : $this->getContext($context, "permanentInstructions")));
        foreach ($context['_seq'] as $context["_key"] => $context["instruction"]) {
            // line 27
            echo "            <tr class=\"odd\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "id", array()), "html", null, true);
            echo "\">
                <th scope=\"row\" class=\"checkbox-cell  sorting_1\"><input type=\"checkbox\" name=\"checked[]\" id=\"check-1\" value=\"1\"></th>
                <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "id", array()), "html", null, true);
            echo "  </td>
                <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "titre", array()), "html", null, true);
            echo "</td>
                <td> ";
            // line 31
            if (($this->getAttribute($context["instruction"], "priorite", array()) == 0)) {
                echo " Haute";
            } elseif (($this->getAttribute($context["instruction"], "priorite", array()) == 1)) {
                echo "Normale ";
            }
            echo "</td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["instruction"], "client", array()), "nom", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["instruction"], "client", array()), "prenom", array()), "html", null, true);
            echo "</td>
                <td> ";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["instruction"], "phraseusuelle", array()), "text", array()), "html", null, true);
            echo "</td>
               ";
            // line 34
            if (((isset($context["archived"]) ? $context["archived"] : $this->getContext($context, "archived")) == 1)) {
                echo " <td> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["instruction"], "datearchivation", array()), "d-m-Y"), "html", null, true);
                echo "</td> ";
            }
            // line 35
            echo "                    <td><a class=\"confirm\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("instruction_update", array("id" => $this->getAttribute($context["instruction"], "id", array()))), "html", null, true);
            echo "\" ><span class=\"button  icon-pencil \"></span> </a> 
                        <a  onclick=\"openDeleteConfirm(";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "id", array()), "html", null, true);
            echo ")\" >   <span class=\"button icon-trash \"></span></a>
                        ";
            // line 37
            if (((isset($context["archived"]) ? $context["archived"] : $this->getContext($context, "archived")) == 0)) {
                echo " 
                            <a  onclick=\"openArchivateConfirm(";
                // line 38
                echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "id", array()), "html", null, true);
                echo ")\" >   <span class=\"button icon-box \"></span></a>
                        ";
            }
            // line 40
            echo "                    </td>
                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['instruction'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "                </tbody>
            </table>





";
    }

    public function getTemplateName()
    {
        return "InstructionBundle:Instruction:listPermanente.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 43,  110 => 40,  105 => 38,  101 => 37,  97 => 36,  92 => 35,  86 => 34,  82 => 33,  76 => 32,  68 => 31,  64 => 30,  60 => 29,  54 => 27,  50 => 26,  46 => 24,  38 => 15,  32 => 12,  19 => 1,);
    }
}
