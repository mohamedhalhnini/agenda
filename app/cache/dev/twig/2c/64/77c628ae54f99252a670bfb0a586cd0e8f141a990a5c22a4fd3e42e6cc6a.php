<?php

/* RendezVousBundle:Default:getHolidays.html.twig */
class __TwigTemplate_2c6477c628ae54f99252a670bfb0a586cd0e8f141a990a5c22a4fd3e42e6cc6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "RendezVousBundle:Default:getHolidays.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["holidays"]) ? $context["holidays"] : $this->getContext($context, "holidays")));
        foreach ($context['_seq'] as $context["_key"] => $context["h"]) {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $context["h"], "d-m-Y"), "html", null, true);
            echo "<br>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['h'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "RendezVousBundle:Default:getHolidays.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 4,  31 => 3,  28 => 2,  11 => 1,);
    }
}
