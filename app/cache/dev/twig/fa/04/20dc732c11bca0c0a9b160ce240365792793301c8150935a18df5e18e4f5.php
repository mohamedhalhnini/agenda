<?php

/* AgendaMemoBundle:Memo:list.html.twig */
class __TwigTemplate_fa0420dc732c11bca0c0a9b160ce240365792793301c8150935a18df5e18e4f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base2.html.twig", "AgendaMemoBundle:Memo:list.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'my_script' => array($this, 'block_my_script'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    Liste des mémos
";
    }

    // line 5
    public function block_my_script($context, array $blocks = array())
    {
        // line 6
        $this->displayParentBlock("my_script", $context, $blocks);
        echo "
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("asset/js/developr.modal.js"), "html", null, true);
        echo "\"></script>
<script>

    // Call template init (optional, but faster if called manually)
    \$.template.init();

    // Table sort - DataTables
    var table = \$('#sorting-advanced');
    table.dataTable({
        'scrollY': \"10px\",
        'scrollCollapse': true,
        'scrollX': true,
        'aoColumnDefs': [
            {'bSortable': false, 'aTargets': [0, 5]}
        ],
        'sPaginationType': 'full_numbers',
        'sDom': '<\"dataTables_header\"lfr>t<\"dataTables_footer\"ip>',
        'fnInitComplete': function(oSettings)
        {
            // Style length select
            table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
            tableStyled = true;
        }
    });
    /*
    \$('#sorting-advanced tfoot th').each( function(){
       var title = \$('#sorting-advanced thead th').eq(\$(this).index()).text();
        \$(this).html('<input class=\"input\" type=\"text\" placeholder=\"Chercher '+title+'\"/>');
    });
    table.columns().eq(0).each( function(colIdx){
        \$('input',table.column(colIdx).footer()).on('keyup change',function(){
            table
                    .column(colIdx)
                    .search(this.value)
                    .draw();
        });
    });*/

    // Table sort - styled
    \$('#sorting-example1').tablesorter({
        headers: {
            0: {sorter: false},
            5: {sorter: false}
        }
    }).on('click', 'tbody td', function(event)
            {
                // Do not process if something else has been clicked
                if (event.target !== this)
                {
                    return;
                }

                var tr = \$(this).parent(),
                        row = tr.next('.row-drop'),
                        rows;

                // If click on a special row
                if (tr.hasClass('row-drop'))
                {
                    return;
                }

                // If there is already a special row
                if (row.length > 0)
                {
                    // Un-style row
                    tr.children().removeClass('anthracite-gradient glossy');

                    // Remove row
                    row.remove();

                    return;
                }

                // Remove existing special rows
                rows = tr.siblings('.row-drop');
                if (rows.length > 0)
                {
                    // Un-style previous rows
                    rows.prev().children().removeClass('anthracite-gradient glossy');

                    // Remove rows
                    rows.remove();
                }

                // Style row
                tr.children().addClass('anthracite-gradient glossy');

                // Add fake row
                \$('<tr class=\"row-drop\">' +
                        '<td colspan=\"' + tr.children().length + '\">' +
                        '<div class=\"float-right\">' +
                        '<button type=\"submit\" class=\"button glossy mid-margin-right\">' +
                        '<span class=\"button-icon\"><span class=\"icon-mail\"></span></span>' +
                        'Send mail' +
                        '</button>' +
                        '<button type=\"submit\" class=\"button glossy\">' +
                        '<span class=\"button-icon red-gradient\"><span class=\"icon-cross\"></span></span>' +
                        'Remove' +
                        '</button>' +
                        '</div>' +
                        '<strong>Name:</strong> John Doe<br>' +
                        '<strong>Account:</strong> admin<br>' +
                        '<strong>Last connect:</strong> 05-07-2011<br>' +
                        '<strong>Email:</strong> john@doe.com' +
                        '</td>' +
                        '</tr>').insertAfter(tr);

            }).on('sortStart', function()
            {
                var rows = \$(this).find('.row-drop');
                if (rows.length > 0)
                {
                    // Un-style previous rows
                    rows.prev().children().removeClass('anthracite-gradient glossy');

                    // Remove rows
                    rows.remove();
                }
            });

    // Table sort - simple
    \$('#sorting-example2').tablesorter({
        headers: {
            5: {sorter: false}
        }
    });

</script>
<script>
/*function openConfirm()
{
\$.modal.confirm('Confirmez-vous vouloir supprimer ce mémo ?', function()
{
\$.modal.alert('La supression est effectuée !');

}, function()
{
\$.modal.alert('La supression est annulée !');
});
};*/

function openModal()
{
    \$.modal({
        title: 'Suppression du mémo',
        content: '<h4 class=\"green-gradient glossy\">Suppression du mémo est effectuée</h4>',
        width: 300,
        resizable: false,
        scrolling: false,
        actions: {
            'Fermer' : {
                color: 'red',
                click: function(win) { win.closeModal(); }
            }
        },
        buttons: {
            'Annuler': {
                classes:\t'blue-gradient glossy',
                click:\t\tfunction(win) { win.closeModal(); }
            }
        }
    });
}
function openComplexModal(id)
{
    \$.modal({
        content: '<p>Confirmez-vous vouloir supprimer ce mémo ?</p>',
        title: 'Suppression de mémo',
        width: 300,
        resizable: false,
        scrolling: false,
        actions: {
            'Fermer' : {
                color: 'red',
                click: function(win) { win.closeModal(); }
            }
        },
        buttons: {
            'Oui,supprimer': {
                classes:\t'red-gradient glossy',
                click: function(result) {
                    window.location.href=\"http://localhost/monagenda_/web/app_dev.php/memo/supp/\"+id;
                    window.event.onload = openModal()
                }
            },
            'Non': {
                classes:\t'blue-gradient glossy',
                click:\t\tfunction(win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
};
function openComplexModal1(id)
{
    \$.modal({
        content: '<p>Confirmez-vous vouloir modifier ce mémo ?</p>',
        title: 'Modification de mémo',
        width: 300,
        resizable: false,
        scrolling: false,
        actions: {
            'Fermer' : {
                color: 'red',
                click: function(win) { win.closeModal(); }
            }
        },
        buttons: {
            'Oui,modifier': {
                classes:\t'red-gradient glossy',
                click: function(result) {
                    window.location.href=\"http://localhost/monagenda_/web/app_dev.php/memo/edit/\"+id;
                }
            },
            'Non': {
                classes:\t'blue-gradient glossy',
                click:\t\tfunction(win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
};
</script>
";
    }

    // line 232
    public function block_content($context, array $blocks = array())
    {
        // line 233
        echo "<div class=\"with-padding\">
    <div class=\"with-padding with-border\">
<h3>Liste des mémos</h3>
<table class=\"table responsive-table\" id=\"sorting-advanced\">
    <thead>
    <tr>
        <th align=\"center\">ID</th>
        <th align=\"center\">Phrase usuelle</th>
        <th align=\"center\">Client Fréquent</th>
        <th align=\"center\">Détail</th>
        <th align=\"center\">Date d'enregistrement</th>
        <th align=\"center\">Prioritaire</th>
        <th align=\"center\">Alerte</th>
        <th align=\"center\">Status de transmission</th>
        <th align=\"center\">Permanente</th>
        <th align=\"center\">Action</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th align=\"center\">ID</th>
        <th align=\"center\">Phrase usuelle</th>
        <th align=\"center\">Client Fréquent</th>
        <th align=\"center\">Détail</th>
        <th align=\"center\">Date d'enregistrement</th>
        <th align=\"center\">Prioritaire</th>
        <th align=\"center\">Alerte</th>
        <th align=\"center\">Status de transmission</th>
        <th align=\"center\">Permanente</th>
        <th align=\"center\">Action</th>
    </tr>
    </tfoot>

    <tbody>
    ";
        // line 267
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["memos"]) ? $context["memos"] : $this->getContext($context, "memos")));
        foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
            // line 268
            echo "    <tr>
        <td align=\"center\">
            ";
            // line 270
            echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "id", array()), "html", null, true);
            echo "
        </td>
        <td align=\"center\">
            ";
            // line 273
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["m"], "phraseUsuelle", array()), "text", array()), "html", null, true);
            echo "
        </td>
        <td align=\"center\">
            ";
            // line 276
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["m"], "clientFrequent", array()), "nom", array()), "html", null, true);
            echo "
        </td>
        <td align=\"center\">
            ";
            // line 279
            echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "detail", array()), "html", null, true);
            echo "
        </td>
        <td align=\"center\">
            ";
            // line 282
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["m"], "dateenregistrement", array()), "Y-m-d"), "html", null, true);
            echo "
        </td>
        <td align=\"center\">
            ";
            // line 285
            if (($this->getAttribute($context["m"], "priorite", array()) == 1)) {
                // line 286
                echo "            Oui
            ";
            } else {
                // line 288
                echo "            Non
            ";
            }
            // line 290
            echo "        </td>
        <td align=\"center\">
            ";
            // line 292
            if (($this->getAttribute($context["m"], "alert", array()) == 1)) {
                // line 293
                echo "            Par Mail
            ";
            } elseif (($this->getAttribute(            // line 294
$context["m"], "alert", array()) == 2)) {
                // line 295
                echo "            par SMS
            ";
            } elseif (($this->getAttribute(            // line 296
$context["m"], "alert", array()) == 4)) {
                // line 297
                echo "            par FAX
            ";
            } elseif (($this->getAttribute(            // line 298
$context["m"], "alert", array()) == 3)) {
                // line 299
                echo "            Par Mail et par SMS
            ";
            } elseif (($this->getAttribute(            // line 300
$context["m"], "alert", array()) == 5)) {
                // line 301
                echo "            Par Mail et par FAX
            ";
            } elseif (($this->getAttribute(            // line 302
$context["m"], "alert", array()) == 6)) {
                // line 303
                echo "            Par SMS et par FAX
            ";
            } else {
                // line 305
                echo "            Par Mail, par SMS et par Fax
            ";
            }
            // line 307
            echo "        </td>
        <td align=\"center\">
            ";
            // line 309
            if (($this->getAttribute($context["m"], "status", array()) == 1)) {
                // line 310
                echo "            Oui
            ";
            } else {
                // line 312
                echo "            Auto
            ";
            }
            // line 314
            echo "        </td>
        <td align=\"center\">
            ";
            // line 316
            echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "permanante", array()), "html", null, true);
            echo "
        </td>
        <td class=\"center\">
            <a href=\"javascript: openComplexModal(";
            // line 319
            echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "id", array()), "html", null, true);
            echo ")\" title=\"Supprimer\" alt=\"Supprimer\"> <span class=\"icon-trash\"></span> </a>
            <a href=\"javascript: openComplexModal1(";
            // line 320
            echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "id", array()), "html", null, true);
            echo ")\" title=\"Modifier\" alt=\"Modifier\"> <span class=\"icon-pencil\"></span> </a>
        </td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 324
        echo "    </tbody>
</table>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "AgendaMemoBundle:Memo:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  438 => 324,  428 => 320,  424 => 319,  418 => 316,  414 => 314,  410 => 312,  406 => 310,  404 => 309,  400 => 307,  396 => 305,  392 => 303,  390 => 302,  387 => 301,  385 => 300,  382 => 299,  380 => 298,  377 => 297,  375 => 296,  372 => 295,  370 => 294,  367 => 293,  365 => 292,  361 => 290,  357 => 288,  353 => 286,  351 => 285,  345 => 282,  339 => 279,  333 => 276,  327 => 273,  321 => 270,  317 => 268,  313 => 267,  277 => 233,  274 => 232,  45 => 7,  41 => 6,  38 => 5,  33 => 3,  30 => 2,  11 => 1,);
    }
}
