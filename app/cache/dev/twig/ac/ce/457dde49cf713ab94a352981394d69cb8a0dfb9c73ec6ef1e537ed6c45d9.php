<?php

/* ::login_base.html.twig */
class __TwigTemplate_acce457dde49cf713ab94a352981394d69cb8a0dfb9c73ec6ef1e537ed6c45d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'meta' => array($this, 'block_meta'),
            'title' => array($this, 'block_title'),
            'stylesheet' => array($this, 'block_stylesheet'),
            'js_head' => array($this, 'block_js_head'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'fos_user_content_password' => array($this, 'block_fos_user_content_password'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!DOCTYPE html>

<!--[if IEMobile 7]><html class=\"no-js iem7 oldie linen\"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class=\"no-js ie7 oldie linen\" lang=\"en\"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class=\"no-js ie8 oldie linen\" lang=\"en\"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class=\"no-js ie9 linen\" lang=\"en\"><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)]><!--><html class=\"no-js linen\" lang=\"en\"><!--<![endif]-->

<head>
     ";
        // line 11
        $this->displayBlock('meta', $context, $blocks);
        // line 30
        echo "\t<title>";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        
\t";
        // line 32
        $this->displayBlock('stylesheet', $context, $blocks);
        // line 82
        echo "    
        ";
        // line 83
        $this->displayBlock('js_head', $context, $blocks);
        // line 87
        echo "</head>

<body>

\t<div id=\"container\">

\t\t<hgroup id=\"login-title\" class=\"large-margin-bottom\">
\t\t\t<h1 class=\"login-title\">M'Agenda</h1>
\t\t\t<h5>&copy; Your Company</h5>
\t\t</hgroup>

\t\t<div id=\"form-wrapper\">

        <div id=\"form-block\" class=\"scratch-metal\">
                <div id=\"form-viewport\">

                      
                            ";
        // line 104
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 105
        echo "    
                             ";
        // line 106
        $this->displayBlock('fos_user_content_password', $context, $blocks);
        // line 107
        echo "  
";
        // line 119
        echo "                </div>
        </div>

\t\t</div>

\t</div>

\t<!-- JavaScript at the bottom for fast page loading -->

\t<!-- Scripts -->
\t<script src=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/jquery-1.10.2.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/setup.js"), "html", null, true);
        echo "\"></script>

\t<!-- Template functions -->
\t<script src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.input.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.message.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.notify.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.tooltip.js"), "html", null, true);
        echo "\"></script>

\t

</body>
</html>";
    }

    // line 11
    public function block_meta($context, array $blocks = array())
    {
        // line 12
        echo "\t<meta charset=\"utf-8\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <meta name=\"description\" content=\"\">
\t<meta name=\"author\" content=\"\">

\t<!-- http://davidbcalhoun.com/2010/viewport-metatag -->
\t<meta name=\"HandheldFriendly\" content=\"True\">
\t<meta name=\"MobileOptimized\" content=\"320\">

\t<!-- http://www.kylejlarson.com/blog/2012/iphone-5-web-design/ and http://darkforge.blogspot.fr/2010/05/customize-android-browser-scaling-with.html -->
\t<meta name=\"viewport\" content=\"user-scalable=0, initial-scale=1.0, target-densitydpi=115\">
        
\t<!-- iOS web-app metas -->
\t<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
\t<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">
        \t<!-- Microsoft clear type rendering -->
\t<meta http-equiv=\"cleartype\" content=\"on\">
        ";
    }

    // line 30
    public function block_title($context, array $blocks = array())
    {
        echo "M-Agenda";
    }

    // line 32
    public function block_stylesheet($context, array $blocks = array())
    {
        // line 33
        echo "\t<!-- For all browsers -->
\t<link rel=\"stylesheet\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/reset.css?v=1"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/style.css?v=1"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/colors.css?v=1"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" media=\"print\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/print.css?v=1"), "html", null, true);
        echo "\">
\t<!-- For progressively larger displays -->
\t<link rel=\"stylesheet\" media=\"only all and (min-width: 480px)\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/480.css?v=1"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" media=\"only all and (min-width: 768px)\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/768.css?v=1"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" media=\"only all and (min-width: 992px)\" href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/992.css?v=1"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" media=\"only all and (min-width: 1200px)\" href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/1200.css?v=1"), "html", null, true);
        echo "\">
\t<!-- For Retina displays -->
\t<link rel=\"stylesheet\" media=\"only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)\" href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/2x.css?v=1"), "html", null, true);
        echo "\">

\t<!-- Additional styles -->
\t<link rel=\"stylesheet\" href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/form.css?v=1"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/switches.css?v=1"), "html", null, true);
        echo "\">

\t<!-- Login pages styles -->
\t<link rel=\"stylesheet\" media=\"screen\" href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/login.css?v=1"), "html", null, true);
        echo "\">

\t<!-- For Modern Browsers -->
\t<link rel=\"shortcut icon\" href=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/favicon.png"), "html", null, true);
        echo "\">
\t<!-- For everything else -->
\t<link rel=\"shortcut icon\" href=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/favicon.ico"), "html", null, true);
        echo "\">

\t<!-- iPhone ICON -->
\t<link rel=\"apple-touch-icon\" href=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/apple-touch-icon.png"), "html", null, true);
        echo "\" sizes=\"57x57\">
\t<!-- iPad ICON -->
\t<link rel=\"apple-touch-icon\" href=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/apple-touch-icon-ipad.png"), "html", null, true);
        echo "\" sizes=\"72x72\">
\t<!-- iPhone (Retina) ICON -->
\t<link rel=\"apple-touch-icon\" href=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/apple-touch-icon-retina.png"), "html", null, true);
        echo "\" sizes=\"114x114\">
\t<!-- iPad (Retina) ICON -->
\t<link rel=\"apple-touch-icon\" href=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/apple-touch-icon-ipad-retina.png"), "html", null, true);
        echo "\" sizes=\"144x144\">

\t<!-- iPhone SPLASHSCREEN (320x460) -->
\t<link rel=\"apple-touch-startup-image\" href=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/iphone.png"), "html", null, true);
        echo "\" media=\"(device-width: 320px)\">
\t<!-- iPhone (Retina) SPLASHSCREEN (640x960) -->
\t<link rel=\"apple-touch-startup-image\" href=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/iphone-retina.png"), "html", null, true);
        echo "\" media=\"(device-width: 320px) and (-webkit-device-pixel-ratio: 2)\">
\t<!-- iPhone 5 SPLASHSCREEN (640×1096) -->
\t<link rel=\"apple-touch-startup-image\" href=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/iphone5.png"), "html", null, true);
        echo "\" media=\"(device-height: 568px) and (-webkit-device-pixel-ratio: 2)\">
\t<!-- iPad (portrait) SPLASHSCREEN (748x1024) -->
\t<link rel=\"apple-touch-startup-image\" href=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/ipad-portrait.png"), "html", null, true);
        echo "\" media=\"(device-width: 768px) and (orientation: portrait)\">
\t<!-- iPad (landscape) SPLASHSCREEN (768x1004) -->
\t<link rel=\"apple-touch-startup-image\" href=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/ipad-landscape.png"), "html", null, true);
        echo "\" media=\"(device-width: 768px) and (orientation: landscape)\">
\t<!-- iPad (Retina, portrait) SPLASHSCREEN (2048x1496) -->
\t<link rel=\"apple-touch-startup-image\" href=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/ipad-portrait-retina.png"), "html", null, true);
        echo "\" media=\"(device-width: 1536px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2)\">
\t<!-- iPad (Retina, landscape) SPLASHSCREEN (1536x2008) -->
\t<link rel=\"apple-touch-startup-image\" href=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/ipad-landscape-retina.png"), "html", null, true);
        echo "\" media=\"(device-width: 1536px)  and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2)\">
        ";
    }

    // line 83
    public function block_js_head($context, array $blocks = array())
    {
        // line 84
        echo "\t<!-- JavaScript at bottom except for Modernizr -->
\t<script src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/modernizr.custom.js"), "html", null, true);
        echo "\"></script>
        ";
    }

    // line 104
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 105
        echo "                             ";
    }

    // line 106
    public function block_fos_user_content_password($context, array $blocks = array())
    {
        // line 107
        echo "                             ";
    }

    public function getTemplateName()
    {
        return "::login_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  307 => 107,  304 => 106,  300 => 105,  297 => 104,  291 => 85,  288 => 84,  285 => 83,  279 => 80,  274 => 78,  269 => 76,  264 => 74,  259 => 72,  254 => 70,  249 => 68,  243 => 65,  238 => 63,  233 => 61,  228 => 59,  222 => 56,  217 => 54,  211 => 51,  205 => 48,  201 => 47,  195 => 44,  190 => 42,  186 => 41,  182 => 40,  178 => 39,  173 => 37,  169 => 36,  165 => 35,  161 => 34,  158 => 33,  155 => 32,  149 => 30,  128 => 12,  125 => 11,  115 => 136,  111 => 135,  107 => 134,  103 => 133,  97 => 130,  93 => 129,  81 => 119,  78 => 107,  76 => 106,  73 => 105,  71 => 104,  52 => 87,  50 => 83,  47 => 82,  45 => 32,  39 => 30,  37 => 11,  25 => 1,);
    }
}
