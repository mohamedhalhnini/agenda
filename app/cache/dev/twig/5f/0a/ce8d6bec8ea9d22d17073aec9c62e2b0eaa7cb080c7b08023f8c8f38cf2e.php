<?php

/* InstructionBundle:Instruction:update.html.twig */
class __TwigTemplate_5f0ace8d6bec8ea9d22d17073aec9c62e2b0eaa7cb080c7b08023f8c8f38cf2e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "InstructionBundle:Instruction:update.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <form action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("instruction_update", array("id" => $this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "id", array()))), "html", null, true);
        echo "\" method=\"POST\">
    <table class=\"table simple-table \" id=\"sorting-example1\">

    <thead>
    <tr>
        <!--\t\t\t\t\t\t<th scope=\"col\"><input type=\"checkbox\" name=\"check-all\" id=\"check-all\" value=\"1\"></th>
                                                        <th scope=\"col\" class=\"header\">Text</th>
                                                        <th scope=\"col\" width=\"15%\" class=\"align-center hide-on-mobile header\">Date</th>
                                                        <th scope=\"col\" width=\"15%\" class=\"align-center hide-on-mobile-portrait header\">Status</th>
                                                        <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet header\">Tags</th>-->
        <th scope=\"col\" width=\"30%\" colspan=\"2\" class=\"align-center\">Enregistrer une instruction</th>
    </tr>
    </thead>

    <tfoot>
    <tr>
        <td colspan=\"6\">

        </td>
    </tr>
    </tfoot>

    <tbody>
    <tr>
        <td><strong>Titre</strong></td>
        <td  width=\"80%\" class=\"hide-on-mobile\"><div class=\"row\">
                <p class=\"button-height time\">
                    <select id=\"phraseUsuelle\" name=\"phraseUsuelle\" style=\"width:373px\" class=\"blue-gradient select validate[required]\">
                        <option value=\"0\">-- Phrases usuelles --</option>
                        ";
        // line 32
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["phrases"]) ? $context["phrases"] : $this->getContext($context, "phrases")));
        foreach ($context['_seq'] as $context["_key"] => $context["phrase"]) {
            // line 33
            echo "                            <option ";
            if (($this->getAttribute($this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "phraseusuelle", array()), "id", array()) == $this->getAttribute($context["phrase"], "id", array()))) {
                echo " selected='";
                echo twig_escape_filter($this->env, $this->getAttribute($context["phrase"], "id", array()), "html", null, true);
                echo "' ";
            }
            echo "value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["phrase"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["phrase"], "text", array()), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phrase'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "                    </select>
                </p>
                <p>
                    <input type=\"radio\" name=\"radio1\" id=\"radio_1\" value=\"1\" class=\"radio mid-margin-left\"> <label for=\"radio-1\" >Début</label>
                    <input type=\"radio\" name=\"radio1\" id=\"radio_2\" value=\"2\" class=\"radio mid-margin-left\"> <label for=\"radio-1\" >Contient</label>

                    <button type=\"submit\" class=\"button icon-plus blue-gradient\" name=\"Ajout\">Ajout</button>
                    <button type=\"submit\" class=\"button icon-download blue-gradient\" name=\"Histo\">Histo</button>
                    <button type=\"submit\" class=\"button icon-new-tab blue-gradient\" name=\"Notes\">Notes</button>
                    <button type=\"submit\" class=\"button icon-new-tab blue-gradient\" name=\"Nouv\">Nouv</button>
                    <button type=\"submit\" class=\"button icon-pencil blue-gradient\" name=\"Motif\">Motif</button>


                    <!--                                        <a href=\"javascript:void(0)\" class=\"button icon-star blue-gradient\">Ajout</a>-->
                    <!--                                        <a href=\"javascript:void(0)\" class=\"button icon-download blue-gradient\">Histo</a>
                                                            <a href=\"javascript:void(0)\" class=\"button icon-new-tab blue-gradient\">Notes</a>
                                                            <a href=\"javascript:void(0)\" class=\"button icon-new-tab blue-gradient\">Nouv</a>
                                                            <a href=\"javascript:void(0)\" class=\"button icon-pencil blue-gradient\">Modif</a>-->
                </p>
                <p class=\"button-height\">

                    <input type=\"text\" name=\"input-2\" id=\"input-2\" size=\"20\" class=\"input\" value=\"Styled text input\">

                    <select id=\"client\" name=\"client\" class=\" blue-gradient select validate[required]\">
                        <option value=\"\">-- Clients fréquents --</option>
                        ";
        // line 60
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clients"]) ? $context["clients"] : $this->getContext($context, "clients")));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 61
            echo "                            <option  ";
            if (($this->getAttribute($this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "client", array()), "id", array()) == $this->getAttribute($context["client"], "id", array()))) {
                echo " selected='";
                echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "id", array()), "html", null, true);
                echo "' ";
            }
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "id", array()), "html", null, true);
            echo "\"  >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "nom", array()), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "prenom", array()), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "                    </select>

                </p>
                <p>
                    <input type=\"text\" name=\"titre\" id=\"titre\" class=\"input full-width\" value=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "titre", array()), "html", null, true);
        echo "\">
                </p>
            </div></td>
    </tr>
    <tr>
        <td><strong>Détails</strong></td>
        <td class=\"hide-on-mobile\">
            <textarea name=\"detail\" id=\"detail\" class=\"input full-width autoexpanding\" style=\"overflow: hidden; resize: none; height: 124px;\" > ";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "detail", array()), "html", null, true);
        echo "</textarea>

        </td>

    </tr>
    <tr>
        <td><strong>Validité</strong></td>
        <td class=\"hide-on-mobile\">
            <p>
                <input type=\"radio\" ";
        // line 83
        if (($this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "type", array()) == 0)) {
            echo " checked=\"true\"";
        }
        echo " name=\"type\"   value=\"0\" class=\"radio mid-margin-left\"> <label for=\"radio_1\" >Permanente(valable tout les temp)</label>
            </p>


            <p class=\"button-height\">
                <input type=\"radio\" name=\"type\"  ";
        // line 88
        if (($this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "type", array()) == 1)) {
            echo " checked=\"true\"";
        }
        echo " value=\"1\" class=\"radio mid-margin-left\"> <label for=\"radio_2\" >Ponctuel le : </label>

                        <span class=\"input\">
                            <span class=\"icon-calendar\"></span>
                            <input type=\"text\" name=\"dateLe\" id=\"special-input-3\" class=\"input-unstyled datepicker gldp-el\"  ";
        // line 92
        if (($this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "type", array()) == 1)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "le", array()), "d/m/Y"), "html", null, true);
            echo "\" ";
        }
        echo " gldp-id=\"gldp-2690895398\">
                        </span>

            </p>


            <p class=\"button-height\">
                <input type=\"radio\"  ";
        // line 99
        if (($this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "type", array()) == 2)) {
            echo " checked=\"true\"";
        }
        echo " name=\"type\" value=\"2\" class=\"radio mid-margin-left\"> <label for=\"radio_3\"  >Periode du   : </label>

                        <span class=\"input\">
                            <span class=\"icon-calendar\"></span>
                            <input type=\"text\" name=\"dateDe\" id=\"special-input-3\" class=\"input-unstyled datepicker gldp-el\" ";
        // line 103
        if (($this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "type", array()) == 2)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "de", array()), "d/m/Y"), "html", null, true);
            echo "\" ";
        }
        echo " gldp-id=\"gldp-2690895399\">
                        </span>
                <label>au :</label>
                        <span class=\"input\">
                            <span class=\"icon-calendar\"></span>
                            <input type=\"text\" name=\"dateA\" id=\"special-input-3\" class=\"input-unstyled datepicker gldp-el\" ";
        // line 108
        if (($this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "type", array()) == 2)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "a", array()), "d/m/Y"), "html", null, true);
            echo "\" ";
        }
        echo " gldp-id=\"gldp-2690895390\">
                        </span>

            </p>
        </td>

    </tr>
    <tr>
        <td><strong>Priorité</strong></td>
        <td class=\"hide-on-mobile\">
            <p>
                <input type=\"radio\"";
        // line 119
        if (($this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "priorite", array()) == 0)) {
            echo " checked=\"true\"";
        }
        echo " name=\"radio3\" id=\"radio_1\" value=\"1\" class=\"radio mid-margin-left\"> <label for=\"radio-1\" >Normale</label>
                <input type=\"radio\" ";
        // line 120
        if (($this->getAttribute((isset($context["instruction"]) ? $context["instruction"] : $this->getContext($context, "instruction")), "priorite", array()) == 1)) {
            echo " checked=\"true\"";
        }
        echo " name=\"radio3\" id=\"radio_2\" value=\"1\" class=\"radio mid-margin-left\"> <label for=\"radio-1\" >Haute</label>
            </p>
        </td>

    </tr>
    <tr>

        <td class=\"hide-on-mobile align-center\" colspan=\"2\">

            <button name=\"enregistrer\" type=\"submit\" class=\"button icon-save blue-gradient\">Enregistrer</button>
            <button type=\"submit\" class=\"button icon-forbidden blue-gradient\">Annuler</button>
            <button type=\"submit\" class=\"button icon-folder blue-gradient\">Accéder aux instructions archivées</button>
            <button type=\"submit\" class=\"button icon-printer blue-gradient\">Imprimer</button>

            <!--
                                            <a href=\"javascript:void(0)\" class=\"button icon-save blue-gradient\">Enregistrer</a>
                                            <a href=\"javascript:void(0)\" class=\"button icon-forbidden blue-gradient\">Annuler</a>
                                            <a href=\"javascript:void(0)\" class=\"button icon-folder blue-gradient\">Accéder aux instructions archivées</a>
                                            <a href=\"javascript:void(0)\" class=\"button icon-printer blue-gradient\">Imprimer</a>
            -->

        </td>

    </tr>
    <tr>

        <td class=\"hide-on-mobile\" colspan=\"2\">
            <div class=\"columns\">
                <p>
                <div class=\"four-columns\"><p><strong>Tarifs</strong></p>
                    <p><span class=\"small\">à partir de 40 euro</span></p></div>
                <div class=\"two-columns \"><strong>Permanemt</strong></div>
                <div class=\"one-column \"><strong>Haute</strong></div>
                <div class=\"three-columns\">
                            <span class=\"button-group\">

                                <button type=\"submit\" class=\"button \">M</button>
                                <button type=\"submit\" class=\"button \">A</button>
                                <button type=\"submit\" class=\"button \">S</button>
                                <button type=\"submit\" class=\"button \">I</button>

                            </span>
                </div>
                <div class=\"one-column \"><a href=\"javascript:void(0)\" class=\"button\">impr</a></div>
                <input type=\"checkbox\" name=\"checkbox-1\" id=\"checkbox-1\" value=\"1\" class=\"checkbox mid-margin-left replacement\">

            </div>

        </td>

    </tr>
    <tr>

        <td class=\"hide-on-mobile\" colspan=\"2\">
            <div class=\"columns\">
                <p>
                <div class=\"four-columns\"><p><strong>Pose au CHU</strong></p>
                    <p><span class=\"small\">à partir de 40 euro</span></p></div>
                <div class=\"two-columns \"><strong>Permanemt</strong></div>
                <div class=\"one-column \"><strong>Haute</strong></div>
                <div class=\"three-columns\">
                            <span class=\"button-group\">

                                <button type=\"submit\" class=\"button \">M</button>
                                <button type=\"submit\" class=\"button \">A</button>
                                <button type=\"submit\" class=\"button \">S</button>
                                <button type=\"submit\" class=\"button\">I</button>
                            </span>
                </div>
                <div class=\"one-column \"><a href=\"javascript:void(0)\" class=\"button\">impr</a></div>
                <input type=\"checkbox\" name=\"checkbox-1\" id=\"checkbox-1\" value=\"1\" class=\"checkbox mid-margin-left replacement\">


            </div>

        </td>

    </tr>
    <tr>

        <td class=\"hide-on-mobile\" colspan=\"2\">
            <div class=\"columns\">
                <p>
                <div class=\"four-columns\"><p><strong>Rendez-vous du matin</strong></p>
                    <p><span class=\"small\">à partir de 40 euro</span></p></div>
                <div class=\"two-columns \"><strong>Permanemt</strong></div>
                <div class=\"one-column \"><strong>Haute</strong></div>
                <div class=\"three-columns\">
                            <span class=\"button-group\">
                                <button type=\"submit\" class=\"button \">M</button>
                                <button type=\"submit\" class=\"button \">A</button>
                                <button type=\"submit\" class=\"button \">S</button>
                                <button type=\"submit\" class=\"button\">I</button>
                            </span>
                </div>
                <div class=\"one-column \"><a href=\"javascript:void(0)\" class=\"button\">impr</a></div>
                <input type=\"checkbox\" name=\"checkbox-1\" id=\"checkbox-1\" value=\"1\" class=\"checkbox mid-margin-left replacement\">


            </div>

        </td>

    </tr>
    <tr>

        <td class=\"hide-on-mobile\" colspan=\"2\">
            <div class=\"columns\">
                <p>
                <div class=\"four-columns\"><p><strong>Rendez-vous du matin</strong></p>
                    <p><span class=\"small\">à partir de 40 euro</span></p></div>
                <div class=\"two-columns \"><strong>Permanemt</strong></div>
                <div class=\"one-column \"><strong>Haute</strong></div>
                <div class=\"three-columns\">
                            <span class=\"button-group\">
                                <button type=\"submit\" class=\"button \">M</button>
                                <button type=\"submit\" class=\"button \">A</button>
                                <button type=\"submit\" class=\"button \">S</button>
                                <button type=\"submit\" class=\"button\">I</button>
                            </span>
                </div>
                <div class=\"one-column \"><a href=\"javascript:void(0)\" class=\"button\">impr</a></div>
                <input type=\"checkbox\" name=\"checkbox-1\" id=\"checkbox-1\" value=\"1\" class=\"checkbox mid-margin-left replacement\">


            </div>

        </td>

    </tr>
    <tr>

        <td class=\"hide-on-mobile\" colspan=\"2\">
            <div class=\"columns\">
                <p>
                <div class=\"four-columns\"><p><strong>Rendez-vous du matin</strong></p>
                    <p><span class=\"small\">à partir de 40 euro</span></p></div>
                <div class=\"two-columns \"><strong>Permanemt</strong></div>
                <div class=\"one-column \"><strong>Haute</strong></div>
                <div class=\"three-columns\">
                            <span class=\"button-group\">
                                <button type=\"submit\" class=\"button \">M</button>
                                <button type=\"submit\" class=\"button \">A</button>
                                <button type=\"submit\" class=\"button \">S</button>
                                <button type=\"submit\" class=\"button\">I</button>
                            </span>
                </div>
                <div class=\"one-column \"><a href=\"javascript:void(0)\" class=\"button\">impr</a></div>
                <input type=\"checkbox\" name=\"checkbox-1\" id=\"checkbox-1\" value=\"1\" class=\"checkbox mid-margin-left replacement\">

            </div>

        </td>

    </tr>
    </tbody>

    </table>
    </form>
";
    }

    public function getTemplateName()
    {
        return "InstructionBundle:Instruction:update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 120,  234 => 119,  216 => 108,  204 => 103,  195 => 99,  181 => 92,  172 => 88,  162 => 83,  150 => 74,  140 => 67,  134 => 63,  116 => 61,  112 => 60,  85 => 35,  68 => 33,  64 => 32,  31 => 3,  28 => 2,  11 => 1,);
    }
}
