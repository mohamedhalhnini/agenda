<?php

/* AgendaMemoBundle:Client:details.html.twig */
class __TwigTemplate_a3b4e0f1ca2ed348769132bc27d4cae95b134e3c1fe4ad31eb3ef49e55c53940 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
    <meta charset=\"utf-8\">
</head>
<body>
<div>
    <p>Accédez à votre espace Web pour consulter / prendre des rendez-vous, à l'adresse :</p>
    <p align=\"center\"><a href=\"#\">Espace web</a></p>
    <br><br><p><u>Rappel des identifiants pour <b>";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["nom"]) ? $context["nom"] : $this->getContext($context, "nom")), "html", null, true);
        echo "</b>:</u><br><br>
        - adresse email : ";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email")), "html", null, true);
        echo "<br><br>- mot de passe :";
        echo twig_escape_filter($this->env, (isset($context["pw"]) ? $context["pw"] : $this->getContext($context, "pw")), "html", null, true);
        echo "<br><br></p></div>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "AgendaMemoBundle:Client:details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 10,  29 => 9,  19 => 1,);
    }
}
