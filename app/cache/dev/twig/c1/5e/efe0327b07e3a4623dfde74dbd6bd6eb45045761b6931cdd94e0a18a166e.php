<?php

/* AgendaMemoBundle:Client:modifier.html.twig */
class __TwigTemplate_c15eefe0327b07e3a4623dfde74dbd6bd6eb45045761b6931cdd94e0a18a166e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AgendaMemoBundle:Client:modifier.html.twig", 1);
        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_javascripts($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("asset/js/developr.modal.js"), "html", null, true);
        echo "\"></script>
    <script>
    function generer_password() {
    pwIsExist();
    var ok = 'azertyupqsdfghjkmwxcvbn23456789AZERTYUPQSDFGHJKMWXCVBN';
    var pass = '';
    longueur = 8;
    for(i=0;i<longueur;i++){
    var wpos = Math.round(Math.random()*ok.length);
    pass+=ok.substring(wpos,wpos+1);
    }
    document.getElementById(\"pw\").value = pass;
    }
    function openConfirm()
    {
        \$.modal.confirm('Challenge accepted?', function()
        {
            \$.modal.alert('Me gusta!');

        }, function()
        {
            \$.modal.alert('Meh.');
        });
    };
    function openLoadingModal()
    {
        var timeout;

        \$.modal({
            contentAlign: 'center',
            width: 240,
            title: 'Loading',
            content: '<div style=\"line-height: 25px; padding: 0 0 10px\"><span id=\"modal-status\">Envoie d\\'Email en cours...</span><br><span id=\"modal-progress\">0%</span></div>',
            buttons: {},
            scrolling: false,
            actions: {
                'Cancel': {
                    color:\t'red',
                    click:\tfunction(win) { win.closeModal(); }
                }
            },
            onOpen: function()
            {
                // Progress bar
                var progress = \$('#modal-progress').progress(100, {
                            size: 200,
                            style: 'large',
                            barClasses: ['anthracite-gradient', 'glossy'],
                            stripes: true,
                            darkStripes: false,
                            showValue: false
                        }),

                // Loading state
                        loaded = 0,

                // Window
                        win = \$(this),

                // Status text
                        status = \$('#modal-status'),

                // Function to simulate loading
                        simulateLoading = function()
                        {
                            ++loaded;
                            progress.setProgressValue(loaded+'%', true);
                            if (loaded === 100)
                            {
                                progress.hideProgressStripes().changeProgressBarColor('green-gradient');
                                status.text('Done!');
                                /*win.getModalContentBlock().message('Content loaded!', {
                                 classes: ['green-gradient', 'align-center'],
                                 arrow: 'bottom'
                                 });*/
                                setTimeout(function() { win.closeModal(); }, 1500);
                            }
                            else
                            {
                                if (loaded === 1)
                                {
                                    status.text('Loading data...');
                                    progress.changeProgressBarColor('blue-gradient');
                                }
                                else if (loaded === 25)
                                {
                                    status.text('Loading assets (1/3)...');
                                }
                                else if (loaded === 45)
                                {
                                    status.text('Loading assets (2/3)...');
                                }
                                else if (loaded === 85)
                                {
                                    status.text('Loading assets (3/3)...');
                                }
                                else if (loaded === 92)
                                {
                                    status.text('Entrain d\\'envoyer...');
                                }
                                timeout = setTimeout(simulateLoading, 50);
                            }
                        };

                // Start
                timeout = setTimeout(simulateLoading, 2000);
            },
            onClose: function()
            {
                // Stop simulated loading if needed
                clearTimeout(timeout);
            }
        });
    };
    function verif_champPw()
    {
        if (document.getElementById(\"pw\").value == \"\")
        {   if(!confirm(\"Aucun mot de passe n'a encore été affecté au client fréquent. Cliquer sur Annuler pour revenir en spécifier un ou sur OK pour poursuivre\",
                            {width:300, okLabel: \"Ok\",
                                width:300, cancelLabel: \"Annuler\",
                                buttonClass: \"buttons\",
                                cancel:function(win){debug(\"cancel confirm panel\");} ,
                                ok:function(win) {debug(\"cancel confirm panel\"); return true;}
                            })
            )
            {
                alert('success');
            }
        }else{
            return false;
        }
        return true;
    }
    function pwIsExist()
    {
        if (document.getElementById(\"pw\").value != \"\")
        {   if(!confirm(\"Une valeur est déjà présente pour le mot de passe , souhaitez-vous \\n la remplacer une autre ?\",
                        {width:300, okLabel: \"Ok\",
                            width:300, cancelLabel: \"Annuler\",
                            buttonClass: \"buttons\",
                            cancel:function(win){debug(\"cancel confirm panel\");} ,
                            ok:function(win) {debug(\"cancel confirm panel\"); return true;}
                        })
        )
        {
        }
        }else{
            return false;
        }
        return true;
    }
    function openAlert()
    {
        \$.modal.alert('This is an alert message');
    };

    function openComplexModal()
    {
        \$.modal({
            content: \"<p>Aucun mot de passe n'a encore été affecté au client fréquent.\\n Cliquer sur OK pour revenir en spécifier un \\n ou sur Annuler pour poursuivre</p>\",
            title: 'Générer PDF',
            width: 300,
            resizable: false,
            scrolling: false,
            actions: {
                'Fermer' : {
                    color: 'red',
                    click: function(win) { win.closeModal(); }
                }
            },
            buttons: {
                'Annuler': {
                    classes:\t'red-gradient glossy',
                    click: function(result) {
                        window.event.onload = openAlert();
                    }
                },
                'Ok': {
                    classes:\t'blue-gradient glossy',
                    click: function(win) { win.closeModal(); }
                }
            },
            buttonsLowPadding: true
        });
    };
    \$(document).ready(function(){
        \$(\"#form\").validationEngine();
    });

    </script>
    <script>
        var email = document.getElementById(\"email\").innerHTML;
    </script>
    <script>
        var pw = document.getElementById(\"pw\").innerHTML;
    </script>
";
    }

    // line 201
    public function block_content($context, array $blocks = array())
    {
        // line 202
        echo "    <form name=\"form1\" id=\"form\" action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("agenda_client_update", array("id" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")))), "html", null, true);
        echo "\" method=\"POST\">
        <table class=\"table simple-table responsive-table responsive-table-on w \" style=\"  height: 10em;\" >
            ";
        // line 205
        echo "            <tfoot>
            <tr>
                <td colspan=\"2\"  class=\"align-center\">
                    <div class=\"columns\">
                        <div class=\"five-columns\">
                        </div>
                        <div class=\"two-columns\">
                            <button type=\"submit\" class=\"button blue-gradient Huge full-width²\" value=\"Modifier\" name=\"modifier\" onclick=\"recupNom()\" >Modifier</button>
                        </div>
                    </div>
                </td>
            </tr>
            </tfoot>
            <tbody>
            <tr>
                <td>
                </td>
                <td>
                    <input style=\"width:20em\" type=\"hidden\" class=\"input validate[required]\" name=\"nom\" id=\"nom\" value=\"";
        // line 223
        echo twig_escape_filter($this->env, (isset($context["nom"]) ? $context["nom"] : $this->getContext($context, "nom")), "html", null, true);
        echo "\">
                </td>
            </tr>
            <tr>
                <td>Civilité </td>
                <td>
                    <div class=\"three-columns\">
                        <select class=\"select expandable-list\" style=\"width:15em\" name=\"civilite\" >
                            <option value=\"0\" >--Civilité--</option>
                            <option value=\"1\" >M</option>
                            <option value=\"2\" >Mme</option>
                            <option value=\"3\" >Mlle</option>
                            <option value=\"4\" >Enfant</option>
                            <option value=\"5\" >Dr</option>
                            <option value=\"6\" >Pr</option>
                            <option value=\"7\" >Me</option>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Prénom </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input validate[required]\" name=\"prenom\" value=\"";
        // line 247
        echo twig_escape_filter($this->env, (isset($context["prenom"]) ? $context["prenom"] : $this->getContext($context, "prenom")), "html", null, true);
        echo "\">
                </td>



            </tr> <tr>
                <td >Tél. Domicile </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input validate[required,custom[phone]]\" name=\"telDomicile\" value=\"";
        // line 256
        echo twig_escape_filter($this->env, (isset($context["teldom"]) ? $context["teldom"] : $this->getContext($context, "teldom")), "html", null, true);
        echo "\">
                </td>
            </tr>
            <tr>
                <td >Tél. Mob

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input validate[required,custom[phone]]\" name=\"telMob\" value=\"";
        // line 263
        echo twig_escape_filter($this->env, (isset($context["telmob"]) ? $context["telmob"] : $this->getContext($context, "telmob")), "html", null, true);
        echo "\">
                </td>
            </tr>
            <tr>
                <td >Té. Pro </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input validate[required,custom[phone]]\" name=\"telPro\" value=\"";
        // line 270
        echo twig_escape_filter($this->env, (isset($context["telpro"]) ? $context["telpro"] : $this->getContext($context, "telpro")), "html", null, true);
        echo "\">
                </td>
            </tr>
            <tr>
                <td>Tél. Box </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input validate[required,custom[phone]]\" name=\"telBox\" value=\"";
        // line 277
        echo twig_escape_filter($this->env, (isset($context["telbox"]) ? $context["telbox"] : $this->getContext($context, "telbox")), "html", null, true);
        echo "\">
                </td>
            </tr>
            <tr>
                <td> Adresse </td>

                <td>
                    <textarea style=\"width: 20em\"class=\"input validate[required]\" name=\"adresse\" value=\"\"></textarea>
                </td>
            </tr>

            <tr>
                <td >Email </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input validate[required,custom[email]]\" id=\"email\" name=\"email\" value=\"";
        // line 292
        echo twig_escape_filter($this->env, (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email")), "html", null, true);
        echo "\">
                </td>
            </tr>

            <tr>
                <td >Note  </td>

                <td>
                    <textarea style=\"width: 20em\" data-prompt-position=\"bottomRight:-100,3\" type=\"text\" class=\"input validate[required]\" data-errormessage=\"This is the fall-back error message.\" data-errormessage-value-missing=\"Note n'est pas saisi\" name=\"note\" value=\"";
        // line 300
        echo twig_escape_filter($this->env, (isset($context["note"]) ? $context["note"] : $this->getContext($context, "note")), "html", null, true);
        echo "\"></textarea>
                </td>
            </tr>


            <tr>
                <td  >Mot de passe Web :</td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" id=\"pw\" name=\"password\">
                    <a href=\"#\" class=\"button icon-replay-all\" onclick=\"javascript:generer_password('password');\">Générer</a>
                    <button type=\"submit\" onclick=\"openLoadingModal()\" name=\"sendmail\" class=\"button icon-replay-all\">Envoie par mail</button>
                    <button name=\"print\" onclick=\"javascript:verif_champPw()\" type=\"submit\" class=\"button icon-replay-all\">Impression</button>
                </td>
            </tbody>


        </table>
    </form>
";
    }

    public function getTemplateName()
    {
        return "AgendaMemoBundle:Client:modifier.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  365 => 300,  354 => 292,  336 => 277,  326 => 270,  316 => 263,  306 => 256,  294 => 247,  267 => 223,  247 => 205,  241 => 202,  238 => 201,  37 => 4,  32 => 3,  29 => 2,  11 => 1,);
    }
}
