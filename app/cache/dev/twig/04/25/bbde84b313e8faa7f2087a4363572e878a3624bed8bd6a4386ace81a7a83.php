<?php

/* MemoMemoBundle:Memo:list.html.twig */
class __TwigTemplate_0425bbde84b313e8faa7f2087a4363572e878a3624bed8bd6a4386ace81a7a83 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "MemoMemoBundle:Memo:list.html.twig", 1);
        $this->blocks = array(
            'mesScript' => array($this, 'block_mesScript'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_mesScript($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("mesScript", $context, $blocks);
        echo "
<script>

    // Call template init (optional, but faster if called manually)
    \$.template.init();

    // Table sort - DataTables
    var table = \$('#sorting-advanced');
    table.dataTable({
        'aoColumnDefs': [
            {'bSortable': false, 'aTargets': [0, 5]}
        ],
        'sPaginationType': 'full_numbers',
        'sDom': '<\"dataTables_header\"lfr>t<\"dataTables_footer\"ip>',
        'fnInitComplete': function(oSettings)
        {
            // Style length select
            table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
            tableStyled = true;
        }
    });

    // Table sort - styled
    \$('#sorting-example1').tablesorter({
        headers: {
            0: {sorter: false},
            5: {sorter: false}
        }
    }).on('click', 'tbody td', function(event)
            {
                // Do not process if something else has been clicked
                if (event.target !== this)
                {
                    return;
                }

                var tr = \$(this).parent(),
                        row = tr.next('.row-drop'),
                        rows;

                // If click on a special row
                if (tr.hasClass('row-drop'))
                {
                    return;
                }

                // If there is already a special row
                if (row.length > 0)
                {
                    // Un-style row
                    tr.children().removeClass('anthracite-gradient glossy');

                    // Remove row
                    row.remove();

                    return;
                }

                // Remove existing special rows
                rows = tr.siblings('.row-drop');
                if (rows.length > 0)
                {
                    // Un-style previous rows
                    rows.prev().children().removeClass('anthracite-gradient glossy');

                    // Remove rows
                    rows.remove();
                }

                // Style row
                tr.children().addClass('anthracite-gradient glossy');

                // Add fake row
                \$('<tr class=\"row-drop\">' +
                        '<td colspan=\"' + tr.children().length + '\">' +
                        '<div class=\"float-right\">' +
                        '<button type=\"submit\" class=\"button glossy mid-margin-right\">' +
                        '<span class=\"button-icon\"><span class=\"icon-mail\"></span></span>' +
                        'Send mail' +
                        '</button>' +
                        '<button type=\"submit\" class=\"button glossy\">' +
                        '<span class=\"button-icon red-gradient\"><span class=\"icon-cross\"></span></span>' +
                        'Remove' +
                        '</button>' +
                        '</div>' +
                        '<strong>Name:</strong> John Doe<br>' +
                        '<strong>Account:</strong> admin<br>' +
                        '<strong>Last connect:</strong> 05-07-2011<br>' +
                        '<strong>Email:</strong> john@doe.com' +
                        '</td>' +
                        '</tr>').insertAfter(tr);

            }).on('sortStart', function()
            {
                var rows = \$(this).find('.row-drop');
                if (rows.length > 0)
                {
                    // Un-style previous rows
                    rows.prev().children().removeClass('anthracite-gradient glossy');

                    // Remove rows
                    rows.remove();
                }
            });

    // Table sort - simple
    \$('#sorting-example2').tablesorter({
        headers: {
            5: {sorter: false}
        }
    });

</script>
<script>
/*function openConfirm()
{
\$.modal.confirm('Confirmez-vous vouloir supprimer ce mémo ?', function()
{
\$.modal.alert('La supression est effectuée !');

}, function()
{
\$.modal.alert('La supression est annulée !');
});
};*/

function openModal()
{
    \$.modal({
        title: 'Suppression du mémo',
        content: '<h4 class=\"green-gradient glossy\">Suppression du mémo est effectuée</h4>',
        width: 300,
        resizable: false,
        scrolling: false,
        actions: {
            'Fermer' : {
                color: 'red',
                click: function(win) { win.closeModal(); }
            }
        },
        buttons: {
            'Annuler': {
                classes:\t'blue-gradient glossy',
                click:\t\tfunction(win) { win.closeModal(); }
            }
        }
    });
}
function openComplexModal(id)
{
    \$.modal({
        content: '<p>Confirmez-vous vouloir supprimer ce mémo ?</p>',
        title: 'Suppression de mémo',
        width: 300,
        resizable: false,
        scrolling: false,
        actions: {
            'Fermer' : {
                color: 'red',
                click: function(win) { win.closeModal(); }
            }
        },
        buttons: {
            'Oui,supprimer': {
                classes:\t'red-gradient glossy',
                click: function(result) {
                    window.location.href=\"http://localhost/monagenda/web/app_dev.php/memo/supp/\"+id;
                    window.event.onload = openModal()
                }
            },
            'Non': {
                classes:\t'blue-gradient glossy',
                click:\t\tfunction(win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
};
</script>
";
    }

    // line 183
    public function block_content($context, array $blocks = array())
    {
        // line 184
        echo "<div class=\"with-padding\">
    <div class=\"with-padding with-border\">
<h3>Liste des mémos</h3>
<table class=\"table responsive-table\" id=\"sorting-advanced\">
    <thead>
    <tr>
        <th align=\"center\">ID</th>
        <th align=\"center\">Phrase usuelle</th>
        <th align=\"center\">Client Fréquent</th>
        <th align=\"center\">Détail</th>
        <th align=\"center\">Date d'enregistrement</th>
        <th align=\"center\">Prioritaire</th>
        <th align=\"center\">Alerte</th>
        <th align=\"center\">Status de transmission</th>
        <th align=\"center\">Permanente</th>
        <th align=\"center\">Action</th>
    </tr>
    </thead>

    <tbody>
    ";
        // line 204
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["memos"]) ? $context["memos"] : $this->getContext($context, "memos")));
        foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
            // line 205
            echo "    <tr>
        <td align=\"center\">
            ";
            // line 207
            echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "id", array()), "html", null, true);
            echo "
        </td>
        <td align=\"center\">
            ";
            // line 210
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["m"], "phraseUsuelle", array()), "text", array()), "html", null, true);
            echo "
        </td>
        <td align=\"center\">
            ";
            // line 213
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["m"], "clientFrequent", array()), "nom", array()), "html", null, true);
            echo "
        </td>
        <td align=\"center\">
            ";
            // line 216
            echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "detail", array()), "html", null, true);
            echo "
        </td>
        <td align=\"center\">
            ";
            // line 219
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["m"], "dateenregistrement", array()), "Y-m-d"), "html", null, true);
            echo "
        </td>
        <td align=\"center\">
            ";
            // line 222
            if (($this->getAttribute($context["m"], "priorite", array()) == 1)) {
                // line 223
                echo "            Oui
            ";
            } else {
                // line 225
                echo "            Non
            ";
            }
            // line 227
            echo "        </td>
        <td align=\"center\">
            ";
            // line 229
            if (($this->getAttribute($context["m"], "alert", array()) == 1)) {
                // line 230
                echo "            Par Mail
            ";
            } elseif (($this->getAttribute(            // line 231
$context["m"], "alert", array()) == 2)) {
                // line 232
                echo "            par SMS
            ";
            } elseif (($this->getAttribute(            // line 233
$context["m"], "alert", array()) == 4)) {
                // line 234
                echo "            par FAX
            ";
            } elseif (($this->getAttribute(            // line 235
$context["m"], "alert", array()) == 3)) {
                // line 236
                echo "            Par Mail et par SMS
            ";
            } elseif (($this->getAttribute(            // line 237
$context["m"], "alert", array()) == 5)) {
                // line 238
                echo "            Par Mail et par FAX
            ";
            } elseif (($this->getAttribute(            // line 239
$context["m"], "alert", array()) == 6)) {
                // line 240
                echo "            Par SMS et par FAX
            ";
            } else {
                // line 242
                echo "            Par Mail, par SMS et par Fax
            ";
            }
            // line 244
            echo "        </td>
        <td align=\"center\">
            ";
            // line 246
            if (($this->getAttribute($context["m"], "status", array()) == 1)) {
                // line 247
                echo "            Oui
            ";
            } else {
                // line 249
                echo "            Auto
            ";
            }
            // line 251
            echo "        </td>
        <td align=\"center\">
            ";
            // line 253
            echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "permanante", array()), "html", null, true);
            echo "
        </td>
        <td class=\"center\">
            <a href=\"javascript: openComplexModal(";
            // line 256
            echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "id", array()), "html", null, true);
            echo ")\" title=\"Supprimer\" alt=\"Supprimer\"> <span class=\"icon-trash\"></span> </a>
            <a href=\"";
            // line 257
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("memo_memo_updateMemo", array("id" => $this->getAttribute($context["m"], "id", array()))), "html", null, true);
            echo "\" title=\"Modifier\" alt=\"Modifier\"> <span class=\"icon-pencil\"></span> </a>
        </td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 261
        echo "    </tbody>
</table>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "MemoMemoBundle:Memo:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  366 => 261,  356 => 257,  352 => 256,  346 => 253,  342 => 251,  338 => 249,  334 => 247,  332 => 246,  328 => 244,  324 => 242,  320 => 240,  318 => 239,  315 => 238,  313 => 237,  310 => 236,  308 => 235,  305 => 234,  303 => 233,  300 => 232,  298 => 231,  295 => 230,  293 => 229,  289 => 227,  285 => 225,  281 => 223,  279 => 222,  273 => 219,  267 => 216,  261 => 213,  255 => 210,  249 => 207,  245 => 205,  241 => 204,  219 => 184,  216 => 183,  32 => 3,  29 => 2,  11 => 1,);
    }
}
