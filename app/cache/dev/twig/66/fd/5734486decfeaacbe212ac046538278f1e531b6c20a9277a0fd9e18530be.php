<?php

/* AgendaMemoBundle:Memo:periodicite.html.twig */
class __TwigTemplate_66fd5734486decfeaacbe212ac046538278f1e531b6c20a9277a0fd9e18530be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<div id=\"annuler\">
<table class=\"simple-table responsive-table responsive-table-on\">
<tbody>
<tr>

    <td style=\"width:10em; text-align:center; margin-top:20px \" width=\"40%\" rowspan=\"5\" class=\"vertical-center\"><strong>Périodicité</strong></td>
    <td>
       
           <p>
                <input type=\"radio\" name=\"uneFois\" id=\"radio_1\" value=\"0\" class=\"radio\" checked=\"checked\"> <label for=\"NON\">Une fois</label>
            </p>
    
    </td>
</tr>
<tr>
    <td>
        <div class=\"columns\">
            <div class=\"center three-columns\">
                <p style=\"margin-top: 50px\">   <input type=\"radio\" name=\"uneFois\" id=\"radio_1\" value=\"1\" class=\"radio\"> <label for=\"NON\">Quetidienne</label>  </p>
            </div>

            <div>
                <input type=\"radio\" name=\"ttLes\" id=\"radio_2\" value=\"1\" class=\"radio\"> <label for=\"oui\">Tous les </label>
                <input type=\"text\" value=\"1\" name=\"jours\" style=\"width: 4em\" class=\"input\">  jour(s) <br><br>
                <input type=\"radio\" name=\"ttLes\" id=\"radio_3\" value=\"2\" class=\"radio\"> <label for=\"oui\">Tous les jours ouvrables</label><br><br>
                <input type=\"radio\" name=\"ttLes\" id=\"radio_4\" value=\"3\" class=\"radio\"> <label for=\"oui\">Tous les jours selectionnés :</label><br><br>

                <div class=\"one-columns\">
                    <p>
                        <input class=\"mid-margin-left styled checkbox\" type=\"checkbox\" name=\"quotid[]\" id=\"lu\" value=\"1\"> LU



                        <input class=\"mid-margin-left styled checkbox\" type=\"checkbox\" name=\"quotid[]\" id=\"ma\" value=\"2\"> MA


                        <input class=\"mid-margin-left styled checkbox\" type=\"checkbox\" name=\"quotid[]\" id=\"me\" value=\"3\"> ME


                        <input class=\"mid-margin-left styled checkbox\" type=\"checkbox\" name=\"quotid[]\" id=\"je\" value=\"4\"> Je


                        <input class=\"mid-margin-left styled checkbox\" type=\"checkbox\" name=\"quotid[]\" id=\"ve\" value=\"5\"> Ve


                        <input class=\"mid-margin-left styled checkbox\" type=\"checkbox\" name=\"quotid[]\" id=\"sa\" value=\"6\"> Sa

                        <input class=\"mid-margin-left styled checkbox\" type=\"checkbox\" name=\"quotid[]\" id=\"di\" value=\"7\"> Di
                    </p>
                </div>
            </div>

            <br>
         
    </td>
</tr>
<tr>

    <td>
        <div class=\"columns\">
            <div>
                <p  style=\"margin-top: 7px\"><input  type=\"radio\" name=\"uneFois\" id=\"radio_1\" value=\"2\" class=\"radio\"> <label for=\"NON\">Hebdomadair</label></p>
            </div>
            <div class=\"two-columns\" ></div>
            <div >
                Tous les <select name=\"daySelect\" class=\"select expandable-list\" style=\"width:6em\" >
                    <option value=\"1\" >lundi</option>
                    <option value=\"2\" >mardi</option>
                    <option value=\"3\" >mercredi</option>
                    <option value=\"4\" >jeudi</option>
                    <option value=\"5\" >vendredi</option>
                    <option value=\"6\" >samedi</option>

            </div>

        </div>
    </td>
</tr>
<tr>

    
</tr>
<tr>
 </tr>
<tr>
    <td style=\"width:10em; text-align:center; margin-top:20px \" width=\"40%\" rowspan=\"2\" class=\"vertical-center\"><strong>Plage de périodicité </strong></td>
    <td>
        <div class=\"columns\">
            <p> <div >
                <input type=\"radio\" name=\"finApres\" id=\"radio_1\" value=\"1\" class=\"radio\"> <label for=\"NON\">Fin après</label>
                <input type=\"text\" name=\"occurence\" style=\"width: 4em\" class=\"input\"> occurence(s)
            </div></p>
        </div>
    </td>
</tr>
<tr>

    <td>
        <div class=\"columns\">
            <p> <div >
                <input type=\"radio\" name=\"finApres\" id=\"radio_2\" value=\"2\" class=\"radio\"> <label for=\"NON\">Fin le</label>
                                        <span class=\"input\">
                                            <span class=\"icon-calendar\"></span>
                                            <input type=\"text\" name=\"datePickerFinLe\" id=\"special-input-3\" class=\"input-unstyled datepicker gldp-el\" value=\"\" gldp-id=\"gldp-4634471992\">
                                        </span>
            </div></p>
        </div>
    </td>
</tr>
<tr>
    <td style=\"width:10em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Lien</strong> </td>
    <td>
        <div class=\"columns\">
            <p> <div >
                Lier ces notes : <input type=\"radio\" name=\"radio7\" id=\"radio_1\" value=\"non\" class=\"radio\"> <label for=\"NON\">Oui   </label>
                <input type=\"radio\" name=\"radio7\" id=\"radio_2\" value=\"non\" class=\"radio\"> <label for=\"NON\"> Non  </label>

            </div></p>
        </div>
    </td>
</tr>
</tbody>

</table>
</div>";
    }

    public function getTemplateName()
    {
        return "AgendaMemoBundle:Memo:periodicite.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
