<?php

/* FOSUserBundle:Resetting:request_content.html.twig */
class __TwigTemplate_5d937200b6a37c007f7a59050a8c24fc6fe0edb7c41dea76ec35f026b24a8258 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<form action=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_send_email");
        echo "\" method=\"POST\" class=\"input-wrapper orange-gradient glossy\" title=\"Oublié?\">
    <p class=\"message\">
       Si vous ne pouvez pas vous souvenir de votre mot de passe , remplissez l'entrée ci-dessous avec votre adresse e -mail et nous vous enverrons un nouveau:
        <span class=\"block-arrow\"><span></span></span>
   </p>
    
    
        ";
        // line 10
        if (array_key_exists("invalid_username", $context)) {
            // line 11
            echo "            <p>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.invalid_username", array("%username%" => (isset($context["invalid_username"]) ? $context["invalid_username"] : $this->getContext($context, "invalid_username"))), "FOSUserBundle"), "html", null, true);
            echo "</p>
        ";
        }
        // line 13
        echo "      <ul class=\"inputs black-input large\">
          <li>
              <span class=\"icon-mail mid-margin-right\"></span>
             <input type=\"text\" class=\"input-unstyled\" autocomplete=\"off\" id=\"username\" name=\"username\" required=\"required\" placeholder=\"Identifiant ou E-Mail\" /> 
          
          </li>
          
         </ul> 
   
    
        
        <button type=\"submit\" class=\"button glossy full-width\" >Envoyer un nouveau mot de passe</button>
 
</form>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 13,  34 => 11,  32 => 10,  22 => 3,  19 => 2,);
    }
}
