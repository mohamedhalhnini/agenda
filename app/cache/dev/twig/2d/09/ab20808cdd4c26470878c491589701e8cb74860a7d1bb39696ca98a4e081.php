<?php

/* utilisateurBundle:Utilisateur:liste.html.twig */
class __TwigTemplate_2d09ab20808cdd4c26470878c491589701e8cb74860a7d1bb39696ca98a4e081 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "utilisateurBundle:Utilisateur:liste.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'js_content' => array($this, 'block_js_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "




    <section role=\"main\" id=\"main\">

        <hgroup id=\"main-title\" class=\"thin\">
            <h1>Utilisateur</h1>
            <h2>nov <strong>10</strong></h2>
        </hgroup>


        <div class=\"with-padding\">

            <div id=\"sorting-advanced_wrapper\" class=\"dataTables_wrapper\" role=\"grid\">
                <table class=\"table responsive-table responsive-table-on dataTable\" id=\"sorting-advanced\" aria-describedby=\"sorting-advanced_info\">

                    <thead>
                        <tr role=\"row\">
                            <th scope=\"col\" class=\"sorting_disabled\" role=\"columnheader\"><input type=\"checkbox\" name=\"check-all\" id=\"check-all\" value=\"1\"></th>
                            <th scope=\"col\" class=\"sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" >Nom d'utilisateu</th>
                            <th scope=\"col\" class=\"sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" >Nom</th>
                            <th scope=\"col\"  class=\"align-center hide-on-mobile sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" aria-label=\"Date: activate to sort column ascending\" style=\"width: 151px;\">Prenom</th>
                            <th scope=\"col\"  class=\"align-center hide-on-mobile-portrait sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" aria-label=\"Status: activate to sort column ascending\" style=\"width: 151px;\">E-Mail</th>
                            <th scope=\"col\"  class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" aria-label=\"Tags: activate to sort column ascending\" >Fonction</th>
                            <th scope=\"col\"  class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" aria-label=\"Tags: activate to sort column ascending\" >Dernière connexion</th>
                            <th scope=\"col\"  class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" aria-label=\"Tags: activate to sort column ascending\" >Etat</th>
                            <th scope=\"col\"  class=\"align-center sorting_disabled\" role=\"columnheader\" aria-label=\"Actions\" >Actions</th></tr>
                    </thead>
                    <tfoot>

                    </tfoot>



                    <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\">
                        ";
        // line 40
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            // line 41
            echo "                            ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()) != $this->getAttribute($context["u"], "username", array()))) {
                // line 42
                echo "                        <tr class=\"odd\">
                            <th scope=\"row\" class=\"checkbox-cell  sorting_1\"><input type=\"checkbox\" name=\"checked[]\" id=\"check-1\" value=\"1\"></th>
                            <td class=\" \">";
                // line 44
                echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "username", array()), "html", null, true);
                echo "</td>
                            <td class=\"hide-on-mobile \">";
                // line 45
                echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "nom", array()), "html", null, true);
                echo "</td>
                            <td class=\"hide-on-mobile-portrait \">";
                // line 46
                echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "prenom", array()), "html", null, true);
                echo "</td>
                            <td class=\"hide-on-mobile-portrait \">";
                // line 47
                echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "Email", array()), "html", null, true);
                echo "</td>
                            <td class=\"hide-on-mobile-portrait \">
                                ";
                // line 49
                if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
                    // line 50
                    echo "                                    Administrateur
                                ";
                } elseif ($this->env->getExtension('security')->isGranted("ROLE_MEDECIN")) {
                    // line 52
                    echo "                                    Medecin
                                ";
                } elseif ($this->env->getExtension('security')->isGranted("ROLE_SECRETAIRE")) {
                    // line 54
                    echo "                                    Secretaire
                                ";
                }
                // line 55
                echo "   
                            </td>
                            <td class=\"hide-on-mobile-portrait \">
                          ";
                // line 58
                if ($this->getAttribute($context["u"], "lastLogin", array())) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["u"], "lastLogin", array()), "Y-m-d H:i:s"), "html", null, true);
                }
                // line 59
                echo "                            
                            </td>
                            <td class=\"hide-on-tablet \">
                             ";
                // line 62
                if (($this->getAttribute($context["u"], "enabled", array()) == 1)) {
                    // line 63
                    echo "                                <small class=\"tag green-bg\">Activé</small>
                            ";
                } elseif (($this->getAttribute(                // line 64
$context["u"], "enabled", array()) == 0)) {
                    // line 65
                    echo "                                 <small class=\"tag red-bg\">Desactivé</small> 
                            ";
                }
                // line 67
                echo "           
                            </td>
                            <td class=\"low-padding align-center \"><a href=\"#\" class=\"button compact icon-gear\">Edit</a></td>
                        </tr>
                        ";
            }
            // line 72
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "                    </tbody>
                </table>             
            </div>
        </div>
    </section>
";
        // line 82
        echo "
 ";
        // line 83
        $this->displayBlock('js_content', $context, $blocks);
    }

    public function block_js_content($context, array $blocks = array())
    {
        // line 84
        echo "   
    
    <script>

        // Call template init (optional, but faster if called manually)
    

        // Table sort - DataTables
        var table = \$('#sorting-advanced');
        table.dataTable({
            'aoColumnDefs': [
                {'bSortable': false, 'aTargets': [0, 5]}
            ],
            'sPaginationType': 'full_numbers',
            'sDom': '<\"dataTables_header\"lfr>t<\"dataTables_footer\"ip>',
            'fnInitComplete': function (oSettings)
            {
                // Style length select
                table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                tableStyled = true;
            }
        });


   
    </script>
 ";
    }

    public function getTemplateName()
    {
        return "utilisateurBundle:Utilisateur:liste.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 84,  161 => 83,  158 => 82,  151 => 73,  145 => 72,  138 => 67,  134 => 65,  132 => 64,  129 => 63,  127 => 62,  122 => 59,  118 => 58,  113 => 55,  109 => 54,  105 => 52,  101 => 50,  99 => 49,  94 => 47,  90 => 46,  86 => 45,  82 => 44,  78 => 42,  75 => 41,  71 => 40,  32 => 3,  29 => 2,  11 => 1,);
    }
}
