<?php

/* AgendaMemoBundle:Memo:modifier.html.twig */
class __TwigTemplate_a1d63f929d975a17e57018db04610da9f241def7d5aa9c059a1cd7f53c63de99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AgendaMemoBundle:Memo:modifier.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'my_script' => array($this, 'block_my_script'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "Modifier Memo
";
    }

    // line 6
    public function block_my_script($context, array $blocks = array())
    {
        // line 7
        $this->displayParentBlock("my_script", $context, $blocks);
        echo "
    <script>
    function affecterDateAujourdhui(id){
        var d = new Date();
        var mois = d.getMonth()+1;
        var now = d.getDay()+'/'+ mois +'/'+d.getFullYear();
    document.getElementById(id).value = now;
    }
    function recup1(){
        var valeur1;
        return valeur1 = document.getElementById('agenda_memobundle_general_type_clientFrequent').options[document.getElementById('agenda_memobundle_general_type_clientFrequent').selectedIndex].value;
        document.getElementById(\"agenda_memobundle_general_type_detail\").value = valeur1;

    }
    function recup2(){
        var valeur2;
        valeur2 = document.getElementById('agenda_memobundle_general_type_phraseUsuelle').options[document.getElementById('agenda_memobundle_general_type_phraseUsuelle').selectedIndex].text;
        document.getElementById(\"agenda_memobundle_general_type_detail\").value = valeur2;
    }
    </script>
";
    }

    // line 28
    public function block_content($context, array $blocks = array())
    {
        // line 29
        echo "<div class=\"with-padding\">
    <div class=\"with-padding with-border\">
        ";
        // line 31
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        <table class=\"table responsive-table\">

            <thead>
            <tr>
                <th class=\"scratch-metal\" scope=\"col\" colspan=\"8\" align=\"center\">Gestion de mémos</th>
            </tr>
            </thead>

            <tbody>
            <tr>
            <tr>
                <th scope=\"row\" >
                    Phrase usuelle
                </th>
                <td colspan=\"2\">";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phraseUsuelle", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phraseUsuelle", array()), 'errors');
        echo "</span></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Client Fréquent
                </th>
                <td colspan=\"2\">
                    ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clientFrequent", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clientFrequent", array()), 'errors');
        echo "</span>
                </td>
                <td>

                </td>
                <td>

                </td>
                <td>

                </td>
                <td>

                </td>
                <td>

                </td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Détail
                </th>
                <td colspan=\"8\">
                    ";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "detail", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "detail", array()), 'errors');
        echo "</span>
                </td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Date
                </th>
                <td colspan=\"2\">
                    <span class=\"icon-calendar\"></span>
                    <input id=\"agenda_memobundle_general_type_dateenregistrement\" name=\"dateenregistrement\" type=\"text\" class=\"input datepicker\" value=\"";
        // line 93
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "dateenregistrement", array()), "d/m/Y"), "html", null, true);
        echo "\" readonly>
                    <span class=\"help-inline\"></span>
                </td>
                <td>
                                                    <span class=\"button-group\">
                                                        <button type=\"button\"  class=\"button\" onclick=affecterDateAujourdhui(\"agenda_memobundle_general_type_dateenregistrement\")>Aujourd'hui</button>
                                                    </span>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Prioritaire
                </th>
                <td colspan=\"2\">
                    ";
        // line 111
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "priorite", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 112
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "priorite", array()), 'errors');
        echo "</span>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Alerter
                </th>
                ";
        // line 124
        if (($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "alert", array()) == 1)) {
            // line 125
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\" checked> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\"> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\"> par Fax</td>
                ";
        } elseif (($this->getAttribute(        // line 128
(isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "alert", array()) == 2)) {
            // line 129
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\"> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\" checked> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\"> par Fax</td>
                ";
        } elseif (($this->getAttribute(        // line 132
(isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "alert", array()) == 4)) {
            // line 133
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\"> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\"> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\" checked> par Fax</td>
                ";
        } elseif (($this->getAttribute(        // line 136
(isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "alert", array()) == 3)) {
            // line 137
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\" checked> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\" checked> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\"> par Fax</td>
                ";
        } elseif (($this->getAttribute(        // line 140
(isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "alert", array()) == 5)) {
            // line 141
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\" checked> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\"> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\" checked> par Fax</td>
                ";
        } elseif (($this->getAttribute(        // line 144
(isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "alert", array()) == 6)) {
            // line 145
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\"> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\" checked> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\" checked> par Fax</td>
                ";
        } else {
            // line 149
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\" checked> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\" checked> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\" checked> par Fax</td>
                ";
        }
        // line 153
        echo "                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Etat
                </th>
                <td colspan=\"3\">Considérer le mémo comme transmis: </td>
                <td>
                    ";
        // line 165
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 166
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'errors');
        echo "</span>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <th scope=\"row\"></th>
                <td></td>
                <td></td>
                <td colspan=\"2\">
                    ";
        // line 178
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Enregistrer", array()), 'widget');
        echo "
                    <a href=\"";
        // line 179
        echo $this->env->getExtension('routing')->getPath("agenda_memo_calendrier");
        echo "\" class=\"button icon-replay-all\">Annuler</a>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
        ";
        // line 187
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "s
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "AgendaMemoBundle:Memo:modifier.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  293 => 187,  282 => 179,  278 => 178,  263 => 166,  259 => 165,  245 => 153,  239 => 149,  233 => 145,  231 => 144,  226 => 141,  224 => 140,  219 => 137,  217 => 136,  212 => 133,  210 => 132,  205 => 129,  203 => 128,  198 => 125,  196 => 124,  181 => 112,  177 => 111,  156 => 93,  144 => 84,  140 => 83,  114 => 60,  110 => 59,  95 => 47,  91 => 46,  73 => 31,  69 => 29,  66 => 28,  41 => 7,  38 => 6,  33 => 4,  30 => 3,  11 => 1,);
    }
}
