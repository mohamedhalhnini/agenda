<?php

/* InstructionBundle:Instruction:add.html.twig */
class __TwigTemplate_118d62a6c67f33d0c14db3103c0ae72e2faf49928c8cced429f72d301b537905 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "InstructionBundle:Instruction:add.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "    <form name=\"form2\" id=\"form2\" action=\"";
        echo $this->env->getExtension('routing')->getPath("instruction_ajouter");
        echo "\" method=\"POST\">
        <table class=\"table simple-table \" id=\"sorting-example1\">
            <thead>
                <tr>
                    <!--\t\t\t\t\t\t<th scope=\"col\"><input type=\"checkbox\" name=\"check-all\" id=\"check-all\" value=\"1\"></th>
                                                                    <th scope=\"col\" class=\"header\">Text</th>
                                                                    <th scope=\"col\" width=\"15%\" class=\"align-center hide-on-mobile header\">Date</th>
                                                                    <th scope=\"col\" width=\"15%\" class=\"align-center hide-on-mobile-portrait header\">Status</th>
                                                                    <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet header\">Tags</th>-->
                    <th scope=\"col\" width=\"30%\" colspan=\"2\" class=\"align-center\">Enregistrer une instruction</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan=\"6\">
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <td><strong>Titre</strong></td>
                    <td  width=\"80%\" class=\"hide-on-mobile\"><div class=\"row\">
                            <p class=\"button-height time\">
                                <select id=\"phraseUsuelle\" name=\"phraseUsuelle\" style=\"width:20em\" class=\"blue-gradient select validate[required]\">
                                    <option value=\"0\">-- Phrases usuelles --</option>
                                    ";
        // line 32
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["phrases"]) ? $context["phrases"] : $this->getContext($context, "phrases")));
        foreach ($context['_seq'] as $context["_key"] => $context["phrase"]) {
            // line 33
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["phrase"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["phrase"], "text", array()), "html", null, true);
            echo "</option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phrase'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "                                </select>
                            </p>
                            <p>
                              

                                <!--                                        <a href=\"javascript:void(0)\" class=\"button icon-star blue-gradient\">Ajout</a>-->
                                <!--                                        <a href=\"javascript:void(0)\" class=\"button icon-download blue-gradient\">Histo</a>
                                                                        <a href=\"javascript:void(0)\" class=\"button icon-new-tab blue-gradient\">Notes</a>
                                                                        <a href=\"javascript:void(0)\" class=\"button icon-new-tab blue-gradient\">Nouv</a>
                                                                        <a href=\"javascript:void(0)\" class=\"button icon-pencil blue-gradient\">Modif</a>-->
                            </p>
                            <p class=\"button-height\">
                                <select id=\"client\" name=\"client\" class=\"blue-gradient select validate[required]\" style=\"width:20em\">
                                    <option value=\"0\">-- Clients fréquents --</option>
                                    ";
        // line 49
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clients"]) ? $context["clients"] : $this->getContext($context, "clients")));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 50
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "nom", array()), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "prenom", array()), "html", null, true);
            echo "</option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "                                </select>
                                
                                  <button type=\"submit\" class=\"button icon-plus blue-gradient\" name=\"Ajout\">Ajout</button>
                                <button type=\"submit\" class=\"button icon-download blue-gradient\" name=\"Histo\">Histo</button>
                                <button type=\"submit\" class=\"button icon-new-tab blue-gradient\" name=\"Notes\">Notes</button>
                                <button type=\"button\" class=\"button  icon-new-tab blue-gradient\" onclick=\"openIframeClient()\" >Nouv</button>
                                <button type=\"submit\" class=\"button icon-pencil blue-gradient\" name=\"Motif\">Motif</button>

                            </p>
                            <p>
                                <input type=\"text\" name=\"titre\" id=\"titre\" class=\"input full-width\" value=\"\">
                            </p>
                        </div></td>
                </tr>
                <tr>
                    <td><strong>Détails</strong></td>
                    <td class=\"hide-on-mobile\">
                        <textarea name=\"detail\" id=\"detail\" class=\"input full-width autoexpanding\" style=\"overflow: hidden; resize: none; height: 124px;\"></textarea>
                    </td>
                </tr>
                <tr>
                    <td><strong>Validité</strong></td>
                    <td class=\"hide-on-mobile\">
                        <p>
                            <input type=\"radio\" name=\"type\" checked=\"true\" value=\"0\" class=\"radio mid-margin-left\"> <label for=\"radio_1\" >Permanente(valable tout les temp)</label>
                        </p>
                        <p class=\"button-height\">
                            <input type=\"radio\" name=\"type\"  value=\"1\" class=\"radio mid-margin-left\"> <label for=\"radio_2\" >Ponctuel le : </label>

                            <span class=\"input\">
                                <span class=\"icon-calendar\"></span>
                                <input type=\"text\" name=\"dateLe\" id=\"special-input-3\" class=\"input-unstyled datepicker gldp-el\" value=\"\" gldp-id=\"gldp-2690895398\">
                            </span>
                        </p>
                        <p class=\"button-height\">
                            <input type=\"radio\" name=\"type\" value=\"2\" class=\"radio mid-margin-left\"> <label for=\"radio_3\"  >Periode du   : </label>

                            <span class=\"input\">
                                <span class=\"icon-calendar\"></span>
                                <input type=\"text\" name=\"dateDe\" id=\"special-input-3\" class=\"input-unstyled datepicker gldp-el\" value=\"12/11/2015\" gldp-id=\"gldp-2690895399\">
                            </span>
                            <label>au :</label>
                            <span class=\"input\">
                                <span class=\"icon-calendar\"></span>
                                <input type=\"text\" name=\"dateA\" id=\"special-input-3\" class=\"input-unstyled datepicker gldp-el\" value=\"10/12/2015\" gldp-id=\"gldp-2690895390\">
                            </span>

                        </p>
                    </td>

                </tr>
                <tr>
                    <td><strong>Priorité</strong></td>
                    <td class=\"hide-on-mobile\">
                        <p>
                            <input type=\"radio\" checked=\"true\" name=\"priorite\" id=\"radio_1\" value=\"0\" class=\"radio mid-margin-left\"> <label for=\"radio-1\" >Normale</label>
                            <input type=\"radio\" name=\"priorite\" id=\"radio_2\" value=\"1\" class=\"radio mid-margin-left\"> <label for=\"radio-1\" >Haute</label>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td class=\"hide-on-mobile align-center\" colspan=\"2\">
                        <button name=\"enregistrer\" type=\"submit\" class=\"button icon-save blue-gradient\">Enregistrer</button>
                        <button type=\"submit\" class=\"button icon-forbidden blue-gradient\">Annuler</button>
                        <button type=\"submit\" class=\"button icon-folder blue-gradient\">Accéder aux instructions archivées</button>
                        <button type=\"submit\" class=\"button icon-printer blue-gradient\">Imprimer</button>

                        <!--
                                                        <a href=\"javascript:void(0)\" class=\"button icon-save blue-gradient\">Enregistrer</a>
                                                        <a href=\"javascript:void(0)\" class=\"button icon-forbidden blue-gradient\">Annuler</a>
                                                        <a href=\"javascript:void(0)\" class=\"button icon-folder blue-gradient\">Accéder aux instructions archivées</a>
                                                        <a href=\"javascript:void(0)\" class=\"button icon-printer blue-gradient\">Imprimer</a>
                        -->

                    </td>
                </tr>
                ";
        // line 128
        if (array_key_exists("instructioninsred", $context)) {
            echo " 
                    <tr>

                        <td class=\"hide-on-mobile\" colspan=\"2\">
                            <div class=\"columns\">
                                <p>
                                <div class=\"four-columns\"><p><strong>";
            // line 134
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["instructioninsred"]) ? $context["instructioninsred"] : $this->getContext($context, "instructioninsred")), "titre", array()), "html", null, true);
            echo "</strong><p>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["instructioninsred"]) ? $context["instructioninsred"] : $this->getContext($context, "instructioninsred")), "detail", array()), "html", null, true);
            echo "</p></p>
                                    <p><span class=\"small\"></span></p></div>
                                <div class=\"two-columns \"><strong>";
            // line 136
            if (($this->getAttribute((isset($context["instructioninsred"]) ? $context["instructioninsred"] : $this->getContext($context, "instructioninsred")), "type", array()) == 0)) {
                echo "Permanente";
            } elseif (($this->getAttribute((isset($context["instructioninsred"]) ? $context["instructioninsred"] : $this->getContext($context, "instructioninsred")), "type", array()) == 1)) {
                echo " Ponctuel";
            } else {
                echo "Periode";
            }
            echo "</strong></div>
                                <div class=\"one-column \"><strong>";
            // line 137
            if (($this->getAttribute((isset($context["instructioninsred"]) ? $context["instructioninsred"] : $this->getContext($context, "instructioninsred")), "priorite", array()) == 0)) {
                echo "Haute";
            }
            echo " ";
            if (($this->getAttribute((isset($context["instructioninsred"]) ? $context["instructioninsred"] : $this->getContext($context, "instructioninsred")), "priorite", array()) == 1)) {
                echo "Normale";
            }
            echo "</strong></div>
                                <div class=\"three-columns\">
                                    <span class=\"button-group\">

                                        <button type=\"submit\" class=\"button \">M</button>
                                        <button type=\"submit\" class=\"button \">A</button>
                                        <button type=\"submit\" class=\"button \">S</button>
                                        <button type=\"submit\" class=\"button \">I</button>

                                    </span>
                                </div>
                                <div class=\"one-column \"><a href=\"javascript:void(0)\" class=\"button\">impr</a></div>
                                <input type=\"checkbox\" name=\"checkbox-1\" id=\"checkbox-1\" value=\"1\" class=\"checkbox mid-margin-left replacement\">

                            </div>

                        </td>

                    </tr>
                ";
        }
        // line 157
        echo "                ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["instructions"]) ? $context["instructions"] : $this->getContext($context, "instructions")));
        foreach ($context['_seq'] as $context["_key"] => $context["instruction"]) {
            // line 158
            echo "                    <tr>
                        <td class=\"hide-on-mobile\" colspan=\"2\">
                            <div class=\"columns\">
                                <p>
                                <div class=\"four-columns\"><p><strong>";
            // line 162
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "titre", array()), "html", null, true);
            echo "</strong><p>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "detail", array()), "html", null, true);
            echo "</p></p>
                                    <p><span class=\"small\"></span></p></div>
                                <div class=\"two-columns \"><strong>";
            // line 164
            if (($this->getAttribute($context["instruction"], "type", array()) == 0)) {
                echo "Permanente";
            } elseif (($this->getAttribute($context["instruction"], "type", array()) == 1)) {
                echo " Ponctuel";
            } else {
                echo "Periode";
            }
            echo "</strong></div>
                                <div class=\"one-column \"><strong>";
            // line 165
            if (($this->getAttribute($context["instruction"], "priorite", array()) == 0)) {
                echo "Haute";
            }
            echo " ";
            if (($this->getAttribute($context["instruction"], "priorite", array()) == 1)) {
                echo "Normale";
            }
            echo "</strong></div>
                                <div class=\"three-columns\">
                                    <span class=\"button-group\">

                                        <button type=\"submit\" class=\"button \">M</button>
                                        <button type=\"submit\" class=\"button \">A</button>
                                        <button type=\"submit\" class=\"button \">S</button>
                                        <button type=\"submit\" class=\"button \">I</button>
                                    </span>
                                </div>
                                <div class=\"one-column \"><a href=\"javascript:void(0)\" class=\"button\">impr</a></div>
                                <input type=\"checkbox\" name=\"checkbox-1\" id=\"checkbox-1\" value=\"1\" class=\"checkbox mid-margin-left replacement\">
                            </div>
                        </td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['instruction'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 181
        echo "            </tbody>
        </table>
    </form>

    <script>
        // Demo Iframe loading
        function openIframelastVersion()
        {
            \$.modal({
                title: 'Client Fréquent',
                url: '";
        // line 191
        echo $this->env->getExtension('routing')->getPath("client_create");
        echo "',
                useIframe: true,
                width: 600,
                height: 600
            });
        }

        function openIframeClient()
        {
            \$.modal({
                title: ' <strong>client frequent</strong>',
                content: ''
                        + '<table class=\"table simple-table responsive-table responsive-table-on w \" style=\"  height: 10em;\">'
                        + '<tfoot>'
                        + '<tr>'
                        + '<td colspan=\"2\"  class=\"align-center\">'
                        + '<div class=\"columns\">'
                        + '<div class=\"three-columns\">'
                        + '</div>'
                        + '<div class=\"two-columns\">'
                        + '<button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" name=\"iframClient\" onclick=\"saveClient()\" id=\"iframClient\">Enregistrer</button>'
                        + '</div>'
                        + ' <div class=\"tow-columns\">'
                        + '   <button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" name=\"annuler\">annuler</button>'
                        + ' </div>'
                        + ' </div>'
                        + ' </td>'
                        + ' </tr>'
                        + ' </tfoot>'
                        + ' <tbody>'
                        + '   <tr>'
                        + ' <td>Civilité </td>'
                        + ' <td>'
                        + '    <div class=\"three-columns\">'
                        + '       <select class=\"select blue-gradient expandable-list\" style=\"width:15em\" name=\"civilite\" id=\"civilite\">'
                        + '     <option value=\"0\" >--Civilité--</option>'
                        + '     <option value=\"1\" >M</option>'
                        + '     <option value=\"2\" >Mme</option>'
                        + '     <option value=\"3\" >Mlle</option>'
                        + '     <option value=\"4\" >Enfant</option>'
                        + '     <option value=\"5\" >Dr</option>'
                        + '     <option value=\"6\" >Pr</option>'
                        + '     <option value=\"7\" >Me</option>'
                        + ' </select>'
                        + ' </div>'
                        + '            </td>'
                        + '        </tr>'
                        + '        <tr>'
                        + '            <td>Nom</td>'
                        + '                <td>'
                        + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"nom\" id=\"nom\">'
                        + '                </td>'
                        + '            </tr>'
                        + '            <tr>'
                        + '                <td>Prénom </td>'
                        + '                <td>'
                        + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"prenom\" id=\"prenom\">'
                        + '                </td>'
                        + '            </tr> <tr>'
                        + '                <td >Tél. Domicile </td>'
                        + '                <td>'
                        + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" nam=\"telDomicile\" id=\"tel\">'
                        + '                </td>'
                        + '            </tr>'
                        + '            <tr>'
                        + '                <td >Tél. Mob'
                        + '                <td>'
                        + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telMob\" id=\"telMob\">'
                        + '                </td>'
                        + '         </tr>'
                        + '          <tr>'
                        + '              <td >Té. Pro </td>'
                        + '                <td>'
                        + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telPro\" id=\"telPro\">'
                        + '                </td>'
                        + '            </tr>'
                        + '            <tr>'
                        + '              <td>Tél. Box </td>'
                        + '              <td>'
                        + '                  <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telBox\" id=\"telBox\">'
                        + '            </td>'
                        + '        </tr>'
                        + '        <tr>'
                        + '            <td> Adresse </td>'
                        + '                <td>'
                        + '                  <textarea style=\"width: 20em\"class=\"input\" name=\"adresse\" id=\"adresse\"></textarea>'
                        + '              </td>'
                        + '          </tr>'
                        + '          <tr>'
                        + '              <td >Email </td>'
                        + '              <td>'
                        + '                  <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"email\" id=\"email\">'
                        + '              </td>'
                        + '          </tr>'
                        + '            <tr>'
                        + '                <td >Note  </td>'
                        + '                <td>'
                        + '                    <textarea style=\"width: 20em\"class=\"input\" name=\"note\" id=\"note\"></textarea>'
                        + '                </td>'
                        + '            </tr>'
                        + '            <tr>'
                        + '                <td  >Mot de passe Web :</td>'
                        + '                <td>'
                        + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"password\" id=\"password\">'
                        + '                    <button type=\"submit\" class=\"button  blue-gradient\"  name=\"generer\" id=\"generer\">Générer</button>'
                        + '                </td>'
                        + '        </tbody> '
                        + '    </table>',
                buttonsAlign: 'center',
                resizable: true,
                width: 600,
                height: 600
            });
        }

        function rafrecheSelect() {
            var length = document.form2.client.length;
            document.form2.client[length] = new Option(\"text ggsgsgsgsgsg\");
        }
   
    
        function saveClient()
        {
            // on sécurise les données
            var civilite = encodeURIComponent(\$('#civilite').val());
            var nom = encodeURIComponent(\$('#nom').val());
            var prenom = encodeURIComponent(\$('#prenom').val());
            var telDomicile = encodeURIComponent(\$('#tel').val());
            var telMob = encodeURIComponent(\$('#telMob').val());
            var telPro = encodeURIComponent(\$('#telPro').val());
            var telBox = encodeURIComponent(\$('#telBox').val());
            var adresse = encodeURIComponent(\$('#adresse').val());
            var email = encodeURIComponent(\$('#email').val());
            var note = encodeURIComponent(\$('#note').val());
            var password = encodeURIComponent(\$('#password').val());
            var generer = encodeURIComponent(\$('#generer').val());
           
            if (nom != \"\") { // on vérifie que les variables ne sont pas vides
                 console.log('befor success');
                \$.ajax({
                    url: '";
        // line 331
        echo $this->env->getExtension('routing')->getPath("client_create");
        echo "', // on donne l'URL du fichier de traitement
                    type: \"POST\", // la requête est de type POST
                    data: \"civilite=\" + civilite + \"&nom=\" + nom + \"&prenom=\" + prenom + \"&tel=\" + tel + \"&telMob=\" + telMob + \"&telPro=\" + telPro + \"&telBox=\" + telBox + \"&adresse=\" + adresse + \"&email=\" + email + \"&note=\" + note + \"&password=\" + password, // et on envoie nos données
                    success: function (data) {
                        console.log('success');
                        console.log(data.nom);
                        \$('#client').append('<option value=\"'+data.id +'\" >' + data.nom + ' '+data.prenom+ '</option>');
                        document.querySelector('#client [value=\"' + data.id + '\"]').selected = true;
                        \$('#client').change();
                        console.log('success fermer');
                    }
                });
               \$('#modals').remove();
            }
        }
    </script>



";
    }

    public function getTemplateName()
    {
        return "InstructionBundle:Instruction:add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  451 => 331,  308 => 191,  296 => 181,  268 => 165,  258 => 164,  251 => 162,  245 => 158,  240 => 157,  211 => 137,  201 => 136,  194 => 134,  185 => 128,  107 => 52,  95 => 50,  91 => 49,  75 => 35,  64 => 33,  60 => 32,  31 => 7,  28 => 6,  11 => 1,);
    }
}
