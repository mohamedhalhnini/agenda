<?php

/* RendezVousBundle:Default:addRendezVous.html.twig */
class __TwigTemplate_e61f030b9ae70c300fe9245b8c966964d8ccbfb650dee9ad2f49f0b48bace92d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "RendezVousBundle:Default:addRendezVous.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"with-padding\">

    <h4>Styled table with advanced sorting</h4>

    <p>This example uses the plugin <a href=\"http://datatables.net\">DataTables</a>:</p>
    <center>
        c moiiii
        <form action=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("create");
        echo "\" method=\"post\">
            <table class=\"simple-table responsive-table responsive-table-on\" >

                <tbody>

                    <tr>
                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Titre</strong></td>
                        <td >
                            <select name=\"libelle\" class=\"select expandable-list\" style=\"width:40em\" >
                                <option >--Libellés personnalisés--</option>
                                <option value=\"Rendez-vous important\" >Rendez-vous important</option>
                                <option value=\"Consultation Mr zouani\" >Consultation Mr zouani</option>
                                <option value=\"Test de diabète\" >Test de diabète</option>

                            </select><br><br>
                            <select name=\"entete\" class=\"select expandable-list\" style=\"width:40em\" >
                                <option  >--Entetes--</option>
                                <option value=\"kamal1\" >kamal</option>
                                <option value=\"smail1\" >smail</option>
                                <option value=\"twitchi1\" >tiwitchi</option>
                            </select><br><br>
                            <div class=\"myDiv\">
                                <select id=\"mySelect\" name=\"rdvType\" class=\"blabla select\" style=\"width:40em\" >
                                    <option  >--RDV types--</option>
           ";
        // line 36
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")));
        foreach ($context['_seq'] as $context["_key"] => $context["rdv"]) {
            // line 37
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["rdv"], "id", array()), "html", null, true);
            echo "\" style=\" background-color:";
            echo twig_escape_filter($this->env, $this->getAttribute($context["rdv"], "couleur", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["rdv"], "libelle", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["rdv"], "dureee", array()), "H:i"), "html", null, true);
            echo "</option>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rdv'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>clients féquents</strong></td>
                        <td>
                            <p>

                                <input type=\"radio\" name=\"radio1\" id=\"debut\" value=\"contient\" class=\"radio\">Début <label for=\"gender_female\"></label>
                                <input type=\"radio\" name=\"radio1\" id=\"contient\" value=\"contient\" class=\"radio\">Contient <label for=\"gender_female\"></label>


                                <!--button-->

                                <button  class=\"button icon-plus blue-gradient\">Ajout</button>
                                <button  class=\"button icon-download blue-gradient\">Histo</button>
                                <button  class=\"button icon-new-tab blue-gradient\">Notes</button>

                                <button type=\"button\"  class=\"button icon-new-tab blue-gradient\" onclick=\"openIframeClient()\">Nouv</button>

                                <button  class=\"button icon-pencil blue-gradient\">Motif</button>
                            </p>

                            <p>
                            <div class=\"row\">

                                <select name=\"client\" id=\"pff\" class=\"select expandable-list\" style=\"width:40em\" >
                                    <option selected  >--select--</option>
                ";
        // line 68
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clients"]) ? $context["clients"] : $this->getContext($context, "clients")));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 69
            echo "
                                    <option value=\"";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "nom", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "prenom", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "telpro", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "email", array()), "html", null, true);
            echo ")</option>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "                                </select>
                            </div>
                            </p>
                        </td>

                    </tr>
                    <tr>
                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Détail</strong></td>
                        <td>
                            <div class=\"row\">
                                <input type=\"text\" class=\"input\" style=\"width:40em\"><br><br>

                                <textarea name=\"autoexpanding\" id=\"autoexpanding\" class=\"input full-width autoexpanding\" style=\"overflow: hidden; resize: none; height: 124px;\">Plus de détails.....</textarea>

                            </div>
                            <br>

                        </td>

                    </tr>
                    <tr>
                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Disponibilité</strong></td>
                        <td>
                            <div class=\"columns\" class=\"center\">
                                <div class=\"three-columns\">
                                    <button type=\"submit\" class=\" button blue-gradient\" value=\"Ajout\" >Prochaine plage</button> </div>
                                <div class=\"one-column\"><strong>ou</strong></div>
                                <div class=\"two-columns\"><button type=\"submit\" class=\"button blue-gradient three-column\" value=\"Ajout\" >Rechrcher</button></div>
                            </div>
                            <br>

                        </td>

                    </tr>
                    <tr>
                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Date et Heure</strong></td>
                        <td>
                            <div>
                                <p class=\"button-height\">
                                    Date du rendez-vous :
                ";
        // line 113
        echo "                                    <span class=\"input\">
                                        <span class=\"icon-calendar\"></span>
                                        <input type=\"text\" name=\"datepicker1\" id=\"rdvdate\" class=\"input-unstyled datepicker gldp-el\" value=\" ";
        // line 115
        echo twig_escape_filter($this->env, (isset($context["startD"]) ? $context["startD"] : $this->getContext($context, "startD")), "html", null, true);
        echo "\" gldp-id=\"gldp-4634471992\">
                                    </span>

                                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"jrEntiere\" id=\"check-1\" value=\"true\"> journée entière
                                </p>
                            </div><br>
                            <div >

                                <p class=\"button-height\">Début a :

                                    <input type=\"text\" class=\"ouf input\" style=\"width:40px\"   id=\"time1\" name=\"timeDebut\" value=\"";
        // line 125
        echo twig_escape_filter($this->env, (isset($context["startH"]) ? $context["startH"] : $this->getContext($context, "startH")), "html", null, true);
        echo "\">

                                    Termine à :
                                    <input type=\"text\" class=\"input\" style=\"width:40px\"  id=\"time2\" name=\"timeFin\" value=\"";
        // line 128
        echo twig_escape_filter($this->env, (isset($context["endH"]) ? $context["endH"] : $this->getContext($context, "endH")), "html", null, true);
        echo "\">



                                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"checked[]\" id=\"check-1\" value=\"1\"> Durée alternative
                                    <button type=\"submit\" class=\" button blue-gradient one-columns\" value=\"Ajout\" >voir la légende</button>
                                </p>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Couleur</strong></td>
                        <td>
                            <input name=\"color\" type=\"color\" width=\"500px\" id=\"html5colorpicker\" class=\"form-control\" onchange=\"clickColor(0, -1, -1, 5)\" value=\"#ff0000\">
                            <button type=\"submit\" class=\" button blue-gradient three-columns mid-margin-left\" value=\"Ajout\">Aparence de la note</button>
                        </td>
                    </tr>
                    <tr>
                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Partage</strong></td>
                        <td>
                            <p>
                                <input type=\"radio\" name=\"partage\" id=\"radio_1\" value=\"1\" class=\"radio\"> <label for=\"gender_male\">Note public(note détaillée dans le partage de planning)</label>
                            </p>
                            <p>
                                <input type=\"radio\" name=\"partage\" id=\"radio_2\" value=\"0\" class=\"radio\"> <label for=\"gender_female\">Note privée(mention \"Occupé\" dans le partage de planning)</label>
                            </p>

                        </td>

                    </tr>
                    <tr>

                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Alerter</strong></td>
                        <td>
                            <div class=\"columns\">
                                <div class=\"two-columns\">
                                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"alert[]\" id=\"check-1\" value=\"1\"> par mail</div>
                                <div class=\"two-columns\"> <input class=\"styled checkbox\" type=\"checkbox\" name=\"alert[]\" id=\"check-2\" value=\"2\" > par SMS</div>

                            </div>

                            <div class=\"columns\">
                                <div class=\"two-columns\">le client fréquent : </div>
                                <div class=\"two-columns\"> <input class=\"styled checkbox\" type=\"checkbox\" name=\"alertCli[]\" id=\"check-4\" value=\"1\"> par mail</div>
                                <div class=\"two-columns\">   <input class=\"styled checkbox\" type=\"checkbox\" name=\"alertCli[]\" id=\"check-5\" value=\"2\"> par SMS</div>
                            </div>

                        </td>

                    </tr>
                    <tr>
                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Etat</strong></td>
                        <td>
                            <p>
                                Considérer le RDV comme transmis :
                                <input type=\"radio\" name=\"etat\" id=\"radio_6\" value=\"1\" class=\"radio\"> <label for=\"auto\">Auto   </label>
                                <input type=\"radio\" name=\"etat\" id=\"radio_7\" value=\"0\" class=\"radio\"> <label for=\"oui\">Oui</label>
                            </p>
                        </td>

                    </tr>
                    <tr>
                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Rappel</strong></td>
                        <td>
                            <p>
                                <input type=\"radio\" name=\"rappel\" id=\"pasRappel\" value=\"0\" class=\"radio\"> <label for=\"pas de rappel\">Pas de rappel </label>
                            </p>
                            <p> <div class=\"columns\">
                                <div class=\"one-columns\">
                                    <input type=\"radio\" name=\"rappel\" id=\"ouiRappel\" value=\"1\" class=\"radio\"> <label for=\"rappel\">Rappel</label>
                                </div>
                                <div class=\"one-columns\">
                                </div>
                                <div class=\"one-columns\">
                                    <span class=\"number input margin-right\">
                                        <button type=\"button\" class=\"button number-down\">-</button>
                                        <input name=\"myRappel\" type=\"text\" value=\"5\" size=\"3\" class=\"input-unstyled\">
                                        <button type=\"button\" class=\"button number-up\">+</button>
                                    </span>
                                </div>
                                <div class=\"one-columns\">
                                    <select name=\"duree1\" class=\"select expandable-list\" style=\"width:10em\" > à l'avance
                                        <option value=\"1\" >minute(s)</option>
                                        <option value=\"2\" >heure(s)</option>

                                    </select>
                                </div>
                                <div class=\"one-columns\">

                                </div>
                                <div class=\"one-columns\">
                                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"rap[]\" id=\"interne\" value=\"1\"> interne
                                </div>
                                <div class=\"one-columns\">

                                </div>
                                <div class=\"one-columns\">
                                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"rap[]\" id=\"mail\" value=\"2\"> par mail
                                </div>
                                <div class=\"one-columns\">
                                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"rap[]\" id=\"sms\" value=\"3\"> par SMS
                                </div>
                            </div></p>
                        </td>

                    </tr>
                    <tr>
                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Rappel pour le client fréquent</strong></td>
                        <td>
                            <p>
                                <input type=\"radio\" name=\"rapCli\" id=\"radio_1\" value=\"0\" class=\"radio\"> <label for=\"pas de rappel\">Pas de rappel </label>
                            </p>
                            <p>   <div class=\"columns\">
                                <div class=\"one-columns\">
                                    <input type=\"radio\" name=\"rapCli\" id=\"radio_2\" value=\"1\" class=\"radio\"> <label for=\"rappel\">Rappel</label>
                                </div>
                                <div class=\"one-columns\">
                                </div>
                                <div class=\"one-columns\">
                                    <span class=\"number input margin-right\">
                                        <button type=\"button\" class=\"button number-down\">-</button>
                                        <input name=\"rappelCli\" type=\"text\" value=\"5\" size=\"3\" class=\"input-unstyled\">
                                        <button type=\"button\" class=\"button number-up\">+</button>
                                    </span>
                                </div>
                                <div class=\"one-columns\">
                                    <select name=\"duree2\" class=\"select expandable-list\" style=\"width:10em\" > à l'avance
                                        <option value=\"1\" >minute(s)</option>
                                        <option value=\"2\" >heure(s)</option>

                                    </select>
                                </div>

                                <div class=\"one-columns\">

                                </div>
                                <div class=\"one-columns\">
                                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"cli[]\" id=\"check-1\" value=\"1\"> par mail
                                </div>
                                <div class=\"one-columns\">
                                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"cli[]\" id=\"check-1\" value=\"2\"> par SMS
                                </div>
                            </div></p>
                        </td>
                    </tr>
                    <tr>
                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Recopie</strong> </td>
                        <td>
                            <div class=\"columns\">
                                <div class=\"two-column\"> <input type=\"radio\" name=\"recopie\" id=\"recopie1\" value=\"0\" class=\"radio\"> <label for=\"NON\">Non </label>
                                </div>
                                <div class=\"one-columns\">
                                </div>
                                <div class=\"one-columns\">
                                    <input type=\"radio\" name=\"recopie\" id=\"recopie2\" value=\"1\" class=\"radio\"> <label for=\"oui\">Oui,conserver les informations</label>
                                </div>

                            </div>
                        </td>
                    </tr>



                    <tr>
                        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Avancé</strong></td>
                        <td>
                            <div class=\"columns\">
                                <div class=\"four-columns\">

                                    <a  onclick=\"periodiciteshow();\" >Périodicité </a>


                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>

            </table>
";
        // line 308
        $this->loadTemplate("RendezVousBundle:Default:annulerRdv.html.twig", "RendezVousBundle:Default:addRendezVous.html.twig", 308)->display($context);
        // line 309
        echo "            <button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" >Ajouter</button>
";
        // line 311
        echo "    ";
        // line 312
        echo "    ";
        // line 315
        echo "


        </form>

    </center>


    <h4>Styled table with simple sorting</h4>

    <p>Simple sorting and manual controls if you prefer to handle the table output server-side:</p>

    <p><b>Tip:</b> try clicking on a row to show an extra line style!</p>


</div>
<script>
    function openIframeClient()
    {
        \$.modal({
            title: ' <strong>client frequent</strong>',
            content: ''
                    + '<table class=\"table simple-table responsive-table responsive-table-on w \" style=\"  height: 10em;\">'
                    + '<tfoot>'
                    + '<tr>'
                    + '<td colspan=\"2\"  class=\"align-center\">'
                    + '<div class=\"columns\">'
                    + '<div class=\"three-columns\">'
                    + '</div>'
                    + '<div class=\"two-columns\">'
                    + '<button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" name=\"iframClient\" onclick=\"saveClient()\" id=\"iframClient\">Enregistrer</button>'
                    + '</div>'
                    + ' <div class=\"tow-columns\">'
                    + ' <button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" name=\"annuler\">annuler</button>'
                    + ' </div>'
                    + ' </div>'
                    + ' </td>'
                    + ' </tr>'
                    + ' </tfoot>'
                    + ' <tbody>'
                    + '   <tr>'
                    + ' <td>Civilité </td>'
                    + ' <td>'
                    + '    <div class=\"three-columns\">'
                    + '       <select class=\"select blue-gradient expandable-list\" style=\"width:15em\" name=\"civilite\" id=\"civilite\">'
                    + '     <option value=\"0\" >--Civilité--</option>'
                    + '     <option value=\"1\" >M</option>'
                    + '     <option value=\"2\" >Mme</option>'
                    + '     <option value=\"3\" >Mlle</option>'
                    + '     <option value=\"4\" >Enfant</option>'
                    + '     <option value=\"5\" >Dr</option>'
                    + '     <option value=\"6\" >Pr</option>'
                    + '     <option value=\"7\" >Me</option>'
                    + ' </select>'
                    + ' </div>'
                    + '            </td>'
                    + '        </tr>'
                    + '        <tr>'
                    + '            <td>Nom</td>'
                    + '                <td>'
                    + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"nom\" id=\"nom\">'
                    + '                </td>'
                    + '            </tr>'
                    + '            <tr>'
                    + '                <td>Prénom </td>'
                    + '                <td>'
                    + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"prenom\" id=\"prenom\">'
                    + '                </td>'
                    + '            </tr> <tr>'
                    + '                <td >Tél. Domicile </td>'
                    + '                <td>'
                    + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" nam=\"telDomicile\" id=\"tel\">'
                    + '                </td>'
                    + '            </tr>'
                    + '            <tr>'
                    + '                <td >Tél. Mob'
                    + '                <td>'
                    + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telMob\" id=\"telMob\">'
                    + '                </td>'
                    + '         </tr>'
                    + '          <tr>'
                    + '              <td >Té. Pro </td>'
                    + '                <td>'
                    + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telPro\" id=\"telPro\">'
                    + '                </td>'
                    + '            </tr>'
                    + '            <tr>'
                    + '              <td>Tél. Box </td>'
                    + '              <td>'
                    + '                  <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telBox\" id=\"telBox\">'
                    + '            </td>'
                    + '        </tr>'
                    + '        <tr>'
                    + '            <td> Adresse </td>'
                    + '                <td>'
                    + '                  <textarea style=\"width: 20em\"class=\"input\" name=\"adresse\" id=\"adresse\"></textarea>'
                    + '              </td>'
                    + '          </tr>'
                    + '          <tr>'
                    + '              <td >Email </td>'
                    + '              <td>'
                    + '                  <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"email\" id=\"email\">'
                    + '              </td>'
                    + '          </tr>'
                    + '            <tr>'
                    + '                <td >Note  </td>'
                    + '                <td>'
                    + '                    <textarea style=\"width: 20em\"class=\"input\" name=\"note\" id=\"note\"></textarea>'
                    + '                </td>'
                    + '            </tr>'
                    + '            <tr>'
                    + '                <td  >Mot de passe Web :</td>'
                    + '                <td>'
                    + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"password\" id=\"password\">'
                    + '                    <button type=\"submit\" class=\"button  blue-gradient\"  name=\"generer\" id=\"generer\">Générer</button>'
                    + '                </td>'
                    + '        </tbody> '
                    + '    </table>',
            buttonsAlign: 'center',
            resizable: true,
            width: 600,
            height: 600
        });
    }
    function saveClient()
    {
        // on sécurise les données
        var civilite = encodeURIComponent(\$('#civilite').val());
        var nom = encodeURIComponent(\$('#nom').val());
        var prenom = encodeURIComponent(\$('#prenom').val());
        var telDomicile = encodeURIComponent(\$('#tel').val());
        var telMob = encodeURIComponent(\$('#telMob').val());
        var telPro = encodeURIComponent(\$('#telPro').val());
        var telBox = encodeURIComponent(\$('#telBox').val());
        var adresse = encodeURIComponent(\$('#adresse').val());
        var email = encodeURIComponent(\$('#email').val());
        var note = encodeURIComponent(\$('#note').val());
        var password = encodeURIComponent(\$('#password').val());
        var generer = encodeURIComponent(\$('#generer').val());

        if (nom != \"\") { // on vérifie que les variables ne sont pas vides
            console.log('befor success');
            \$.ajax({
                url: '";
        // line 458
        echo $this->env->getExtension('routing')->getPath("createMyClient");
        echo "', // on donne l'URL du fichier de traitement
                type: \"POST\", // la requête est de type POST
                data: \"civilite=\" + civilite + \"&nom=\" + nom + \"&prenom=\" + prenom + \"&tel=\" + tel + \"&telMob=\" + telMob + \"&telPro=\" + telPro + \"&telBox=\" + telBox + \"&adresse=\" + adresse + \"&email=\" + email + \"&note=\" + note + \"&password=\" + password, // et on envoie nos données
                success: function(data) {
                    console.log('success');
                    console.log(data.nom);
                    \$('#pff').append('<option value=\"' + data.id + '\" >' + data.nom + ' ' + data.prenom + '</option>');
                    document.querySelector('#pff [value=\"' + data.id + '\"]').selected = true;
                    \$('#pff').change();
                    console.log('success fermer');
                }
            });
            \$('#modals').remove();
        }
    }
</script>
";
    }

    public function getTemplateName()
    {
        return "RendezVousBundle:Default:addRendezVous.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  543 => 458,  398 => 315,  396 => 312,  394 => 311,  391 => 309,  389 => 308,  206 => 128,  200 => 125,  187 => 115,  183 => 113,  141 => 72,  125 => 70,  122 => 69,  118 => 68,  87 => 39,  72 => 37,  68 => 36,  41 => 12,  31 => 4,  28 => 3,  11 => 1,);
    }
}
