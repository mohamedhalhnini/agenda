<?php

/* RendezVousBundle:Default:rdvType.html.twig */
class __TwigTemplate_0228a0b457b16b7a12c9d47bca14144c4c867635fd7193395a80fd5ab4c42fba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "RendezVousBundle:Default:rdvType.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<form action =\"";
        echo $this->env->getExtension('routing')->getPath("createTypeRdv");
        echo "\" method=\"post\">
<table class=\"simple-table responsive-table fluid\" id=\"sorting-example2\" >
<thead>
<tr>
    <td align=\"center\" colspan=\"2\">
        Gestion des rendez-vous types
    </td>
</tr>
</thead>
<tbody>
<tr>
    <td style=\"width: 14em\">
        <p>
            <strong>Choix du RDV type :</strong>
        </p>
    </td>
    <td>
        <select name=\"newRdv\"  class=\"select compact l\" style=\"width: 27em\">
            <option value=\"1\">-- Nouvelle RDV type --</option>
            <option value=\"2\">consultation</option>
            <option value=\"3\">examination</option>
            <option value=\"4\">test respiration</option>
        </select>
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Libellé</strong>
        </p>
    </td>

    <td>
        <input name=\"libelle\" type=\"text\" size=\"25\" value=\"Consultation\" class=\"input\" >
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Durée</strong>
        </p>
    </td>

    <td>
        <p>

            d'une durée :
            <input type=\"text\" name=\"duree\" style=\"width: 40px\" id=\"time3\" class=\"input\" >
            (concaténer le texte suivant au libllé : <input name=\"concat\" type=\"text\" size=\"25\" value=\"NV\" class=\"input\">)
        </p>
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Couleur</strong>
        </p>
    </td>

    <td>
        <input  type=\"color\" name=\"couleur\" class=\"input\" style=\"width: 15em\">
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Information</strong>
        </p>
    </td>

    <td>
        <textarea name=\"information\" id=\"autoexpanding\" class=\"input full-width autoexpanding\">

           </textarea>


    </td>
</tr>
<tr>
    <td class=\"vertical-center\">
        <p>
            <strong>Champs du client fréquent requis</strong>
        </p>
    </td>

    <td>
        <output class=\"underline\">Champs permanent :</output>
        <br>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"telDomicile\" id=\"checkbox-1\" value=\"1\" checked>
        <label for=\"checkbox-1\" class=\"label\">Tél. Domicile</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"telTravail\" id=\"checkbox-2\" value=\"1\" checked>
        <label for=\"checkbox-1\" class=\"label\">Tél. Travail</label>
        <br>
        <input  class=\"styled checkbox\" type=\"checkbox\" name=\"telMobile\" id=\"checkbox-3\" value=\"1\" >
        <label for=\"checkbox-1\" class=\"label\">Tél. Mobile</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"telBox\" id=\"checkbox-4\" value=\"1\" >
        <label for=\"checkbox-1\" class=\"label\">Tél. Box</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"adresse\" id=\"checkbox-5\" value=\"1\">
        <label for=\"checkbox-1\" class=\"label\">Adresse</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"email\" id=\"checkbox-6\" value=\"1\">
        <label for=\"checkbox-1\" class=\"label\">E-mail</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"notes\" id=\"checkbox-7\" value=\"1\" >
        <label for=\"checkbox-1\" class=\"label\">Notes</label>
        <br>
        <br>
        <output class=\"underline\">Champs nom-partagés :</output>
        <br>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"dateNaissance\" id=\"checkbox-8\" value=\"1\">
        <label for=\"checkbox-1\" class=\"label\">Date de naissance</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"nomJF\" id=\"checkbox-9\" value=\"1\">
        <label for=\"checkbox-1\" class=\"label\">Nom de JF</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"lastVisit\" id=\"checkbox-10\" value=\"1\" checked>
        <label for=\"checkbox-1\" class=\"label\">Derniére visite</label>
    </td>
</tr>
<tr>
    <td class=\"vertical-center\">
        <p>
            <strong>Champs du client fréquent a reporter</strong>
        </p>
    </td>

    <td class=\"vertical-center\">
        <input class=\"radio\" type=\"radio\" name=\"champClient\" id=\"radio-1\" value=\"1\">
        Tous les champs renseignés
        <output style=\"color: white\">.  .</output>
        <input class=\"radio\" type=\"radio\" name=\"champClient\" id=\"radio-1\" value=\"0\" checked>
        Seuls les champs requis
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Selectionnable lors de la prise de rendez-vous par internet</strong>
        </p>
    </td>

    <td class=\"vertical-center\">
        <input class=\"radio\" type=\"radio\" name=\"selectionnable\" id=\"radio-1\" value=\"0\">
        Non
        <output style=\"color: white\">.  .</output>
        <input class=\"radio\" type=\"radio\" name=\"selectionnable\" id=\"radio-1\" value=\"1\" checked>
        Oui
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Information supplémentaires pour notes récurrentes</strong>
        </p>
    </td>

    <td class=\"vertical-center\">
        <input class=\"radio\" type=\"radio\" name=\"infoSup\" id=\"radio-1\" value=\"0\" checked>
        Non
        <output style=\"color: white\">.  .</output>
        <input class=\"radio\" type=\"radio\" name=\"infoSup\" id=\"radio-1\" value=\"1\">
        Oui
    </td>
</tr>
<tr>
    <td class=\"vertical-center\">
        <p>
            <strong>Sélectionné par défaut</strong>
        </p>
    </td>

    <td>
        <input class=\"radio\" type=\"radio\" name=\"selectParDef\" id=\"radio-1\" value=\"0\" checked>
        Non
        <output style=\"color: white\">.  .</output>
        <input class=\"radio\" type=\"radio\" name=\"selectParDef\" id=\"radio-1\" value=\"1\">
        Oui
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Choix spécifique sur alertes/rappels</strong>
        </p>
    </td>

    <td class=\"vertical-center\">
        <input class=\"radio\" type=\"radio\" name=\"choixAlertRappel\" id=\"radio-1\" value=\"0\" checked>
        Non
        <output style=\"color: white\">.  .</output>
        <input class=\"radio\" type=\"radio\" name=\"choixAlertRappel\" id=\"radio-1\" value=\"1\" >
        Oui
    </td>
</tr>
<tr>
    <td colspan=\"2\" align=\"center\">
        <button type=\"submit\" class=\"button compact\">Enregistrer</button>
        <button type=\"button\" class=\"button compact\">Annuler</button>
        <button type=\"button\" class=\"button compact\" disabled>Supprimer</button>
    </td>
</tr>
</tbody>
</table>

    </form>
";
    }

    public function getTemplateName()
    {
        return "RendezVousBundle:Default:rdvType.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
