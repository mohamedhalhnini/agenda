<?php

/* RendezVousBundle:Default:listRdvType.html.twig */
class __TwigTemplate_b647e14177c44433791847538cff4310e9f0a6a824468eb1e80cefd2bf7ddcb5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "RendezVousBundle:Default:listRdvType.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
<table class=\"table responsive-table\" id=\"sorting-advanced\">

    <thead>
        <tr>
            <th scope=\"col\">Libellé</th>
            <th scope=\"col\" width=\"15%\" class=\"align-center hide-on-mobile\">Durée(/durée altérnatif)</th>
            <th scope=\"col\" width=\"60\" class=\"align-center\">Actions</th>
        </tr>
    </thead>

    <tfoot>
        <tr>
            <td colspan=\"6\">
                6 entries found
            </td>
        </tr>
    </tfoot>

    <tbody>
                                    ";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["rdvTypes"]) ? $context["rdvTypes"] : $this->getContext($context, "rdvTypes")));
        foreach ($context['_seq'] as $context["_key"] => $context["rdvType"]) {
            // line 24
            echo "        <tr id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["rdvType"], "id", array()), "html", null, true);
            echo "\">
            <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["rdvType"], "libelle", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["rdvType"], "dureee", array()), "H:i"), "html", null, true);
            echo "</td>

            <td class=\"low-padding align-center\">           
                <span class=\"button-group compact\">
                    <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("modifierRdvType", array("id" => $this->getAttribute($context["rdvType"], "id", array()))), "html", null, true);
            echo "\" class=\"button icon-pencil with-tooltip\" title=\"Modifier\"></a>
                    <a onclick=\"openDeleteConfirm(";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["rdvType"], "id", array()), "html", null, true);
            echo ")\" class=\"button icon-trash with-tooltip confirm\" title=\"Supprimer\"></a>
                </span>
            </td>   
        </tr>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rdvType'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "\t
    </tbody>

</table>
<script>
    function openDeleteConfirm(id)
    {
        \$.modal.confirm('voulez vous vraiment supprimer ?', function()
        {
            deleteLigne(id);
    ";
        // line 46
        echo "        }, function()
        {
        });
    }
    ;
    function deleteLigne(id) {
        var myId = id;
        console.log(id);
        \$.ajax({
            url: 'http://localhost/monagenda/web/app_dev.php/rendezVous/deleteRdvType/'+id,
            type: \"get\",
            success: function(data) {
                \$(\"#\" + myId).remove();
                console.log(\"success\");
            }
        });
    }

</script>

";
    }

    public function getTemplateName()
    {
        return "RendezVousBundle:Default:listRdvType.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 46,  88 => 35,  77 => 31,  73 => 30,  66 => 26,  62 => 25,  57 => 24,  53 => 23,  31 => 3,  28 => 2,  11 => 1,);
    }
}
