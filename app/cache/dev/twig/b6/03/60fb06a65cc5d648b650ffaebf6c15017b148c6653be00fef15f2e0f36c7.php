<?php

/* utilisateurBundle:Utilisateur:profil.html.twig */
class __TwigTemplate_b60360fb06a65cc5d648b650ffaebf6c15017b148c6653be00fef15f2e0f36c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "utilisateurBundle:Utilisateur:profil.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "                    <style>
                        body{padding-top:30px;}

.glyphicon {  margin-bottom: 10px;margin-right: 10px;}

small {
display: block;
line-height: 1.428571429;
color: #999;
}
                    </style>
<section role=\"main\" id=\"main\">

        <hgroup id=\"main-title\" class=\"thin\">
            <h1>Utilisateur</h1>
            <h2>nov <strong>10</strong></h2>
        </hgroup>


        <div class=\"with-padding\">

          <table class=\"simple-table responsive-table responsive-table-on\">
                    <tr>
                        <th class=\"per30\" style=\" background-color:#e4e9eb;\">Nom d'utilisateu :</th><td>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>
                        <th class=\"per30\" style=\" background-color:#e4e9eb;\">Nom :</th><td>";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "nom", array()), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>
                        <th class=\"per30\" style=\" background-color:#e4e9eb;\">Prenom :</th><td>";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "prenom", array()), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>
                        <th class=\"per30\"style=\" background-color:#e4e9eb;\">Email : </th><td>";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>
                        <th class=\"per30\" style=\" background-color:#e4e9eb;\">Fonction  :</th><td>
                            
                            ";
        // line 40
        if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
            // line 41
            echo "                                    Administrateur
                                ";
        } elseif ($this->env->getExtension('security')->isGranted("ROLE_MEDECIN")) {
            // line 43
            echo "                                    Medecin
                                ";
        } elseif ($this->env->getExtension('security')->isGranted("ROLE_SECRETAIRE")) {
            // line 45
            echo "                                    Secretaire
                                ";
        }
        // line 46
        echo " 
                        </td>  
                    <tr>
                         <th class=\"per30\" style=\" background-color:#e4e9eb;\">Dernière connexion  :</th><td>  ";
        // line 49
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "lastlogin", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "lastlogin", array()), "Y-m-d h:m:s"), "html", null, true);
        }
        echo "</td>
                    </tr>
                    
           </table>  
                         <a href=\"";
        // line 53
        echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
        echo "\" >Changer Mot de Pass</a>

        </div>

    </section>
           
              ";
    }

    public function getTemplateName()
    {
        return "utilisateurBundle:Utilisateur:profil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 53,  101 => 49,  96 => 46,  92 => 45,  88 => 43,  84 => 41,  82 => 40,  74 => 35,  68 => 32,  62 => 29,  56 => 26,  31 => 3,  28 => 2,  11 => 1,);
    }
}
