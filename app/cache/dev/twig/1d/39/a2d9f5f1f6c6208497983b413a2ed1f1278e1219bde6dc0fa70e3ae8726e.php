<?php

/* InstructionBundle:Instruction:listPeriodique.html.twig */
class __TwigTemplate_1d39a2d9f5f1f6c6208497983b413a2ed1f1278e1219bde6dc0fa70e3ae8726e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<div id=\"sorting-advanced_wrapper\" class=\"dataTables_wrapper\" role=\"grid\">
   
    <table class=\"table responsive-table responsive-table-on dataTable\" id=\"sorting-advanced\" aria-describedby=\"sorting-advanced_info\">

        <thead>
            <tr role=\"row\">
                <th scope=\"col\" class=\"sorting_disabled\" role=\"columnheader\" rowspan=\"1\" colspan=\"1\" aria-label=\"\" style=\"width: 12px;\">
                    <input type=\"checkbox\" name=\"check-all\" id=\"check-all\" value=\"1\"></th>
                <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Id</th>
                <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Titre</th>
                <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Priorite</th>
                <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Date de debut</th>
                <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Date de fine</th>
                <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Client</th>
                <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">PhraseUseuelle</th>
                    ";
        // line 17
        if (((isset($context["archived"]) ? $context["archived"] : $this->getContext($context, "archived")) == 1)) {
            echo " 
                    <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\"> date d'archivation</th>
                    ";
        }
        // line 20
        echo "                <th scope=\"col\" width=\"60\" class=\"align-center sorting_disabled\" role=\"columnheader\" rowspan=\"1\" colspan=\"1\" aria-label=\"Actions\" style=\"width: 100px;\">Actions</th>

            </tr>            </thead>
        <tfoot>
        </tfoot>


        <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\">
            ";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["periodiqueInstructions"]) ? $context["periodiqueInstructions"] : $this->getContext($context, "periodiqueInstructions")));
        foreach ($context['_seq'] as $context["_key"] => $context["instruction"]) {
            // line 29
            echo "                <tr class=\"odd\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "id", array()), "html", null, true);
            echo "\">
                    <th scope=\"row\" class=\"checkbox-cell  sorting_1\"><input type=\"checkbox\" name=\"checked[]\" id=\"check-1\" value=\"1\"></th>
                    <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "id", array()), "html", null, true);
            echo "  </td>
                    <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "titre", array()), "html", null, true);
            echo "</td>
                    <td> ";
            // line 33
            if (($this->getAttribute($context["instruction"], "priorite", array()) == 0)) {
                echo " Haute";
            } elseif (($this->getAttribute($context["instruction"], "priorite", array()) == 1)) {
                echo "Normale ";
            }
            echo "</td>
                    <td>";
            // line 34
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["instruction"], "de", array()), "d/m/Y"), "html", null, true);
            echo "</td>
                    <td>";
            // line 35
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["instruction"], "a", array()), "d/m/Y"), "html", null, true);
            echo "</td>
                    <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["instruction"], "client", array()), "nom", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["instruction"], "client", array()), "prenom", array()), "html", null, true);
            echo "</td>
                    <td> ";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["instruction"], "phraseusuelle", array()), "text", array()), "html", null, true);
            echo "</td>
                    ";
            // line 38
            if (((isset($context["archived"]) ? $context["archived"] : $this->getContext($context, "archived")) == 1)) {
                echo " <td> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["instruction"], "datearchivation", array()), "d-m-Y"), "html", null, true);
                echo "</td> ";
            }
            // line 39
            echo "
                    <td><a  class=\"confirm\" href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("instruction_update", array("id" => $this->getAttribute($context["instruction"], "id", array()))), "html", null, true);
            echo "\"><span class=\"button  icon-pencil \"></span> </a> 
                        <a  onclick=\"openDeleteConfirm(";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "id", array()), "html", null, true);
            echo ")\" >   <span class=\"button icon-trash \"></span> </a>
                     ";
            // line 42
            if (((isset($context["archived"]) ? $context["archived"] : $this->getContext($context, "archived")) == 0)) {
                echo " 
                            <a  onclick=\"openArchivateConfirm(";
                // line 43
                echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "id", array()), "html", null, true);
                echo ")\" >   <span class=\"button icon-box \"></span></a>
                        ";
            }
            // line 45
            echo "                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['instruction'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "
        </tbody>
    </table>
   
</div> 









";
    }

    public function getTemplateName()
    {
        return "InstructionBundle:Instruction:listPeriodique.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 48,  123 => 45,  118 => 43,  114 => 42,  110 => 41,  106 => 40,  103 => 39,  97 => 38,  93 => 37,  87 => 36,  83 => 35,  79 => 34,  71 => 33,  67 => 32,  63 => 31,  57 => 29,  53 => 28,  43 => 20,  37 => 17,  19 => 1,);
    }
}
