<?php

/* AgendaMemoBundle:Memo:fullCalendar.html.twig */
class __TwigTemplate_dfc28e63d1bd4b69c4f052b86099dbbe36fc677a530b96f186e76e8396247d8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AgendaMemoBundle:Memo:fullCalendar.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <link href='";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/fullcalendar.css"), "html", null, true);
        echo "' rel='stylesheet' />
    <link href='";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/fullcalendar.print.css"), "html", null, true);
        echo "' rel='stylesheet' media='print' />
    <script src='";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/lib/moment.min.js"), "html", null, true);
        echo "'></script>
    <script src='";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/lib/jquery.min.js"), "html", null, true);
        echo "'></script>
    <script src='";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/fullcalendar.min.js"), "html", null, true);
        echo "'></script>
    <script src='";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/lang-all.js"), "html", null, true);
        echo "'></script>
    <script type=\"text/javascript\">
    function compareDates(dateFrom, dateTo){
        if ( Date.parse( dateTo ) < Date.parse( dateFrom ) )
            return true;

        return false;
    }
    function print_calendar()
    {
        window.print();
    }
    \$(document).ready(function() {
        var merde = '';
        ";
        // line 31
        echo "

        \$('#my-button').click(function() {

            var moment = \$('#calendar').fullCalendar('getDate');
            alert(\"The current date of the calendar is \" + moment.format());
        });
        \$('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'

            },
            minTime:'08:00',
            maxTime: '18:00',
            lang: 'fr',
            editable: true,
            defaultView :'agendaWeek',
            axisFormat : 'HH:mm',
            slotDuration :'00:05:00',
            timeFormat: {
                agenda : 'HH:mm'
            },
            eventLimit: true, // allow \"more\" link when too many events
            eventSources: [
                ";
        // line 57
        echo (isset($context["memos"]) ? $context["memos"] : $this->getContext($context, "memos"));
        echo "
            ],
            eventColor: '#000066',
            //***********************myAjax*********************************

            //******************************************************

            eventRender: function(event, element, merde){
                \$.ajax({
                    type: 'get',
                    url : 'http://localhost/myagenda/web/app_dev.php/agenda/getClient/'+event.id,
                    beforeSend: function(){
                        console.log('me voila!!');
                    },
                    success:function(data){
                        console.log('cool!!');
//                                \$(\"#time11\").val(data.myTime);
                        element.attr('id', event.id);
                        element.attr('title', '<strong>Client fréquent:</strong>' + ' ' + data.nom + ' ' + data.prenom + '<br>' + 'Email:' + ' ' + data.email + '<br>TelPro:' + data.tel+ '<br>Détail:' + data.detail);
                        element.addClass('with-tooltip');
                    }

                });
            },

            ";
        // line 89
        echo "            eventClick: //function(event, jsEvent, view) {
//
//                    alert('Event: ' +event.id+ event.start.format() + event.end.format() );
//                    alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//                    alert('View: ' + view.name);
//                    \$(this).css('border-color', 'red');
                    ";
        // line 96
        echo "
            // change the border color just for fun

                    function openComplexModal(event)
                    {
                        \$.modal({
                            content:


                            '<strong>Titre :</strong>' + event.id + '<br>' +
                            '<strong>Detail :</strong>' + event.title + '<br>' +
                            '<strong>Date :</strong>' + event.start.format()



                            ,
                            title: 'Détails du mémo',
                            width: 300,
                            scrolling: false,
                            actions: {
                                'fermer' : {
                                    color: 'red',
                                    click: function(win) { win.closeModal(); }
                                }
                            },
                            buttons: {
                                'modifier': {
                                    classes:\t' green-gradient glossy ',
                                    click:\t\tfunction(win) {
                                        document.location.href = \"http://localhost/myagenda/web/app_dev.php/agenda/edit/\" + event.id;
                                        win.closeModal(); }
                                },
                                'supprimer': {
                                    classes:\t' red-gradient glossy ',
                                    click:\t\tfunction(win) {
                                        \$.ajax({
                                            type: 'get',
                                            url : 'http://localhost/myagenda/web/app_dev.php/agenda/supp/' + event.id,
                                            beforeSend: function(){
                                                console.log('je me supprime mtn!!');
                                            },
                                            success:function(data){
                                                console.log('c fait!');
                                            }
                                        });
                                        \$('#calendar').fullCalendar('removeEvents', event.id);
                                        win.closeModal();
                                    }
                                }
                            },
                            buttonsLowPadding: true
                        });
                    },
            //dayClick: function(date,jsEvent,view) {
            //    \$('#calendar').fullcalendar('select', date);

            //    \$('#calendar').fullCalendar('unselect');
            //},
            selectable: true,
            selectHelper: true,
            select: function(start, end) {
//                    var title = prompt('Event Title:');
//                    var eventData;
//                    if (title) {
//                        eventData = {
//                            title: title,
//                            start: start,
//                            end: end
//                        };
//                        \$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
//                    }
            var startDate = moment(start).format('DD-MM-YYYY HH:mm');
                var regex = /^[0-9]{2}\\-[0-9]{2}\\-[0-9]{4} [0]{2}\\:[0]{2}\$/;
                var test = regex.test(startDate);
                if(test == false) {
            \$.modal({
            content:'<strong>Start:</strong>' + start.format('DD-MM-YYYY HH:mm') + '<br>'
                    + '<strong>End:</strong>' + end.format('DD-MM-YYYY HH:mm')
//                        '<strong>Titre :</strong>'+ start.title+'<br>'+
//                        '<strong>Date :</strong>'+event.start.format()
                    ,
                    title: 'Détails du rendez-vous',
                    width: 300,
                    scrolling: false,
                    actions: {
                    'fermer' : {
                    color: 'red',
                            click: function(win) { win.closeModal(); }
                    }
                    },
                    buttons: {
                    'Nouveau rendez-Vous': {
                    classes:\t' blue-gradient glossy ',
                            click:\t\tfunction(win) {
                            document.location.href = \"";
        // line 190
        echo $this->env->getExtension('routing')->getPath("startEnd");
        echo "/\" + start.format('DD-MM-YYYY') + \"/\" + start.format('HH:mm') + \"/\" + end.format('HH:mm');
                                    win.closeModal(); }
                    },
                   'Nouveau Delimiteur': {
                                classes:\t' blue-gradient glossy ',
                                 click:\t\tfunction(win) {
                                    document.location.href = \"";
        // line 196
        echo $this->env->getExtension('routing')->getPath("ajoutDelimiteurCalendar");
        echo "/\"+start.format('DD-MM-YYYY')+\"/\"+start.format('HH:mm')+\"/\"+end.format('HH:mm');
                                    win.closeModal(); }
                    }, 
                            'annuler': {
                            classes:\t' blue-gradient glossy ',
                                    click:\t\tfunction(win) {
                                ";
        // line 203
        echo "                                    win.closeModal();
                                    }
                            }
                    },
                    buttonsLowPadding: true
            });
            }
                    \$('#calendar').fullCalendar('unselect');
            
            },
            dayClick: function(date, jsEvent, view) {
//                    var title = prompt('Event Title:');
//                    var eventData;
//                    if (title) {
//                        eventData = {
//                            title: title,
//                            start: start,
//                            end: end
//                        };
//                        \$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
//                    }
                
                var now = new Date();
                var startDateStr = moment(date).format('DD-MM-YYYY HH:mm');
                var nowDate = moment(now).format('DD-MM-YYYY');
                var regex = /^[0-9]{2}\\-[0-9]{2}\\-[0-9]{4} [0]{2}\\:[0]{2}\$/;
                var test = regex.test(startDateStr);
                if(test == true) {
                if (compareDates(nowDate,startDateStr) == false) {
                    \$.modal({
                        content: '<strong>Vous voulez ajouté <br> un  mémo pour ce jour : ' + date.format('DD-MM-YYYY') + '</strong>',
//                        '<strong>Titre :</strong>'+ start.title+'<br>'+
//                        '<strong>Date :</strong>'+event.start.format()
                        title: 'Ajout du mémo',
                        width: 300,
                        scrolling: false,
                        actions: {
                            'fermer': {
                                color: 'red',
                                click: function (win) {
                                    win.closeModal();
                                }
                            }
                        },
                        buttons: {
                            'Nouveau memo': {
                                classes: 'green-gradient glossy',
                                click: function (win) {
                                    document.location.href = \"http://localhost/myagenda/web/app_dev.php/agenda/nouveau/\" + date.format('DD-MM-YYYY');
                                    win.closeModal();
                                }
                            },
                            'annuler': {
                                classes: ' red-gradient glossy ',
                                click: function (win) {
                                    win.closeModal();
                                }
                            }
                        },
                        buttonsLowPadding: true
                    });
                }
                else {
                    \$.modal({
                        content: '<strong>Attention la demande d\\'ajout de mémo concerne une date passée ' + date.format('DD-MM-YYYY') +'</strong>',
//                        '<strong>Titre :</strong>'+ start.title+'<br>'+
//                        '<strong>Date :</strong>'+event.start.format()
                        title: 'Ajout du mémo',
                        width: 300,
                        scrolling: false,
                        actions: {
                            'fermer': {
                                color: 'red',
                                click: function (win) {
                                    win.closeModal();
                                }
                            }
                        },
                        buttons: {
                            'Nouveau memo': {
                                classes: 'green-gradient glossy',
                                click: function (win) {
                                    document.location.href = \"http://localhost/myagenda/web/app_dev.php/agenda/nouveau/\" + date.format('DD-MM-YYYY');
                                    win.closeModal();
                                }
                            },
                            'annuler': {
                                classes: ' red-gradient glossy ',
                                click: function (win) {
                                    win.closeModal();
                                }
                            }
                        },
                        buttonsLowPadding: true
                    });
                }
                \$(this).css('background-color', '#FFFFCC');
                \$('#calendar').fullCalendar('unselect');
            }},
            /*
            eventMouseover : function( event, jsEvent, view ) {
                \$(this).css('background-color', '#FF0000');
            },
            eventMouseout : function( event, jsEvent, view ) {
                \$(this).css('background-color', '#000066');
            },*/
            eventDrop:function(event, delta, revertFunc){
                var start1 = event.start.format('DD-MM-YYYY');
                var startH = event.start.format('HH:mm');
                var start = start1+' '+startH;
                var now = new Date();
                //var annee   = now.getFullYear();
                //var mois    = ('0'+now.getMonth()+1).slice(-2);
                //var jour    = ('0'+now.getDate()   ).slice(-2);
                //var heure   = ('0'+now.getHours()  ).slice(-2);
                //var minute  = ('0'+now.getMinutes()).slice(-2);
                //var d = jour+'-'+mois+'-'+annee+' '+heure+':'+minute;
                var startDateStr = moment(start).format('DD-MM-YYYY');
                var nowDate = moment(now).format('DD-MM-YYYY');
                var regex = /^[0-9]{2}\\-[0-9]{2}\\-[0-9]{4} [0]{2}\\:[0]{2}\$/;
                var test = regex.test(start);
                if(test == true) {
                    if (compareDates(nowDate,startDateStr) == true) {
                            \$.modal({
                                content: '<p>Attention la demande de déplacement de mémo concerne une date passée <br>' +
                                'Souhaitez-vous poursuivre ?' +
                                '</p>',
                                title: 'Déplacement de mémo',
                                width: 300,
                                resizable: false,
                                scrolling: false,
                                actions: {
                                    'Fermer' : {
                                        color: 'red',
                                        click: function(win) {
                                            revertFunc();
                                            win.closeModal();
                                            \$('#calendar').fullCalendar('refetchEvents');
                                        }
                                    }
                                },
                                buttons: {
                                    'Oui,déplacé': {
                                        classes:\t'green-gradient glossy',
                                        click: function(win) {
                                            \$.ajax({
                                                type: 'get',
                                                url: 'http://localhost/monagenda_/web/app_dev.php/memo/drop/' + event.id + '/' + start1 + '/' + startH,
                                                beforeSend: function () {
                                                    notify('Memo déplacé!', 'Date de déplacement :' + start, {groupSimilar: false});
                                                    console.log('droped');
                                                },
                                                success: function (data) {
                                                    console.log('updaate!!');
                                                }
                                            });
                                            win.closeModal();
                                            \$('#calendar').fullCalendar('refetchEvents');
                                        }
                                    },
                                    'Non': {
                                        classes:\t'red-gradient glossy',
                                        click:\t\tfunction(win) {
                                            revertFunc();
                                            win.closeModal();
                                            \$('#calendar').fullCalendar('refetchEvents');
                                        }
                                    }
                                },
                                buttonsLowPadding: true
                            });
                    }
                    else if (compareDates(nowDate,startDateStr) == false) {
                        \$.ajax({
                            type: 'get',
                            url: 'http://localhost/monagenda_/web/app_dev.php/memo/drop/' + event.id + '/' + start1 + '/' + startH,
                            beforeSend: function () {
                                notify('Memo déplacé!', 'Date de déplacement :' + start, {groupSimilar: false});
                                console.log('droped');
                                console.log(startDateStr);
                                console.log(nowDate);
                            },
                            success: function (data) {
                                console.log('updaate!!');
                            }
                        });
                        \$('#calendar').fullCalendar('refetchEvents');
                    }
                }
                
                else{
                            revertFunc();
                            \$('#calendar').fullCalendar('refetchEvents');
                            \$.modal.alert('Vous ne pouvez pas déplacé ce mémo ici !');
                
                }
            }
//else

        });
    });
    </script>

    <style>

        body {
            margin: 40px 10px;
            padding: 0;
            font-family: \"Lucida Grande\",Helvetica,Arial,Verdana,sans-serif;
            font-size: 14px;
        }

        #calendar {
            max-width: 2000px;
            margin: 0 auto;
        }

    </style>
    <div id=\"calendar\"></div>

";
    }

    public function getTemplateName()
    {
        return "AgendaMemoBundle:Memo:fullCalendar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  247 => 203,  238 => 196,  229 => 190,  133 => 96,  125 => 89,  97 => 57,  69 => 31,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
