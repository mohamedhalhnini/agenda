<?php

/* MemoMemoBundle:Memo:index.html.twig */
class __TwigTemplate_5aef0451185a68ad26d1981339272b0807d154b6fa820e2fef88405aa811fdee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "MemoMemoBundle:Memo:index.html.twig", 1);
        $this->blocks = array(
            'mesScript' => array($this, 'block_mesScript'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_mesScript($context, array $blocks = array())
    {
        // line 3
        $this->displayParentBlock("mesScript", $context, $blocks);
        echo "
<script>
    /*function openConfirm()
     {
     \$.modal.confirm('Confirmez-vous vouloir supprimer ce mémo ?', function()
     {
     \$.modal.alert('La supression est effectuée !');

     }, function()
     {
     \$.modal.alert('La supression est annulée !');
     });
     };*/

    function openModal()
    {
        \$.modal({
            title: 'Ajout du mémo',
            content: '<h4 class=\"green-gradient glossy\">Le mémo est bien ajouté</h4>',
            width: 300,
            resizable: false,
            scrolling: false,
            actions: {
                'Fermer' : {
                    color: 'red',
                    click: function(win) { win.closeModal();}
                }
            },
            buttons: {
                'Annuler': {
                    classes:\t'blue-gradient glossy',
                    click:\t\tfunction(win) { win.closeModal();}
                }
            }
        });
    }
</script>
<script>
    /*var liste1;
    var valeur1;
    liste1 = document.getElementById(\"agenda_memobundle_general_type_clientFrequent\");
    valeur1 = liste1.options[liste1.selectedIndex].text;*/
    function recup1(){
        var valeur1;
       return valeur1 = document.getElementById('agenda_memobundle_general_type_clientFrequent').options[document.getElementById('agenda_memobundle_general_type_clientFrequent').selectedIndex].text;
    }
</script>
<script>
    /*
    var liste2;
    var valeur2;
    liste2 = document.getElementById(\"agenda_memobundle_general_type_phraseUsuelle\");
    valeur2 = liste2.options[liste2.selectedIndex].text;*/
    function recup2(){
       var valeur2;
       return valeur2 = document.getElementById('agenda_memobundle_general_type_phraseUsuelle').options[document.getElementById('agenda_memobundle_general_type_phraseUsuelle').selectedIndex].text;
    }
</script>
    <script>
        // Demo Iframe loading
        function openIframe()
        {
            \$.modal({
                title: 'Client Fréquent',
                url: '";
        // line 67
        echo $this->env->getExtension('routing')->getPath("agenda_client_create");
        echo "',
                useIframe: true,
                width: 600,
                height: 600,
                buttons: {
                    'Enregistrer' : {
                        classes: 'blue-gradient glossy big full-width',
                        click: function(modal) { modal.closeModal(); }
                    }
            });
        }
    </script>
    <script>
        function openModal2()
        {
            \$.modal({
                title: 'Modal window',
                content: '<form name=\"fomr12\" method=\"get\"> '
                + ' <div class=\"three-columns\">'
                + ' </div>'
                + ' <div class=\"two-columns\">'
                + '     <button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" name=\"enregistrer\" onclick=\"envoyerDonnees()\">Enregistrer</button>'
                + '  </div>'
                + '  <div class=\"tow-columns\">'
                + '       <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"nom\"  id=\"nom\">'
                + '  </div>'
                + '  </div> </form>',
                buttonsAlign: 'center',
                resizable: true,
                width: 600,
                height: 600,
                buttons: {
                    'enregistrer': {
                        classes:\t'blue-gradient glossy',
                        click:\t\tfunction(win) { win.closeModal();}
                    }
                }
            });


        }
        </script>
    <script>
        function rafrecheSelect() {
            var length = document.form2.client.length;
            document.form2.client[length] = new Option(\"nouvelle entrée\");
        }


    </script>
";
    }

    // line 118
    public function block_content($context, array $blocks = array())
    {
        // line 119
        echo "<div class=\"with-padding\">
    <div class=\"with-padding with-border\">
        ";
        // line 121
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        <table class=\"table1 responsive-table\">

            <thead>
            <tr>
                <th class=\"scratch-metal\" scope=\"col\" colspan=\"8\" align=\"center\">Gestion de mémos</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <th scope=\"row\" >
                    Phrase usuelle
                </th>
                <td colspan=\"2\">";
        // line 135
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phraseUsuelle", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 136
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phraseUsuelle", array()), 'errors');
        echo "</span></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Client Fréquent
                </th>
                <td colspan=\"2\">
                    ";
        // line 148
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clientFrequent", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 149
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clientFrequent", array()), 'errors');
        echo "</span>
                </td>
                <td>
                    <a href=\"#\" class=\"button\">
                        <span class=\"button-icon\"><span class=\"icon-plus-round\"></span></span>
                        Ajout
                    </a>
                </td>
                <td>
                    <a href=\"#\" class=\"button\">
                        <span class=\"button-icon\"><span class=\"icon-page-list\"></span></span>
                        Histo
                    </a>
                </td>
                <td>
                    <a href=\"#\" class=\"button\">
                        <span class=\"button-icon\"><span class=\"icon-quote\"></span></span>
                        Notes
                    </a>
                </td>
                <td>
                    <a href=\"#\" class=\"button\" onclick=\"openIframe\">
                        <span class=\"button-icon\"><span class=\"icon-plus\"></span></span>
                        Nouv
                    </a>
                </td>
                <td>
                    <a onclick=\"confirm(recup1())\" href=\"#\" class=\"button\">
                        <span class=\"button-icon\"><span class=\"icon-pencil\"></span></span>
                        Modif
                    </a>
                </td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Détail
                </th>
                <td colspan=\"8\">
                    ";
        // line 187
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "detail", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 188
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "detail", array()), 'errors');
        echo "</span>
                </td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Date
                </th>
                <td colspan=\"2\">
                <span class=\"input\">
\t                <span class=\"icon-calendar\"></span>
\t                    ";
        // line 198
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dateenregistrement", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 199
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dateenregistrement", array()), 'errors');
        echo "</span>
                </span>
                </td>
                <td>
                                                    <span class=\"button-group\">
                                                        <a href=\"#\" class=\"button\">Aujourd'hui</a>
                                                    </span>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Prioritaire
                </th>
                <td colspan=\"2\">
                    ";
        // line 217
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "priorite", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 218
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "priorite", array()), 'errors');
        echo "</span>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Alerter
                </th>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\" checked> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\"> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\"> par Fax</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Etat
                </th>
                <td colspan=\"3\">Considérer le mémo comme transmis: </td>
                <td>
                    ";
        // line 245
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 246
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'errors');
        echo "</span>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <th scope=\"row\"></th>
                <td></td>
                <td></td>
                <td colspan=\"2\">
                    ";
        // line 258
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Enregistrer", array()), 'widget');
        echo "
                    <a href=\"";
        // line 259
        echo $this->env->getExtension('routing')->getPath("agenda_memo_listMemo");
        echo "\" class=\"button icon-replay-all\">Annuler</a>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
        ";
        // line 267
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "MemoMemoBundle:Memo:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  352 => 267,  341 => 259,  337 => 258,  322 => 246,  318 => 245,  288 => 218,  284 => 217,  263 => 199,  259 => 198,  246 => 188,  242 => 187,  201 => 149,  197 => 148,  182 => 136,  178 => 135,  161 => 121,  157 => 119,  154 => 118,  99 => 67,  32 => 3,  29 => 2,  11 => 1,);
    }
}
