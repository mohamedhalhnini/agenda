<?php

/* RendezVousBundle:RdvClient:login.html.twig */
class __TwigTemplate_c460d16d6f0cea3e57769e4870da27b834887458dfaf05cf93056cd5ba2a2871 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base2.html.twig", "RendezVousBundle:RdvClient:login.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "


<form method=\"post\" action=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("clientRdv");
        echo "\">
<div style=\"margin: 150px\">
<ul class=\"inputs large\">
    <li><span class=\"icon-user mid-margin-right\"></span><input type=\"text\" name=\"login\" id=\"login\" value=\"\" class=\"input-unstyled\"  placeholder=\"Login\" autocomplete=\"off\"></li>
    <li><span class=\"icon-lock mid-margin-right\"></span><input type=\"password\" name=\"pass\" id=\"pass\" value=\"\" class=\"input-unstyled\" placeholder=\"Password\" autocomplete=\"off\"></li>
</ul>
<button style=\"margin-left:200px;width: 20em\"  class=\"button\" >se connecter</button>
</div>
</form>






";
    }

    public function getTemplateName()
    {
        return "RendezVousBundle:RdvClient:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }
}
