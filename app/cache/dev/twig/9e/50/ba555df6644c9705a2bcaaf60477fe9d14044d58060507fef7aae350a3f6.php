<?php

/* InstructionBundle:Instruction:list.html.twig */
class __TwigTemplate_9e50ba555df6644c9705a2bcaaf60477fe9d14044d58060507fef7aae350a3f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "InstructionBundle:Instruction:list.html.twig", 1);
        $this->blocks = array(
            'mesScript' => array($this, 'block_mesScript'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_mesScript($context, array $blocks = array())
    {
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        // line 6
        echo "    <p>  <h1>";
        echo twig_escape_filter($this->env, (isset($context["titre"]) ? $context["titre"] : $this->getContext($context, "titre")), "html", null, true);
        echo "</h1></p>

<form name=\"fomrRecherche\" action=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("instruction_list_permanente");
        echo "\" method=\"post\" class='center'>
    <p><select id=\"type\" name=\"type\" style=\"width:15em\" class=\"blue-gradient select validate[required]\" onchange=\"rehcerche()\">
            <option value=\"O\">--select--</option>
            <option value=\"1\">permannte</option>
            <option value=\"2\">ponctuel</option>
            <option value=\"3\">entre deux dates</option>
        </select>        
        <input type=\"radio\" name=\"typeArchived\" id=\"radio_2\" value=\"1\" class=\"radio mid-margin-left\" checked=\"true\"> <label for=\"radio-1\" >non archivés</label>
        <input type=\"radio\"  name=\"typeArchived\" id=\"radio_1\" value=\"2\" class=\"radio mid-margin-left\"> <label for=\"radio-1\" >archivés</label>
        <input type=\"radio\" name=\"typeArchived\" id=\"radio_2\" value=\"3\" class=\"radio mid-margin-left\"> <label for=\"radio-1\" >supprimées</label>
        <button id=\"recherche\" name=\"recherche\" type=\"submit\" class=\"button icon-search blue-gradient hidden\">Recherche</button>
    </p>
</form>
";
        // line 22
        if (array_key_exists("permanentInstructions", $context)) {
            // line 23
            echo "
    ";
            // line 24
            $this->loadTemplate("InstructionBundle:instruction:listPermanente.html.twig", "InstructionBundle:Instruction:list.html.twig", 24)->display($context);
        }
        // line 26
        echo "
";
        // line 28
        if (array_key_exists("ponctualInstructions", $context)) {
            // line 29
            echo "
    ";
            // line 30
            $this->loadTemplate("InstructionBundle:instruction:listPunctual.html.twig", "InstructionBundle:Instruction:list.html.twig", 30)->display($context);
        }
        // line 33
        if (array_key_exists("periodiqueInstructions", $context)) {
            // line 34
            echo "
    ";
            // line 35
            $this->loadTemplate("InstructionBundle:instruction:listPeriodique.html.twig", "InstructionBundle:Instruction:list.html.twig", 35)->display($context);
        }
        // line 37
        echo "


<script>

    function rehcerche()
    {
        var type = \$('#type').val();
        var archived = \$('#typeArchived').val();// on sécurise les données
        \$(\"#recherche\").click();
    }
    function refreche() {
        var aa;
        valeur = \$('#list').val();
        \$.ajax({
            url: '";
        // line 52
        echo $this->env->getExtension('routing')->getPath("instruction_list_permanente");
        echo "', // on donne l'URL du fichier de traitement
            type: \"get\", // la requête est de type POST
            data: \"type=\" + valeur, // et on envoie nos données
            success: function (response) {
                \$(\"#recherche\").submit();
    ";
        // line 58
        echo "                    a = response;
                }
            });
        }

        //archuvate dialogue confirmation
        function openArchivateConfirm(id) {
            \$.modal.confirm(\"voulez vous vraiment archivée l'instruction ?\", function ()
            {
                archivateInstruction(id);
            }, function ()
            {

            });
        }
              // archivate instruction methode
              
             function archivateInstruction(id){
                 var idDelete=id;
                 console.log('befor sent ');
        \$.ajax({
            url: 'http://localhost/monagenda/web/app_dev.php/Instruction/instruction/archivate/'+id, // on donne l'URL du fichier de traitement
            type: \"get\", // la requête est de type get
            success: function (response) {
               console.log('success');
                \$(\"#\" + idDelete).remove();
                }
            });
                  
              }
              
              
        function openDeleteConfirm(id)
        {
            \$.modal.confirm('voulez vous vraiment supprimer ?', function ()
            {
                deleteLigne(id);
    ";
        // line 96
        echo "            }, function ()
            {

            });
        }
        ;

        function deleteLigne(id) {
            var idd = id;
            console.log(id);
            \$.ajax({
                url: '";
        // line 107
        echo $this->env->getExtension('routing')->getPath("instruction_delete");
        echo "', // on donne l'URL du fichier de traitement
                type: \"get\", // la requête est de type POST
                data: 'id=' + idd, // et on envoie nos données
                success: function (data) {
                    \$(\"#\" + idd).remove();
                    console.log(\"data.id\");
                }
            });

        }



        // Call template init (optional, but faster if called manually)


        // Table sort - DataTables
        var table = \$('#sorting-advanced');
        table.dataTable({
            'aoColumnDefs': [
                {'bSortable': false, 'aTargets': [0, 5]}
            ],
            'sPaginationType': 'full_numbers',
            'sDom': '<\"dataTables_header\"lfr>t<\"dataTables_footer\"ip>',
            'fnInitComplete': function (oSettings)
            {
                // Style length select
                table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                tableStyled = true;
            }
        });

        // Table sort - styled
        \$('#sorting-example1').tablesorter({
            headers: {
                0: {sorter: false},
                5: {sorter: false}
            }
        }).on('click', 'tbody td', function (event)
        {
            // Do not process if something else has been clicked
            if (event.target !== this)
            {
                return;
            }

            var tr = \$(this).parent(),
                    row = tr.next('.row-drop'),
                    rows;

            // If click on a special row
            if (tr.hasClass('row-drop'))
            {
                return;
            }

            // If there is already a special row
            if (row.length > 0)
            {
                // Un-style row
                tr.children().removeClass('anthracite-gradient glossy');

                // Remove row
                row.remove();

                return;
            }

            // Remove existing special rows
            rows = tr.siblings('.row-drop');
            if (rows.length > 0)
            {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }

            // Style row
            tr.children().addClass('anthracite-gradient glossy');
            // Add fake row
            \$('<tr class=\"row-drop\">' +
                    '<td colspan=\"' + tr.children().length + '\">' +
                    '<div class=\"float-right\">' +
                    '<button type=\"submit\" class=\"button glossy mid-margin-right\">' +
                    '<span class=\"button-icon\"><span class=\"icon-mail\"></span></span>' +
                    'Send mail' +
                    '</button>' +
                    '<button type=\"submit\" class=\"button glossy\">' +
                    '<span class=\"button-icon red-gradient\"><span class=\"icon-cross\"></span></span>' +
                    'Remove' +
                    '</button>' +
                    '</div>' +
                    '<strong>Name:</strong> John Doe<br>' +
                    '<strong>Account:</strong> admin<br>' +
                    '<strong>Last connect:</strong> 05-07-2011<br>' +
                    '<strong>Email:</strong> john@doe.com' +
                    '</td>' +
                    '</tr>').insertAfter(tr);

        }).on('sortStart', function ()
        {
            var rows = \$(this).find('.row-drop');
            if (rows.length > 0)
            {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');
                // Remove rows
                rows.remove();
            }
        });
        // Table sort - simple
        \$('#sorting-example2').tablesorter({
            headers: {
                5: {sorter: false}
            }
        });


</script>
";
    }

    public function getTemplateName()
    {
        return "InstructionBundle:Instruction:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  165 => 107,  152 => 96,  113 => 58,  105 => 52,  88 => 37,  85 => 35,  82 => 34,  80 => 33,  77 => 30,  74 => 29,  72 => 28,  69 => 26,  66 => 24,  63 => 23,  61 => 22,  45 => 8,  39 => 6,  37 => 5,  34 => 4,  29 => 2,  11 => 1,);
    }
}
