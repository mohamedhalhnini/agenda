<?php

/* RendezVousBundle:Default:updateRendezVous.html.twig */
class __TwigTemplate_e9f4d53ac217587ffbc1a7e838d45b5fa39f4fa0a775878b02c59b97904df781 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "RendezVousBundle:Default:updateRendezVous.html.twig", 1);
        $this->blocks = array(
            'mesScript' => array($this, 'block_mesScript'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_mesScript($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        // line 4
        echo "   ";
        // line 5
        echo "
    ";
    }

    // line 8
    public function block_content($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"with-padding\">

    <form action=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("update", array("id" => $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "id", array()))), "html", null, true);
        echo "\" method=\"post\">
    <table class=\"simple-table responsive-table responsive-table-on\" >

    <tbody>
    <tr>
        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">John Doe</td>
        <td >
            <select name=\"libelle\" class=\"select expandable-list\" style=\"width:40em\" >
                <option >--Libellés personnalisés--</option>
                <option value=\"libelle 1\" >libelle 1</option>
                <option value=\"libelle 2\" >libelle 2</option>
                <option value=\"libelle 3\" >libelle 3</option>

            </select><br><br>
            <select name=\"entete\" class=\"select expandable-list\" style=\"width:40em\" >
                <option  >--Entetes--</option>
                <option value=\"entete 1\" >entete 1</option>
                <option value=\"entete 2\" >entete 2</option>
                <option value=\"entete 3\" >entete 3</option>
            </select><br><br>
            <select name=\"typeRdv\" id=\"mySelect\" class=\"blabla select expandable-list\" style=\"width:40em\" >

                <option >--Rendez-vous types--</option>
               ";
        // line 34
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["rdvTypes"]) ? $context["rdvTypes"] : $this->getContext($context, "rdvTypes")));
        foreach ($context['_seq'] as $context["_key"] => $context["rdvType"]) {
            // line 35
            echo "                <option";
            if (($this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "rendezVousType", array()), "id", array()) == $this->getAttribute($context["rdvType"], "id", array()))) {
                echo " selected=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "rendezVousType", array()), "libelle", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "rendezVousType", array()), "dureee", array()), "H:i"), "html", null, true);
                echo "\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["rdvType"], "id", array()), "html", null, true);
                echo "\"";
            } else {
                echo "value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["rdvType"], "id", array()), "html", null, true);
                echo "\"";
            }
            echo " >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["rdvType"], "libelle", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["rdvType"], "dureee", array()), "H:i"), "html", null, true);
            echo "</option>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rdvType'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "            </select><br>
        </td>
    </tr>
    <tr>
        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">clients féquents</td>
        <td>
            <p>

                                <input type=\"radio\" name=\"radio1\" id=\"debut\" value=\"contient\" class=\"radio\">Début <label for=\"gender_female\"></label>
                                <input type=\"radio\" name=\"radio1\" id=\"contient\" value=\"contient\" class=\"radio\">Contient <label for=\"gender_female\"></label>


                                <!--button-->

                                <button type=\"submit\" class=\"button icon-plus blue-gradient\">Ajout</button>
                                <button type=\"submit\" class=\"button icon-download blue-gradient\">Histo</button>
                                <button type=\"submit\" class=\"button icon-new-tab blue-gradient\">Notes</button>
                                <button type=\"submit\" class=\"button icon-new-tab blue-gradient\">Nouv</button>
                                <button type=\"submit\" class=\"button icon-pencil blue-gradient\">Motif</button>
                            </p>
<p>
                <div class=\"row\">
                    ";
        // line 60
        echo "                    <select name=\"client\" class=\"select expandable-list\" style=\"width:40em\" >
                        <option >--select--</option>
                        ";
        // line 62
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clients"]) ? $context["clients"] : $this->getContext($context, "clients")));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 63
            echo "
                            <option ";
            // line 64
            if (($this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "client", array()), "id", array()) == $this->getAttribute($context["client"], "id", array()))) {
                echo " selected='";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "client", array()), "nom", array()), "html", null, true);
                echo "' value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "id", array()), "html", null, true);
                echo "\" ";
            } else {
                echo " value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "id", array()), "html", null, true);
                echo "\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "nom", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "prenom", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "telpro", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "email", array()), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "                      ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 50));
        foreach ($context['_seq'] as $context["_key"] => $context["clt"]) {
            // line 67
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, $context["clt"], "html", null, true);
            echo "\">client-";
            echo twig_escape_filter($this->env, $context["clt"], "html", null, true);
            echo "</option>
                          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "                    </select>
                </div>
 </p>
        </td>

    </tr>
    <tr>
        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Détail</td>
        <td>
            <div class=\"row\">
                <input type=\"text\" class=\"input\" style=\"width:40em\"><br><br>
           <textarea name=\"autoexpanding\" id=\"autoexpanding\" class=\"input full-width autoexpanding\" style=\"overflow: hidden; resize: none; height: 124px;\">";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "detail", array()), "html", null, true);
        echo "</textarea>

               
            </div>
            <br>

        </td>

    </tr>
    <tr>
        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Disponibilité</td>
        <td>
            <div class=\"columns\" class=\"center\">
                <div class=\"three-columns\">
                    <button type=\"submit\" class=\" button blue-gradient\" value=\"Ajout\" >Prochaine plage</button> </div>
                <div class=\"one-column\"><strong>ou</strong></div>
                <div class=\"two-columns\"><button type=\"submit\" class=\"button blue-gradient three-column\" value=\"Ajout\" >Rechrcher</button></div>
            </div>
            <br>

        </td>

    </tr>
    <tr>
        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Date et Heure</td>
        <td>
            <div>
                <p class=\"button-height\">
                    Date du rendez-vous :
                    ";
        // line 110
        echo "                    <span class=\"input\">
                    <span class=\"icon-calendar\"></span>
                    <input type=\"text\" name=\"datepicker1\" id=\"rdvdate\" class=\"input-unstyled datepicker gldp-el\" value=\"";
        // line 112
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "datedebut", array()), "d-m-Y"), "html", null, true);
        echo "\" gldp-id=\"gldp-4634471992\">
                    </span>

                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"jrEntiere\" id=\"check-1\" value=\"1\"> journée entière
                </p>
            </div>
            <div >

                <p class=\"button-height\">Début a :
                    <input type=\"text\" class=\"input\" style=\"width:40px\"   id=\"time1\" name=\"timeDebut\" value=\"";
        // line 121
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "datedebut", array()), "H:i"), "html", null, true);
        echo "\">
                    Termine à :
                    <input type=\"text\" class=\"input\" style=\"width:40px\"  id=\"time2\" name=\"timeFin\" value=\"";
        // line 123
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "end", array()), "H:i"), "html", null, true);
        echo "\">

                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"checked[]\" id=\"check-1\" value=\"1\"> Durée alternative
                    <button type=\"submit\" class=\" button blue-gradient one-columns\" value=\"Ajout\" >voir la légende</button>
                </p>
            </div>
            <br>
        </td>
    </tr>
    <tr>
        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\" >Couleur</td>
        <td>
            <input name=\"color\" type=\"color\" width=\"500px\" id=\"html5colorpicker\" class=\"form-control\" onchange=\"clickColor(0, -1, -1, 5)\" value=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "couleur", array()), "html", null, true);
        echo "\" style=\"width:25em\">
            <button type=\"submit\" class=\" button blue-gradient three-columns mid-margin-left\" value=\"Ajout\">Aparence de la note</button>
        </td>
    </tr>
    <tr>
        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Partage</td>
        <td>
            <p>




                <input type=\"radio\" name=\"partage\"  id=\"radio_1\" value=\"1\" class=\"radio\" ";
        // line 147
        if (($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "partage", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo " > <label for=\"gender_male\">Note public(note détaillée dans le partage de planning)</label>

            </p>
            <p>

                <input type=\"radio\" name=\"partage\" id=\"radio_2\" value=\"0\" class=\"radio\" ";
        // line 152
        if (($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "partage", array()) == 0)) {
            echo "checked=\"checked\" ";
        }
        echo " > <label for=\"gender_female\">Note privée(mention \"Occupé\" dans le partage de planning)</label>

            </p>
        </td>

    </tr>
    <tr>

        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Alerter</td>
        <td>
          

                <div class=\"columns\">
                <div class=\"two-columns\">

                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"alert[]\" id=\"check-1\" value=\"1\"  ";
        // line 167
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "myalert", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            if (($context["a"] == 1)) {
                echo " checked=\"checked\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "> par mail</div>
                <div class=\"two-columns\"> <input class=\"styled checkbox\" type=\"checkbox\" name=\"alert[]\" id=\"check-2\" value=\"2\" ";
        // line 168
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "myalert", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            if (($context["a"] == 2)) {
                echo " checked=\"checked\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " > par SMS</div>
                <div class=\"two-columns\"> <input class=\"styled checkbox\" type=\"checkbox\" name=\"alert[]\" id=\"check-3\" value=\"3\"";
        // line 169
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "myalert", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            if (($context["a"] == 3)) {
                echo " checked=\"checked\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "> par FAX</div>

            </div>  
           

            <div class=\"columns\">
                <div class=\"two-columns\">le client fréquent : </div>
                <div class=\"two-columns\"> <input class=\"styled checkbox\" type=\"checkbox\" name=\"alertCli[]\" id=\"check-4\" value=\"1\"  ";
        // line 176
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "clientalert", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            if (($context["b"] == 1)) {
                echo " checked=\"checked\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "> par mail</div>
                <div class=\"two-columns\">   <input class=\"styled checkbox\" type=\"checkbox\" name=\"alertCli[]\" id=\"check-5\" value=\"2\"  ";
        // line 177
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "clientalert", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            if (($context["b"] == 2)) {
                echo " checked=\"checked\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "> par SMS</div>

            </div>
           
        </td>

    </tr>
    <tr>
        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Etat</td>
        <td>
            <p>
                Considérer le RDV comme transmis :
                <input type=\"radio\" name=\"etat\" id=\"radio_6\" value=\"1\" class=\"radio\" ";
        // line 189
        if (($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "etat", array()) == 1)) {
            echo "checked=\"checked\" ";
        }
        echo "> <label for=\"auto\">Auto   </label>
                <input type=\"radio\" name=\"etat\" id=\"radio_7\" value=\"0\" class=\"radio\" ";
        // line 190
        if (($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "etat", array()) == 0)) {
            echo "checked=\"checked\" ";
        }
        echo "> <label for=\"oui\">Oui</label>
            </p>
        </td>

    </tr>
    <tr>
        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Rappel</td>

        <td>
            <p>
                <input type=\"radio\" name=\"rappel\" id=\"pasRappel\" value=\"0\" class=\"radio\"  > <label for=\"pas de rappel\">Pas de rappel </label>
            </p>
            <p> <div class=\"columns\">
                <div class=\"one-columns\">
                    <input type=\"radio\" name=\"rappel\" id=\"ouiRappel\" value=\"1\" class=\"radio\" > <label for=\"rappel\">Rappel</label>
                </div>
                <div class=\"one-columns\">
                </div>
                <div class=\"one-columns\">
                                            <span class=\"number input margin-right\">
                                                <button type=\"button\" class=\"button number-down\">-</button>
                                                <input name=\"myRappel\" type=\"text\" value=\"5\" size=\"3\" class=\"input-unstyled\">
                                                <button type=\"button\" class=\"button number-up\">+</button>
                                            </span>
                </div>
                <div class=\"one-columns\">
                    <select class=\"select expandable-list\" style=\"width:10em\" > à l'avance
                        <option value=\"1\" >minute(s)</option>
                        <option value=\"2\" >heure(s)</option>

                    </select>
                </div>
                <div class=\"one-columns\">

                </div>
                <div class=\"one-columns\">
                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"rap[]\" id=\"interne\" value=\"1\"";
        // line 226
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "myRappel", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            if (($context["b"] == 1)) {
                echo " checked=\"checked\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "> interne
                </div>
                <div class=\"one-columns\">

                </div>
                <div class=\"one-columns\">
                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"rap[]\" id=\"mail\" value=\"2\"";
        // line 232
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "myRappel", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            if (($context["b"] == 2)) {
                echo " checked=\"checked\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "> par mail
                </div>
                <div class=\"one-columns\">
                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"rap[]\" id=\"sms\" value=\"3\" ";
        // line 235
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "myRappel", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            if (($context["b"] == 3)) {
                echo " checked=\"checked\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "> par SMS
                </div>F
            </div></p>
        </td>

    </tr>
    <tr>
        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Rappel pour le client fréquent</td>
        <td>
            <p>
                <input type=\"radio\" name=\"rapCli\" id=\"radio_1\" value=\"0\" class=\"radio\" ";
        // line 245
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "clientRappel", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            if (($context["b"] == "")) {
                echo "checked=\"checked\" ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " > <label for=\"pas de rappel\">Pas de rappel </label>
            </p>
            <p>   <div class=\"columns\">
                <div class=\"one-columns\">
                    <input type=\"radio\" name=\"rapCli\" id=\"radio_2\" value=\"1\" class=\"radio\"";
        // line 249
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "clientRappel", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            if (($context["b"] != "")) {
                echo "checked=\"checked\" ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "  > <label for=\"rappel\">Rappel</label>
                </div>
                <div class=\"one-columns\">
                </div>
                <div class=\"one-columns\">
                                            <span class=\"number input margin-right\">
                                                <button type=\"button\" class=\"button number-down\">-</button>
                                                <input name=\"rappelCli\" type=\"text\" value=\"5\" size=\"3\" class=\"input-unstyled\">
                                                <button type=\"button\" class=\"button number-up\">+</button>
                                            </span>
                </div>
                <div class=\"one-columns\">
                    <select class=\"select expandable-list\" style=\"width:10em\" > à l'avance
                        <option value=\"1\" >minute(s)</option>
                        <option value=\"2\" >heure(s)</option>

                    </select>
                </div>

                <div class=\"one-columns\">

                </div>
                <div class=\"one-columns\">
                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"cli[]\" id=\"check-1\" value=\"1\" ";
        // line 272
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "clientRappel", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            if (($context["b"] == 1)) {
                echo " checked=\"checked\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "> par mail
                </div>
                <div class=\"one-columns\">
                    <input class=\"styled checkbox\" type=\"checkbox\" name=\"cli[]\" id=\"check-1\" value=\"2\"";
        // line 275
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "clientRappel", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            if (($context["b"] == 2)) {
                echo " checked=\"checked\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "> par SMS
                </div>
            </div></p>
        </td>
    </tr>
    <tr>
        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Recopie </td>
        <td>
            <div class=\"columns\">
                <div class=\"two-column\"> <input type=\"radio\" name=\"recopie\" id=\"recopie1\" value=\"0\" class=\"radio\" ";
        // line 284
        if (($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "recopie", array()) == 0)) {
            echo "checked=\"checked\" ";
        }
        echo " > <label for=\"NON\">Non </label>
                </div>
                <div class=\"one-columns\">
                </div>
                <div class=\"one-columns\">
                    <input type=\"radio\" name=\"recopie\" id=\"recopie2\" value=\"1\" class=\"radio\" ";
        // line 289
        if (($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "recopie", array()) == 1)) {
            echo "checked=\"checked\" ";
        }
        echo " > <label for=\"oui\">Oui,conserver les informations</label>
                </div>

            </div>
        </td>
    </tr>
   
   
  
    <tr>
        <td style=\"width:14em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\"><strong>Avancé</strong></td>
        <td>
            <div class=\"columns\">
                <div class=\"four-columns\">

                    <a href=\"#\" >Périodicité </a>


                </div>
            </div>
        </td>
    </tr>
    </tbody>

    </table>

    <button type=\"submit\" class=\"button blue-gradient\" >Modifier</button>
    <h4 class=\"relative\">
        Iframe
        <button type=\"button\" class=\"button compact absolute-right\" onclick=\"openIframe()\">Test</button>
    </h4>
    <button type=\"button\" class=\"button blue-gradient glossy\" onclick=\"openComplexModal()\">Complex</button>



    </form>

    </center>
    <h4>Styled table with simple sorting</h4>

    <p>Simple sorting and manual controls if you prefer to handle the table output server-side:</p>

    <p><b>Tip:</b> try clicking on a row to show an extra line style!</p>


    </div>

";
    }

    public function getTemplateName()
    {
        return "RendezVousBundle:Default:updateRendezVous.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  587 => 289,  577 => 284,  556 => 275,  541 => 272,  506 => 249,  490 => 245,  468 => 235,  453 => 232,  435 => 226,  394 => 190,  388 => 189,  364 => 177,  351 => 176,  332 => 169,  319 => 168,  306 => 167,  286 => 152,  276 => 147,  261 => 135,  246 => 123,  241 => 121,  229 => 112,  225 => 110,  193 => 80,  180 => 69,  169 => 67,  164 => 66,  138 => 64,  135 => 63,  131 => 62,  127 => 60,  103 => 37,  78 => 35,  74 => 34,  48 => 11,  44 => 9,  41 => 8,  36 => 5,  34 => 4,  32 => 3,  29 => 2,  11 => 1,);
    }
}
