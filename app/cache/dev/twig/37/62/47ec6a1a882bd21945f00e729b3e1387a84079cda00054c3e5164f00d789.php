<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_376247ec6a1a882bd21945f00e729b3e1387a84079cda00054c3e5164f00d789 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::login_base.html.twig", "FOSUserBundle:Resetting:request.html.twig", 2);
        $this->blocks = array(
            'fos_user_content_password' => array($this, 'block_fos_user_content_password'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::login_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_fos_user_content_password($context, array $blocks = array())
    {
        // line 5
        $this->loadTemplate("FOSUserBundle:Resetting:request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 5)->display($context);
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 5,  28 => 4,  11 => 2,);
    }
}
