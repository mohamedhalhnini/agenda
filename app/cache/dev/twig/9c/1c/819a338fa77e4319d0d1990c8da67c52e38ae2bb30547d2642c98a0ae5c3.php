<?php

/* InstructionBundle:Client:create.html.twig */
class __TwigTemplate_9c1c819a338fa77e4319d0d1990c8da67c52e38ae2bb30547d2642c98a0ae5c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("InstructionBundle:Client:layout.html.twig", "InstructionBundle:Client:create.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "InstructionBundle:Client:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "     <form name=\"form1\" action=\"";
        echo $this->env->getExtension('routing')->getPath("client_create");
        echo "\" method=\"POST\">
    <table class=\"table simple-table responsive-table responsive-table-on w \" style=\"  height: 10em;\" >
      ";
        // line 6
        echo "        <tfoot>
            <tr>
                <td colspan=\"2\"  class=\"align-center\">
                    <div class=\"columns\">
                        <div class=\"three-columns\">
                        </div>
                        <div class=\"two-columns\">
                            <button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" name=\"enregistrer\" id=\"submit\">Enregistrer</button>
                        </div>
                        <div class=\"tow-columns\">
                            <button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" name=\"annuler\">annuler</button>
                        </div>
                    </div>
                </td>
            </tr>
        </tfoot>             
        <tbody>
            <tr>
                <td>Civilité </td>
                <td>
                    <div class=\"three-columns\">
                        <select class=\"select expandable-list\" style=\"width:15em\" name=\"civilite\" >
                            <option value=\"0\" >--Civilité--</option>
                            <option value=\"1\" >M</option>
                            <option value=\"2\" >Mme</option>
                            <option value=\"3\" >Mlle</option>
                            <option value=\"4\" >Enfant</option>
                            <option value=\"5\" >Dr</option>
                            <option value=\"6\" >Pr</option>
                            <option value=\"7\" >Me</option>
                        </select>
                    </div>      
                </td>
            </tr>
            <tr>
                <td>Nom</td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"nom\">
                </td>
            </tr>
            <tr>
                <td>Prénom </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"prenom\">
                </td>



            </tr> <tr>
                <td >Tél. Domicile </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" nam=\"telDomicile\">
                </td>
            </tr>
            <tr>
                <td >Tél. Mob

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telMob\">
                </td>
            </tr>
            <tr>
                <td >Té. Pro </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telPro\">
                </td>
            </tr>
            <tr>
                <td>Tél. Box </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telBox\">
                </td>
            </tr>
            <tr>
                <td> Adresse </td>

                <td>
                    <textarea style=\"width: 20em\"class=\"input\" name=\"adresse\"></textarea>
                </td>
            </tr>

            <tr>
                <td >Email </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"email\">
                </td>
            </tr>

            <tr>
                <td >Note  </td>

                <td>
                    <textarea style=\"width: 20em\"class=\"input\" name=\"note\"></textarea>
                </td>
            </tr>


            <tr>
                <td  >Mot de passe Web :</td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"password\">
                    <button type=\"submit\" class=\"button  blue-gradient\" name=\"Ajout\" name=\"generer\">Générer</button>

                </td>


        </tbody>    


    </table>
</form>
";
    }

    public function getTemplateName()
    {
        return "InstructionBundle:Client:create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 6,  31 => 3,  28 => 2,  11 => 1,);
    }
}
