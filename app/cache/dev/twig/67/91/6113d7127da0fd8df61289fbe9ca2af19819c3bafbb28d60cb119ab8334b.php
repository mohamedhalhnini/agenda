<?php

/* AgendaMemoBundle:Memo:index.html.twig */
class __TwigTemplate_67916113d7127da0fd8df61289fbe9ca2af19819c3bafbb28d60cb119ab8334b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AgendaMemoBundle:Memo:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'my_script' => array($this, 'block_my_script'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "Ajouter Memo
";
    }

    // line 5
    public function block_my_script($context, array $blocks = array())
    {
        // line 6
        $this->displayParentBlock("my_script", $context, $blocks);
        echo "
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("asset/js/developr.modal.js"), "html", null, true);
        echo "\"></script>
<script>
    /*function openConfirm()
     {
     \$.modal.confirm('Confirmez-vous vouloir supprimer ce mémo ?', function()
     {
     \$.modal.alert('La supression est effectuée !');

     }, function()
     {
     \$.modal.alert('La supression est annulée !');
     });
     };*/
    function periodiciteshow() {

        if (\$(\"#annuler\").is(\":hidden\")) {
            \$(\"#annuler\").show(\"slow\");
        } else {
            \$(\"#annuler\").hide(\"slow\");
        }
    }
    function post_en_url(url, parametres) {
//Création dynamique du formulaire
        var form = document.createElement(form);
        form.setAttribute(method, POST);
        form.setAttribute(action, url);
//Ajout des paramètres sous forme de champs cachés
        for(var cle in parametres) {
            if(parametres.hasOwnProperty(cle)) {
                var champCache = document.createElement(input);
                champCache.setAttribute(type, hidden);
                champCache.setAttribute(name, cle);
                champCache.setAttribute(value, parametres[cle]);
                form.appendChild(champCache);
            }
        }
//Ajout du formulaire à la page et soumission du formulaire
        document.body.appendChild(form);
        form.submit();
    }
    function openModal()
    {
        \$.modal({
            title: 'Ajout du mémo',
            content: '<h4 class=\"green-gradient glossy\">Le mémo est bien ajouté</h4>',
            width: 300,
            resizable: false,
            scrolling: false,
            actions: {
                'Fermer' : {
                    color: 'red',
                    click: function(win) { win.closeModal();}
                }
            },
            buttons: {
                'Annuler': {
                    classes:\t'blue-gradient glossy',
                    click:\t\tfunction(win) { win.closeModal();}
                }
            }
        });
    }
    function generer_password(id) {
        pwIsExist(id);
        var ok = 'azertyupqsdfghjkmwxcvbn23456789AZERTYUPQSDFGHJKMWXCVBN';
        var pass = '';
        longueur = 8;
        for(i=0;i<longueur;i++){
            var wpos = Math.round(Math.random()*ok.length);
            pass+=ok.substring(wpos,wpos+1);
        }
        document.getElementById(id).value = pass;
    }
    function openConfirm()
    {
        \$.modal.confirm('Challenge accepted?', function()
        {
            \$.modal.alert('Me gusta!');

        }, function()
        {
            \$.modal.alert('Meh.');
        });
    };
    function openLoadingModal()
    {
        var timeout;

        \$.modal({
            contentAlign: 'center',
            width: 240,
            title: 'Loading',
            content: '<div style=\"line-height: 25px; padding: 0 0 10px\"><span id=\"modal-status\">Envoie d\\'Email en cours...</span><br><span id=\"modal-progress\">0%</span></div>',
            buttons: {},
            scrolling: false,
            actions: {
                'Cancel': {
                    color:\t'red',
                    click:\tfunction(win) { win.closeModal(); }
                }
            },
            onOpen: function()
            {
                // Progress bar
                var progress = \$('#modal-progress').progress(100, {
                            size: 200,
                            style: 'large',
                            barClasses: ['anthracite-gradient', 'glossy'],
                            stripes: true,
                            darkStripes: false,
                            showValue: false
                        }),

                // Loading state
                        loaded = 0,

                // Window
                        win = \$(this),

                // Status text
                        status = \$('#modal-status'),

                // Function to simulate loading
                        simulateLoading = function()
                        {
                            ++loaded;
                            progress.setProgressValue(loaded+'%', true);
                            if (loaded === 100)
                            {
                                progress.hideProgressStripes().changeProgressBarColor('green-gradient');
                                status.text('Done!');
                                /*win.getModalContentBlock().message('Content loaded!', {
                                 classes: ['green-gradient', 'align-center'],
                                 arrow: 'bottom'
                                 });*/
                                setTimeout(function() { win.closeModal(); }, 1500);
                            }
                            else
                            {
                                if (loaded === 1)
                                {
                                    status.text('Loading data...');
                                    progress.changeProgressBarColor('blue-gradient');
                                }
                                else if (loaded === 25)
                                {
                                    status.text('Loading assets (1/3)...');
                                }
                                else if (loaded === 45)
                                {
                                    status.text('Loading assets (2/3)...');
                                }
                                else if (loaded === 85)
                                {
                                    status.text('Loading assets (3/3)...');
                                }
                                else if (loaded === 92)
                                {
                                    status.text('Entrain d\\'envoyer...');
                                }
                                timeout = setTimeout(simulateLoading, 50);
                            }
                        };

                // Start
                timeout = setTimeout(simulateLoading, 2000);
            },
            onClose: function()
            {
                // Stop simulated loading if needed
                clearTimeout(timeout);
            }
        });
    };
    function affecterDateAujourdhui(id){
        var d = new Date();
        var mois = d.getMonth()+1;
        var now = d.getDay()+'/'+ mois +'/'+d.getFullYear();
        document.getElementById(id).value = now;
    }
    function verif_champPw(id)
    {
        if (document.getElementById(id).value == \"\")
        {   if(!confirm(\"Aucun mot de passe n'a encore été affecté au client fréquent. Cliquer sur Annuler pour revenir en spécifier un ou sur OK pour poursuivre\",
                        {width:300, okLabel: \"Ok\",
                            width:300, cancelLabel: \"Annuler\",
                            buttonClass: \"buttons\",
                            cancel:function(win){debug(\"cancel confirm panel\");} ,
                            ok:function(win) {debug(\"cancel confirm panel\"); return true;}
                        })
        )
        {
            alert('success');
        }
        }else{
            return false;
        }
        return true;
    }
    function pwIsExist(id)
    {
        if (document.getElementById(id).value != \"\")
        {   if(!confirm(\"Une valeur est déjà présente pour le mot de passe , souhaitez-vous \\n la remplacer une autre ?\",
                        {width:300, okLabel: \"Ok\",
                            width:300, cancelLabel: \"Annuler\",
                            buttonClass: \"buttons\",
                            cancel:function(win){debug(\"cancel confirm panel\");} ,
                            ok:function(win) {debug(\"cancel confirm panel\"); return true;}
                        })
        )
        {
        }
        }else{
            return false;
        }
        return true;
    }

    function openAlert()
    {
        \$.modal.alert('This is an alert message');
    };

    function openComplexModal()
    {
        \$.modal({
            content: \"<p>Aucun mot de passe n'a encore été affecté au client fréquent.\\n Cliquer sur OK pour revenir en spécifier un \\n ou sur Annuler pour poursuivre</p>\",
            title: 'Générer PDF',
            width: 300,
            resizable: false,
            scrolling: false,
            actions: {
                'Fermer' : {
                    color: 'red',
                    click: function(win) { win.closeModal(); }
                }
            },
            buttons: {
                'Annuler': {
                    classes:\t'red-gradient glossy',
                    click: function(result) {
                        window.event.onload = openAlert();
                    }
                },
                'Ok': {
                    classes:\t'blue-gradient glossy',
                    click: function(win) { win.closeModal(); }
                }
            },
            buttonsLowPadding: true
        });
    };
    \$(document).ready(function(){
        \$(\"#form\").validationEngine();
    });
        var email = document.getElementById(\"email\").innerHTML;
        var pw = document.getElementById(\"pw\").innerHTML;
</script>
<script>
    /*var liste1;
    var valeur1;
    liste1 = document.getElementById(\"agenda_memobundle_general_type_clientFrequent\");
    valeur1 = liste1.options[liste1.selectedIndex].text;*/
    function recup1(){
        var valeur1;
       return valeur1 = document.getElementById('agenda_memobundle_general_type_clientFrequent').options[document.getElementById('agenda_memobundle_general_type_clientFrequent').selectedIndex].value;
        document.getElementById(\"agenda_memobundle_general_type_detail\").value = valeur1;

    }
</script>
<script>
    /*
    var liste2;
    var valeur2;
    liste2 = document.getElementById(\"agenda_memobundle_general_type_phraseUsuelle\");
    valeur2 = liste2.options[liste2.selectedIndex].text;*/
    function recup2(){
       var valeur2;
       valeur2 = document.getElementById('agenda_memobundle_general_type_phraseUsuelle').options[document.getElementById('agenda_memobundle_general_type_phraseUsuelle').selectedIndex].text;
        document.getElementById(\"agenda_memobundle_general_type_detail\").value = valeur2;
    }
</script>
    <script>
        // Demo Iframe loading
        function openIframe()
        {
            \$.modal({
                title: '<strong>Client fréquent</strong>',
                content: ''
                + '<table class=\"table simple-table responsive-table responsive-table-on w \" style=\"  height: 10em;\">'
                + '<tfoot>'
                + '<tr>'
                + '<td colspan=\"2\"  class=\"align-center\">'
                + '<div class=\"columns\">'
                + '<div class=\"three-columns\">'
                + '</div>'
                + '<div class=\"two-columns\">'
                + '<button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" name=\"iframClient\" onclick=\"saveClient()\" id=\"iframClient\">Enregistrer</button>'
                + '</div>'
                + ' <div class=\"tow-columns\">'
                + '   <button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" name=\"annuler\">annuler</button>'
                + ' </div>'
                + ' </div>'
                + ' </td>'
                + ' </tr>'
                + ' </tfoot>'
                + ' <tbody>'
                + '   <tr>'
                + ' <td>Civilité </td>'
                + ' <td>'
                + '    <div class=\"three-columns\">'
                + '       <select class=\"select blue-gradient expandable-list\" style=\"width:15em\" name=\"civilite\" id=\"civilite\">'
                + '     <option value=\"0\" >--Civilité--</option>'
                + '     <option value=\"1\" >M</option>'
                + '     <option value=\"2\" >Mme</option>'
                + '     <option value=\"3\" >Mlle</option>'
                + '     <option value=\"4\" >Enfant</option>'
                + '     <option value=\"5\" >Dr</option>'
                + '     <option value=\"6\" >Pr</option>'
                + '     <option value=\"7\" >Me</option>'
                + ' </select>'
                + ' </div>'
                + '            </td>'
                + '        </tr>'
                + '        <tr>'
                + '            <td>Nom</td>'
                + '                <td>'
                + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"nom\" id=\"nom\">'
                + '                </td>'
                + '            </tr>'
                + '            <tr>'
                + '                <td>Prénom </td>'
                + '                <td>'
                + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"prenom\" id=\"prenom\">'
                + '                </td>'
                + '            </tr> <tr>'
                + '                <td >Tél. Domicile </td>'
                + '                <td>'
                + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" nam=\"telDomicile\" id=\"tel\">'
                + '                </td>'
                + '            </tr>'
                + '            <tr>'
                + '                <td >Tél. Mob'
                + '                <td>'
                + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telMob\" id=\"telMob\">'
                + '                </td>'
                + '         </tr>'
                + '          <tr>'
                + '              <td >Té. Pro </td>'
                + '                <td>'
                + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telPro\" id=\"telPro\">'
                + '                </td>'
                + '            </tr>'
                + '            <tr>'
                + '              <td>Tél. Box </td>'
                + '              <td>'
                + '                  <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telBox\" id=\"telBox\">'
                + '            </td>'
                + '        </tr>'
                + '        <tr>'
                + '            <td> Adresse </td>'
                + '                <td>'
                + '                  <textarea style=\"width: 20em\"class=\"input\" name=\"adresse\" id=\"adresse\"></textarea>'
                + '              </td>'
                + '          </tr>'
                + '          <tr>'
                + '              <td >Email </td>'
                + '              <td>'
                + '                  <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"email\" id=\"email\">'
                + '              </td>'
                + '          </tr>'
                + '            <tr>'
                + '                <td >Note  </td>'
                + '                <td>'
                + '                    <textarea style=\"width: 20em\"class=\"input\" name=\"note\" id=\"note\"></textarea>'
                + '                </td>'
                + '            </tr>'
                + '            <tr>'
                + '                <td  >Mot de passe Web :</td>'
                + '                <td>'
                + '                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"password\" id=\"password\">'
                + '                    <button type=\"submit\" class=\"button  blue-gradient\"  onclick=generer_password(\"pw\") name=\"generer\" id=\"generer\">Générer</button>'
                + '                </td>'
                + '        </tbody> '
                + '    </table>',
                buttonsAlign: 'center',
                resizable: true,
                width: 600,
                height: 600
            });
        }
    </script>
    <script>
    function modifierClient()
    {
        // on sécurise les données
        var civilite = encodeURIComponent(\$('#civilite').val());
        var nom = encodeURIComponent(\$('#nommodif').val());
        var prenom = encodeURIComponent(\$('#prenommodif').val());
        var telDomicile = encodeURIComponent(\$('#telDomicilemodif').val());
        var telMob = encodeURIComponent(\$('#telMobmodif').val());
        var telPro = encodeURIComponent(\$('#telPromodif').val());
        var telBox = encodeURIComponent(\$('#telBoxmodif').val());
        var adresse = encodeURIComponent(\$('#adressemodif').val());
        var email = encodeURIComponent(\$('#emailmodif').val());
        var note = encodeURIComponent(\$('#notemodif').val());
        var password = encodeURIComponent(\$('#pwmodif').val());

            console.log('befor success');
            \$.ajax({
                url: 'http://localhost/monagenda_/web/app_dev.php/memo/client/edit/'+recup1(), // on donne l'URL du fichier de traitement
                type: \"POST\", // la requête est de type POST
                data: \"nom=\" + nom + \"&prenom=\" + prenom + \"&telDomicile=\" + telDomicile + \"&telMob=\" + telMob + \"&telPro=\" + telPro + \"&telBox=\" + telBox + \"&adresse=\" + adresse + \"&email=\" + email + \"&note=\" + note, // et on envoie nos données
                success: function (data) {
                    console.log('success');
                    console.log(data.nom);
                    \$('#agenda_memobundle_general_type_clientFrequent option[value=\"'+data.id +'\"]').remove();
                    \$('#agenda_memobundle_general_type_clientFrequent').append('<option value=\"'+data.id +'\" >' + data.nom + '</option>');
                    document.querySelector('#agenda_memobundle_general_type_clientFrequent [value=\"' + data.id + '\"]').selected = true;
                    \$('#agenda_memobundle_general_type_clientFrequent').change();

                    //var option = \$('<option></option>').attr(\"value\", \"option value\").text(\"Text\");
                    //\$(\"#agenda_memobundle_general_type_clientFrequent\").empty().append(option);

                    console.log('success fermer');
                }
            });
            \$('#modals').remove();
    }
        // Demo Iframe loading
        function saveClient()
        {
            // on sécurise les données
            var civilite = encodeURIComponent(\$('#civilite').val());
            var nom = encodeURIComponent(\$('#nom').val());
            var prenom = encodeURIComponent(\$('#prenom').val());
            var telDomicile = encodeURIComponent(\$('#tel').val());
            var telMob = encodeURIComponent(\$('#telMob').val());
            var telPro = encodeURIComponent(\$('#telPro').val());
            var telBox = encodeURIComponent(\$('#telBox').val());
            var adresse = encodeURIComponent(\$('#adresse').val());
            var email = encodeURIComponent(\$('#email').val());
            var note = encodeURIComponent(\$('#ntryote').val());
            var password = encodeURIComponent(\$('#password').val());
            var generer = encodeURIComponent(\$('#generer').val());

            if (nom != \"\") { // on vérifie que les variables ne sont pas vides
                console.log('befor success');
                \$.ajax({
                    url: '";
        // line 456
        echo $this->env->getExtension('routing')->getPath("agenda_client_create");
        echo "', // on donne l'URL du fichier de traitement
                    type: \"POST\", // la requête est de type POST
                    data: \"civilite=\" + civilite + \"&nom=\" + nom + \"&prenom=\" + prenom + \"&tel=\" + tel + \"&telMob=\" + telMob + \"&telPro=\" + telPro + \"&telBox=\" + telBox + \"&adresse=\" + adresse + \"&email=\" + email + \"&note=\" + note + \"&password=\" + password, // et on envoie nos données
                    success: function (data) {
                        console.log('success');
                        console.log(data.nom);
                        \$('#agenda_memobundle_general_type_clientFrequent').append('<option value=\"'+data.id +'\" >' + data.nom + '</option>');
                        document.querySelector('#agenda_memobundle_general_type_clientFrequent [value=\"' + data.id + '\"]').selected = true;
                        \$('#agenda_memobundle_general_type_clientFrequent').change();
                        console.log('success fermer');
                    }
                });
                \$('#modals').remove();
            }
        }
        function findClient()
        {
            // on sécurise les données
            //var civilite = encodeURIComponent(\$('#civilite').val());
            /*var nom = encodeURIComponent(\$('#nom').val());
            var prenom = encodeURIComponent(\$('#prenom').val());
            var telDomicile = encodeURIComponent(\$('#tel').val());
            var telMob = encodeURIComponent(\$('#telMob').val());
            var telPro = encodeURIComponent(\$('#telPro').val());
            var telBox = encodeURIComponent(\$('#telBox').val());
            var adresse = encodeURIComponent(\$('#adresse').val());
            var email = encodeURIComponent(\$('#email').val());
            var note = encodeURIComponent(\$('#ntryote').val());
            var password = encodeURIComponent(\$('#password').val());
            var generer = encodeURIComponent(\$('#generer').val());*/

            //if (nom != \"\") { // on vérifie que les variables ne sont pas vides
                console.log('befor success');
                \$.ajax({
                    url: 'http://localhost/monagenda_/web/app_dev.php/memo/client/edit/'+recup1(), // on donne l'URL du fichier de traitement
                    type: \"GET\", // la requête est de type POST
                    data: \"&id=\" +recup1(),// et on envoie nos données
                    success: function (data) {
                        console.log('success');
                        openIframe2();
                        console.log(data.nom);
                        document.getElementById(\"nommodif\").value =data.nom;
                        document.getElementById(\"prenommodif\").value =data.prenom;
                        document.getElementById(\"telDomicilemodif\").value =data.teldom;
                        document.getElementById(\"telMobmodif\").value =data.telmob;
                        document.getElementById(\"telPromodif\").value =data.telpro;
                        document.getElementById(\"telBoxmodif\").value =data.telbox;
                        document.getElementById(\"emailmodif\").value =data.email;
                        document.getElementById(\"notemodif\").value =data.note;
                        console.log('success fermer');

                    }
                });
                //\$('#modals').remove();
            //}
        }

    // Demo Iframe loading
    function pdfClient()
    {
        //verif_champPw(\"passwordmodif\");
        // on sécurise les données
        var nom = encodeURIComponent(\$('#nommodif').val());
        var email = encodeURIComponent(\$('#emailmodif').val());
        var pw = encodeURIComponent(\$('#passwordmodif').val());

            console.log('befor success');
            \$.ajax({
                url: 'http://localhost/monagenda_/web/app_dev.php/memo/client/pdf/'+recup1(), // on donne l'URL du fichier de traitement
                type: \"POST\", // la requête est de type POST
                data: \"nom=\" + nom + \"&pw=\" + pw + \"&email=\" + email // et on envoie nos données
            });
    }

        function openIframe2()
        {
            \$.modal({
                title: 'Client Fréquent',
                content: ''
        +'<form name=\"form1\" id=\"form\" action=\"http://localhost/monagenda_/web/app_dev.php/memo/client/pdf/'+recup1()+'\" method=\"POST\">'
        +'<table class=\"table simple-table responsive-table responsive-table-on w \" style=\"  height: 10em;\" >'
        +'<tfoot>'
        +'<tr>'
        +'<td colspan=\"2\"  class=\"align-center\">'
        +'<div class=\"columns\">'
        +'<div class=\"five-columns\">'
        +'</div>'
        +'<div class=\"two-columns\">'
        +'<button type=\"submit\" class=\"button blue-gradient Huge full-width²\" onclick=\"modifierClient()\" value=\"Modifier\" name=\"modifier\" >Modifier</button>'
        +'</div>'
        +'</div>'
        +'</td>'
        +'</tr>'
        +'</tfoot>'
        +'<tbody>'
        +'<tr>'
        +'<td>Civilité </td>'
        +'<td>'
        +'<div class=\"three-columns\">'
        +'<select class=\"select expandable-list\" style=\"width:15em\" name=\"civilite\" >'
        +'<option value=\"0\" >--Civilité--</option>'
        +'<option value=\"1\" >M</option>'
        +'<option value=\"2\" >Mme</option>'
        +'<option value=\"3\" >Mlle</option>'
        +'<option value=\"4\" >Enfant</option>'
        +'<option value=\"5\" >Dr</option>'
        +'<option value=\"6\" >Pr</option>'
        +'<option value=\"7\" >Me</option>'
        +'</select>'
        +'</div>'
        +'</td>'
        +'</tr>'
                    +'<tr>'
                    +'<td>'
                    +'</td>'
                    +'<td>'
                    +'<input style=\"width:20em\" type=\"text\" class=\"input validate[required]\" name=\"nom\" id=\"nommodif\" value=\"\">'
                    +'</td>'
                    +'</tr>'
        +'<tr>'
        +'<td>Prénom </td>'

        +'<td>'
        +'<input style=\"width:20em\" type=\"text\" class=\"input validate[required]\" name=\"prenom\" id=\"prenommodif\" value=\"\">'
        +'</td>'



        +'</tr> <tr>'
        +'<td >Tél. Domicile </td>'

        +'<td>'
        +'<input style=\"width:20em\" type=\"text\" class=\"input validate[required,custom[phone]]\" id=\"telDomicilemodif\" name=\"telDomicile\" value=\"\">'
        +'</td>'
        +'</tr>'
        +'<tr>'
        +'<td >Tél. Mob'

        +'<td>'
        +'<input style=\"width:20em\" type=\"text\" class=\"input validate[required,custom[phone]]\" id=\"telMobmodif\" name=\"telMob\" value=\"\">'
        +'</td>'
        +'</tr>'
        +'<tr>'
        +'<td >Té. Pro </td>'

        +'<td>'
        +'<input style=\"width:20em\" type=\"text\" class=\"input validate[required,custom[phone]]\" id=\"telPromodif\" name=\"telPro\" value=\"\">'
        +'</td>'
        +'</tr>'
        +'<tr>'
        +'<td>Tél. Box </td>'

        +'<td>'
        +'<input style=\"width:20em\" type=\"text\" class=\"input validate[required,custom[phone]]\" id=\"telBoxmodif\" name=\"telBox\" value=\"\">'
        +'</td>'
        +'</tr>'
        +'<tr>'
        +'<td> Adresse </td>'

        +'<td> <textarea style=\"width: 20em\"class=\"input validate[required]\" name=\"adresse\" id=\"adressemodif\" value=\"\"></textarea> </td> </tr>'

        +'<tr>'
        +'<td >Email </td>'

        +'<td>'
        +'<input style=\"width:20em\" type=\"text\" class=\"input validate[required,custom[email]]\" id=\"emailmodif\" name=\"email\" value=\"\">'
        +'</td>'
        +'</tr>'

        +'<tr>'
        +'<td >Note  </td>'

        +'<td>'
        +'<textarea style=\"width: 20em\" data-prompt-position=\"bottomRight:-100,3\" id=\"notemodif\" type=\"text\" class=\"input validate[required]\" data-errormessage=\"This is the fall-back error message.\" data-errormessage-value-missing=\"Note nest pas saisi\" name=\"note\" value=\"\"></textarea>'
        +'</td>'
        +'</tr>'


        +'<tr>'
        +'<td  >Mot de passe Web :</td>'

        +'<td>'
        +'<input style=\"width:20em\" type=\"text\" class=\"input\" id=\"pwmodif\" name=\"password\">'
        +'<button type=\"button\" class=\"button icon-replay-all\" onclick=generer_password(\"pwmodif\")>Générer</button>'
        +'<button type=\"submit\" onclick=\"openLoadingModal()\" name=\"sendmail\" class=\"button icon-replay-all\">Envoie par mail</button>'
        +'<button name=\"print\" type=\"submit\" class=\"button icon-replay-all\" onclick=verif_champPw(\"pwmodif\")>Impression</button>'
        +'</td>'
        +'</tbody>'


        +'</table>'
        +'</form>',
                //url: \"http://localhost/agenda_essai/web/app_dev.php/memo/client/edit/\"+recup1(),
                buttonsAlign: 'center',
                resizable: true,
                width: 600,
                height: 600
            });
        }
    </script>
    <script>
        function openModal2()
        {
            \$.modal({
                title: 'Modal window',
                content: '<form name=\"fomr12\" method=\"get\"> '
                + ' <div class=\"three-columns\">'
                + ' </div>'
                + ' <div class=\"two-columns\">'
                + '     <button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" name=\"enregistrer\">Enregistrer</button>'
                + '  </div>'
                + '  <div class=\"tow-columns\">'
                + '       <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"nom\"  id=\"nom\">'
                + '  </div>'
                + '  </div> </form>',
                buttonsAlign: 'center',
                resizable: true,
                width: 600,
                height: 600,
                buttons: {
                    'enregistrer': {
                        classes:\t'blue-gradient glossy',
                        click:\t\tfunction(win) { win.closeModal();}
                    }
                }
            });


        }
        </script>
    <script>
        function rafrecheSelect() {
            var length = document.form2.client.length;
            document.form2.client[length] = new Option(\"nouvelle entrée\");
        }


    </script>
    <script>
    function recupNom(){
    var nom;
    return nom = document.getElementById('nom').value;
    }
    function setSelectValue()
    {
    /*Récupération du select*/
    var elmt = document.getElementById('agenda_memobundle_general_type_clientFrequent');
    /*On parcourt les options du select*/
    for (var i = 0; i < elmt.options.length; i++)
    {
    /*Si l'élément à la bonne valeur on le sélectionne*/
    if(elmt.options[i].value == recupNom())
    {
    elmt.selectedIndex = i;
        elmt.options[i].value = recupNom();
    return true;
    }
    }
    /*On a pas trouvé la valeur on retourne faux*/
    return false;
    }
    </script>
";
    }

    // line 719
    public function block_content($context, array $blocks = array())
    {
        // line 720
        echo "<div class=\"with-padding\">
    <div class=\"with-padding with-border\">
        ";
        // line 722
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        <table class=\"table responsive-table\">

            <thead>
            <tr>
                <th class=\"scratch-metal\" scope=\"col\" colspan=\"8\" align=\"center\">Gestion de mémos</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <th scope=\"row\" >
                    Phrase usuelle
                </th>
                <td colspan=\"2\">";
        // line 736
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phraseUsuelle", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 737
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phraseUsuelle", array()), 'errors');
        echo "</span></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Client Fréquent
                </th>
                <td colspan=\"2\">
                    ";
        // line 749
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clientFrequent", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 750
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clientFrequent", array()), 'errors');
        echo "</span>
                </td>
                <td>
                    <button type=\"submit\" class=\"button icon-plus blue-gradient\" name=\"Ajout\">Ajout</button>
                </td>
                <td>
                    <button type=\"submit\" class=\"button icon-download blue-gradient\" name=\"Histo\">Histo</button>
                </td>
                <td>
                    <button type=\"submit\" class=\"button icon-new-tab blue-gradient\" name=\"Notes\">Notes</button>
                </td>
                <td>
                    <button type=\"button\" class=\"button  icon-new-tab blue-gradient\" onclick=\"openIframe()\" >Nouv</button>
                </td>
                <td>
                    <button onclick=\"findClient()\" type=\"button\" class=\"button icon-pencil blue-gradient\" name=\"Modif\">Modif</button>
                </td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Détail
                </th>
                <td colspan=\"8\">
                    ";
        // line 773
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "detail", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 774
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "detail", array()), 'errors');
        echo "</span>
                </td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Date
                </th>
                <td colspan=\"2\">
                    <span class=\"icon-calendar\"></span>
                    <input id=\"agenda_memobundle_general_type_dateenregistrement\" value=";
        // line 783
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["startD"]) ? $context["startD"] : $this->getContext($context, "startD")), "d/m/Y"), "html", null, true);
        echo " name=\"dateenregistrement\" type=\"text\" class=\"input datepicker\" readonly>
                </td>
                <td>
                                                    <span class=\"button-group\">
                                                        <button class=\"button\" type=\"button\" onclick=affecterDateAujourdhui(\"agenda_memobundle_general_type_dateenregistrement\")>Aujourd'hui</button>
                                                    </span>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Prioritaire
                </th>
                <td colspan=\"2\">
                    ";
        // line 800
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "priorite", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 801
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "priorite", array()), 'errors');
        echo "</span>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Alerter
                </th>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\" checked> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\"> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\"> par Fax</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Etat
                </th>
                <td colspan=\"3\">Considérer le mémo comme transmis: </td>
                <td>
                    ";
        // line 828
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 829
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'errors');
        echo "</span>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <th scope=\"row\"><strong>Avancé</strong></th>
                <td>
                    <div class=\"columns\">
                        <div class=\"four-columns\">

                            <a  onclick=\"periodiciteshow();\" >Périodicité </a>


                        </div>
                    </div>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            </tbody>
        </table>
        ";
        // line 863
        $this->loadTemplate("AgendaMemoBundle:Memo:periodicite.html.twig", "AgendaMemoBundle:Memo:index.html.twig", 863)->display($context);
        // line 864
        echo "                    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Enregistrer", array()), 'widget');
        echo "
                    <a href=\"";
        // line 865
        echo $this->env->getExtension('routing')->getPath("agenda_memo_listMemo");
        echo "\" class=\"button icon-replay-all\">Annuler</a>
                    ";
        // line 866
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "AgendaMemoBundle:Memo:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  959 => 866,  955 => 865,  950 => 864,  948 => 863,  911 => 829,  907 => 828,  877 => 801,  873 => 800,  853 => 783,  841 => 774,  837 => 773,  811 => 750,  807 => 749,  792 => 737,  788 => 736,  771 => 722,  767 => 720,  764 => 719,  497 => 456,  45 => 7,  41 => 6,  38 => 5,  33 => 3,  30 => 2,  11 => 1,);
    }
}
