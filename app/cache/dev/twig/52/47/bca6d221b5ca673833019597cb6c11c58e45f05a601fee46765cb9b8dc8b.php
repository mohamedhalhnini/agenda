<?php

/* FOSUserBundle:Registration:register_content.html.twig */
class __TwigTemplate_5247bca6d221b5ca673833019597cb6c11c58e45f05a601fee46765cb9b8dc8b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<form action=\"";
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
        echo "\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " method=\"POST\"  id=\"form-register\" class=\"input-wrapper green-gradient glossy\" title=\"Inscription\" style=\"display: block;\">

\t\t\t\t\t\t<p class=\"message\">
\t\t\t\t\t\t\tNouvel utilisateur? Laissez-nous en savons un peu plus sur vous avant de commencer
\t\t\t\t\t\t\t<span class=\"block-arrow\"><span></span></span>
\t\t\t\t\t\t</p>

\t\t\t\t\t\t<ul class=\"inputs black-input large\">
\t\t\t\t\t\t\t<li><span class=\"icon-card mid-margin-right\"></span>
                                                        ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("class" => "input-unstyled", "placeholder" => "Identifiant")));
        echo "
                                                        </li>
\t\t\t\t\t\t\t<li><span class=\"icon-mail mid-margin-right\"></span>
                                                        ";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "input-unstyled", "placeholder" => "E-Mail")));
        echo "
                                                        </li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<ul class=\"inputs black-input large\">
\t\t\t\t\t\t\t<li><span class=\"icon-user mid-margin-right\"></span>
                                                        ";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'widget', array("attr" => array("class" => "input-unstyled", "placeholder" => "Mot de passe")));
        echo "    
                                                        </li>
\t\t\t\t\t\t\t<li><span class=\"icon-lock mid-margin-right\"></span>
                                                        ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'widget', array("attr" => array("class" => "input-unstyled", "placeholder" => "Verification du Mot de passe")));
        echo "        
                                                        </li>
\t\t\t\t\t\t</ul>

\t\t\t\t\t\t <button type=\"submit\" class=\"button glossy full-width\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" >Inscription</button>

</form>";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 26,  54 => 22,  48 => 19,  40 => 14,  34 => 11,  19 => 2,);
    }
}
