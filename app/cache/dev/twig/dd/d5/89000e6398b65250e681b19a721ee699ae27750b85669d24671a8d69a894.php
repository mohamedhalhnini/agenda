<?php

/* MemoMemoBundle:Memo:modifier.html.twig */
class __TwigTemplate_ddd589000e6398b65250e681b19a721ee699ae27750b85669d24671a8d69a894 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "MemoMemoBundle:Memo:modifier.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"with-padding\">
    <div class=\"with-padding with-border\">
        ";
        // line 6
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        <table class=\"table1 responsive-table\">

            <thead>
            <tr>
                <th class=\"scratch-metal\" scope=\"col\" colspan=\"8\" align=\"center\">Gestion de mémos</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <th scope=\"row\" >
                    Phrase usuelle
                </th>
                <td colspan=\"2\">";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phraseUsuelle", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phraseUsuelle", array()), 'errors');
        echo "</span></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Client Fréquent
                </th>
                <td colspan=\"2\">
                    ";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clientFrequent", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clientFrequent", array()), 'errors');
        echo "</span>
                </td>
                <td>
                    <a href=\"#\" class=\"button\">
                        <span class=\"button-icon\"><span class=\"icon-plus-round\"></span></span>
                        Ajout
                    </a>
                </td>
                <td>
                    <a href=\"#\" class=\"button\">
                        <span class=\"button-icon\"><span class=\"icon-page-list\"></span></span>
                        Histo
                    </a>
                </td>
                <td>
                    <a href=\"#\" class=\"button\">
                        <span class=\"button-icon\"><span class=\"icon-quote\"></span></span>
                        Notes
                    </a>
                </td>
                <td>
                    <a href=\"#\" class=\"button\">
                        <span class=\"button-icon\"><span class=\"icon-plus\"></span></span>
                        Nouv
                    </a>
                </td>
                <td>
                    <a href=\"#\" class=\"button\">
                        <span class=\"button-icon\"><span class=\"icon-pencil\"></span></span>
                        Modif
                    </a>
                </td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Détail
                </th>
                <td colspan=\"8\">
                    ";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "detail", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "detail", array()), 'errors');
        echo "</span>
                </td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Date
                </th>
                <td colspan=\"2\">
                                                    <span class=\"input\">
\t<span class=\"icon-calendar\"></span>
\t                    ";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dateenregistrement", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dateenregistrement", array()), 'errors');
        echo "</span>
</span>
                </td>
                <td>
                                                    <span class=\"button-group\">
                                                        <a href=\"#\" class=\"button\">Aujourd'hui</a>
                                                    </span>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Prioritaire
                </th>
                <td colspan=\"2\">
                    ";
        // line 102
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "priorite", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "priorite", array()), 'errors');
        echo "</span>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Alerter
                </th>
                ";
        // line 115
        if (($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "alert", array()) == 1)) {
            // line 116
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\" checked> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\"> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\"> par Fax</td>
                ";
        } elseif (($this->getAttribute(        // line 119
(isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "alert", array()) == 2)) {
            // line 120
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\"> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\" checked> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\"> par Fax</td>
                ";
        } elseif (($this->getAttribute(        // line 123
(isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "alert", array()) == 4)) {
            // line 124
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\"> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\"> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\" checked> par Fax</td>
                ";
        } elseif (($this->getAttribute(        // line 127
(isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "alert", array()) == 3)) {
            // line 128
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\" checked> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\" checked> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\"> par Fax</td>
                ";
        } elseif (($this->getAttribute(        // line 131
(isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "alert", array()) == 5)) {
            // line 132
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\" checked> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\"> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\" checked> par Fax</td>
                ";
        } elseif (($this->getAttribute(        // line 135
(isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "alert", array()) == 6)) {
            // line 136
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\"> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\" checked> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\" checked> par Fax</td>
                ";
        } else {
            // line 140
            echo "                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"1\" class=\"styled checkbox\" checked> par Mail</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"2\" class=\"styled checkbox\" checked> par SMS</td>
                <td><input type=\"checkbox\" name=\"alerte[]\" value=\"4\" class=\"styled checkbox\" checked> par Fax</td>
                ";
        }
        // line 144
        echo "                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope=\"row\">
                    Etat
                </th>
                <td colspan=\"3\">Considérer le mémo comme transmis: </td>
                <td>
                    ";
        // line 156
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'widget');
        echo "
                    <span class=\"help-inline\">";
        // line 157
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'errors');
        echo "</span>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <th scope=\"row\"></th>
                <td></td>
                <td></td>
                <td colspan=\"2\">
                    ";
        // line 169
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Enregistrer", array()), 'widget');
        echo "
                    <a href=\"";
        // line 170
        echo $this->env->getExtension('routing')->getPath("agenda_memo_listMemo");
        echo "\" class=\"button icon-replay-all\">Annuler</a>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
        ";
        // line 178
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "MemoMemoBundle:Memo:modifier.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  274 => 178,  263 => 170,  259 => 169,  244 => 157,  240 => 156,  226 => 144,  220 => 140,  214 => 136,  212 => 135,  207 => 132,  205 => 131,  200 => 128,  198 => 127,  193 => 124,  191 => 123,  186 => 120,  184 => 119,  179 => 116,  177 => 115,  162 => 103,  158 => 102,  137 => 84,  133 => 83,  120 => 73,  116 => 72,  75 => 34,  71 => 33,  56 => 21,  52 => 20,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }
}
