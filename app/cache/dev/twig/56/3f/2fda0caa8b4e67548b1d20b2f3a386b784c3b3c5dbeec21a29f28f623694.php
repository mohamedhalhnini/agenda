<?php

/* DelimiteurBundle:Default:editType.html.twig */
class __TwigTemplate_563f2fda0caa8b4e67548b1d20b2f3a386b784c3b3c5dbeec21a29f28f623694 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base2.html.twig", "DelimiteurBundle:Default:editType.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "<hgroup id=\"main-title\" class=\"thin\">
    <h1>Ajouter un type</h1>
            </hgroup>
<form action=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("editType", array("id" => $this->getAttribute((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "id", array()))), "html", null, true);
        echo "\" method=\"POST\">
<div class=\"left-column-200px margin-bottom\" style=\"background-color:gainsboro\">

                    <div class=\"left-column\">
                        Type de delimiteur
                    </div>

                    <div class=\"right-column\">
                        <input type=\"text\" id=\"titre\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "nom", array()), "html", null, true);
        echo "\" class=\"input\" name=\"nom\">
                    </div>

                </div>
<div class=\"left-column-200px margin-bottom\" style=\"background-color:gainsboro\">

                    <div class=\"left-column\">
                        couleur
                    </div>

                    <div class=\"right-column\">
                        <input type=\"color\" id=\"titre\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "couleur", array()), "html", null, true);
        echo "\" class=\"input\" name=\"couleur\">
                    </div>

                </div>
    <button type=\"submit\" class=\"button glossy mid-margin-right\">
                        <span class=\"button-icon\"><span class=\"icon-tick\"></span></span>
                        enregistrer
                    </button>
</form>
";
    }

    public function getTemplateName()
    {
        return "DelimiteurBundle:Default:editType.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 27,  47 => 16,  36 => 8,  31 => 5,  28 => 4,  11 => 1,);
    }
}
