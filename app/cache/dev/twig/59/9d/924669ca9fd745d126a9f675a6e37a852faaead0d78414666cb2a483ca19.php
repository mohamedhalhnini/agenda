<?php

/* DelimiteurBundle:Default:edit.html.twig */
class __TwigTemplate_599d924669ca9fd745d126a9f675a6e37a852faaead0d78414666cb2a483ca19 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "DelimiteurBundle:Default:edit.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "

<form action=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("editDelimiteur", array("id" => $this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "id", array()))), "html", null, true);
        echo "\" method=\"POST\">
    <table class=\"table simple-table \" id=\"sorting-example1\">
        <thead>
            <tr><th colspan=\"2\">Gestion des delimiteurs</th></tr>

        </thead>
        <tbody>
            <tr>
        <div class=\"left-column-200px margin-bottom\">
            <td>
                <div class=\"left-column\">
                    choix du delimiteur
                </div>
            </td>
            <td>
                <div class=\"right-column\">
                    <select class=\"\" name=\"delimiteur\" id=\"delimiteur\" >
                            ";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["typeDelimiteurs"]) ? $context["typeDelimiteurs"] : $this->getContext($context, "typeDelimiteurs")));
        foreach ($context['_seq'] as $context["_key"] => $context["typedelimiteur"]) {
            // line 24
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "id", array()), "html", null, true);
            echo "\" style=\"background-color: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "couleur", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "nom", array()), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['typedelimiteur'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "                    </select>
                </div>
            </td>
        </div>
    </tr><tr>
<div class=\"left-column-200px margin-bottom\" >
    <td>
        <div class=\"left-column\">
            titre du delimiteur
        </div>
    </td>
    <td>
        <div class=\"right-column\">
            <input type=\"text\" id=\"titre\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "titre", array()), "html", null, true);
        echo "\" class=\"input\" name=\"titre\">
        </div>
    </td>
</div></tr>
<tr>
<div class=\"left-column-200px margin-bottom\">
    <td>
        <div class=\"left-column\">
            choix d'une plage 
        </div>
    </td><td>
        <div class=\"right-column\">
            <select class=\"select\">
                <option>nouvelle plage</option>
            </select>
        </div>
    </td>
</div></tr>
<tr>
<div class=\"left-column-200px margin-bottom\">
    <td>
        <div class=\"left-column\">
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            Periodicite
        </div>
    </td><td>
        <div class=\"right-column\">

            <input type=\"radio\" name=\"type\" id=\"type\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"1\" ";
        // line 74
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "type", array()) == 1)) {
            echo " checked=\"checked\" ";
        }
        echo ">
            une fois
            <br/>
            <br/>
            <br/>
            <div class=\"left-column-200px margin-bottom\">

                <div class=\"left-column\">
                    <input type=\"radio\" name=\"type\" id=\"type\" class=\"radio mid-margin-left\" id=\"button-radio-2\" value=\"2\" ";
        // line 82
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "type", array()) == 2)) {
            echo " checked=\"checked\" ";
        }
        echo " >
                    quotidienne
                </div>

                <div class=\"right-column\">
                    <input type=\"radio\" name=\"quotidienne\" id=\"radio_2\" value=\"1\" class=\"radio\"  ";
        // line 87
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "typequotidient", array()) == 1)) {
            echo " checked=\"checked\" ";
        }
        echo " > <label for=\"oui\">Tous les </label>
                                <input type=\"text\"  name=\"jours\" ";
        // line 88
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "typequotidient", array()) == 1)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "touslesjours", array()), "html", null, true);
            echo "\" ";
        }
        echo ">  jour(s) <br><br>
                                <input type=\"radio\" name=\"quotidienne\" id=\"radio_2\" value=\"2\" class=\"radio\" ";
        // line 89
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "typequotidient", array()) == 2)) {
            echo " checked=\"checked\" ";
        }
        echo "> <label for=\"oui\">Tous les jours ouvrables</label><br><br>                      
                                <input type=\"radio\" name=\"quotidienne\" id=\"radio_2\" value=\"3\" class=\"radio\" ";
        // line 90
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "typequotidient", array()) == 3)) {
            echo " checked=\"checked\" ";
        }
        echo "> <label for=\"oui\">Tous les jours selectionnés :</label><br><br> 
                    <p>
                         <input class=\"mid-margin-left\" type=\"checkbox\" name=\"checked[]\" id=\"lu\" value=\"1\" ";
        // line 92
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "typequotidient", array()) == 3)) {
            echo " ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "jourselectionne", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo " ";
                if (($context["i"] == 1)) {
                    echo " checked=\"checked\"";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        echo "> LU


                                    <input class=\"mid-margin-left\" type=\"checkbox\" name=\"checked[]\" id=\"ma\" value=\"2\" ";
        // line 95
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "typequotidient", array()) == 3)) {
            echo " ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "jourselectionne", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo " ";
                if (($context["i"] == 2)) {
                    echo " checked=\"checked\"";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        echo "> MA


                                    <input class=\"mid-margin-left\" type=\"checkbox\" name=\"checked[]\" id=\"me\" value=\"3\" ";
        // line 98
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "typequotidient", array()) == 3)) {
            echo " ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "jourselectionne", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo " ";
                if (($context["i"] == 3)) {
                    echo " checked=\"checked\"";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        echo "> ME


                                    <input class=\"mid-margin-left\" type=\"checkbox\" name=\"checked[]\" id=\"je\" value=\"4\" ";
        // line 101
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "typequotidient", array()) == 3)) {
            echo " ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "jourselectionne", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo " ";
                if (($context["i"] == 4)) {
                    echo " checked=\"checked\"";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        echo "> Je


                                    <input class=\"mid-margin-left\" type=\"checkbox\" name=\"checked[]\" id=\"ve\" value=\"5\" ";
        // line 104
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "typequotidient", array()) == 3)) {
            echo " ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "jourselectionne", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo " ";
                if (($context["i"] == 5)) {
                    echo " checked=\"checked\"";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        echo "> Ve


                                    <input class=\"mid-margin-left\" type=\"checkbox\" name=\"checked[]\" id=\"sa\" value=\"6\" ";
        // line 107
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "typequotidient", array()) == 3)) {
            echo " ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "jourselectionne", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo " ";
                if (($context["i"] == 6)) {
                    echo " checked=\"checked\"";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        echo "> Sa

                                    <input class=\"mid-margin-left\" type=\"checkbox\" name=\"checked[]\" id=\"di\" value=\"7\" ";
        // line 109
        if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "typequotidient", array()) == 3)) {
            echo " ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "jourselectionne", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo " ";
                if (($context["i"] == 7)) {
                    echo " checked=\"checked\"";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        echo "> Di
                    </p>
                </div>

            </div>
            <br/>
            <div class=\"left-column-200px margin-bottom\">

                <div class=\"left-column\">
                    <input type=\"radio\" name=\"type\" id=\"type\" class=\"radio mid-margin-left\" id=\"button-radio-3\" value=\"3\">
                    hebdomadaire
                </div>

                <div class=\"right-column\">
                    Tous les <select class=\"select expandable-list\" style=\"width:6em\" name=\"hebdomadaire\">
                              ";
        // line 124
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["jours"]) ? $context["jours"] : $this->getContext($context, "jours")));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 125
            echo "                                    <option  ";
            if (($this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "tousles", array()) == $context["value"])) {
                echo " selected='";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "' value=\"";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "\" ";
            } else {
                echo " value=\"";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "</option>
                              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 127
        echo "                </div>

            </div>
            <br/>



        </div>

</div>
</tr>
<tr>
<div class=\"left-column-200px margin-bottom\" style=\"background-color:white\">
    <td>
        <div class=\"left-column\">
            Plage de periodicite
        </div>
    </td><td>
        <div class=\"right-column\">
            prend effet le <span class=\"input\">
                <span class=\"icon-calendar\"></span>
                <input type=\"text\" name=\"prendEffet\" id=\"datetimepicker\" style=\"width:5em\" class=\"input-unstyled datepicker gldp-el\" value=\"";
        // line 148
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "effet", array()), "d/m/Y"), "html", null, true);
        echo "\" gldp-id=\"gldp-9964084781\">
            </span><br/>
            <br/>
            <br/>
            <div class=\"left-column-200px margin-bottom\">

                <div class=\"left-column\">
                    Et prend fin
                </div>

                <div class=\"right-column\">
                    <input type=\"radio\" name=\"prendFin\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"1\" ";
        // line 159
        if ( !(null === $this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "finapresoccurence", array()))) {
            echo " checked=\"checked\" ";
        }
        echo ">apres 
                                <input type=\"text\" id=\"login\" name=\"occurences\" ";
        // line 160
        if ( !(null === $this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "finapresoccurence", array()))) {
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "finapresoccurence", array()), "html", null, true);
            echo "\"  ";
        } else {
            echo " value=\"0\" ";
        }
        echo "style=\"width:2em\" class=\"input\">occurence(s)<br/><br/>
                                <input type=\"radio\" name=\"prendFin\" class=\"radio mid-margin-left\" id=\"button-radio-2\" value=\"2\" ";
        // line 161
        if ((null === $this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "finapresoccurence", array()))) {
            echo " checked=\"checked\" ";
        }
        echo ">
                                le<span class=\"input\">
                                    <span class=\"icon-calendar\"></span>
                                    <input type=\"text\" name=\"dateFin\" id=\"special-input-3\" class=\"input-unstyled datepicker gldp-el\"  gldp-id=\"gldp-9964084782\"  value=\"";
        // line 164
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "end", array()), "d/m/Y"), "html", null, true);
        echo "\" >
                                </span>
                </div>

            </div>
        </div>
    </td>
</div>
</tr>
<tr>
<div class=\"left-column-200px margin-bottom\">
    <td>
        <div class=\"left-column\">
            Duree
        </div>
    </td><td>
        <div class=\"right-column\">
            <input type=\"time\" name=\"debute\" value=\"";
        // line 181
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "debute", array()), "H:i"), "html", null, true);
        echo "\">
            et prend fin a 
            <input type=\"time\" name=\"fin\" value=\"";
        // line 183
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "fin", array()), "H:i"), "html", null, true);
        echo "\">
            <div id=\"colorSelector\"><div style=\"background-color: rgb(74, 74, 161);\"></div></div>
        </div>
    </td>
</div>
</tr>
<tr>
<div class=\"left-column-200px margin-bottom\">
    <td>
        <div class=\"left-column\">
            Couleur
        </div>
    </td><td>
        <div class=\"right-column\">
            <select class=\"select\">
                <option>Defaut</option>

            </select>&nbsp;
            <input type=\"color\" name=\"couleur\" class=\"input\" id=\"couleur\" value=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "periodicite", array()), "couleuralloccurence", array()), "html", null, true);
        echo "\">
        </div>
    </td>
</div></tr>
<tr>
<div class=\"left-column-200px margin-bottom\">
    <td>
        <div class=\"left-column\">
            Partage
        </div>
    </td><td>
        <div class=\"right-column\">
            <input type=\"radio\" name=\"partage\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"1\" ";
        // line 213
        if (($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "partage", array()) == 1)) {
            echo " checked=\"checked\" ";
        }
        echo ">
            Public <br/><input type=\"radio\" name=\"partage\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"0\" ";
        // line 214
        if (($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "partage", array()) == 0)) {
            echo " checked=\"checked\" ";
        }
        echo ">
            Privee(mention \"occuppee\"dans le partage du planing)
        </div>
    </td>
</div></tr>
<tr>
<div class=\"left-column-200px margin-bottom\">
    <td>
        <div class=\"left-column\">
            effet lors de la recherche de plages libres
        </div>
    </td><td>
        <div class=\"right-column\">
           <input type=\"radio\" name=\"blockage\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"0\" ";
        // line 227
        if (($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "blockage", array()) == 0)) {
            echo " checked=\"checked\" ";
        }
        echo ">
           non bloquant(cette plage sera considere comme libre) <br/><input type=\"radio\" name=\"blockage\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"1\" ";
        // line 228
        if (($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "blockage", array()) == 1)) {
            echo " checked=\"checked\" ";
        }
        echo ">
           Bloquant(cette plage sera considere comme occuppee)
        </div>
    </td>
</div></tr>
<tr>
<div class=\"left-column-200px margin-bottom\">
    <td>
        <div class=\"left-column\">
            Associer
        </div>
    </td><td>
        <div class=\"right-column\">
            avec un rendez-vous    <select class=\"select\">
                <option>aucun</option>

            </select>
        </div>
    </td>
</div></tr>
<tr>
<div class=\"left-column-200px margin-bottom\">
    <td>
        <div class=\"left-column\">
            plage accessible a la prise de rendez-vous par internet
        </div>
    </td><td>
        <div class=\"right-column\">
           <input type=\"radio\" name=\"accessInternet\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"0\" ";
        // line 256
        if (($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "accessinternet", array()) == 0)) {
            echo " checked=\"checked\" ";
        }
        echo ">
           non <input type=\"radio\" name=\"accessInternet\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"1\" ";
        // line 257
        if (($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "accessinternet", array()) == 1)) {
            echo " checked=\"checked\" ";
        }
        echo ">
           oui
        </div>
    </td>
</div></tr>
<tr>
<div class=\"left-column-200px margin-bottom\" >
    <td>
        <div class=\"left-column\">
            Priorite
        </div>
    </td><td>
        <div class=\"right-column\">
            <select class=\"select\" name=\"priorite\">
            <option value=\"1\" ";
        // line 271
        if (($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "priorite", array()) == 1)) {
            echo " selected=\"true\" ";
        }
        echo ">la plus haute</option>
            <option value=\"0\" ";
        // line 272
        if (($this->getAttribute((isset($context["delimiteur"]) ? $context["delimiteur"] : $this->getContext($context, "delimiteur")), "priorite", array()) == 0)) {
            echo " selected=\"true\" ";
        }
        echo ">la plus basse</option>
            </select>
        </div>
    </td>
</div></tr>
<tr>
<div class=\"field-block button-height\">
    <td></td>
    <td>
        <button type=\"submit\" class=\"button glossy mid-margin-right\">
            <span class=\"button-icon\"><span class=\"icon-tick\"></span></span>
            enregistrer
        </button>


        <button type=\"reset\" class=\"button glossy\">
            <span class=\"button-icon red-gradient\"><span class=\"icon-cross-round\"></span></span>
            annuler
        </button>

    </td>
</div></tr>
</tbody>
</table>
</form>

<script>
    \$(\"document\").ready(function() {

        \$(\"#delimiteur\").change(function() {
            \$.ajax({
                type: 'get',
                url: 'http://localhost/myagenda/web/app_dev.php/delimiteur/select/' + \$(this).val(),
                beforeSend: function() {
                    console.log('me voila!!');
                },
                success: function(data) {
                    console.log('me voila3!!');

                    \$(\"#titre\").val(data.nom);
                    \$(\"#couleur\").val(data.couleur);
                    \$(\"#delimiteur\").css('background-color' , data.couleur);
                }




            });
        });

    });

</script>








";
    }

    public function getTemplateName()
    {
        return "DelimiteurBundle:Default:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  560 => 272,  554 => 271,  535 => 257,  529 => 256,  496 => 228,  490 => 227,  472 => 214,  466 => 213,  451 => 201,  430 => 183,  425 => 181,  405 => 164,  397 => 161,  387 => 160,  381 => 159,  367 => 148,  344 => 127,  323 => 125,  319 => 124,  288 => 109,  270 => 107,  251 => 104,  232 => 101,  213 => 98,  194 => 95,  175 => 92,  168 => 90,  162 => 89,  154 => 88,  148 => 87,  138 => 82,  125 => 74,  87 => 39,  72 => 26,  59 => 24,  55 => 23,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }
}
