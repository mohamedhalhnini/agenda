<?php

/* AgendaMemoBundle:Client:create.html.twig */
class __TwigTemplate_0b77b727d6fa13362b928b015bc598d5c1d3036c00eb118dc544c1e05d88215c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AgendaMemoBundle:Client:create.html.twig", 1);
        $this->blocks = array(
            'mesScript' => array($this, 'block_mesScript'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_mesScript($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("mesScript", $context, $blocks);
        echo "
    <script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("asset/js/developr.modal.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "     <form name=\"form1\" action=\"";
        echo $this->env->getExtension('routing')->getPath("agenda_client_create");
        echo "\" method=\"POST\">
    <table class=\"table simple-table responsive-table responsive-table-on w \" style=\"  height: 10em;\" >
      ";
        // line 10
        echo "        <tfoot>
            <tr>
                <td colspan=\"2\"  class=\"align-center\">
                    <div class=\"columns\">
                        <div class=\"three-columns\">
                        </div>
                        <div class=\"two-columns\">
                            <button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" name=\"Enregistrer\" >Enregistrer</button>
                        </div>
                        <div class=\"tow-columns\">
                            <button type=\"submit\" class=\"button blue-gradient\" value=\"Annuler\" name=\"Annuler\">Annuler</button>
                        </div>
                    </div>
                </td>
            </tr>
        </tfoot>             
        <tbody>
            <tr>
                <td>Civilité </td>
                <td>
                    <div class=\"three-columns\">
                        <select class=\"select expandable-list\" style=\"width:15em\" name=\"civilite\" >
                            <option value=\"0\" >--Civilité--</option>
                            <option value=\"1\" >M</option>
                            <option value=\"2\" >Mme</option>
                            <option value=\"3\" >Mlle</option>
                            <option value=\"4\" >Enfant</option>
                            <option value=\"5\" >Dr</option>
                            <option value=\"6\" >Pr</option>
                            <option value=\"7\" >Me</option>
                        </select>
                    </div>      
                </td>
            </tr>
            <tr>
                <td>Nom</td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"nom\">
                </td>
            </tr>
            <tr>
                <td>Prénom </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"prenom\">
                </td>



            </tr> <tr>
                <td >Tél. Domicile </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" nam=\"telDomicile\">
                </td>
            </tr>
            <tr>
                <td >Tél. Mob

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telMob\">
                </td>
            </tr>
            <tr>
                <td >Té. Pro </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telPro\">
                </td>
            </tr>
            <tr>
                <td>Tél. Box </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"telBox\">
                </td>
            </tr>
            <tr>
                <td> Adresse </td>

                <td>
                    <textarea style=\"width: 20em\"class=\"input\" name=\"adresse\"></textarea>
                </td>
            </tr>

            <tr>
                <td >Email </td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"email\">
                </td>
            </tr>

            <tr>
                <td >Note  </td>

                <td>
                    <textarea style=\"width: 20em\"class=\"input\" name=\"note\"></textarea>
                </td>
            </tr>


            <tr>
                <td  >Mot de passe Web :</td>

                <td>
                    <input style=\"width:20em\" type=\"text\" class=\"input\" name=\"password\">
                    <button type=\"submit\" class=\"button  blue-gradient\" name=\"Ajout\" name=\"generer\">Générer</button>

                </td>


        </tbody>    


    </table>
</form>
";
    }

    public function getTemplateName()
    {
        return "AgendaMemoBundle:Client:create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 10,  46 => 7,  43 => 6,  37 => 4,  32 => 3,  29 => 2,  11 => 1,);
    }
}
