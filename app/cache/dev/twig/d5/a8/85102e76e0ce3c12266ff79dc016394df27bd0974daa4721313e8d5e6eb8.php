<?php

/* RendezVousBundle:Default:updateRendezVousType.html.twig */
class __TwigTemplate_d5a885102e76e0ce3c12266ff79dc016394df27bd0974daa4721313e8d5e6eb8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "RendezVousBundle:Default:updateRendezVousType.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<form action =\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("updateRdvType", array("id" => $this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "id", array()))), "html", null, true);
        echo "\" method=\"post\">
<table class=\"simple-table responsive-table fluid\" id=\"sorting-example2\" >
<thead>
<tr>
    <td align=\"center\" colspan=\"2\">
        Gestion des rendez-vous types
    </td>
</tr>
</thead>
<tbody>
<tr>
    <td style=\"width: 14em\">
        <p>
            <strong>Choix du RDV type :</strong>
        </p>
    </td>
    <td>
        <select name=\"newRdv\"  class=\"select compact l\" style=\"width: 27em\">
            <option value=\"1\">-- Nouvelle RDV type --</option>
            <option value=\"2\">consultation</option>
            <option value=\"3\">examination</option>
            <option value=\"4\">test respiration</option>
        </select>
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Libellé</strong>
        </p>
    </td>

    <td>
        <input name=\"libelle\" type=\"text\" size=\"25\" value=\"Consultation\" class=\"input\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "libelle", array()), "html", null, true);
        echo "\" >
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Durée</strong>
        </p>
    </td>

    <td>
        <p>

            d'une durée :
            <input type=\"text\" name=\"duree\" style=\"width: 40px\" id=\"time3\" class=\"input\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "dureee", array()), "H:i"), "html", null, true);
        echo "\" >
            (concaténer le texte suivant au libllé : <input name=\"concat\" type=\"text\" size=\"25\"  class=\"input\" >)
        </p>
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Couleur</strong>
        </p>
    </td>

    <td>
        <input  type=\"color\" name=\"couleur\" class=\"input\" style=\"width: 15em\" value=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "couleur", array()), "html", null, true);
        echo "\">
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Information</strong>
        </p>
    </td>

    <td>
        <textarea name=\"information\" id=\"autoexpanding\" class=\"input full-width autoexpanding\">
";
        // line 75
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "information", array()), "html", null, true);
        echo "
           </textarea>


    </td>
</tr>
<tr>
    <td class=\"vertical-center\">
        <p>
            <strong>Champs du client fréquent requis</strong>
        </p>
    </td>

    <td>
        <output class=\"underline\">Champs permanent :</output>
        <br>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"telDomicile\" id=\"checkbox-1\" value=\"1\" ";
        // line 92
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "telDomicie", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo ">
        <label for=\"checkbox-1\" class=\"label\">Tél. Domicile</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"telTravail\" id=\"checkbox-2\" value=\"1\" ";
        // line 95
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "telTravail", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo ">
        <label for=\"checkbox-1\" class=\"label\">Tél. Travail</label>
        <br>
        <input  class=\"styled checkbox\" type=\"checkbox\" name=\"telMobile\" id=\"checkbox-3\" value=\"1\" ";
        // line 98
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "telMobile", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo " >
        <label for=\"checkbox-1\" class=\"label\">Tél. Mobile</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"telBox\" id=\"checkbox-4\" value=\"1\" ";
        // line 101
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "telBox", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo " >
        <label for=\"checkbox-1\" class=\"label\">Tél. Box</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"adresse\" id=\"checkbox-5\" value=\"1\" ";
        // line 104
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "adresse", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo ">
        <label for=\"checkbox-1\" class=\"label\">Adresse</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"email\" id=\"checkbox-6\" value=\"1\" ";
        // line 107
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "email", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo ">
        <label for=\"checkbox-1\" class=\"label\">E-mail</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"notes\" id=\"checkbox-7\" value=\"1\" ";
        // line 110
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "notes", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo " >
        <label for=\"checkbox-1\" class=\"label\">Notes</label>
        <br>
        <br>
        <output class=\"underline\">Champs nom-partagés :</output>
        <br>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"dateNaissance\" id=\"checkbox-8\" value=\"1\" ";
        // line 117
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "dateNaissance", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo ">
        <label for=\"checkbox-1\" class=\"label\">Date de naissance</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"nomJF\" id=\"checkbox-9\" value=\"1\" ";
        // line 120
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "nomJF", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo ">
        <label for=\"checkbox-1\" class=\"label\">Nom de JF</label>
        <br>
        <input class=\"styled checkbox\" type=\"checkbox\" name=\"lastVisit\" id=\"checkbox-10\" value=\"1\" ";
        // line 123
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "lastVisit", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo ">
        <label for=\"checkbox-1\" class=\"label\">Derniére visite</label>
    </td>
</tr>
<tr>
    <td class=\"vertical-center\">
        <p>
            <strong>Champs du client fréquent a reporter</strong>
        </p>
    </td>

    <td class=\"vertical-center\">
        <input class=\"radio\" type=\"radio\" name=\"champClient\" id=\"radio-1\" value=\"1\" ";
        // line 135
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "champsClient", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo ">
        Tous les champs renseignés
        <output style=\"color: white\">.  .</output>
        <input class=\"radio\" type=\"radio\" name=\"champClient\" id=\"radio-1\" value=\"0\" ";
        // line 138
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "champsClient", array()) == 0)) {
            echo "checked=\"checked\"";
        }
        echo ">
        Seuls les champs requis
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Selectionnable lors de la prise de rendez-vous par internet</strong>
        </p>
    </td>

    <td class=\"vertical-center\">
        <input class=\"radio\" type=\"radio\" name=\"selectionnable\" id=\"radio-1\" value=\"0\" ";
        // line 150
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "selectionnable", array()) == 0)) {
            echo "checked=\"checked\"";
        }
        echo " >
        Non
        <output style=\"color: white\">.  .</output>
        <input class=\"radio\" type=\"radio\" name=\"selectionnable\" id=\"radio-1\" value=\"1\" ";
        // line 153
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "selectionnable", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo ">
        Oui
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Information supplémentaires pour notes récurrentes</strong>
        </p>
    </td>

    <td class=\"vertical-center\">
        <input class=\"radio\" type=\"radio\" name=\"infoSup\" id=\"radio-1\" value=\"0\" ";
        // line 165
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "infoSup", array()) == 0)) {
            echo "checked=\"checked\"";
        }
        echo ">
        Non
        <output style=\"color: white\">.  .</output>
        <input class=\"radio\" type=\"radio\" name=\"infoSup\" id=\"radio-1\" value=\"1\" ";
        // line 168
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "infoSup", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo ">
        Oui
    </td>
</tr>
<tr>
    <td class=\"vertical-center\">
        <p>
            <strong>Sélectionné par défaut</strong>
        </p>
    </td>

    <td>
        <input class=\"radio\" type=\"radio\" name=\"selectParDef\" id=\"radio-1\" value=\"0\" ";
        // line 180
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "selectParDef", array()) == 0)) {
            echo "checked=\"checked\"";
        }
        echo ">
        Non
        <output style=\"color: white\">.  .</output>
        <input class=\"radio\" type=\"radio\" name=\"selectParDef\" id=\"radio-1\" value=\"1\" ";
        // line 183
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "selectParDef", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo ">
        Oui
    </td>
</tr>
<tr>
    <td>
        <p>
            <strong>Choix spécifique sur alertes/rappels</strong>
        </p>
    </td>

    <td class=\"vertical-center\">
        <input class=\"radio\" type=\"radio\" name=\"choixAlertRappel\" id=\"radio-1\" value=\"0\" ";
        // line 195
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "choixAlertRappel", array()) == 0)) {
            echo "checked=\"checked\"";
        }
        echo ">
        Non
        <output style=\"color: white\">.  .</output>
        <input class=\"radio\" type=\"radio\" name=\"choixAlertRappel\" id=\"radio-1\" value=\"1\" ";
        // line 198
        if (($this->getAttribute((isset($context["rdvType"]) ? $context["rdvType"] : $this->getContext($context, "rdvType")), "choixAlertRappel", array()) == 1)) {
            echo "checked=\"checked\"";
        }
        echo " >
        Oui
    </td>
</tr>
<tr>
    <td colspan=\"2\" align=\"center\">
        <button type=\"submit\" class=\"button compact\">Enregistrer</button>
        <button type=\"button\" class=\"button compact\">Annuler</button>
        <button type=\"button\" class=\"button compact\" disabled>Supprimer</button>
    </td>
</tr>
</tbody>
</table>

    </form>
";
    }

    public function getTemplateName()
    {
        return "RendezVousBundle:Default:updateRendezVousType.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  337 => 198,  329 => 195,  312 => 183,  304 => 180,  287 => 168,  279 => 165,  262 => 153,  254 => 150,  237 => 138,  229 => 135,  212 => 123,  204 => 120,  196 => 117,  184 => 110,  176 => 107,  168 => 104,  160 => 101,  152 => 98,  144 => 95,  136 => 92,  116 => 75,  101 => 63,  85 => 50,  68 => 36,  31 => 3,  28 => 2,  11 => 1,);
    }
}
