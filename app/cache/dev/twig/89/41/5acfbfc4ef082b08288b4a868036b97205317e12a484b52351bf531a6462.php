<?php

/* DelimiteurBundle:Default:listTypeDelimiteur.html.twig */
class __TwigTemplate_89415acfbfc4ef082b08288b4a868036b97205317e12a484b52351bf531a6462 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "DelimiteurBundle:Default:listTypeDelimiteur.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "            <div class=\"with-padding\">

                <p class=\"wrapped left-icon icon-info-round\">
                    Tables are responsive, too! Try resizing your browser
                </p>











                

                <table class=\"simple-table responsive-table\" id=\"sorting-example2\">

                    <thead>
                        <tr>
                            <th scope=\"col\">Titre delimiteur</th>
                            <th scope=\"col\">couleur</th>
                            <th scope=\"col\" width=\"120\" class=\"align-right\">Actions</th>
                        </tr>
                    </thead>


                    <tbody>
                                    ";
        // line 34
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["typedelimiteurs"]) ? $context["typedelimiteurs"] : $this->getContext($context, "typedelimiteurs")));
        foreach ($context['_seq'] as $context["_key"] => $context["typedelimiteur"]) {
            // line 35
            echo "                        <tr>
                            <th scope=\"row\">
\t\t\t\t\t\t\t";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "nom", array()), "html", null, true);
            echo "<br>
                            </th>
                            <td style=\"background-color: ";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "couleur", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "couleur", array()), "html", null, true);
            echo "</td>
                            <td class=\"align-right vertical-center\">
                                <span class=\"button-group compact\">
                                    <a href=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("update", array("id" => $this->getAttribute($context["typedelimiteur"], "id", array()))), "html", null, true);
            echo "\" class=\"button icon-pencil confirm\" title=\"Update\"></a>

";
            // line 45
            echo "                                    <a href=\"#\" class=\"button icon-gear with-tooltip\" title=\"Other actions\"></a>
                                    <a href=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("delete", array("id" => $this->getAttribute($context["typedelimiteur"], "id", array()))), "html", null, true);
            echo "\" class=\"button icon-trash with-tooltip confirm\" title=\"Delete\"></a>
                                </span>
                            </td>
                        </tr>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['typedelimiteur'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "                    </tbody>

                </table>

            </div>
";
    }

    public function getTemplateName()
    {
        return "DelimiteurBundle:Default:listTypeDelimiteur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 51,  92 => 46,  89 => 45,  84 => 42,  76 => 39,  71 => 37,  67 => 35,  63 => 34,  31 => 4,  28 => 3,  11 => 1,);
    }
}
