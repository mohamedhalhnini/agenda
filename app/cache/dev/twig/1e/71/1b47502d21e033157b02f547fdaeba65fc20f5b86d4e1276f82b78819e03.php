<?php

/* DelimiteurBundle:Default:test.html.twig */
class __TwigTemplate_1e711b47502d21e033157b02f547fdaeba65fc20f5b86d4e1276f82b78819e03 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "DelimiteurBundle:Default:test.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<link href='";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/fullcalendar.css"), "html", null, true);
        echo "' rel='stylesheet' />
<link href='";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/fullcalendar.print.css"), "html", null, true);
        echo "' rel='stylesheet' media='print' />
<script src='";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/lib/moment.min.js"), "html", null, true);
        echo "'></script>
<script src='";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/lib/jquery.min.js"), "html", null, true);
        echo "'></script>
<script src='";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/fullcalendar.min.js"), "html", null, true);
        echo "'></script>
<script src='";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/lang-all.js"), "html", null, true);
        echo "'></script>
    ";
        // line 11
        echo "<script type=\"text/javascript\">
            function print_calendar()
            {
            window.print();
            }
         
    ";
        // line 25
        echo "
    \$(document).ready(function() {
 

          
    \$('#my-button').click(function() {

    var moment = \$('#calendar').fullCalendar('getDate');
            alert(\"The current date of the calendar is \" +moment.format('YYYY-MM-DD'));
    });
    
   
            
                    
    \$('#calendar').fullCalendar({
    header: {
    left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,basicWeek,agendaDay,basicDay'

    },
    buttonText: {
               
                basicDay: 'Basic day',
                basicWeek:'Basic Week'
            },
            minTime:'08:00',
            maxTime: '18:00',
            lang: 'fr',
            editable: true,
            defaultView :'agendaWeek',
            axisFormat : 'HH:mm',
            slotDuration :'00:05:00',
            timeFormat: {
            agenda : 'HH:mm'
            },
            eventLimit: true, // allow \"more\" link when too many events
           // events: ouf,
           eventSources: [
                    ";
        // line 64
        echo (isset($context["events"]) ? $context["events"] : $this->getContext($context, "events"));
        echo "
                ],
    //***********************myAjax*********************************    

    //******************************************************    
viewRender: function(view){
       
         var moment = \$('#calendar').fullCalendar('getDate');
             console.log(moment.format('YYYY-MM-DD'));
               \$.ajax({
            type: 'get',
                    url : 'http://localhost/monagenda/web/app_dev.php/rendezVous/calendar/'+moment.format('YYYY-MM-DD'),
                    beforeSend: function(){
                    console.log('dateRefresh Before!!');
                    },
                    success:function(data){
";
        // line 82
        echo "            \$('#calendar').fullCalendar('rerenderEvents' );

             
                    console.log('dateRefresh after!!');
                }
      });

   
   },
    eventSources: [
               ";
        // line 92
        echo (isset($context["events"]) ? $context["events"] : $this->getContext($context, "events"));
        echo "
                 ";
        // line 94
        echo "                          ],
   
             
    eventClick: //function(event, jsEvent, view) {
//
//                    alert('Event: ' +event.id+ event.start.format() + event.end.format() );
//                    alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//                    alert('View: ' + view.name);
//                    \$(this).css('border-color', 'red');
              ";
        // line 289
        echo "            selectable: true,
            selectHelper: true,
            select: function(start, end) {
//                    var title = prompt('Event Title:');
//                    var eventData;
//                    if (title) {
//                        eventData = {
//                            title: title,
//                            start: start,
//                            end: end
//                        };
//                        \$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
//                    }
            \$.modal({
            content:'<strong>Start:</strong>' + start.format('DD-MM-YYYY HH:mm') + '<br>'
                    + '<strong>End:</strong>' + end.format('DD-MM-YYYY HH:mm')
//                        '<strong>Titre :</strong>'+ start.title+'<br>'+
//                        '<strong>Date :</strong>'+event.start.format()
                    ,
                    title: 'Détails du rendez-vous',
                    width: 300,
                    scrolling: false,
                    actions: {
                    'fermer' : {
                    color: 'red',
                            click: function(win) { win.closeModal(); }
                    }
                    },
                    buttons: {
                    'Nouveau rendez-Vous': {
                    classes:\t' blue-gradient glossy ',
                            click:\t\tfunction(win) {
                            document.location.href = \"";
        // line 321
        echo $this->env->getExtension('routing')->getPath("startEnd");
        echo "/\" + start.format('DD-MM-YYYY') + \"/\" + start.format('HH:mm') + \"/\" + end.format('HH:mm');
                                    win.closeModal(); }
                    },
                   'Nouveau Delimiteur': {
                                classes:\t' blue-gradient glossy ',
                                 click:\t\tfunction(win) {
                                    document.location.href = \"";
        // line 327
        echo $this->env->getExtension('routing')->getPath("ajoutDelimiteurCalendar");
        echo "/\"+start.format('DD-MM-YYYY')+\"/\"+start.format('HH:mm')+\"/\"+end.format('HH:mm');
                                    win.closeModal(); }
                    }, 
                            'annuler': {
                            classes:\t' blue-gradient glossy ',
                                    click:\t\tfunction(win) {
                                ";
        // line 334
        echo "                                    win.closeModal();
                                    }
                            }
                    },
                    buttonsLowPadding: true
            });
                    \$('#calendar').fullCalendar('unselect');
            },
            eventDrop:function(event){
            var start = event.start.format('DD-MM-YYYY');
                    var startH = event.start.format('HH:mm');
                    var endH = event.end.format('HH:mm');
                    //   alert(event.title + \" was dropped on \" + event.start.format());
                    \$.ajax({
                    type: 'get',
                            url : '";
        // line 349
        echo $this->env->getExtension('routing')->getPath("dropDelimiteur");
        echo "' + event.id + '/' + start + '/' + startH + '/' + endH,
                            beforeSend: function(){
                            notify('Rendez-Vous déplacé!', 'Date de dépalcement :' + start + '<br>' + 'Heure début: ' + startH + '<br>' + 'Heure fin: ' + endH, {groupSimilar: false});
                                    console.log('droped');
                            },
                            success:function(data){

                            console.log('updaate!!');
                            }
                    });
                    \$('#calendar').fullCalendar('refetchEvents');
            },
            eventResize: function(event) {
            var start = event.start.format('DD-MM-YYYY');
                    var startH = event.start.format('HH:mm');
                    var endH = event.end.format('HH:mm');
                    //   alert(event.title + \" was dropped on \" + event.start.format());
                    \$.ajax({
                    type: 'get', //type request
                            url : 'http://localhost/monagenda/web/app_dev.php/rendezVous/drop/' + event.id + '/' + start + '/' + startH + '/' + endH, //route de la methode à executer
                            beforeSend: function(){// là on peut mettre une action à executer avant l'envoi des données....
                            notify('Rendez-Vous modifié!', 'Date:' + start + '<br>' + 'Heure début: ' + startH + '<br>' + 'Heure fin: ' + endH, {groupSimilar: false});
                                    console.log('resize init');
                            },
                            success:function(data){//si tout va bien...la fonction au sein du \"success\" sera exécutée...
                            console.log('resized!!');
                            }
                    });
                    \$('#calendar').fullCalendar('refetchEvents');
            }
  });
   ";
        // line 391
        echo "   

});</script>
<script>
            function createAnnulation()
                    {
                    // on sécurise les données
                    var phraseusuelle = encodeURIComponent(\$('#phraseusuelle').val());
                            var entete = encodeURIComponent(\$('#entete').val());
                            var myEntete = encodeURIComponent(\$('#myEntete').val());
                            var myAlert = encodeURIComponent(\$('#myAlert[]').val());
                            var clientAlert = encodeURIComponent(\$('#clientAlert[]').val());
                            var report = encodeURIComponent(\$('#report').val());
   ";
        // line 411
        echo "                    // on vérifie que les variables ne sont pas vides
                    console.log('befor success');
                            \$.ajax({
                            url: 'http://localhost/monagenda/web/app_dev.php/rendezVous/annulation/' + ev, // on donne l'URL du fichier de traitement
                                    type: \"POST\", // la requête est de type POST
                                    data: \"phraseusuelle=\" + phraseusuelle + \"&entete=\" + entete + \"&myEntete=\" + myEntete + \"&myAlert=\" + myAlert + \"&clientAlert=\" + clientAlert + \"&report=\" + report, // et on envoie nos données
                                    success: function(data) {
                                    console.log('success');
                                            //console.log(data.nom);
                                            // \$('#pff').append('<option value=\"' + data.id + '\" >' + data.nom + ' ' + data.prenom + '</option>');
                                            //document.querySelector('#client [value=\"' + data.id + '\"]').selected = true;
                                            //\$('#pff').change();
                                            console.log('success fermer');
                                    }
                            });
                            \$('#modals').remove();
                            }
</script>

<style>

    body {
        margin: 40px 10px;
        padding: 0;
        font-family: \"Lucida Grande\",Helvetica,Arial,Verdana,sans-serif;
        font-size: 14px;
    }

    #calendar {
        max-width: 2000px;
        margin: 0 auto;
    }

</style>

<button class=\"button icon-printer \" style=\"float:right\" id=\"my-button\" >GetDate</button>
<div id=\"haha\" style=\"float:right\"></div>
<button class=\"button icon-printer \" style=\"float:right\" onclick=\"print_calendar()\" >imprimer</button>
<div id=\"calendar\"></div>


<div id=\"eventContent\" title=\"Event Details\">
    <div id=\"eventInfo\"></div>
    <p><strong><a id=\"eventLink\" target=\"_blank\">Read More</a></strong></p>
</div>




";
    }

    public function getTemplateName()
    {
        return "DelimiteurBundle:Default:test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  269 => 411,  254 => 391,  220 => 349,  203 => 334,  194 => 327,  185 => 321,  151 => 289,  140 => 94,  136 => 92,  124 => 82,  105 => 64,  64 => 25,  56 => 11,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
