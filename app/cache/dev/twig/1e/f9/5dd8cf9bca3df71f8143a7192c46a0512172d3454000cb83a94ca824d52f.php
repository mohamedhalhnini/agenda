<?php

/* DelimiteurBundle:Default:fullCalendar.html.twig */
class __TwigTemplate_1ef95dd8cf9bca3df71f8143a7192c46a0512172d3454000cb83a94ca824d52f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "DelimiteurBundle:Default:fullCalendar.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <link href='";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/fullcalendar.css"), "html", null, true);
        echo "' rel='stylesheet' />
    <link href='";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/fullcalendar.print.css"), "html", null, true);
        echo "' rel='stylesheet' media='print' />
    <script src='";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/lib/moment.min.js"), "html", null, true);
        echo "'></script>
    <script src='";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/lib/jquery.min.js"), "html", null, true);
        echo "'></script>
    <script src='";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/fullcalendar.min.js"), "html", null, true);
        echo "'></script>
    <script src='";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/lang-all.js"), "html", null, true);
        echo "'></script>
    ";
        // line 11
        echo "   
    <p class=\"wrapped left-icon \">
        Choisis un type : 
    <select class=\"\" name=\"delimiteur\" id=\"delimiteur\" >
                            ";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["typeDelimiteurs"]) ? $context["typeDelimiteurs"] : $this->getContext($context, "typeDelimiteurs")));
        foreach ($context['_seq'] as $context["_key"] => $context["typedelimiteur"]) {
            // line 16
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "id", array()), "html", null, true);
            echo "\" style=\"background-color: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "couleur", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "nom", array()), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['typedelimiteur'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "                    </select>    
    </p>
      
    <script type=\"text/javascript\">

        \$(document).ready(function() {
             \$(\"#delimiteur\").change(function() {
            \$.ajax({
                type: 'get',
                url: 'http://localhost/myagenda/web/app_dev.php/delimiteur/select/' + \$(this).val(),
                beforeSend: function() {
                    console.log('me voila!!');
                },
                success: function(data) {
                    console.log('me voila3!!');

                   // \$(\"#titre\").val(data.nom);
                    //\$(\"#couleur\").val(data.couleur);
                    \$(\"#delimiteur\").css('background-color' , data.couleur);
                }




            });
        });

            \$('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'agendaWeek,basicWeek,agendaDay,basicDay'
                },
                  buttonText: {
               
                basicDay: 'Basic day',
                basicWeek:'Basic Week'
            },
                minTime:'07:00',
                maxTime: '18:00',
                lang: 'fr',
                editable: true,
                defaultView :'agendaWeek',
                axisFormat : 'HH:mm',
                slotDuration :'00:05:00',
                timeFormat: {
                    agenda : 'HH:mm'
                    },

                eventLimit: true, // allow \"more\" link when too many events
                eventSources: [
                    ";
        // line 69
        echo (isset($context["events"]) ? $context["events"] : $this->getContext($context, "events"));
        echo ",
                    ";
        // line 70
        echo (isset($context["rdvs"]) ? $context["rdvs"] : $this->getContext($context, "rdvs"));
        echo "     
                ],

                eventClick: //function(event, jsEvent, view) {

                    function openComplexModal(event)
                    {
                         \$.ajax({
                         type: 'get',
                         url: 'http://localhost/myagenda/web/app_dev.php/delimiteur/trouve/' + event.id,
                         beforeSend: function() {
                         console.log('me voila!!');
                                                },
                         success: function(data) {
                         if(data.blockage==1){
                             \$.modal({
                            content:


                                 '<strong>Titre :</strong>'+ event.title+'<br>'+
                                  '<strong>Date :</strong>'+event.start.format()


                            ,
                            title: 'Détails du Delimiteur',
                            width: 300,
                            scrolling: false,
                            actions: {
                                'fermer' : {
                                    color: 'red',
                                    click: function(win) { win.closeModal(); }
                                }
                            },
                            buttons: {
                                
                                'modifier': {
                                    classes:\t' blue-gradient glossy ',
                                    click:\t\tfunction(win) {
                                        
                                        document.location.href=\"";
        // line 109
        echo $this->env->getExtension('routing')->getPath("updateDelimiteur");
        echo "/\"+event.id;
                                                                     
                                        win.closeModal(); }
                                },
                                'annuler ce delimiteur': {
                                    classes:\t' blue-gradient glossy ',
                                    click:\t\tfunction(win) {
                                        
                                       document.location.href=\"";
        // line 117
        echo $this->env->getExtension('routing')->getPath("delete");
        echo "/\"+event.id;
                                        \$('#calendar').fullCalendar('removeEvents', event.id);
                                        win.closeModal(); }
                                }
                                
                                 
                            },
                            buttonsLowPadding: true
                        });
                         
        }
        else {
        \$.modal({
                            content:


                                 '<strong>Titre :</strong>'+ event.title+'<br>'+
                                  '<strong>Date :</strong>'+event.start.format()


                            ,
                            title: 'Détails du Delimiteur',
                            width: 300,
                            scrolling: false,
                            actions: {
                                'fermer' : {
                                    color: 'red',
                                    click: function(win) { win.closeModal(); }
                                }
                            },
                            buttons: {
                                
                                'modifier': {
                                    classes:\t' blue-gradient glossy ',
                                    click:\t\tfunction(win) {
                                        
                                        document.location.href=\"";
        // line 153
        echo $this->env->getExtension('routing')->getPath("updateDelimiteur");
        echo "/\"+event.id;
                                                                     
                                        win.closeModal(); }
                                },
                                'ajouter RDV': {
                                    classes:\t' blue-gradient glossy ',
                                    click:\t\tfunction(win) {
                                        
                                        document.location.href=\"";
        // line 161
        echo $this->env->getExtension('routing')->getPath("startEnd");
        echo "/\"+event.start.format('DD-MM-YYYY')+\"/\"+event.start.format('HH:mm')+\"/\"+event.end.format('HH:mm');
                                                                     
                                        win.closeModal(); }
                                },    
                                'annuler ce delimiteur': {
                                    classes:\t' blue-gradient glossy ',
                                    click:\t\tfunction(win) {
                                        
                                       document.location.href=\"";
        // line 169
        echo $this->env->getExtension('routing')->getPath("delete");
        echo "/\"+event.id;
                                        \$('#calendar').fullCalendar('removeEvents', event.id);
                                        win.closeModal(); }
                                }
                                
                                 
                            },
                            buttonsLowPadding: true
                        });
        }                 
                                                 }
                                             });
                        
                    },



                selectable: true,
                selectHelper: true,
                select: function(start, end) {
//                    var title = prompt('Event Title:');
//                    var eventData;
//                    if (title) {
//                        eventData = {
//                            title: title,
//                            start: start,
//                            end: end
//                        };
//                        \$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
//                    }
                    \$.modal({
                        content:'<strong>Start:</strong>'+start.format('DD-MM-YYYY HH:mm')+'<br>'
                               +'<strong>End:</strong>'+end.format('DD-MM-YYYY HH:mm')
//                        '<strong>Titre :</strong>'+ start.title+'<br>'+
//                        '<strong>Date :</strong>'+event.start.format()


                        ,
                        title: 'Détails du Delimiteur',
                        width: 300,
                        scrolling: false,
                        actions: {
                            'fermer' : {
                                color: 'red',
                                click: function(win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                            'Nouveau rendez-Vous': {
                                classes:\t' blue-gradient glossy ',
                                click:\t\tfunction(win) {
                                    document.location.href=\"";
        // line 220
        echo $this->env->getExtension('routing')->getPath("startEnd");
        echo "/\"+start.format('DD-MM-YYYY')+\"/\"+start.format('HH:mm')+\"/\"+end.format('HH:mm');
                                    win.closeModal(); }
                            },
                             'Nouveau Delimiteur avancée': {
                                classes:\t' blue-gradient glossy ',
                                 click:\t\tfunction(win) {
                                    document.location.href = \"";
        // line 226
        echo $this->env->getExtension('routing')->getPath("ajoutDelimiteurCalendar");
        echo "/\"+start.format('DD-MM-YYYY')+\"/\"+start.format('HH:mm')+\"/\"+end.format('HH:mm');
                                    win.closeModal(); }
                    },        
                        'Nouveau Delimiteur': {
                                classes:\t' blue-gradient glossy ',
                                 click:\t\tfunction(win) {
                                    document.location.href = \"";
        // line 232
        echo $this->env->getExtension('routing')->getPath("ajoutSimple");
        echo "/\"+start.format('DD-MM-YYYY')+\"/\"+start.format('HH:mm')+\"/\"+end.format('HH:mm')+\"/\"+\$(\"#delimiteur\").val();
                                    \$('#calendar').fullCalendar('refetchEvents');
                                    win.closeModal(); }
                    },        
                            'annuler': {
                                classes:\t' blue-gradient glossy ',
                                click:\t\tfunction(win) {
                                  
                                    win.closeModal(); }
                            }
                        },
                        buttonsLowPadding: true
                    });
                    \$('#calendar').fullCalendar('unselect');
                },
              eventDrop:function(event){
            var start = event.start.format('DD-MM-YYYY');
                    var startH = event.start.format('HH:mm');
                    var end = event.end.format('DD-MM-YYYY');
                    var endH = event.end.format('HH:mm');
                     // alert(event.title + \" was dropped on \" + event.start.format());
                    \$.ajax({
                       
                            type : 'get',
                            url : 'http://localhost/myagenda/web/app_dev.php/delimiteur/dropDel/' + event.id + '/' + start + '/' + startH + '/' + end+ '/' + endH,
                            beforeSend: function(){
                             notify('Délimiteur déplacé!', 'Date de dépalcement :'+start+'<br>'+'Heure début: '+startH+'<br>',{groupSimilar: false});
                            console.log('droped');
                            },
                            success:function(data){
                           
                           alert('hani dkhallttttt');
                            console.log('updaate!!');
                                    
                            }
                    });
                    \$('#calendar').fullCalendar('refetchEvents');
            }

            });

        });
        
    </script>
    
   
    <style>

        body {
            margin: 40px 10px;
            padding: 0;
            font-family: \"Lucida Grande\",Helvetica,Arial,Verdana,sans-serif;
            font-size: 14px;
        }

        #calendar {
            max-width: 2000px;
            margin: 0 auto;
        }

    </style>

    <div id=\"calendar\"></div>

    <div id=\"eventContent\" title=\"Event Details\">
        <div id=\"eventInfo\"></div>
        <p><strong><a id=\"eventLink\" target=\"_blank\">Read More</a></strong></p>
    </div>





";
    }

    public function getTemplateName()
    {
        return "DelimiteurBundle:Default:fullCalendar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  322 => 232,  313 => 226,  304 => 220,  250 => 169,  239 => 161,  228 => 153,  189 => 117,  178 => 109,  136 => 70,  132 => 69,  79 => 18,  66 => 16,  62 => 15,  56 => 11,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
