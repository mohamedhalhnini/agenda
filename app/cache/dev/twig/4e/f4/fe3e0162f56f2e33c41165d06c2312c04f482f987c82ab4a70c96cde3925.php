<?php

/* RendezVousBundle:Default:annulerRdvv.html.twig */
class __TwigTemplate_4ef4fe3e0162f56f2e33c41165d06c2312c04f482f987c82ab4a70c96cde3925 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("annulation", array("id" => $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "id", array()))), "html", null, true);
        echo "\">
                            <table class=\" table simple-table\" >
                                <thead ><tr><th scope=\"col\" width=\"30%\" colspan=\"2\" class=\"align-center\" >Annulation de rendez-vous</th></tr></thead>
                              
                            <tbody>
                            <tr>
                                <td style=\"width:10em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Rendez-vous </td>
                               
                                <td>
                                    <p style=\"font-size: 2àpx\"> <strong>de :</strong> ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "client", array()), "nom", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "client", array()), "prenom", array()), "html", null, true);
        echo "<br>
                                     <strong>du :</strong>";
        // line 11
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "datedebut", array()), "D-M-Y"), "html", null, true);
        echo " <strong> de </strong>";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "datedebut", array()), "H:i"), "html", null, true);
        echo "<strong> à </strong>";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "end", array()), "H:i"), "html", null, true);
        echo "<br>
                                     <strong>avec le client fréquent </strong> ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "client", array()), "nom", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "client", array()), "prenom", array()), "html", null, true);
        echo "<br>
                                     <strong>libellé :</strong> ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "libelle", array()), "html", null, true);
        echo "
                                   </p>
                                </td>
                            </tr>
                            
                            
                             <tr>
                                <td style=\"width:10em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Motif</td>
                                
                                <td>
                                   <p><select name=\"phraseusuelle\" class=\"select expandable-list\" style=\"width:30em\" >
                                       <option  >--Phrases useuelles--</option>
                                        ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["phrases"]) ? $context["phrases"] : $this->getContext($context, "phrases")));
        foreach ($context['_seq'] as $context["_key"] => $context["phrase"]) {
            // line 26
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["phrase"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["phrase"], "text", array()), "html", null, true);
            echo "</option>
                                       
                                          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phrase'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "                                    </select><br>
                                     <select name=\"entete\" class=\"select expandable-list\" style=\"width:30em\" >
                                        <option  >--Entétes--</option>
                                        ";
        // line 32
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 33
            echo "                                        <option value=\"entete-";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" >entete-";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "</option>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "
                                    </select></p>
                                  <textarea style=\"width: 30em\"class=\"input\" name=\"myEntete\">Default textarea</textarea>
                                </td>
                            </tr>
                           
                            
                            <tr>
                                <td style=\"width:10em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\" >Groupe</td>
                               
                                <td>
                                    <div class=\"columns\">
                                        <div class=\"one-columns\">
                                            <input  type=\"radio\" name=\"radio1\" id=\"radio_1\" value=\"non\" class=\"radio\"> <label for=\"NON\">Non</label>                  
                                        </div>       
                                        <div class=\"one-columns\">
                                        </div>
                                        <div class=\"one-columns\">
                                            <input  type=\"radio\" name=\"radio1\" id=\"radio_1\" value=\"non\" class=\"radio\"> <label for=\"NON\">Oui,conserver les infromations</label>                                                      
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                 <td style=\"width:10em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Alerter</td>
                                
                                  <td>
                                      <div class=\"columns\">
                                          <div class=\"one-columns\">
                                              le prssesseur de l'agenda :
                                          </div>
                                        <div class=\"one-columns\">     
                                            <input class=\"styled checkbox\" type=\"checkbox\" name=\"myAlert[]\" id=\"check-1\" value=\"1\"> par mail
                                        </div>       
                                        <div class=\"one-columns\">
                                              <input class=\"styled checkbox\" type=\"checkbox\" name=\"myAlert[]\" id=\"check-1\" value=\"2\"> par SMS
                                        </div>
                                        
                                    </div>
                                       <div class=\"columns\">
                                          <div class=\"one-columns\">
                                              le client fréquent :
                                          </div>
                                        <div class=\"one-columns\">     
                                              <input class=\"styled checkbox\" type=\"checkbox\" name=\"clientAlert[]\" id=\"check-1\" value=\"1\"> par mail
                                        </div>       
                                        <div class=\"one-columns\">
                                              <input class=\"styled checkbox\" type=\"checkbox\" name=\"clientAlert[]\" id=\"check-1\" value=\"2\"> par SMS
                                        </div>
                                       
                                    </div>
                                </td>
                                </tr>
                                
                               <tr>
                                <td style=\"width:10em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Créer</td>
                                
                                <td>
                                    <div class=\"columns\">
                                        <div class=\"one-columns\">
                                             Considérer l'annulation comme transmise :
                                        </div>
                                        <div class=\"one-columns\">
                                            <input  type=\"radio\" name=\"report\" id=\"radio_1\" value=\"0\" class=\"radio\"> <label for=\"NON\">Non</label>                  
                                        </div>       
                                        <div class=\"one-columns\">
                                        </div>
                                        <div class=\"one-columns\">
                                            <input  type=\"radio\" name=\"report\" id=\"radio_1\" value=\"1\" class=\"radio\"> <label for=\"NON\">Oui </label>                                                      
                                        </div>
                                    </div>
                                </td>
                            </tr>
                             <tfoot>
                        <tr>
                            <td colspan=\"2\" class=\"align-center\">
                                 <div class=\"columns\">
                                    <div class=\"three-columns\">
                                    </div>
                                    <div class=\"four-columns\">
                                        <button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\">Annuler ce rendez-vous</button>
                                    </div>
                                    <div class=\"tow-columns\">
                                        <button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\">Fermer cette fenetre</button>
                                    </div>
                                </div>
                          
                            </td>
                        </tr>
                    </tfoot>
                            </table>
</form>";
    }

    public function getTemplateName()
    {
        return "RendezVousBundle:Default:annulerRdvv.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 35,  92 => 33,  88 => 32,  83 => 29,  71 => 26,  67 => 25,  52 => 13,  46 => 12,  38 => 11,  32 => 10,  19 => 1,);
    }
}
