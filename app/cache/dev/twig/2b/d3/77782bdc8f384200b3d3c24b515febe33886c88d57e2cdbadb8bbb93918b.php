<?php

/* InstructionBundle:Instruction:listPunctual.html.twig */
class __TwigTemplate_2bd377782bdc8f384200b3d3c24b515febe33886c88d57e2cdbadb8bbb93918b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo " 
               <div id=\"sorting-advanced_wrapper\" class=\"dataTables_wrapper\" role=\"grid\">
        ";
        // line 21
        echo "        <table class=\"table responsive-table responsive-table-on dataTable\" id=\"sorting-advanced\" aria-describedby=\"sorting-advanced_info\">

            <thead>
                <tr role=\"row\">
                    <th scope=\"col\" class=\"sorting_disabled\" role=\"columnheader\" rowspan=\"1\" colspan=\"1\" aria-label=\"\" style=\"width: 12px;\">
                        <input type=\"checkbox\" name=\"check-all\" id=\"check-all\" value=\"1\"></th>
                    <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Id</th>
                    <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Titre</th>
                    <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Priorite</th>
                    <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Date Le</th>
                    <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">Client</th>
                    <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\">PhraseUseuelle</th>
    ";
        // line 33
        if (((isset($context["archived"]) ? $context["archived"] : $this->getContext($context, "archived")) == 1)) {
            echo " 
                <th scope=\"col\" width=\"15%\" class=\"hide-on-tablet sorting\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"sorting-advanced\" rowspan=\"1\" colspan=\"1\" aria-label=\"Tags: activate to sort column ascending\" style=\"width: 125px;\"> date d'archivation</th>
                ";
        }
        // line 36
        echo "           <th scope=\"col\" width=\"60\" class=\"align-center sorting_disabled\" role=\"columnheader\" rowspan=\"1\" colspan=\"1\" aria-label=\"Actions\" style=\"width: 100px;\">Actions</th>

        </tr>            </thead>

            <tfoot>
               ";
        // line 44
        echo "            </tfoot>

               
                <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\">
                ";
        // line 48
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ponctualInstructions"]) ? $context["ponctualInstructions"] : $this->getContext($context, "ponctualInstructions")));
        foreach ($context['_seq'] as $context["_key"] => $context["instruction"]) {
            // line 49
            echo "                    <tr class=\"odd\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "id", array()), "html", null, true);
            echo "\">
                        <th scope=\"row\" class=\"checkbox-cell  sorting_1\"><input type=\"checkbox\" name=\"checked[]\" id=\"check-1\" value=\"1\"></th>
                        <td>";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "id", array()), "html", null, true);
            echo "  </td>
                        <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "titre", array()), "html", null, true);
            echo "</td>
                         <td> ";
            // line 53
            if (($this->getAttribute($context["instruction"], "priorite", array()) == 0)) {
                echo " Haute";
            } elseif (($this->getAttribute($context["instruction"], "priorite", array()) == 1)) {
                echo "Normale ";
            }
            echo "</td>
                           <td>";
            // line 54
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["instruction"], "le", array()), "d/m/Y"), "html", null, true);
            echo "</td>
                        <td>";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["instruction"], "client", array()), "nom", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["instruction"], "client", array()), "prenom", array()), "html", null, true);
            echo "</td>
                        <td> ";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["instruction"], "phraseusuelle", array()), "text", array()), "html", null, true);
            echo "</td>
                ";
            // line 57
            if (((isset($context["archived"]) ? $context["archived"] : $this->getContext($context, "archived")) == 1)) {
                echo " <td> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["instruction"], "datearchivation", array()), "d-m-Y"), "html", null, true);
                echo "</td> ";
            }
            // line 58
            echo "
                        <td><a class=\"confirm\" href=\"";
            // line 59
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("instruction_update", array("id" => $this->getAttribute($context["instruction"], "id", array()))), "html", null, true);
            echo "\"><span class=\"button  icon-pencil \"></span> </a> 
                            <a  onclick=\"openDeleteConfirm(";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "id", array()), "html", null, true);
            echo ")\" >   <span class=\"button icon-trash \"></span> </a>
                         ";
            // line 61
            if (((isset($context["archived"]) ? $context["archived"] : $this->getContext($context, "archived")) == 0)) {
                echo " 
                            <a  onclick=\"openArchivateConfirm(";
                // line 62
                echo twig_escape_filter($this->env, $this->getAttribute($context["instruction"], "id", array()), "html", null, true);
                echo ")\" >   <span class=\"button icon-box \"></span></a>
                        ";
            }
            // line 64
            echo "                        </td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['instruction'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "
            </tbody>
        </table>
       ";
        // line 80
        echo "    </div>          
               
               
";
    }

    public function getTemplateName()
    {
        return "InstructionBundle:Instruction:listPunctual.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 80,  130 => 67,  122 => 64,  117 => 62,  113 => 61,  109 => 60,  105 => 59,  102 => 58,  96 => 57,  92 => 56,  86 => 55,  82 => 54,  74 => 53,  70 => 52,  66 => 51,  60 => 49,  56 => 48,  50 => 44,  43 => 36,  37 => 33,  23 => 21,  19 => 2,);
    }
}
