<?php

/* ::base.html.twig */
class __TwigTemplate_2bea1321e38701a360e551870b75f151d2eddee843179c4d2db00ea6de136afb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'meta' => array($this, 'block_meta'),
            'title' => array($this, 'block_title'),
            'stylesheet' => array($this, 'block_stylesheet'),
            'mesScript' => array($this, 'block_mesScript'),
            'favicon' => array($this, 'block_favicon'),
            'js_head' => array($this, 'block_js_head'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
            'js_footer' => array($this, 'block_js_footer'),
            'my_script' => array($this, 'block_my_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>

<!--[if IEMobile 7]><html class=\"no-js iem7 oldie\"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class=\"no-js ie7 oldie\" lang=\"en\"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class=\"no-js ie8 oldie\" lang=\"en\"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class=\"no-js ie9\" lang=\"en\"><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)]><!-->
<html class=\"no-js\" lang=\"en\">
    <!--<![endif]-->

    <head>
    ";
        // line 12
        $this->displayBlock('meta', $context, $blocks);
        // line 35
        echo "


";
        // line 38
        $this->displayBlock('stylesheet', $context, $blocks);
        // line 72
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("datetimepicker-master/jquery.datetimepicker.js"), "html", null, true);
        echo "\"></script>
  

    ";
        // line 75
        $this->displayBlock('mesScript', $context, $blocks);
        // line 76
        echo "
        <link rel=\"stylesheet\" href=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/glDatePicker/developr.fixed.css?v=1"), "html", null, true);
        echo "\">

    ";
        // line 79
        $this->displayBlock('favicon', $context, $blocks);
        // line 117
        echo "    ";
        $this->displayBlock('js_head', $context, $blocks);
        // line 123
        echo "    </head>
";
        // line 124
        $this->displayBlock('body', $context, $blocks);
        // line 451
        echo "</html>
";
    }

    // line 12
    public function block_meta($context, array $blocks = array())
    {
        // line 13
        echo "        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>";
        // line 15
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">

        <!-- http://davidbcalhoun.com/2010/viewport-metatag -->
        <meta name=\"HandheldFriendly\" content=\"True\">
        <meta name=\"MobileOptimized\" content=\"320\">
        <!-- Microsoft clear type rendering -->
        <meta http-equiv=\"cleartype\" content=\"on\">

        <!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
        <meta name=\"application-name\" content=\"Developr Admin Skin\">
        <meta name=\"msapplication-tooltip\" content=\"Cross-platform admin template.\">
        <meta name=\"msapplication-starturl\" content=\"http://www.display-inline.fr/demo/developr\">
        <!-- These custom tasks are examples, you need to edit them to show actual pages -->
        <meta name=\"msapplication-task\" content=\"name=Agenda;action-uri=http://www.display-inline.fr/demo/developr/agenda.html;icon-uri=http://www.display-inline.fr/demo/developr/";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/favicon.ico"), "html", null, true);
        echo "\">
        <meta name=\"msapplication-task\" content=\"name=My profile;action-uri=http://www.display-inline.fr/demo/developr/profile.html;icon-uri=http://www.display-inline.fr/demo/developr/";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/favicon.ico"), "html", null, true);
        echo "\">
        <!-- http://www.kylejlarson.com/blog/2012/iphone-5-web-design/ and http://darkforge.blogspot.fr/2010/05/customize-android-browser-scaling-with.html -->
";
        // line 34
        echo "    ";
    }

    // line 15
    public function block_title($context, array $blocks = array())
    {
        echo "M-Agenda ";
    }

    // line 38
    public function block_stylesheet($context, array $blocks = array())
    {
        // line 39
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/glDatePicker/developr.fixed.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/modal.css?v=1"), "html", null, true);
        echo "\">

        <!-- For all browsers -->
        <link rel=\"stylesheet\" href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/reset.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/style.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/colors.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"print\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/print.css?v=1"), "html", null, true);
        echo "\">
        <!-- For progressively larger displays -->
        <link rel=\"stylesheet\" media=\"only all and (min-width: 480px)\" href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/480.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"only all and (min-width: 768px)\" href=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/768.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"only all and (min-width: 992px)\" href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/992.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"only all and (min-width: 1200px)\" href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/1200.css?v=1"), "html", null, true);
        echo "\">
        <!-- For Retina displays -->
        <link rel=\"stylesheet\" media=\"only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)\" href=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/2x.css?v=1"), "html", null, true);
        echo "\">
        <!-- Webfonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>

        <!-- Additional styles -->
        <link rel=\"stylesheet\" href=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/table.css?v=1"), "html", null, true);
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/agenda.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/dashboard.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/form.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/modal.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/progress-slider.css?v=1"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/css/styles/switches.css?v=1"), "html", null, true);
        echo "\">

        <!-- DataTables -->
        <link rel=\"stylesheet\" href=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/DataTables/jquery.dataTables.css?v=1"), "html", null, true);
        echo "\">
        <script src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/modernizr.custom.js"), "html", null, true);
        echo "\"></script>

    ";
    }

    // line 75
    public function block_mesScript($context, array $blocks = array())
    {
    }

    // line 79
    public function block_favicon($context, array $blocks = array())
    {
        // line 80
        echo "        <!-- For Modern Browsers -->
        <link rel=\"shortcut icon\" href=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/favicon.png"), "html", null, true);
        echo "\">
        <!-- For everything else -->
        <link rel=\"shortcut icon\" href=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/favicon.ico"), "html", null, true);
        echo "\">

        <!-- iOS web-app metas -->
        <meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
        <meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">

        <!-- iPhone ICON -->
        <link rel=\"apple-touch-icon\" href=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/apple-touch-icon-ipad-retina.png"), "html", null, true);
        echo "\" sizes=\"144x144\">

        <!-- iPhone SPLASHSCREEN (320x460) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/iphone.png"), "html", null, true);
        echo "\" media=\"(device-width: 320px)\">
        <!-- iPhone (Retina) SPLASHSCREEN (640x960) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/iphone-retina.png"), "html", null, true);
        echo "\" media=\"(device-width: 320px) and (-webkit-device-pixel-ratio: 2)\">
        <!-- iPhone 5 SPLASHSCREEN (640×1096) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/iphone5.png"), "html", null, true);
        echo "\" media=\"(device-height: 568px) and (-webkit-device-pixel-ratio: 2)\">
        <!-- iPad (portrait) SPLASHSCREEN (748x1024) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/ipad-portrait.png"), "html", null, true);
        echo "\" media=\"(device-width: 768px) and (orientation: portrait)\">
        <!-- iPad (landscape) SPLASHSCREEN (768x1004) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/ipad-landscape.png"), "html", null, true);
        echo "\" media=\"(dimg/favicons/apple-touch-icon.png')}}\" sizes=\"57x57\">
        <!-- iPad ICON -->
        <link rel=\"apple-touch-icon\" href=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/apple-touch-icon-ipad.png"), "html", null, true);
        echo "\" sizes=\"72x72\">
        <!-- iPhone (Retina) ICON -->
        <link rel=\"apple-touch-icon\" href=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/favicons/apple-touch-icon-retina.png"), "html", null, true);
        echo "\" sizes=\"114x114\">
        <!-- iPad (Retina) ICON -->
        <link rel=\"apple-touch-icon\" href=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/evice-width: 768px) and (orientation: landscape"), "html", null, true);
        echo "\">
        <!-- iPad (Retina, portrait) SPLASHSCREEN (2048x1496) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/ipad-portrait-retina.png"), "html", null, true);
        echo "\" media=\"(device-width: 1536px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2)\">
        <!-- iPad (Retina, landscape) SPLASHSCREEN (1536x2008) -->
        <link rel=\"apple-touch-startup-image\" href=\"";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/splash/ipad-landscape-retina.png"), "html", null, true);
        echo "\" media=\"(device-width: 1536px)  and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2)\">
        <!-- JavaScript at bottom except for Modernizr -->
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("datetimepicker-master/jquery.datetimepicker.css"), "html", null, true);
        echo "\" >


    ";
    }

    // line 117
    public function block_js_head($context, array $blocks = array())
    {
        // line 118
        echo "        <!-- JavaScript at the bottom for fast page loading -->
        <script src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/jquery-1.10.2.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/jquery.tablesorter.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/DataTables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    // line 124
    public function block_body($context, array $blocks = array())
    {
        // line 125
        echo "    <body class=\"clearfix with-menu with-shortcuts\">

        <!-- Prompt IE 6 users to install Chrome Frame -->
        <!--[if lt IE 7]><p class=\"message red-gradient simpler\">Your browser is <em>ancient!</em> <a href=\"http://browsehappy.com/\">Upgrade to a different browser</a> or <a href=\"http://www.google.com/chromeframe/?redirect=true\">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

        <!-- Title bar -->
        <header role=\"banner\" id=\"title-bar\">
            <h2>M-Agenda</h2>
        </header>

        <!-- Button to open/hide menu -->
        <a href=\"#\" id=\"open-menu\"><span>Menu</span></a>

        <!-- Button to open/hide shortcuts -->
        <a href=\"#\" id=\"open-shortcuts\"><span class=\"icon-thumbs\"></span></a>

    <!-- Main content -->
        <!-- Main content -->
        <section role=\"main\" id=\"main\">



            <hgroup id=\"main-title\" class=\"thin\">
                <h1>Dashboard</h1>
                <h2>nov <strong>10</strong></h2>
            </hgroup>


            <div class=\"with-padding\">

                    ";
        // line 155
        $this->displayBlock('content', $context, $blocks);
        // line 159
        echo "
   
    <!-- End main content -->

    <!-- Side tabs shortcuts -->
    <ul id=\"shortcuts\" role=\"complementary\" class=\"children-tooltip tooltip-right\">
        <li class=\"current\"><a href=\"default\" class=\"shortcut-dashboard\" title=\"Dashboard\">Dashboard</a></li>
        <li><a href=\"inbox.html\" class=\"shortcut-messages\" title=\"Messages\">Messages</a></li>
        <li><a href=\"agenda.html\" class=\"shortcut-agenda\" title=\"Agenda\">Agenda</a></li>
        <li><a href=\"tables.html\" class=\"shortcut-contacts\" title=\"Contacts\">Contacts</a></li>
        <li><a href=\"explorer.html\" class=\"shortcut-medias\" title=\"Medias\">Medias</a></li>
        <li><a href=\"sliders.html\" class=\"shortcut-stats\" title=\"Stats\">Stats</a></li>
        <li class=\"at-bottom\"><a href=\"form.html\" class=\"shortcut-settings\" title=\"Settings\">Settings</a></li>
        <li><span class=\"shortcut-notes\" title=\"Notes\">Notes</span></li>
    </ul>

    <!-- Side tabs shortcuts with legends under the icons -->
    <!-- <ul id=\"shortcuts\" role=\"complementary\" class=\"children-tooltip tooltip-right with-legend\">
        <li class=\"current\"><a href=\"./\" class=\"shortcut-dashboard\" title=\"Dashboard\"><span class=\"shortcut-legend\">Dashboard</span></a></li>
        <li><a href=\"inbox.html\" class=\"shortcut-messages\" title=\"Messages\"><span class=\"shortcut-legend\">Messages</span></a></li>
        <li><a href=\"agenda.html\" class=\"shortcut-agenda\" title=\"Agenda\"><span class=\"shortcut-legend\">Agenda</span></a></li>
        <li><a href=\"tables.html\" class=\"shortcut-contacts\" title=\"Contacts\"><span class=\"shortcut-legend\">Contacts</span></a></li>
        <li><a href=\"explorer.html\" class=\"shortcut-medias\" title=\"Medias\"><span class=\"shortcut-legend\">Medias</span></a></li>
        <li><a href=\"sliders.html\" class=\"shortcut-stats\" title=\"Stats\"><span class=\"shortcut-legend\">Stats</span></a></li>
        <li class=\"at-bottom\"><a href=\"form.html\" class=\"shortcut-settings\" title=\"Settings\"><span class=\"shortcut-legend\">Settings</span></a></li>
        <li><span class=\"shortcut-notes\" title=\"Notes\"><span class=\"shortcut-legend\">Notes</span></span></li>
    </ul> -->

    <!-- Sidebar/drop-down menu -->
    <section id=\"menu\" role=\"complementary\">

        <!-- This wrapper is used by several responsive layouts -->
        <div id=\"menu-content\">
        ";
        // line 192
        if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
            echo " 
            <header>
                Administrator
            </header>
        ";
        } elseif ($this->env->getExtension('security')->isGranted("ROLE_MEDECIN")) {
            // line 197
            echo "            <header>
            MEDECIN
            </header>
            ";
        } elseif ($this->env->getExtension('security')->isGranted("ROLE_SECRETAIRE")) {
            // line 201
            echo "            <header>
            SECRETAIRE
            </header>
            ";
        }
        // line 204
        echo "    
            <div id=\"profile\">
                <img src=\"";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/user.png"), "html", null, true);
        echo "\" width=\"64\" height=\"64\" alt=\"User name\" class=\"user-icon\">
                
";
        // line 209
        echo "            </div>

        </section>
        <!-- End main content -->

        <!-- Side tabs shortcuts -->
        <ul id=\"shortcuts\" role=\"complementary\" class=\"children-tooltip tooltip-right\">
            <li class=\"current\"><a href=\"default\" class=\"shortcut-dashboard\" title=\"Dashboard\">Dashboard</a></li>
            <li><a href=\"inbox.html\" class=\"shortcut-messages\" title=\"Messages\">Messages</a></li>
            <li><a href=\"agenda.html\" class=\"shortcut-agenda\" title=\"Agenda\">Agenda</a></li>
            <li><a href=\"tables.html\" class=\"shortcut-contacts\" title=\"Contacts\">Contacts</a></li>
            <li><a href=\"explorer.html\" class=\"shortcut-medias\" title=\"Medias\">Medias</a></li>
            <li><a href=\"sliders.html\" class=\"shortcut-stats\" title=\"Stats\">Stats</a></li>
            <li class=\"at-bottom\"><a href=\"form.html\" class=\"shortcut-settings\" title=\"Settings\">Settings</a></li>
            <li><span class=\"shortcut-notes\" title=\"Notes\">Notes</span></li>
        </ul>

        <!-- Side tabs shortcuts with legends under the icons -->
        <!-- <ul id=\"shortcuts\" role=\"complementary\" class=\"children-tooltip tooltip-right with-legend\">
            <li class=\"current\"><a href=\"./\" class=\"shortcut-dashboard\" title=\"Dashboard\"><span class=\"shortcut-legend\">Dashboard</span></a></li>
            <li><a href=\"inbox.html\" class=\"shortcut-messages\" title=\"Messages\"><span class=\"shortcut-legend\">Messages</span></a></li>
            <li><a href=\"agenda.html\" class=\"shortcut-agenda\" title=\"Agenda\"><span class=\"shortcut-legend\">Agenda</span></a></li>
            <li><a href=\"tables.html\" class=\"shortcut-contacts\" title=\"Contacts\"><span class=\"shortcut-legend\">Contacts</span></a></li>
            <li><a href=\"explorer.html\" class=\"shortcut-medias\" title=\"Medias\"><span class=\"shortcut-legend\">Medias</span></a></li>
            <li><a href=\"sliders.html\" class=\"shortcut-stats\" title=\"Stats\"><span class=\"shortcut-legend\">Stats</span></a></li>
            <li class=\"at-bottom\"><a href=\"form.html\" class=\"shortcut-settings\" title=\"Settings\"><span class=\"shortcut-legend\">Settings</span></a></li>
            <li><span class=\"shortcut-notes\" title=\"Notes\"><span class=\"shortcut-legend\">Notes</span></span></li>
        </ul> -->

        <!-- Sidebar/drop-down menu -->
        <section id=\"menu\" role=\"complementary\">

            <!-- This wrapper is used by several responsive layouts -->
            <div id=\"menu-content\">

                <header>
                    Administrator
                </header>

                <div id=\"profile\">
                    <img src=\"";
        // line 249
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/img/user.png"), "html", null, true);
        echo "\" width=\"64\" height=\"64\" alt=\"User name\" class=\"user-icon\">
                    Hello
                    <span class=\"name\">halhnini <b>mohammed</b></span>
                </div>

                <!-- By default, this section is made for 4 icons, see the doc to learn how to change this, in \"basic markup explained\" -->
                <ul id=\"access\" class=\"children-tooltip\">
                    <li><a href=\"inbox.html\" title=\"Messages\"><span class=\"icon-inbox\"></span><span class=\"count\">2</span></a></li>
                    <li><a href=\"calendars.html\" title=\"Calendar\"><span class=\"icon-calendar\"></span></a></li>
                    <li><a href=\"login.html\" title=\"Profile\"><span class=\"icon-user\"></span></a></li>
                    <li class=\"disabled\"><span class=\"icon-gear\"></span></li>
                </ul>

                <section class=\"navigable\">
                    <ul class=\"big-menu\">
                        <li class=\"with-right-arrow\">
                            <span><span class=\"list-count\">2</span>Gestion Utilisateur</span>
                            <ul class=\"big-menu\">
";
        // line 268
        echo "                                <li><a href=\"columns.html\">Liste des Utilisateur</a></li>
                           ";
        // line 273
        echo "                           ";
        // line 283
        echo "                          ";
        // line 290
        echo "                       ";
        // line 291
        echo "                            </ul>
                        </li>
                        <li class=\"with-right-arrow\">
                            <span><span class=\"list-count\">8</span>Main features</span>
                            <ul class=\"big-menu\">
                                <li><a href=\"auto-setup.html\">Automatic setup</a></li>
                                <li><a href=\"responsive.html\">Responsiveness</a></li>
                                <li><a href=\"tabs.html\">Tabs</a></li>
                                <li><a href=\"sliders.html\">Slider &amp; progress</a></li>
                                <li><a href=\"modals.html\">Modal windows</a></li>
                                <li class=\"with-right-arrow\">
                                    <span><span class=\"list-count\">3</span>Messages &amp; notifications</span>
                                    <ul class=\"big-menu\">
                                        <li><a href=\"messages.html\">Messages</a></li>
                                        <li><a href=\"notifications.html\">Notifications</a></li>
                                        <li><a href=\"tooltips.html\">Tooltips</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class=\"with-right-arrow\">
                            <a href=\"ajax-demo/submenu.html\" class=\"navigable-ajax\" title=\"Menu title\">With ajax sub-menu</a>
                        </li>
                    </ul>
                </section>

                <ul class=\"unstyled-list\">
                    <li class=\"title-menu\">Now's event</li>
                    <li>
                        <ul class=\"calendar-menu\">
                            <li>
                                <a href=\"#\" title=\"See event\">
                                    <time datetime=\"2011-02-24\"><b>24</b> Feb</time>
                                    <small class=\"green\">10:30</small>
                                    Event's description
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class=\"title-menu\">New messages</li>
                    <li>
                        <ul class=\"message-menu\">
                            <li>
                                <span class=\"message-status\">
                                    <a href=\"#\" class=\"starred\" title=\"Starred\">Starred</a>
                                    <a href=\"#\" class=\"new-message\" title=\"Mark as read\">New</a>
                                </span>
                                <span class=\"message-info\">
                                    <span class=\"blue\">17:12</span>
                                    <a href=\"#\" class=\"attach\" title=\"Download attachment\">Attachment</a>
                                </span>
                                <a href=\"#\" title=\"Read message\">
                                    <strong class=\"blue\">John Doe</strong><br>
                                    <strong>Mail subject</strong>
                                </a>
                            </li>
                            <li>
                                <a href=\"#\" title=\"Read message\">
                                    <span class=\"message-status\">
                                        <span class=\"unstarred\">Not starred</span>
                                        <span class=\"new-message\">New</span>
                                    </span>
                                    <span class=\"message-info\">
                                        <span class=\"blue\">15:47</span>
                                    </span>
                                    <strong class=\"blue\">May Starck</strong><br>
                                    <strong>Mail subject a bit longer</strong>
                                </a>
                            </li>
                            <li>
                                <span class=\"message-status\">
                                    <span class=\"unstarred\">Not starred</span>
                                </span>
                                <span class=\"message-info\">
                                    <span class=\"blue\">15:12</span>
                                </span>
                                <strong class=\"blue\">May Starck</strong><br>
                                Read message
                            </li>
                        </ul>
                    </li>
                </ul>

            </div>
            <!-- End content wrapper -->

            <!-- This is optional -->
            <footer id=\"menu-footer\">
                <p class=\"button-height\">
                    <input type=\"checkbox\" name=\"auto-refresh\" id=\"auto-refresh\" checked=\"checked\" class=\"switch float-right\">
                    <label for=\"auto-refresh\">Auto-refresh</label>
                </p>
            </footer>

        </section>
        <!-- End sidebar/drop-down menu -->

    ";
        // line 388
        $this->displayBlock('js_footer', $context, $blocks);
        // line 426
        echo "
    ";
        // line 427
        $this->displayBlock('my_script', $context, $blocks);
        // line 447
        echo "
    </body>

";
    }

    // line 155
    public function block_content($context, array $blocks = array())
    {
        // line 156
        echo "

                    ";
    }

    // line 388
    public function block_js_footer($context, array $blocks = array())
    {
        // line 389
        echo "



        <!-- Scripts -->

        <script src=\"";
        // line 395
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/setup.js"), "html", null, true);
        echo "\"></script>
      

        <!-- Template functions -->
        <script src=\"";
        // line 399
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.input.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 400
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.message.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 401
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.modal.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 402
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.navigable.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 403
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.notify.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 404
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.scroll.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 405
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.progress-slider.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 406
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.tooltip.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 407
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.confirm.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 408
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.agenda.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 409
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.tabs.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 410
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.table.js"), "html", null, true);
        echo "\"></script>
        <!-- Must be loaded last -->
        <script src=\"";
        // line 412
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/google-code-prettify/prettify.js?v=1"), "html", null, true);
        echo "\"></script>                                       <!-- glDatePicker -->
        <script src=\"";
        // line 413
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/glDatePicker/glDatePicker.min.js?v=1"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 414
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("datetimepicker-master/jquery.datetimepicker.js"), "html", null, true);
        echo "\"></script>

        <!-- Tinycon -->
        <script src=\"";
        // line 417
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/tinycon.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 418
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/formValidator/jquery.validationEngine.js?v=1"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 419
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"), "html", null, true);
        echo "\"></script>
         <!-- form wizard -->
\t<script src=\"";
        // line 421
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("developr/js/developr.wizard.js"), "html", null, true);
        echo "\"></script>
        


    ";
    }

    // line 427
    public function block_my_script($context, array $blocks = array())
    {
        // line 428
        echo "        \t<script>
            // Call template init (optional, but faster if called manually)
            \$.template.init();

            // Demo Iframe loading
            function openIframe()
            {
                \$.modal({
                    content: '<div style=\"line-height: 25px; padding: 0 0 10px\"><span id=\"modal-status\">Contacting server...</span><br><span id=\"modal-progress\">0%</span></div>',
                    title: 'Iframe content',
                    url: 'http://www.envato.com',
                    useIframe: true,
                    width: 600,
                    height: 400
                });
            }

    </script>
    ";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  736 => 428,  733 => 427,  724 => 421,  719 => 419,  715 => 418,  711 => 417,  705 => 414,  701 => 413,  697 => 412,  692 => 410,  688 => 409,  684 => 408,  680 => 407,  676 => 406,  672 => 405,  668 => 404,  664 => 403,  660 => 402,  656 => 401,  652 => 400,  648 => 399,  641 => 395,  633 => 389,  630 => 388,  624 => 156,  621 => 155,  614 => 447,  612 => 427,  609 => 426,  607 => 388,  508 => 291,  506 => 290,  504 => 283,  502 => 273,  499 => 268,  478 => 249,  436 => 209,  431 => 206,  427 => 204,  421 => 201,  415 => 197,  407 => 192,  372 => 159,  370 => 155,  338 => 125,  335 => 124,  329 => 121,  325 => 120,  321 => 119,  318 => 118,  315 => 117,  307 => 113,  302 => 111,  297 => 109,  292 => 107,  287 => 105,  282 => 103,  277 => 101,  272 => 99,  267 => 97,  262 => 95,  257 => 93,  251 => 90,  241 => 83,  236 => 81,  233 => 80,  230 => 79,  225 => 75,  218 => 69,  214 => 68,  208 => 65,  204 => 64,  200 => 63,  196 => 62,  192 => 61,  188 => 60,  183 => 58,  175 => 53,  170 => 51,  166 => 50,  162 => 49,  158 => 48,  153 => 46,  149 => 45,  145 => 44,  141 => 43,  135 => 40,  130 => 39,  127 => 38,  121 => 15,  117 => 34,  112 => 31,  108 => 30,  90 => 15,  86 => 13,  83 => 12,  78 => 451,  76 => 124,  73 => 123,  70 => 117,  68 => 79,  63 => 77,  60 => 76,  58 => 75,  51 => 72,  49 => 38,  44 => 35,  42 => 12,  29 => 1,);
    }
}
