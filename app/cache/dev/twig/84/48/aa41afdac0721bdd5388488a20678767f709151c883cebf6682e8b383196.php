<?php

/* RendezVousBundle:RdvClient:fullCalendar.html.twig */
class __TwigTemplate_8448aa41afdac0721bdd5388488a20678767f709151c883cebf6682e8b383196 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base2.html.twig", "RendezVousBundle:RdvClient:fullCalendar.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<link href='";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/fullcalendar.css"), "html", null, true);
        echo "' rel='stylesheet' />
<link href='";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/fullcalendar.print.css"), "html", null, true);
        echo "' rel='stylesheet' media='print' />
<script src='";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/lib/moment.min.js"), "html", null, true);
        echo "'></script>
<script src='";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/lib/jquery.min.js"), "html", null, true);
        echo "'></script>
<script src='";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/fullcalendar.min.js"), "html", null, true);
        echo "'></script>
<script src='";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fullCalendar/lang-all.js"), "html", null, true);
        echo "'></script>
    ";
        // line 11
        echo "<script type=\"text/javascript\">
            function print_calendar()
            {
            window.print();
            }

    ";
        // line 25
        echo "
    \$(document).ready(function() {
myStart ='';

    \$('#calendar').hide();
            \$('#calendar').fullCalendar({
    header: {
    left: 'prev,next today',
            center: 'title'


    },
            minTime: '09:00',
            maxTime: '11:30',
            lang: 'fr',
            editable: false,
            defaultView: 'agendaWeek',
            axisFormat: 'HH:mm',
            slotDuration: '00:30:00',
            height: 250,
            selectable: true,
            timeFormat: {
            agenda: 'HH:mm'
            },
            eventLimit: true, // allow \"more\" link when too many events
            // events: ouf,

            //***********************myAjax*********************************    

            //******************************************************    

            eventSources: [
               ";
        // line 57
        echo (isset($context["events"]) ? $context["events"] : $this->getContext($context, "events"));
        echo "
            ],
            eventRender: function(event, element, merde) {
  ";
        // line 74
        echo "                  ";
        // line 80
        echo "            },
            eventClick: //function(event, jsEvent, view) {
//
//                    alert('Event: ' +event.id+ event.start.format() + event.end.format() );
//                    alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//                    alert('View: ' + view.name);
//                    \$(this).css('border-color', 'red');
              ";
        // line 88
        echo "
    // change the border color just for fun

    function openComplexModal(event)
    {
    ev = event.id;
            console.log(ev);
            \$.modal({
            content:
                    '<strong>Titre :</strong>' + event.title + '<br>' +
                    '<strong>Description :</strong>' + event.description + '<br>' +
                    '<strong>Date :</strong>' + event.start.format()
                    ,
                    title: 'Détails du rendez-vous',
                    width: 300,
                    scrolling: false,
                    actions: {
                    'fermer': {
                    color: 'red',
                            click: function(win) {
                            win.closeModal();
                            }
                    }
                    },
                    buttons: {
                    'modifier': {
                    classes: ' blue-gradient glossy ',
                            click: function(win) {
                            document.location.href = \"";
        // line 116
        echo $this->env->getExtension('routing')->getPath("modifier");
        echo "/\" + event.id;
                                    win.closeModal();
                            }
                    },
                            'supprimer': {
                            classes: ' blue-gradient glossy ',
                                    click: function(win) {
                                    \$.ajax({
                                    type: 'get',
                                            url: 'http://localhost/monagenda/web/app_dev.php/rendezVous/remove/' + event.id,
                                            beforeSend: function() {
                                            console.log('je me supprime mtn!!');
                                            },
                                            success: function(data) {
                                            console.log('c fait!');
                                            }
                                    });
                                            \$('#calendar').fullCalendar('removeEvents', event.id);
                                            win.closeModal();
                                    }
                            },
                            'annuler ce rendez-vous': {
                            classes: ' blue-gradient glossy ',
                                    click: function(win) {
                                   ";
        // line 141
        echo "                                    win.closeModal();
                                            \$.ajax({
                                            type: 'get',
                                                    url: 'http://localhost/monagenda/web/app_dev.php/rendezVous/getClient/' + event.id,
                                                    beforeSend: function() {
                                                    console.log('me voila!!');
                                                    },
                                                    success: function(data) {
                                                    console.log('cool!!');
                                                            ev = event.id;
                                                            \$.modal({
                                                            title: 'Annuler ce rendez-vous',
                                                                    content: ''
                                                                    + '<table class=\" table simple-table\" >'
                                                                    + '<thead >'
                                                                    + '<tr>'
                                                                    + '<th scope=\"col\" width=\"30%\" colspan=\"2\" class=\"align-center\" >Annulation de rendez-vous</th>'
                                                                    + '</tr>'
                                                                    + '</thead>'
                                                                    + '<tbody>'
                                                                    + '<tr>'
                                                                    + '<td style=\"width:10em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Rendez-vous </td>'
                                                                    + '<td>'
                                                                    + '<p style=\"font-size: 2àpx\">' + ' <strong>de :</strong>' + data.nom + '<br>'
                                                                    + '<strong>du :</strong>' + event.start.format() + '<strong> à </strong>' + event.end.format() + '<br>'
                                                                    + '<strong>avec le client fréquent </strong>' + data.prenom + ' ' + data.nom + '<br>'
                                                                    + '<strong>libellé :</strong>' + event.title
                                                                    + '</p>'
                                                                    + '</td>'
                                                                    + '</tr>'
                                                                    + '<tr>'
                                                                    + '<td style=\"width:10em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Motif</td>'
                                                                    + '<td>'
                                                                    + '<p><select name=\"phraseusuelle\" id=\"phraseusuelle\" class=\"select expandable-list\" style=\"width:30em\" >'
                                                                    + '<option  >--Phrases useuelles--</option>'
                                                                    + '<option value=\"\"></option>'
                                                                    + '</select><br>'
                                                                    + '<select name=\"entete\" id=\"entete\" class=\"select expandable-list\" style=\"width:30em\" >'
                                                                    + '<option  >--Entétes--</option>'
                                                                    + '<option value=\"Entete1\" >Entéte1</option>'
                                                                    + '</select></p>'
                                                                    + '<textarea style=\"width: 30em\"class=\"input\" name=\"myEntete\" id=\"myEntete\">Default textarea</textarea>'
                                                                    + '</td>'
                                                                    + '</tr>'
                                                                    + '<tr>'
                                                                    + '<td style=\"width:10em; text-align:center; margin-top:20px\" width=\"40%\" class=\"vertical-center\" >Groupe</td>'
                                                                    + '<td>'
                                                                    + '<div class=\"columns\">'
                                                                    + '<div class=\"one-columns\">'
                                                                    + '<input  type=\"radio\" name=\"radio1\" id=\"radio_1\" value=\"0\" class=\"radio\"> <label for=\"NON\">Non</label>'
                                                                    + '</div>'
                                                                    + '<div class=\"one-columns\">'
                                                                    + '</div>'
                                                                    + '<div class=\"one-columns\">'
                                                                    + '<input  type=\"radio\" name=\"radio1\" id=\"radio_1\" value=\"1\" class=\"radio\"> <label for=\"NON\">Oui,conserver les infromations</label>'
                                                                    + '</div>'
                                                                    + '</div>'
                                                                    + '</td>'
                                                                    + '</tr>'
                                                                    + '<tr>'
                                                                    + '<td style=\"width:10em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Alerter</td>'
                                                                    + '<td>'
                                                                    + '<div class=\"columns\">'
                                                                    + '<div class=\"one-columns\">'
                                                                    + 'le prssesseur de l\\'agenda :'
                                                                    + '</div>'
                                                                    + '<div class=\"one-columns\">'
                                                                    + '<input class=\"styled checkbox\" type=\"checkbox\" name=\"myAlert[]\" id=\"myAlert\" value=\"1\"> par mail'
                                                                    + '</div>'
                                                                    + '<div class=\"one-columns\">'
                                                                    + '<input class=\"styled checkbox\" type=\"checkbox\" name=\"myAlert[]\" id=\"myAlert\" value=\"2\"> par SMS'
                                                                    + '</div>'
                                                                    + '</div>'
                                                                    + '<div class=\"columns\">'
                                                                    + '<div class=\"one-columns\">'
                                                                    + 'le client fréquent :'
                                                                    + '</div>'
                                                                    + '<div class=\"one-columns\">'
                                                                    + '<input class=\"styled checkbox\" type=\"checkbox\" name=\"clientAlert[]\" id=\"clientAlert\" value=\"1\"> par mail'
                                                                    + '</div>'
                                                                    + '<div class=\"one-columns\">'
                                                                    + '<input class=\"styled checkbox\" type=\"checkbox\" name=\"clientAlert[]\" id=\"clientAlert\" value=\"2\"> par SMS'
                                                                    + '</div>'
                                                                    + '</div>'
                                                                    + '</td>'
                                                                    + '</tr>'
                                                                    + '<tr>'
                                                                    + '<td style=\"width:10em; text-align:center; margin-top:20px \" width=\"40%\" class=\"vertical-center\">Créer</td>'
                                                                    + '<td>'
                                                                    + '<div class=\"columns\">'
                                                                    + '<div class=\"one-columns\">'
                                                                    + 'Considérer l\\'annulation comme transmise :'
                                                                    + '</div>'
                                                                    + '<div class=\"one-columns\">'
                                                                    + ' <input  type=\"radio\" name=\"report\" id=\"report\" value=\"0\" class=\"radio\"><label for=\"NON\">Non</label> '
                                                                    + '</div>'
                                                                    + '<div class=\"one-columns\">'
                                                                    + '</div>'
                                                                    + '<div class=\"one-columns\">'
                                                                    + '<input  type=\"radio\" name=\"report\" id=\"report\" value=\"1\" class=\"radio\"><label for=\"NON\">Oui </label>'
                                                                    + '</div>'
                                                                    + '</div>'
                                                                    + '</td>'
                                                                    + '</tr>'
                                                                    + '<tfoot>'
                                                                    + '<tr>'
                                                                    + '<td colspan=\"2\" class=\"align-center\">'
                                                                    + '<div class=\"columns\">'
                                                                    + '<div class=\"three-columns\">'
                                                                    + '</div>'
                                                                    + '<div class=\"four-columns\">'
                                                                    + '<button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\" onclick=\"createAnnulation()\">Annuler ce rendez-vous</button>'
                                                                    + '</div>'
                                                                    + '<div class=\"tow-columns\">'
                                                                    + '<button type=\"submit\" class=\"button blue-gradient\" value=\"Ajout\">Fermer cette fenetre</button>'
                                                                    + '</div>'
                                                                    + '</div>'

                                                                    + '</td>'
                                                                    + '</tr>'
                                                                    + '</tfoot>'
                                                                    + '</table>',
                                                                    buttonsAlign: 'center',
                                                                    resizable: false,
                                                                    width: 600,
                                                                    height: 465,
                                                            });
                                                    },
                                            });
                                    }
                            }
                    },
                    buttonsLowPadding: true
            });
    },
            selectHelper: true,
            select: function(start, end) {

                    myDate = start.format('DD-MM-YYYY');
                    myStart = start.format('HH:mm');
                    myEnd = end.format('HH:mm');
//                    var title = prompt('Event Title:');
//                    var eventData;
//                    if (title) {
//                        eventData = {
//                            title: title,
//                            start: start,
//                            end: end
//                        };
//                        \$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
//                    }
                    \$.modal({
                    content: 'Confirmez-vous vouloir prendre rendez-vous avec Mohamed Rachidi de' + start.format('HH:mm') + 'à' + end.format('HH:mm'),
                            title: 'Détails du rendez-vous',
                            width: 300,
                            scrolling: false,
                            actions: {
                            'fermer': {
                            color: 'red',
                                    click: function(win) {
                                    win.closeModal();
                                    }
                            }
                            },
                            buttons: {
                            'Oui': {
                            classes: ' blue-gradient glossy ',
                                    click: function(win) {
                                          
                                    console.log('buton oui avant!!');
                                 

    \$.ajax({
    type: 'get',
            url: 'http://localhost/monagenda/web/app_dev.php/rendezVous/duree/' + \$(\"#myRdvType\").val(),
            beforeSend: function() {
            //\$('.blabla').parent().append('<span class=\"loader big working\" id=\"ouf\"></span>');
            console.log('me voila!!');
            },
            success: function(data) {
            merde = data.myTime;
                    //\$('#ouf').remove();
                    //\$(\"#html5colorpicker\").val(data.color);
                    \$.ajax({
                    type: 'get',
                            url: 'http://localhost/monagenda/web/app_dev.php/rendezVous/ajax/'+start.format('HH:mm')+'/'+ merde,
                            beforeSend: function() {
                            console.log('!yees!');
                            },
                            success: function(data) {

                          var myVar = data.idiot;
                                 createRdv(myVar,start.format('DD-MM-YYYY'));
                                    console.log(myVar);
                                    console.log('haha');
                            }
                    });
                    console.log('okii');
            }
    });
   
                                    
                                           
                                            console.log('buton oui action!!');
                                        win.closeModal();
                                            console.log('buton oui après!!');
                                    }
                            },
                                    'Non': {
                                    classes: ' blue-gradient glossy ',
                                            click: function(win) {
                                            win.closeModal();
                                            }
                                    }
                            },
                            buttonsLowPadding: true
                    });
                    \$('#calendar').fullCalendar('unselect');
            }
    });
   ";
        // line 372
        echo "    \$('#myPicker').datetimepicker({
    weeks: true,
            scrollInput: true,
            timepicker: false,
            datepicker: true,
            format: 'Y-m-d',
            onSelectDate: function(ct, inst) {
            var d = new Date(ct.dateFormat('Y-m-d'));
                    \$('#calendar').show();
                    \$('#calendar').fullCalendar('gotoDate', d);
            }
    });
            
    });</script>
<script>
            function createRdv(a,b)
            {
            // on sécurise les données
            var maDate = encodeURIComponent(myDate);
                    var monStart = encodeURIComponent(b);
                    var monEnd = encodeURIComponent(a);
                    var monRdvType = encodeURIComponent(\$('#myRdvType').val());
                    var client = encodeURIComponent(";
        // line 394
        echo twig_escape_filter($this->env, (isset($context["clientId"]) ? $context["clientId"] : $this->getContext($context, "clientId")), "html", null, true);
        echo ");
                    var medecin = encodeURIComponent(\$('#medecin').val());
   ";
        // line 403
        echo "            // on vérifie que les variables ne sont pas vides
            console.log('befor success');
                    \$.ajax({
                    url: \"";
        // line 406
        echo $this->env->getExtension('routing')->getPath("createRdvClient");
        echo "\", // on donne l'URL du fichier de traitement
                            type: \"POST\", // la requête est de type POST
                            data: \"myDate=\" + maDate + \"&myStart=\" + monStart + \"&myEnd=\" + monEnd + \"&myRdvType=\" + monRdvType + \"&client=\" + client + \"&medecin=\" + medecin, // et on envoie nos données
                            success: function(data) {
                            console.log('success');
                                    //console.log(data.nom);
                                    // \$('#pff').append('<option value=\"' + data.id + '\" >' + data.nom + ' ' + data.prenom + '</option>');
                                    //document.querySelector('#client [value=\"' + data.id + '\"]').selected = true;
                                    //\$('#pff').change();
                                    console.log('success fermer');
                            }
                    });
                    \$('#modals').remove();
            }
</script>

<style>

    body {
        margin: 40px 10px;
        padding: 0;
        font-family: \"Lucida Grande\",Helvetica,Arial,Verdana,sans-serif;
        font-size: 14px;
    }

    #calendar {
        max-width: 2000px;
        margin: 0 auto;
    }

</style>

<h4>Nested tabs</h4>

<div class=\"side-tabs same-height\">
    <ul class=\"tabs\">
        <li class=\"active\"><a href=\"#maintab-1\">Voir mes rendez-vous</a></li>
        <li><a href=\"#maintab-2\">Prendre un rendez-vous</a></li>

    </ul>

    <div class=\"tabs-content\">

        <div id=\"maintab-1\" class=\"with-padding\">

            <div class=\"standard-tabs same-height inner-tabs\">

                <ul class=\"tabs\" >
                    <li class=\"active\"><a href=\"#subtab-1\">Passés<span class=\"count\">";
        // line 454
        echo twig_escape_filter($this->env, (isset($context["countPasse"]) ? $context["countPasse"] : $this->getContext($context, "countPasse")), "html", null, true);
        echo "</span></a>
                    </li>
                    <li><a href=\"#subtab-2\">Futurs<span class=\"count green-gradient\">";
        // line 456
        echo twig_escape_filter($this->env, (isset($context["countFutur"]) ? $context["countFutur"] : $this->getContext($context, "countFutur")), "html", null, true);
        echo "</span></a></li>
                    <li><a href=\"#subtab-3\">Tous<span class=\"count blue-gradient\">";
        // line 457
        echo twig_escape_filter($this->env, (isset($context["tout"]) ? $context["tout"] : $this->getContext($context, "tout")), "html", null, true);
        echo "</span></a></li>

                </ul>

                <div class=\"tabs-content\">

                    <div id=\"subtab-1\" class=\"with-padding\"><ul class=\"bullet-list\">
                                                                                    ";
        // line 464
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clientEventsPasse"]) ? $context["clientEventsPasse"] : $this->getContext($context, "clientEventsPasse")));
        foreach ($context['_seq'] as $context["_key"] => $context["clientEvent"]) {
            // line 465
            echo "                                                                                    ";
            if (($this->getAttribute($context["clientEvent"], "annuler", array()) == 1)) {
                // line 466
                echo "                            <li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["clientEvent"], "libelle", array()), "html", null, true);
                echo " de ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["clientEvent"], "datedebut", array()), "H:i"), "html", null, true);
                echo " à ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["clientEvent"], "end", array()), "H:i"), "html", null, true);
                echo " <strong>Annulé</strong></li>
                                                                                    ";
            } else {
                // line 468
                echo "                            <li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["clientEvent"], "libelle", array()), "html", null, true);
                echo " de ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["clientEvent"], "datedebut", array()), "H:i"), "html", null, true);
                echo " à ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["clientEvent"], "end", array()), "H:i"), "html", null, true);
                echo " </li>
                                                                                     ";
            }
            // line 470
            echo "                                                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clientEvent'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 471
        echo "                        </ul>


                    </div>

                    <div id=\"subtab-2\" class=\"with-padding\">

                        <ul class=\"bullet-list\">
                           ";
        // line 479
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clientEventsFutur"]) ? $context["clientEventsFutur"] : $this->getContext($context, "clientEventsFutur")));
        foreach ($context['_seq'] as $context["_key"] => $context["clientEvent"]) {
            // line 480
            echo "

\t\t\t\t\t  ";
            // line 482
            if (($this->getAttribute($context["clientEvent"], "annuler", array()) == 1)) {
                // line 483
                echo "                            <li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["clientEvent"], "libelle", array()), "html", null, true);
                echo " de ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["clientEvent"], "datedebut", array()), "H:i"), "html", null, true);
                echo " à ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["clientEvent"], "end", array()), "H:i"), "html", null, true);
                echo " <strong>Annulé</strong></li>
                                                                                    ";
            } else {
                // line 485
                echo "                            <li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["clientEvent"], "libelle", array()), "html", null, true);
                echo " de ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["clientEvent"], "datedebut", array()), "H:i"), "html", null, true);
                echo " à ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["clientEvent"], "end", array()), "H:i"), "html", null, true);
                echo " </li>
                                                                                     ";
            }
            // line 487
            echo "                                                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clientEvent'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 488
        echo "

                        </ul>



                    </div>

                    <div id=\"subtab-3\" class=\"with-padding\">

                        <ul class=\"bullet-list\">
                           ";
        // line 499
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["findAll"]) ? $context["findAll"] : $this->getContext($context, "findAll")));
        foreach ($context['_seq'] as $context["_key"] => $context["clientEvent"]) {
            // line 500
            echo "

\t\t\t\t\t  ";
            // line 502
            if (($this->getAttribute($context["clientEvent"], "annuler", array()) == 1)) {
                // line 503
                echo "                            <li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["clientEvent"], "libelle", array()), "html", null, true);
                echo " de ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["clientEvent"], "datedebut", array()), "H:i"), "html", null, true);
                echo " à ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["clientEvent"], "end", array()), "H:i"), "html", null, true);
                echo " <strong>Annulé</strong></li>
                                                                                    ";
            } else {
                // line 505
                echo "                            <li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["clientEvent"], "libelle", array()), "html", null, true);
                echo " de ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["clientEvent"], "datedebut", array()), "H:i"), "html", null, true);
                echo " à ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["clientEvent"], "end", array()), "H:i"), "html", null, true);
                echo " </li>
                                                                                     ";
            }
            // line 507
            echo "                                                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clientEvent'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 508
        echo "

                        </ul>

                    </div>

                </div>

            </div>

        </div>

        <div id=\"maintab-2\" class=\"with-padding\"><br><br>

            <p>Rendez-vous avec : <select name=\"medecin\" id=\"medecin\" class=\"select expandable-list\" style=\"width:40em\" >
                    <option >--Choisir un médecin--</option>
                                ";
        // line 524
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["utilisateurs"]) ? $context["utilisateurs"] : $this->getContext($context, "utilisateurs")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 525
            echo "                    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "nom", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "prenom", array()), "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 527
        echo "
                </select></p>
            <p>Type de Rendez-vous : <select name=\"myRdvType\" id=\"myRdvType\" class=\"select expandable-list\" style=\"width:40em\" >
                    <option >--Type de rendez-vous--</option>
                                ";
        // line 531
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["rdvTypes"]) ? $context["rdvTypes"] : $this->getContext($context, "rdvTypes")));
        foreach ($context['_seq'] as $context["_key"] => $context["rdvType"]) {
            // line 532
            echo "                    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["rdvType"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["rdvType"], "libelle", array()), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["rdvType"], "dureee", array()), "H:i"), "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rdvType'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 534
        echo "
                </select></p>
            <p>A partir du :<input id=\"myPicker\" type=\"text\" class=\"input\"></p>
            <div id=\"calendar\"></div>
        </div>



    </div>
</div>

</div>

</div>

</div>



";
        // line 559
        echo "





";
    }

    public function getTemplateName()
    {
        return "RendezVousBundle:RdvClient:fullCalendar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  705 => 559,  684 => 534,  671 => 532,  667 => 531,  661 => 527,  648 => 525,  644 => 524,  626 => 508,  620 => 507,  610 => 505,  600 => 503,  598 => 502,  594 => 500,  590 => 499,  577 => 488,  571 => 487,  561 => 485,  551 => 483,  549 => 482,  545 => 480,  541 => 479,  531 => 471,  525 => 470,  515 => 468,  505 => 466,  502 => 465,  498 => 464,  488 => 457,  484 => 456,  479 => 454,  428 => 406,  423 => 403,  418 => 394,  394 => 372,  172 => 141,  145 => 116,  115 => 88,  106 => 80,  104 => 74,  98 => 57,  64 => 25,  56 => 11,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
