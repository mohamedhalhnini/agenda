<?php

/* DelimiteurBundle:Default:ajoutTypeDelimiteur.html.twig */
class __TwigTemplate_ba452253c9a69d6b7503c19ad1bc7f6b0a77cc82c9234a5122453090ec36e0b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "DelimiteurBundle:Default:ajoutTypeDelimiteur.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "<hgroup id=\"main-title\" class=\"thin\">
    <h1>Ajouter un type</h1>
            </hgroup>
<form action=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("ajoutType");
        echo "\" method=\"POST\">
<div class=\"left-column-200px margin-bottom\" style=\"background-color:gainsboro\">

                    <div class=\"left-column\">
                        Type de delimiteur
                    </div>

                    <div class=\"right-column\">
                        <input type=\"text\" id=\"titre\" value=\"\" class=\"input\" name=\"nom\">
                    </div>

                </div>
<div class=\"left-column-200px margin-bottom\" style=\"background-color:gainsboro\">

                    <div class=\"left-column\">
                        couleur
                    </div>

                    <div class=\"right-column\">
                        <input type=\"color\" id=\"titre\" value=\"\" class=\"input\" name=\"couleur\">
                    </div>

                </div>
    <button type=\"submit\" class=\"button glossy mid-margin-right\">
                        <span class=\"button-icon\"><span class=\"icon-tick\"></span></span>
                        enregistrer
                    </button>
</form>
<br><br>
 <table class=\"simple-table responsive-table\" id=\"sorting-example2 center\" width=\"800px\">

                    <thead>
                        <tr>
                            <th scope=\"col\">Titre delimiteur</th>
                            <th scope=\"col\">couleur</th>
                            <th scope=\"col\" width=\"120\" class=\"align-right\">Actions</th>
                        </tr>
                    </thead>


                    <tbody>
                                    ";
        // line 49
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["typedelimiteurs"]) ? $context["typedelimiteurs"] : $this->getContext($context, "typedelimiteurs")));
        foreach ($context['_seq'] as $context["_key"] => $context["typedelimiteur"]) {
            // line 50
            echo "                        <tr>
                            <th scope=\"row\">
\t\t\t\t\t\t\t";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "nom", array()), "html", null, true);
            echo "<br>
                            </th>
                            <td style=\"background-color: ";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "couleur", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "couleur", array()), "html", null, true);
            echo "</td>
                            <td class=\"align-right vertical-center\">
                                <span class=\"button-group compact\">
                                    <a onclick=\"openIframe(";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "id", array()), "html", null, true);
            echo ");\" class=\"button icon-pencil confirm\" title=\"Update\" ></a>

";
            // line 60
            echo "                                    <a href=\"#\" class=\"button icon-gear with-tooltip\" title=\"Other actions\"></a>
                                    <a href=\"";
            // line 61
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("delete", array("id" => $this->getAttribute($context["typedelimiteur"], "id", array()))), "html", null, true);
            echo "\" class=\"button icon-trash with-tooltip confirm\" title=\"Delete\"></a>
                                </span>
                            </td>
                        </tr>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['typedelimiteur'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "                    </tbody>

                </table>

<script>
    function openIframe(id)
        {
        \$.modal({
                title: 'Modal window',
                url : \"http://localhost/monagenda/web/app_dev.php/delimiteur/updateType/\"+id,
                ";
        // line 98
        echo "                buttonsAlign: 'center',
                resizable: true,
                width: 600,
                height: 600
            });
        }
</script>
";
    }

    public function getTemplateName()
    {
        return "DelimiteurBundle:Default:ajoutTypeDelimiteur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 98,  120 => 66,  109 => 61,  106 => 60,  101 => 57,  93 => 54,  88 => 52,  84 => 50,  80 => 49,  36 => 8,  31 => 5,  28 => 4,  11 => 1,);
    }
}
