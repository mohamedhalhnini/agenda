<?php

/* DelimiteurBundle:Default:list.html.twig */
class __TwigTemplate_f183afa1f420fbb72dc23c8055ccdf7678e2ec693479a413150b3fc1a5ec5c7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "DelimiteurBundle:Default:list.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'js_content' => array($this, 'block_js_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "           

                <p class=\"wrapped left-icon icon-info-round\">
                    Tables are responsive, too! Try resizing your browser
                </p>
                <table class=\"table responsive-table\" id=\"sorting-advanced\">

\t\t\t\t<thead>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th scope=\"col\">Text</th>
\t\t\t\t\t\t<th scope=\"col\">Debut</th>
\t\t\t\t\t\t<th scope=\"col\" width=\"15%\" class=\"align-center hide-on-mobile\">Utilisateur</th>
\t\t\t\t\t\t<th scope=\"col\" width=\"15%\" class=\"align-center hide-on-mobile-portrait\">Couleur</th>
\t\t\t\t\t\t<th scope=\"col\" width=\"15%\" class=\"hide-on-tablet\">Fin</th>
\t\t\t\t\t\t<th scope=\"col\" width=\"60\" class=\"align-center\">Actions</th>
\t\t\t\t\t</tr>
\t\t\t\t</thead>

\t\t\t\t<tfoot>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td colspan=\"6\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>
\t\t\t\t</tfoot>

\t\t\t\t<tbody>
                                    ";
        // line 31
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["delimiteurs"]) ? $context["delimiteurs"] : $this->getContext($context, "delimiteurs")));
        foreach ($context['_seq'] as $context["_key"] => $context["delimiteur"]) {
            // line 32
            echo "\t\t\t\t\t<tr>
\t\t\t\t\t\t<th scope=\"row\" class=\"checkbox-cell\">";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["delimiteur"], "titre", array()), "html", null, true);
            echo "</th>
\t\t\t\t\t\t<td>";
            // line 34
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["delimiteur"], "periodicite", array()), "effet", array()), "Y-m-d"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t<td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["delimiteur"], "periodicite", array()), "couleuralloccurence", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t<td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["delimiteur"], "periodicite", array()), "couleuralloccurence", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t<td>";
            // line 37
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["delimiteur"], "periodicite", array()), "end", array()), "Y-m-d"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t<td class=\"low-padding align-center\"> <span class=\"button-group compact\">
                                    <a href=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("updateDelimiteur", array("id" => $this->getAttribute($context["delimiteur"], "id", array()))), "html", null, true);
            echo "\" class=\"button icon-pencil confirm\" title=\"Update\"></a>

                               ";
            // line 42
            echo "                                   <a href=\"#\" class=\"button icon-gear with-tooltip\" title=\"Other actions\"></a>
                                    <a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("delete", array("id" => $this->getAttribute($context["delimiteur"], "id", array()))), "html", null, true);
            echo "\" class=\"button icon-trash with-tooltip confirm\" title=\"Delete\"></a>
                                </span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['delimiteur'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "\t\t\t\t\t
\t\t\t\t</tbody>

\t\t\t</table>

                ";
        // line 77
        echo "          ";
        // line 86
        echo "
";
    }

    // line 88
    public function block_js_content($context, array $blocks = array())
    {
        // line 89
        echo "   
                \t\t<script>

\t\t// Table sort - DataTables
\t\tvar table = \$('#sorting-advanced');
\t\ttable.dataTable({
\t\t\t'aoColumnDefs': [
\t\t\t\t{ 'bSortable': false, 'aTargets': [ 0, 5 ] }
\t\t\t],
\t\t\t'sPaginationType': 'full_numbers',
\t\t\t'sDom': '<\"dataTables_header\"lfr>t<\"dataTables_footer\"ip>',
\t\t\t'fnInitComplete': function( oSettings )
\t\t\t{
\t\t\t\t// Style length select
\t\t\t\ttable.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
\t\t\t\ttableStyled = true;
\t\t\t}
\t\t});


\t</script>
             
";
    }

    public function getTemplateName()
    {
        return "DelimiteurBundle:Default:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 89,  121 => 88,  116 => 86,  114 => 77,  107 => 47,  97 => 43,  94 => 42,  89 => 39,  84 => 37,  80 => 36,  76 => 35,  72 => 34,  68 => 33,  65 => 32,  61 => 31,  32 => 4,  29 => 3,  11 => 1,);
    }
}
