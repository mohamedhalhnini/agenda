<?php

/* DelimiteurBundle:Default:delimiteur.html.twig */
class __TwigTemplate_f3ebbb1b15a47a8ce5a8734412442d24c1e3c1ce8c53fae167b2681aaf65594a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "DelimiteurBundle:Default:delimiteur.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
    
        <form action=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("addDelimiteur");
        echo "\" method=\"POST\" class=\"twelve-columns twelve-columns-tablet twelve-columns-mobile\">
                <table class=\"table simple-table \" id=\"sorting-example1\">
\t\t\t\t<thead>
                <tr><th colspan=\"2\">Gestion des delimiteurs</th></tr>

\t\t\t\t</thead>
                <tbody>
\t\t\t\t<tr>
\t\t\t\t<div class=\"left-column-200px margin-bottom\">
<td>
                    <div class=\"left-column\">
                        choix du delimiteur
                    </div>
</td><td>
                    <div class=\"right-column\">
                        <select class=\"select\" name=\"delimiteur\" id=\"delimiteur\" >
                            ";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["typeDelimiteurs"]) ? $context["typeDelimiteurs"] : $this->getContext($context, "typeDelimiteurs")));
        foreach ($context['_seq'] as $context["_key"] => $context["typedelimiteur"]) {
            // line 23
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["typedelimiteur"], "nom", array()), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['typedelimiteur'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "                        </select>
                    </div>
</td>
                </div>
\t\t\t\t</tr><tr>
                <div class=\"left-column-200px margin-bottom\" >
<td>
                    <div class=\"left-column\">
                        titre du delimiteur
                    </div>
</td><td>
                    <div class=\"right-column\">
                        <input type=\"text\" id=\"titre\" value=\"\" class=\"input\" name=\"titre\">
                    </div>
</td>
                </div></tr>
\t\t\t\t<tr>
                <div class=\"left-column-200px margin-bottom\">
<td>
                    <div class=\"left-column\">
                        choix d'une plage 
                    </div>
</td><td>
                    <div class=\"right-column\">
                        <select class=\"select\">
                            <option>nouvelle plage</option>
                        </select>
                    </div>
</td>
                </div></tr>
\t\t\t\t<tr>
                <div class=\"left-column-200px margin-bottom\">
<td>
                    <div class=\"left-column\">
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                        Periodicite
                    </div>
</td><td>
                    <div class=\"right-column\">
                        
                        <input type=\"radio\" name=\"type\" id=\"type\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"1\" checked=\"checked\">
                        une fois
                        <br/>
                        <br/>
                        <br/>
                        <div class=\"left-column-200px margin-bottom\">

                            <div class=\"left-column\">
                                <input type=\"radio\" name=\"type\" id=\"type\" class=\"radio mid-margin-left\" id=\"button-radio-2\" value=\"2\">
                                quotidienne
                            </div>

                            <div class=\"right-column\">
                                <input type=\"radio\" name=\"quotidienne\" id=\"radio_2\" value=\"1\" class=\"radio\" > <label for=\"oui\">Tous les </label>
                                <input type=\"text\" value=\"1\" name=\"jours\">  jour(s) <br><br>
                                <input type=\"radio\" name=\"quotidienne\" id=\"radio_2\" value=\"2\" class=\"radio\"> <label for=\"oui\">Tous les jours ouvrables</label><br><br>                      
                                <input type=\"radio\" name=\"quotidienne\" id=\"radio_2\" value=\"3\" class=\"radio\"> <label for=\"oui\">Tous les jours selectionnأ©s :</label><br><br> 
                                <p>
                                    <input  class=\"checkbox\" type=\"checkbox\" name=\"checked[]\" id=\"lu\" value=\"1\"> LU



                                    <input class=\"checkbox\" type=\"checkbox\" name=\"checked[]\" id=\"ma\" value=\"2\"> MA


                                    <input class=\"checkbox\" type=\"checkbox\" name=\"checked[]\" id=\"me\" value=\"3\"> ME


                                    <input class=\"checkbox\" type=\"checkbox\" name=\"checked[]\" id=\"je\" value=\"4\"> Je


                                    <input class=\"checkbox\" type=\"checkbox\" name=\"checked[]\" id=\"ve\" value=\"5\"> Ve


                                    <input class=\"checkbox\" type=\"checkbox\" name=\"checked[]\" id=\"sa\" value=\"6\"> Sa

                                    <input class=\"checkbox\" type=\"checkbox\" name=\"checked[]\" id=\"di\" value=\"7\"> Di
                                </p>
                            </div>

                        </div>
                        <br/>
                        <div class=\"left-column-200px margin-bottom\">

                            <div class=\"left-column\">
                                <input type=\"radio\" name=\"type\" id=\"type\" class=\"radio mid-margin-left\" id=\"button-radio-3\" value=\"3\">
                                hebdomadaire
                            </div>

                            <div class=\"right-column\">
                                Tous les <select class=\"select expandable-list\" style=\"width:6em\" name=\"hebdomadaire\">

                                    <option value=\"1\">lundi</option>
                                    <option value=\"2\">mardi</option>
                                    <option value=\"3\">mercredi</option>
                                    <option value=\"4\">jeudi</option>
                                    <option value=\"5\">vendredi</option>
                                    <option value=\"6\">samedi</option></select>
                            </div>

                        </div>
                        <br/>
                        
                        
                        
                    </div>

                </div>
\t\t\t\t</tr>
\t\t\t\t<tr>
                <div class=\"left-column-200px margin-bottom\" style=\"background-color:white\">
<td>
                    <div class=\"left-column\">
                        Plage de periodicite
                    </div>
</td><td>
                    <div class=\"right-column\">
                        prend effet le <span class=\"input\">
                            <span class=\"icon-calendar\"></span>
                            <input type=\"text\" name=\"prendEffet\" id=\"datetimepicker\" style=\"width:5em\" class=\"input-unstyled datepicker gldp-el\" value=\"";
        // line 151
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["startD"]) ? $context["startD"] : $this->getContext($context, "startD")), "d/m/Y"), "html", null, true);
        echo "\" gldp-id=\"gldp-9964084781\">
                        </span><br/>
                        <br/>
                        <br/>
                        <div class=\"left-column-200px margin-bottom\">

                            <div class=\"left-column\">
                                Et prend fin
                            </div>

                            <div class=\"right-column\">
                                <input type=\"radio\" name=\"prendFin\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"1\" checked=\"checked\">apres 
                                <input type=\"text\" id=\"login\" name=\"occurences\" value=\"1\" style=\"width:2em\" class=\"input\">occurence(s)<br/><br/>
                                <input type=\"radio\" name=\"prendFin\" class=\"radio mid-margin-left\" id=\"button-radio-2\" value=\"2\">
                                le<span class=\"input\">
                                    <span class=\"icon-calendar\"></span>
                                    <input type=\"text\" name=\"dateFin\" id=\"datetimepicker\" class=\"input-unstyled datepicker gldp-el\" value=\"";
        // line 167
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["startD"]) ? $context["startD"] : $this->getContext($context, "startD")), "d/m/Y"), "html", null, true);
        echo "\" gldp-id=\"gldp-9964084782\">
                                </span>
                            </div>

                        </div>
                    </div>
</td>
                </div>
\t\t\t\t</tr>
\t\t\t\t<tr>
                <div class=\"left-column-200px margin-bottom\">
<td>
                    <div class=\"left-column\">
                        Duree
                    </div>
</td><td>
                    <div class=\"right-column\">
                        debute 
                        <input type=\"time\" name=\"debute\" value=\"";
        // line 185
        echo twig_escape_filter($this->env, (isset($context["startH"]) ? $context["startH"] : $this->getContext($context, "startH")), "html", null, true);
        echo "\">
                        et prend fin a 
                        <input type=\"time\" name=\"fin\" value=\"";
        // line 187
        echo twig_escape_filter($this->env, (isset($context["endH"]) ? $context["endH"] : $this->getContext($context, "endH")), "html", null, true);
        echo "\">
                        <div id=\"colorSelector\"><div style=\"background-color: rgb(74, 74, 161);\"></div></div>
                    </div>
</td>
                </div>
\t\t\t\t</tr>
\t\t\t\t<tr>
                <div class=\"left-column-200px margin-bottom\">
<td>
                    <div class=\"left-column\">
                        Couleur
                    </div>
</td><td>
                    <div class=\"right-column\">
                        <select class=\"select\">
                            <option>Defaut</option>

                        </select>&nbsp;
                        <input type=\"color\" name=\"couleur\" class=\"input\" id=\"couleur\">
                    </div>
</td>
                </div></tr>
\t\t\t\t<tr>
                <div class=\"left-column-200px margin-bottom\">
<td>
                    <div class=\"left-column\">
                        Partage
                    </div>
</td><td>
                    <div class=\"right-column\">
                        <input type=\"radio\" name=\"partage\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"1\">
                        Public <br/><input type=\"radio\" name=\"partage\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"0\">
                        Privee(mention \"occuppee\"dans le partage du planing)
                    </div>
</td>
                </div></tr>
\t\t\t\t<tr>
                <div class=\"left-column-200px margin-bottom\">
<td>
                    <div class=\"left-column\">
                        effet lors de la recherche de plages libres
                    </div>
</td><td>
                    <div class=\"right-column\">
                        <input type=\"radio\" name=\"blockage\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"0\">
                        non bloquant(cette plage sera considere comme libre) <br/><input type=\"radio\" name=\"blockage\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"1\">
                        Bloquant(cette plage sera considere comme occuppee)
                    </div>
</td>
                </div></tr>
\t\t\t\t<tr>
                <div class=\"left-column-200px margin-bottom\">
<td>
                    <div class=\"left-column\">
                        Associer
                    </div>
</td><td>
                    <div class=\"right-column\">
                        avec un rendez-vous    <select class=\"select\">
                            <option>aucun</option>

                        </select>
                    </div>
</td>
                </div></tr>
\t\t\t\t<tr>
                <div class=\"left-column-200px margin-bottom\">
<td>
                    <div class=\"left-column\">
                        plage accessible a la prise de rendez-vous par internet
                    </div>
</td><td>
                    <div class=\"right-column\">
                        <input type=\"radio\" name=\"accessInternet\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"0\">
                        non <input type=\"radio\" name=\"accessInternet\" class=\"radio mid-margin-left\" id=\"button-radio-1\" value=\"1\">
                        oui
                    </div>
</td>
                </div></tr>
\t\t\t\t<tr>
                <div class=\"left-column-200px margin-bottom\" >
<td>
                    <div class=\"left-column\">
                        Priorite
                    </div>
</td><td>
                    <div class=\"right-column\">
                        <select class=\"select\" name=\"priorite\">
                            <option value=\"1\">la plus haute</option>
                            <option value=\"0\">la plus basse</option>
                        </select>
                    </div>
</td>
                </div></tr>
\t\t\t\t<tr>
                <div class=\"field-block button-height\">
<td></td>
\t\t\t\t<td>
                    <button type=\"submit\" class=\"button glossy mid-margin-right\">
                        <span class=\"button-icon\"><span class=\"icon-tick\"></span></span>
                        enregistrer
                    </button>


                    <button type=\"reset\" class=\"button glossy\">
                        <span class=\"button-icon red-gradient\"><span class=\"icon-cross-round\"></span></span>
                        annuler
                    </button>

</td>
                </div></tr>
\t\t\t\t</tbody>
\t\t\t\t</table>
                </form>

     <script>
        \$(\"document\").ready(function(){
                    
                    \$(\"#delimiteur\").change(function(){
                        \$.ajax({
                            type: 'get',
                            url : 'http://localhost/myagenda/web/app_dev.php/delimiteur/select/'+\$(this).val(),
                            beforeSend: function(){
                                console.log('me voila!!');
                            },
                            success:function(data){
                                  console.log('me voila3!!');
                                  
                                \$(\"#titre\").val(data.nom);
                                \$(\"#couleur\").val(data.couleur);
                            }
                               

                                
                                
                        });
                    });
                   
                });
                
    </script>
         






            
";
    }

    public function getTemplateName()
    {
        return "DelimiteurBundle:Default:delimiteur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 187,  237 => 185,  216 => 167,  197 => 151,  69 => 25,  58 => 23,  54 => 22,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }
}
