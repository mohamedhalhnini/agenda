<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/utilisateur')) {
            // utilisateur_profil
            if ($pathinfo === '/utilisateur/profil') {
                return array (  '_controller' => 'utilisateur\\utilisateurBundle\\Controller\\UtilisateurController::profilAction',  '_route' => 'utilisateur_profil',);
            }

            // utilisateur_add
            if ($pathinfo === '/utilisateur/ajouter') {
                return array (  '_controller' => 'utilisateur\\utilisateurBundle\\Controller\\UtilisateurController::addAction',  '_route' => 'utilisateur_add',);
            }

            // utilisateur_liste
            if ($pathinfo === '/utilisateur/liste') {
                return array (  '_controller' => 'utilisateur\\utilisateurBundle\\Controller\\UtilisateurController::listeAction',  '_route' => 'utilisateur_liste',);
            }

            // utilisateur_create
            if ($pathinfo === '/utilisateur/create') {
                return array (  '_controller' => 'utilisateur\\utilisateurBundle\\Controller\\UtilisateurController::createAction',  '_route' => 'utilisateur_create',);
            }

        }

        if (0 === strpos($pathinfo, '/delimiteur')) {
            // delimiteur
            if ($pathinfo === '/delimiteur/delimiteur') {
                return array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::delimiteurAction',  '_route' => 'delimiteur',);
            }

            // projet
            if ($pathinfo === '/delimiteur/projet') {
                return array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::calculerAction',  '_route' => 'projet',);
            }

            // addDelimiteur
            if ($pathinfo === '/delimiteur/add') {
                return array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::addDelimiteurAction',  '_route' => 'addDelimiteur',);
            }

            // findDelimiteur
            if (0 === strpos($pathinfo, '/delimiteur/trouve') && preg_match('#^/delimiteur/trouve(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'findDelimiteur')), array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::findDelimiteurAction',  'id' => 1,));
            }

            // listDelimiteur
            if ($pathinfo === '/delimiteur/list') {
                return array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::listDelimiteurAction',  '_route' => 'listDelimiteur',);
            }

            // delete
            if (0 === strpos($pathinfo, '/delimiteur/delete') && preg_match('#^/delimiteur/delete(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete')), array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::deleteAction',  'id' => 1,));
            }

            // updateDelimiteur
            if (0 === strpos($pathinfo, '/delimiteur/update') && preg_match('#^/delimiteur/update(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'updateDelimiteur')), array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::updateAction',  'id' => 1,));
            }

            // select
            if (0 === strpos($pathinfo, '/delimiteur/select') && preg_match('#^/delimiteur/select/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'select')), array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::selectDelimiteurAction',));
            }

            // editDelimiteur
            if (0 === strpos($pathinfo, '/delimiteur/edit') && preg_match('#^/delimiteur/edit(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'editDelimiteur')), array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::editAction',  'id' => 1,));
            }

            // calendarDelimiteur
            if ($pathinfo === '/delimiteur/calendarDelimiteur') {
                return array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::fullAction',  '_route' => 'calendarDelimiteur',);
            }

            // ajoutType
            if ($pathinfo === '/delimiteur/ajoutType') {
                return array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::ajouterTypeDelimiteurAction',  '_route' => 'ajoutType',);
            }

            // updateType
            if (0 === strpos($pathinfo, '/delimiteur/updateType') && preg_match('#^/delimiteur/updateType/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'updateType')), array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::updateTypeAction',));
            }

            // ajoutDelimiteurCalendar
            if (0 === strpos($pathinfo, '/delimiteur/DelCal') && preg_match('#^/delimiteur/DelCal(?:/(?P<startD>[^/]++)(?:/(?P<startH>[^/]++)(?:/(?P<endH>[^/]++))?)?)?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ajoutDelimiteurCalendar')), array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::ajoutdelCalAction',  'startD' => 1,  'startH' => 2,  'endH' => 4,));
            }

            // typeDelimiteur
            if ($pathinfo === '/delimiteur/typedelimiteur') {
                return array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::typeDelimiteurAction',  '_route' => 'typeDelimiteur',);
            }

            // editType
            if (0 === strpos($pathinfo, '/delimiteur/editType') && preg_match('#^/delimiteur/editType/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'editType')), array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::editTypeAction',));
            }

            // dropDelimiteur
            if (0 === strpos($pathinfo, '/delimiteur/dropDel') && preg_match('#^/delimiteur/dropDel(?:/(?P<id>[^/]++)(?:/(?P<startD>[^/]++)(?:/(?P<startH>[^/]++)(?:/(?P<end>[^/]++)(?:/(?P<endH>[^/]++))?)?)?)?)?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dropDelimiteur')), array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::dropDelAction',  'id' => 1,  'startD' => 1,  'startH' => 2,  'end' => 3,  'endH' => 4,));
            }

            // ajoutSimple
            if (0 === strpos($pathinfo, '/delimiteur/ajoutSimple') && preg_match('#^/delimiteur/ajoutSimple(?:/(?P<startD>[^/]++)(?:/(?P<startH>[^/]++)(?:/(?P<endH>[^/]++)(?:/(?P<type>[^/]++))?)?)?)?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ajoutSimple')), array (  '_controller' => 'Delimiteur\\DelimiteurBundle\\Controller\\DelimiteurController::ajoutDelimiteurDirectAction',  'startD' => 1,  'startH' => 2,  'endH' => 4,  'type' => 1,));
            }

        }

        if (0 === strpos($pathinfo, '/rendezVous')) {
            // ajouter
            if ($pathinfo === '/rendezVous/ajouter') {
                return array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvController::addAction',  '_route' => 'ajouter',);
            }

            if (0 === strpos($pathinfo, '/rendezVous/c')) {
                // create
                if ($pathinfo === '/rendezVous/create') {
                    return array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvController::createAction',  '_route' => 'create',);
                }

                // calendar
                if ($pathinfo === '/rendezVous/calendar') {
                    return array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvController::fullAction',  '_route' => 'calendar',);
                }

            }

            // modifier
            if (0 === strpos($pathinfo, '/rendezVous/modifier') && preg_match('#^/rendezVous/modifier(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'modifier')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvController::modifierAction',  'id' => 1,));
            }

            // update
            if (0 === strpos($pathinfo, '/rendezVous/update') && preg_match('#^/rendezVous/update(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'update')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvController::updateAction',  'id' => 1,));
            }

            // startEnd
            if (0 === strpos($pathinfo, '/rendezVous/params') && preg_match('#^/rendezVous/params(?:/(?P<startD>[^/]++)(?:/(?P<startH>[^/]++)(?:/(?P<endH>[^/]++))?)?)?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'startEnd')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvController::startEndAction',  'startD' => 1,  'startH' => 2,  'endH' => 4,));
            }

            // rdvType
            if ($pathinfo === '/rendezVous/rdvType') {
                return array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvTypeController::rdvTypeAction',  '_route' => 'rdvType',);
            }

            // createTypeRdv
            if ($pathinfo === '/rendezVous/pff') {
                return array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvTypeController::createAction',  '_route' => 'createTypeRdv',);
            }

            // check
            if (0 === strpos($pathinfo, '/rendezVous/check') && preg_match('#^/rendezVous/check/(?P<idClient>[^/]++)/(?P<idRdvType>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'check')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\ClientController::checkAction',));
            }

            // drop
            if (0 === strpos($pathinfo, '/rendezVous/drop') && preg_match('#^/rendezVous/drop(?:/(?P<id>[^/]++)(?:/(?P<start>[^/]++)(?:/(?P<startH>[^/]++)(?:/(?P<endH>[^/]++))?)?)?)?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'drop')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvTypeController::dropAction',  'id' => 1,  'start' => 2,  'startH' => 3,  'endH' => 4,));
            }

            // getHolidays
            if (0 === strpos($pathinfo, '/rendezVous/getHolidays') && preg_match('#^/rendezVous/getHolidays/(?P<year>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'getHolidays')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\JourFerierController::getHolidaysAction',));
            }

            // ajax
            if (0 === strpos($pathinfo, '/rendezVous/ajax') && preg_match('#^/rendezVous/ajax/(?P<rdvType1>[^/]++)/(?P<temps>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ajax')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvTypeController::rdvTypeAjaxAction',));
            }

            // rdvTypeDuree
            if (0 === strpos($pathinfo, '/rendezVous/duree') && preg_match('#^/rendezVous/duree/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'rdvTypeDuree')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvTypeController::rdvTypeDureeAction',));
            }

            // rdvAnnuler
            if (0 === strpos($pathinfo, '/rendezVous/rdvAnnuler') && preg_match('#^/rendezVous/rdvAnnuler(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'rdvAnnuler')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvAnnulerController::modifierAction',  'id' => 1,));
            }

            // annulation
            if (0 === strpos($pathinfo, '/rendezVous/annulation') && preg_match('#^/rendezVous/annulation(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'annulation')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvAnnulerController::annulerAction',  'id' => 1,));
            }

            // getClient
            if (0 === strpos($pathinfo, '/rendezVous/getClient') && preg_match('#^/rendezVous/getClient(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'getClient')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\ClientController::getClientAction',  'id' => 1,));
            }

            // removeRdv
            if (0 === strpos($pathinfo, '/rendezVous/remove') && preg_match('#^/rendezVous/remove(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'removeRdv')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvController::removeAction',  'id' => 1,));
            }

            // createMyClient
            if ($pathinfo === '/rendezVous/client/create') {
                return array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\ClientController::createAction',  '_route' => 'createMyClient',);
            }

            // myCalendar
            if ($pathinfo === '/rendezVous/myCalendar') {
                return array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvController::myCalendarAction',  '_route' => 'myCalendar',);
            }

            // testCalendar
            if ($pathinfo === '/rendezVous/testCalendar') {
                return array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvController::calendarTestAction',  '_route' => 'testCalendar',);
            }

            // modifierRdvType
            if (0 === strpos($pathinfo, '/rendezVous/modifierRdvType') && preg_match('#^/rendezVous/modifierRdvType(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'modifierRdvType')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvTypeController::modifierAction',  'id' => 1,));
            }

            // updateRdvType
            if (0 === strpos($pathinfo, '/rendezVous/updateRdvType') && preg_match('#^/rendezVous/updateRdvType(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'updateRdvType')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvTypeController::updateAction',  'id' => 1,));
            }

            // listRdvType
            if ($pathinfo === '/rendezVous/listRdvType') {
                return array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvTypeController::listRdvTypeAction',  '_route' => 'listRdvType',);
            }

            // deletetRdvType
            if (0 === strpos($pathinfo, '/rendezVous/deleteRdvType') && preg_match('#^/rendezVous/deleteRdvType(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'deletetRdvType')), array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\RdvTypeController::deleteTypeAction',  'id' => 1,));
            }

            if (0 === strpos($pathinfo, '/rendezVous/c')) {
                if (0 === strpos($pathinfo, '/rendezVous/client')) {
                    // clientRdv
                    if ($pathinfo === '/rendezVous/clientRdv') {
                        return array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\ClientRdvController::clientRdvAction',  '_route' => 'clientRdv',);
                    }

                    // clientLogin
                    if ($pathinfo === '/rendezVous/clientlogin') {
                        return array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\ClientRdvController::loginAction',  '_route' => 'clientLogin',);
                    }

                }

                // createRdvClient
                if ($pathinfo === '/rendezVous/createRdvClient') {
                    return array (  '_controller' => 'RendezVous\\RendezVousBundle\\Controller\\ClientRdvController::createAction',  '_route' => 'createRdvClient',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/Instruction')) {
            // instruction_homepage
            if (0 === strpos($pathinfo, '/Instruction/hello') && preg_match('#^/Instruction/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'instruction_homepage')), array (  '_controller' => 'Instruction\\InstructionBundle\\Controller\\InstructionController::indexAction',));
            }

            if (0 === strpos($pathinfo, '/Instruction/instruction')) {
                // instruction_ajouter
                if ($pathinfo === '/Instruction/instruction/add') {
                    return array (  '_controller' => 'Instruction\\InstructionBundle\\Controller\\InstructionController::addAction',  '_route' => 'instruction_ajouter',);
                }

                // instruction_update
                if (0 === strpos($pathinfo, '/Instruction/instruction/update') && preg_match('#^/Instruction/instruction/update/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'instruction_update')), array (  '_controller' => 'Instruction\\InstructionBundle\\Controller\\InstructionController::updateAction',));
                }

                // instruction_delete
                if ($pathinfo === '/Instruction/instruction/delete') {
                    return array (  '_controller' => 'Instruction\\InstructionBundle\\Controller\\InstructionController::deleteAction',  '_route' => 'instruction_delete',);
                }

                // instruction_list_permanente
                if ($pathinfo === '/Instruction/instruction/list') {
                    return array (  '_controller' => 'Instruction\\InstructionBundle\\Controller\\InstructionController::listInstructionsAction',  '_route' => 'instruction_list_permanente',);
                }

            }

            if (0 === strpos($pathinfo, '/Instruction/client')) {
                // client_create
                if ($pathinfo === '/Instruction/client/create') {
                    return array (  '_controller' => 'Instruction\\InstructionBundle\\Controller\\ClientController::createAction',  '_route' => 'client_create',);
                }

                // client_delete
                if ($pathinfo === '/Instruction/client/delete') {
                    return array (  '_controller' => 'Instruction\\InstructionBundle\\Controller\\ClientController::deleteAction',  '_route' => 'client_delete',);
                }

            }

            // instruction_archivate
            if (0 === strpos($pathinfo, '/Instruction/instruction/archivate') && preg_match('#^/Instruction/instruction/archivate/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'instruction_archivate')), array (  '_controller' => 'Instruction\\InstructionBundle\\Controller\\InstructionController::archivateAction',));
            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        if (0 === strpos($pathinfo, '/memo')) {
            // memo_memo_homepage
            if (0 === strpos($pathinfo, '/memo/hello') && preg_match('#^/memo/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'memo_memo_homepage')), array (  '_controller' => 'Memo\\MemoBundle\\Controller\\DefaultController::indexAction',));
            }

            // memo_memo_gestionMemo
            if ($pathinfo === '/memo/add') {
                return array (  '_controller' => 'Memo\\MemoBundle\\Controller\\MemoController::addAction',  '_route' => 'memo_memo_gestionMemo',);
            }

            // memo_memo_listMemo
            if ($pathinfo === '/memo/list') {
                return array (  '_controller' => 'Memo\\MemoBundle\\Controller\\MemoController::listMemoAction',  '_route' => 'memo_memo_listMemo',);
            }

            // memo_memo_supprimerMemo
            if (0 === strpos($pathinfo, '/memo/supp') && preg_match('#^/memo/supp/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'memo_memo_supprimerMemo')), array (  '_controller' => 'Memo\\MemoBundle\\Controller\\MemoController::supprimerAction',));
            }

            // memo_memo_updateMemo
            if (0 === strpos($pathinfo, '/memo/edit') && preg_match('#^/memo/edit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'memo_memo_updateMemo')), array (  '_controller' => 'Memo\\MemoBundle\\Controller\\MemoController::modifierAction',));
            }

            // memo_client_create
            if ($pathinfo === '/memo/addClient') {
                return array (  '_controller' => 'Memo\\MemoBundle\\Controller\\ClientController::createAction',  '_route' => 'memo_client_create',);
            }

            // memo_client_delete
            if ($pathinfo === '/memo/deleteClient') {
                return array (  '_controller' => 'Memo\\MemoBundle\\Controller\\ClientController::deleteAction',  '_route' => 'memo_client_delete',);
            }

        }

        if (0 === strpos($pathinfo, '/agenda')) {
            // agenda_memo_homepage
            if (0 === strpos($pathinfo, '/agenda/hello') && preg_match('#^/agenda/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'agenda_memo_homepage')), array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\DefaultController::indexAction',));
            }

            // agenda_memo_gestionMemo
            if ($pathinfo === '/agenda/add') {
                return array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\MemoController::addAction',  '_route' => 'agenda_memo_gestionMemo',);
            }

            // agenda_memo_listMemo
            if ($pathinfo === '/agenda/list') {
                return array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\MemoController::listMemoAction',  '_route' => 'agenda_memo_listMemo',);
            }

            // agenda_memo_supprimerMemo
            if (0 === strpos($pathinfo, '/agenda/supp') && preg_match('#^/agenda/supp/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'agenda_memo_supprimerMemo')), array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\MemoController::supprimerAction',));
            }

            // agenda_memo_updateMemo
            if (0 === strpos($pathinfo, '/agenda/edit') && preg_match('#^/agenda/edit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'agenda_memo_updateMemo')), array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\MemoController::modifierAction',));
            }

            if (0 === strpos($pathinfo, '/agenda/client')) {
                // agenda_client_create
                if ($pathinfo === '/agenda/client/create') {
                    return array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\ClientController::createAction',  '_route' => 'agenda_client_create',);
                }

                if (0 === strpos($pathinfo, '/agenda/client/e')) {
                    // agenda_client_update
                    if (0 === strpos($pathinfo, '/agenda/client/edit') && preg_match('#^/agenda/client/edit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'agenda_client_update')), array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\ClientController::modifierAction',));
                    }

                    // agenda_client_email
                    if (0 === strpos($pathinfo, '/agenda/client/email') && preg_match('#^/agenda/client/email/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'agenda_client_email')), array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\ClientController::envoyerEmailAction',));
                    }

                }

                // agenda_client_pdf
                if (0 === strpos($pathinfo, '/agenda/client/pdf') && preg_match('#^/agenda/client/pdf/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'agenda_client_pdf')), array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\ClientController::imprimerPdfAction',));
                }

            }

            // agenda_memo_get_client
            if (0 === strpos($pathinfo, '/agenda/getClient') && preg_match('#^/agenda/getClient/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'agenda_memo_get_client')), array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\ClientController::getClientAction',));
            }

            if (0 === strpos($pathinfo, '/agenda/client')) {
                // agenda_client_delete
                if ($pathinfo === '/agenda/client/delete') {
                    return array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\ClientController::deleteAction',  '_route' => 'agenda_client_delete',);
                }

                // agenda_client_pw
                if ($pathinfo === '/agenda/client/generate') {
                    return array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\ClientController::generatepwAction',  '_route' => 'agenda_client_pw',);
                }

                // agenda_client_details
                if (0 === strpos($pathinfo, '/agenda/client/details') && preg_match('#^/agenda/client/details/(?P<id>\\d+)(?:\\.(?P<format>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'agenda_client_details')), array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\ClientController::detailsAction',  'format' => 'html',));
                }

            }

            // agenda_memo_drop
            if (0 === strpos($pathinfo, '/agenda/drop') && preg_match('#^/agenda/drop/(?P<id>[^/]++)/(?P<start1>[^/]++)/(?P<startH>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'agenda_memo_drop')), array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\MemoController::dropAction',));
            }

            // agenda_memo_calendrier
            if ($pathinfo === '/agenda/calendrier') {
                return array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\CalendrierController::memoAction',  '_route' => 'agenda_memo_calendrier',);
            }

            // agenda_startEnd
            if (0 === strpos($pathinfo, '/agenda/nouveau') && preg_match('#^/agenda/nouveau/(?P<startD>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'agenda_startEnd')), array (  '_controller' => 'Agenda\\MemoBundle\\Controller\\MemoController::startEndAction',));
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
