<?php

namespace utilisateur\utilisateurBundle\Controller;

use bean\beanBundle\Entity\Configuration;
use bean\beanBundle\Entity\Utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UtilisateurController extends Controller {

    public function addAction() {


        return $this->render('utilisateurBundle:Utilisateur:add_user.html.twig');
    }

    public function listeAction() {
        $em = $this->getDoctrine()->getManager();
        $users=$em->getRepository('beanBundle:Utilisateur')->findAll();
        return $this->render('utilisateurBundle:Utilisateur:liste.html.twig',
                array(
                   'users'=>$users
                ));
    }
    
    public function profilAction() {
        $em = $this->getDoctrine()->getManager();

        $user=$em->getRepository('beanBundle:Utilisateur')->find($this->get('security.context')->getToken()->getUser()->getId());
        return $this->render('utilisateurBundle:Utilisateur:profil.html.twig',
                array(
                  'user'=>$user     
                    
                ));
    }

    
    
    public function createAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $factory = $this->get('security.encoder_factory');
        $request = $this->getRequest('request');
        $user = new Utilisateur();
        $config = new Configuration();
        if ($request->getMethod() == 'POST') {

            //add config
            $config->setCouleur($request->get('couleur'));
            $config->setJeuicones($request->get('jeuIcones'));
            $config->setLargeurtotale($request->get('largeurTotale'));
            $config->setLargeurcolonnedroite($request->get('largeurColonneDroite'));
            $config->setVersiontelephonefrancaise($request->get('versionTelephoneFrancaise'));
            $config->setPlagesdisponiblesuniquement($request->get('plagesDisponiblesUniquement'));
            $config->setPlagesintercalaires($request->get('plagesIntercalaires'));
            $config->setJoursouvresuniquement($request->get('joursOuvresUniquement'));
            $config->setMemosnontransmisuniquement($request->get('memosNonTransmisUniquement'));
            $config->setAffichagehebdomadairetype($request->get('affichageHebdomadaireType'));
            $config->setAffichagehebdomadairemodereduit($request->get('affichageHebdomadaireModeReduit'));
            $config->setAffichagehebdomadaireseparationmijournee(new \DateTime( $request->get('affichageHebdomadaireSeparationMijournee')));
            $config->setAffichagehebdomadairereduitafficher($request->get('affichageHebdomadaireReduitAfficher'));
            $config->setVuemensuelletype($request->get('vueMensuelleType'));
            $config->setComptesgroupeuniquement($request->get('comptesGroupeUniquement'));
            $config->setAffichagedynamique($request->get('affichageDynamique'));
            //$config->set($request->get('modeAffichage'));
            $config->setAccesprisememo($request->get('accesPriseMemo'));
            $config->setMemosjouruniquement($request->get('memosJourUniquement'));
            $config->setIconememononlu($request->get('iconeMemoNonLu'));
            $config->setRayerinfotransmies($request->get('rayerInfoTransmies'));
            $config->setChoixgestionrendezvous($request->get('choixGestionRendezVous'));
            $config->setVersjouractuelle($request->get('versJourActuelle'));
            $config->setContenuplanningressources($request->get('contenuPlanningRessources'));
            $config->setPermierspasdelarrage($request->get('permiersPasDelarrage'));
            $config->setListecomplete($request->get('listeComplete'));
            $config->setMethoderecherche($request->get('methodeRecherche'));
            $config->setAlertecreationmodification($request->get('alerteCreationModification'));
            $config->setAlertetachesnoneffectueeslues($request->get('alerteTachesNonEffectueesLues'));
            $config->setAlertecreemodifiememo($request->get('alerteCreeModifieMemo'));
            $config->setTriermouvements($request->get('trierMouvements'));
            $config->setNotestransmises($request->get('notesTransmises'));
            $config->setNotesrecurrentes($request->get('notesRecurrentes'));
            $config->setMouvementautreutilisateurs($request->get('mouvementAutreUtilisateurs'));
            $config->setAfficherlibelle($request->get('afficherLibelle'));
            $config->setDetaillibelle($request->get('detailLibelle'));
            $config->setMaxinfoagrees($request->get('maxInfoAgrees'));
            $config->setAffichercelles($request->get('afficherCelles'));
            $config->setAffichernomeffectuees($request->get('afficherNomEffectuees'));
            $config->setAffichernomlues($request->get('afficherNomLues'));
            $config->setMemodejatransmis($request->get('memoDejaTransmis'));
            $config->setPositionnercuseur($request->get('positionnerCuseur'));
            $config->setMotifannulationrv($request->get('motifAnnulationRV'));
            $config->setMemoannulationrv($request->get('memoAnnulationRV'));
            $config->setMemoclientinformation($request->get('memoClientInformation'));
            $config->setTachecompteprincipal($request->get('tacheComptePrincipal'));
            $config->setTachevisibleutilisateurconcerne($request->get('tacheVisibleUtilisateurConcerne'));
            $config->setConserverinforvcopie($request->get('conserverInfoRVcopie'));
            $config->setRendezvousafficher($request->get('rendezVousAfficher'));
            $config->setIncluredelimiteurs($request->get('inclureDelimiteurs'));
            
            //add user
            $user->setNom($request->get('nom'));
            $user->setPrenom($request->get('prenom'));
            $user->setUsername($request->get('login'));
            $user->setPassword($request->get('password'));
            $user->setAdresse($request->get('adresse'));
            $user->setEmail($request->get('mail'));
            $user->setVille($request->get('ville'));
            $user->setEnabled(TRUE);
            //attribuer la config
//            $user->setConfiguration($config);
            //attributions de rôles 
            if ($request->get('role') == 'SECRETAIR') {
                $user->setRoles(array('ROLE_SECRETAIRE'));
            } elseif ($request->get('role') == 'MEDECIN') {
                $user->setRoles(array('ROLE_MEDECIN'));
            } elseif ($request->get('role') == 'SUPER_ADMIN') {
                $user->setRoles(array('ROLE_SUPER_ADMIN'));
            }
            //cryptage de password
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);

            $em->persist($user);
            $em->flush();
            //ENVOYER Mail d'inscription

            $message = \Swift_Message::newInstance()
                    ->setSubject('Inscription M\'Agenda')
                    ->setFrom('ahmed.wafdi@edu.uca.ma')
                    ->setTo($user->getEmail())
                    ->setBody($this->renderView('utilisateurBundle:Utilisateur:Mail.html.twig', array('user' => $user,)), 'text/html')
            ;
            $this->get('mailer')->send($message);

//            $response = new JsonResponse();
//            return $response->setData(array());
            return $this->render('utilisateurBundle:Utilisateur:profil.html.twig', array('user' => $user));
        }
        return $this->render('utilisateurBundle:Utilisateur:add_user.html.twig', array('user' => $user));
    }

}
