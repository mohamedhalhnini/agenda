<?php

namespace bean\beanBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Semaine
 *
 * @ORM\Table(name="semaine")
 * @ORM\Entity
 */
class Semaine
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @ORM\OneToMany(targetEntity="Jour",mappedBy="semaine",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $jours;
    

   

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->jours = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add jours
     *
     * @param \bean\beanBundle\Entity\Jour $jours
     * @return Semaine
     */
    public function addJour(\bean\beanBundle\Entity\Jour $jours)
    {
        $this->jours[] = $jours;

        return $this;
    }

    /**
     * Remove jours
     *
     * @param \bean\beanBundle\Entity\Jour $jours
     */
    public function removeJour(\bean\beanBundle\Entity\Jour $jours)
    {
        $this->jours->removeElement($jours);
    }

    /**
     * Get jours
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJours()
    {
        return $this->jours;
    }
}
