<?php

namespace bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JourFerier
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="bean\beanBundle\Entity\JourFerierRepository")
 */
class JourFerier
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="TITLE", type="string", length=255)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="START", type="datetime")
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="END", type="datetime")
     */
    private $end;

    /**
     * @var boolean
     *
     * @ORM\Column(name="OVERLAP", type="boolean")
     */
    private $overlap;

    /**
     * @var string
     *
     * @ORM\Column(name="COLOR", type="string", length=255)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="RENDERING", type="string", length=255)
     */
    private $rendering;

        /**
     * @var \PositionActuelle
     *
     * @ORM\ManyToOne(targetEntity="PositionActuelle",inversedBy="ouvrables")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="POSITIONACTUELLE_ID", referencedColumnName="ID")
     * })
     */
    private $positionActuelle;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return JourFerier
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return JourFerier
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return JourFerier
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set overlap
     *
     * @param boolean $overlap
     * @return JourFerier
     */
    public function setOverlap($overlap)
    {
        $this->overlap = $overlap;

        return $this;
    }

    /**
     * Get overlap
     *
     * @return boolean 
     */
    public function getOverlap()
    {
        return $this->overlap;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return JourFerier
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set rendering
     *
     * @param string $rendering
     * @return JourFerier
     */
    public function setRendering($rendering)
    {
        $this->rendering = $rendering;

        return $this;
    }

    /**
     * Get rendering
     *
     * @return string 
     */
    public function getRendering()
    {
        return $this->rendering;
    }

    /**
     * Set positionActuelle
     *
     * @param \bean\beanBundle\Entity\PositionActuelle $positionActuelle
     * @return JourFerier
     */
    public function setPositionActuelle(\bean\beanBundle\Entity\PositionActuelle $positionActuelle = null)
    {
        $this->positionActuelle = $positionActuelle;

        return $this;
    }

    /**
     * Get positionActuelle
     *
     * @return \bean\beanBundle\Entity\PositionActuelle 
     */
    public function getPositionActuelle()
    {
        return $this->positionActuelle;
    }
}
