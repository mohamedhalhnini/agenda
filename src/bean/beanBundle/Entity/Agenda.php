<?php

namespace bean\beanBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Agenda
 *
 * @ORM\Table(name="agenda")
 * @ORM\Entity
 */
class Agenda
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Semaine
     *
     * @ORM\OneToOne(targetEntity="Semaine")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SEMAINE_ID", referencedColumnName="ID")
     * })
     */
    private $semaine;

    /**
     * @ORM\OneToMany(targetEntity="RendezVous",mappedBy="agenda",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $mesRendezVous;

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mesRendezVous = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set semaine
     *
     * @param \bean\beanBundle\Entity\Semaine $semaine
     * @return Agenda
     */
    public function setSemaine(\bean\beanBundle\Entity\Semaine $semaine = null)
    {
        $this->semaine = $semaine;

        return $this;
    }

    /**
     * Get semaine
     *
     * @return \bean\beanBundle\Entity\Semaine 
     */
    public function getSemaine()
    {
        return $this->semaine;
    }

    /**
     * Add mesRendezVous
     *
     * @param \bean\beanBundle\Entity\RendezVous $mesRendezVous
     * @return Agenda
     */
    public function addMesRendezVous(\bean\beanBundle\Entity\RendezVous $mesRendezVous)
    {
        $this->mesRendezVous[] = $mesRendezVous;

        return $this;
    }

    /**
     * Remove mesRendezVous
     *
     * @param \bean\beanBundle\Entity\RendezVous $mesRendezVous
     */
    public function removeMesRendezVous(\bean\beanBundle\Entity\RendezVous $mesRendezVous)
    {
        $this->mesRendezVous->removeElement($mesRendezVous);
    }

    /**
     * Get mesRendezVous
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMesRendezVous()
    {
        return $this->mesRendezVous;
    }
}
