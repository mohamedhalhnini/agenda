<?php

namespace bean\beanBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * PositionActuelle
 *
 * @ORM\Table(name="positionactuelle")
 * @ORM\Entity
 */
class PositionActuelle
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="PAYS", type="string", length=255, nullable=true)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="VILLE", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @ORM\OneToMany(targetEntity="JourFerier",mappedBy="positionActuelle",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $jourferier;

    /**
     * @ORM\OneToMany(targetEntity="Utilisateur",mappedBy="positionActuelle",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $utilisateurs;
    
    /**
     * @ORM\OneToMany(targetEntity="Ouvrable",mappedBy="positionActuelle",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $ouvrables;
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ouvrables = new \Doctrine\Common\Collections\ArrayCollection();
        $this->utilisateurs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pays
     *
     * @param string $pays
     * @return PositionActuelle
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return PositionActuelle
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Add ouvrables
     *
     * @param \bean\beanBundle\Entity\Ouvrable $ouvrables
     * @return PositionActuelle
     */
    public function addOuvrable(\bean\beanBundle\Entity\Ouvrable $ouvrables)
    {
        $this->ouvrables[] = $ouvrables;

        return $this;
    }

    /**
     * Remove ouvrables
     *
     * @param \bean\beanBundle\Entity\Ouvrable $ouvrables
     */
    public function removeOuvrable(\bean\beanBundle\Entity\Ouvrable $ouvrables)
    {
        $this->ouvrables->removeElement($ouvrables);
    }

    /**
     * Get ouvrables
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOuvrables()
    {
        return $this->ouvrables;
    }

    /**
     * Add utilisateurs
     *
     * @param \bean\beanBundle\Entity\Utilisateur $utilisateurs
     * @return PositionActuelle
     */
    public function addUtilisateur(\bean\beanBundle\Entity\Utilisateur $utilisateurs)
    {
        $this->utilisateurs[] = $utilisateurs;

        return $this;
    }

    /**
     * Remove utilisateurs
     *
     * @param \bean\beanBundle\Entity\Utilisateur $utilisateurs
     */
    public function removeUtilisateur(\bean\beanBundle\Entity\Utilisateur $utilisateurs)
    {
        $this->utilisateurs->removeElement($utilisateurs);
    }

    /**
     * Get utilisateurs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUtilisateurs()
    {
        return $this->utilisateurs;
    }

    /**
     * Add jourferier
     *
     * @param \bean\beanBundle\Entity\JourFerier $jourferier
     * @return PositionActuelle
     */
    public function addJourferier(\bean\beanBundle\Entity\JourFerier $jourferier)
    {
        $this->jourferier[] = $jourferier;

        return $this;
    }

    /**
     * Remove jourferier
     *
     * @param \bean\beanBundle\Entity\JourFerier $jourferier
     */
    public function removeJourferier(\bean\beanBundle\Entity\JourFerier $jourferier)
    {
        $this->jourferier->removeElement($jourferier);
    }

    /**
     * Get jourferier
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJourferier()
    {
        return $this->jourferier;
    }
}
