<?php

namespace bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ouvrable
 *
 * @ORM\Table(name="ouvrable")
 * @ORM\Entity
 */
class Ouvrable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATEDEBUT", type="date", nullable=true)
     */
    private $datedebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATEFIN", type="date", nullable=true)
     */
    private $datefin;

    /**
     * @var string
     *
     * @ORM\Column(name="RAISON", type="string", length=255, nullable=true)
     */
    private $raison;

    /**
     * @var \PositionActuelle
     *
     * @ORM\ManyToOne(targetEntity="PositionActuelle",inversedBy="ouvrables")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="POSITIONACTUELLE_ID", referencedColumnName="ID")
     * })
     */
    private $positionActuelle;




   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datedebut
     *
     * @param \DateTime $datedebut
     * @return Ouvrable
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * Get datedebut
     *
     * @return \DateTime 
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set datefin
     *
     * @param \DateTime $datefin
     * @return Ouvrable
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;

        return $this;
    }

    /**
     * Get datefin
     *
     * @return \DateTime 
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * Set raison
     *
     * @param string $raison
     * @return Ouvrable
     */
    public function setRaison($raison)
    {
        $this->raison = $raison;

        return $this;
    }

    /**
     * Get raison
     *
     * @return string 
     */
    public function getRaison()
    {
        return $this->raison;
    }

    /**
     * Set positionActuelle
     *
     * @param \bean\beanBundle\Entity\PositionActuelle $positionActuelle
     * @return Ouvrable
     */
    public function setPositionActuelle(\bean\beanBundle\Entity\PositionActuelle $positionActuelle = null)
    {
        $this->positionActuelle = $positionActuelle;

        return $this;
    }

    /**
     * Get positionActuelle
     *
     * @return \bean\beanBundle\Entity\PositionActuelle 
     */
    public function getPositionActuelle()
    {
        return $this->positionActuelle;
    }
}
