<?php

namespace bean\beanBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
/**
 * Rendezvous
 *
 * @ORM\Table(name="rendezvous")
 * @ORM\Entity(repositoryClass="\bean\beanBundle\Repository\RendezvousRepository")
 * @ORM\Entity
 */
class RendezVous
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ANNULER", type="integer", nullable=true)
     */
    private $annuler;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="PERIODICITE_TYPE", type="integer", nullable=true)
     */
    private $typePeriodicite;

    /**
     * @var array
     *
     * @ORM\Column(name="CLIENTALERT", type="array", nullable=true)
     */
    private $clientalert;

    /**
     * @var integer
     *
     * @ORM\Column(name="CLIENTDUREE", type="integer", nullable=true)
     */
    private $clientduree;

    /**
     * @var array
     *
     * @ORM\Column(name="CLIENTRAPPEL", type="array", nullable=true)
     */
    private $clientrappel;

    /**
     * @var string
     *
     * @ORM\Column(name="CLIENTUNITE", type="string", length=255, nullable=true)
     */
    private $clientunite;

    /**
     * @var string
     *
     * @ORM\Column(name="COULEUR", type="string", length=255, nullable=true)
     */
    private $couleur;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="DATEDEBUT", type="datetime", nullable=true)
     */
    private $datedebut;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="END", type="datetime", nullable=true)
     */
    private $end;// dateFin

    /**
     * @var string
     *
     * @ORM\Column(name="DETAIL", type="string", length=255, nullable=true)
     */
    private $detail;

    /**
     * @var string
     *
     * @ORM\Column(name="ENTETE", type="string", length=255, nullable=true)
     */
    private $entete;

    /**
     * @var integer
     *
     * @ORM\Column(name="ETAT", type="integer", nullable=true)
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="LIBELLE", type="string", length=255, nullable=true)
     */
    private $libelle;

    /**
     * @var array
     *
     * @ORM\Column(name="MYALERT", type="array", nullable=true)
     */
    private $myalert;

    /**
     * @var integer
     *
     * @ORM\Column(name="MYDUREE", type="integer", nullable=true)
     */
    private $myduree;

    /**
     * @var array
     *
     * @ORM\Column(name="MYRAPPEL", type="array", nullable=true)
     */
    private $myrappel;

    /**
     * @var string
     *
     * @ORM\Column(name="MYUNITE", type="string", length=255, nullable=true)
     */
    private $myunite;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARTAGE", type="integer", nullable=true)
     */
    private $partage;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECOPIE", type="integer", nullable=true)
     */
    private $recopie;
    
     /**
     * @var boolean
     *
     * @ORM\Column(name="ALLDAY", type="string", nullable=true)
     */
    private $allDay;
    
     /**
     * @var boolean
     *
     * @ORM\Column(name="OVERLAP", type="string", nullable=true)
     */
    private $overlap;
   
     /**
     * @var string
     *
     * @ORM\Column(name="RENDERING", type="string", nullable=true)
     */
    private $rendering;
    

    /**
     * @ORM\ManyToOne(targetEntity="Client",inversedBy="mesRendezVous")
       * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CLIENT_ID", referencedColumnName="ID" )
     * }) 
     */
    private $client;
    
    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur",inversedBy="mesRendezVous")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="UTILISATEUR_ID", referencedColumnName="ID" , nullable = true)
     * })
     */
    private $utilisateur;

    /**
     * @var \Agenda
     *
     * @ORM\ManyToOne(targetEntity="Agenda",inversedBy="mesRendezVous")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="AGENDA_ID", referencedColumnName="ID" , nullable = true)
     * })
     */
    private $agenda;

    /**
     *@ORM\ManyToOne(targetEntity="RendezVousType", inversedBy="rendezVous")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="RENDEZVOUSTYPE_ID", referencedColumnName="ID",nullable = true)
     * })
     */
    private $rendezVousType;
    
     /**
     *@ORM\ManyToOne(targetEntity="Periodicite", inversedBy="rendezVous",cascade={"persist","remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERIODICITE_ID", referencedColumnName="ID",nullable = true)
     * })
     */
    private $periodicite;

    

   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set annuler
     *
     * @param integer $annuler
     * @return RendezVous
     */
    public function setAnnuler($annuler)
    {
        $this->annuler = $annuler;
    
        return $this;
    }

    /**
     * Get annuler
     *
     * @return integer 
     */
    public function getAnnuler()
    {
        return $this->annuler;
    }

    /**
     * Set typePeriodicite
     *
     * @param integer $typePeriodicite
     * @return RendezVous
     */
    public function setTypePeriodicite($typePeriodicite)
    {
        $this->typePeriodicite = $typePeriodicite;
    
        return $this;
    }

    /**
     * Get typePeriodicite
     *
     * @return integer 
     */
    public function getTypePeriodicite()
    {
        return $this->typePeriodicite;
    }

    /**
     * Set clientalert
     *
     * @param array $clientalert
     * @return RendezVous
     */
    public function setClientalert($clientalert)
    {
        $this->clientalert = $clientalert;
    
        return $this;
    }

    /**
     * Get clientalert
     *
     * @return array 
     */
    public function getClientalert()
    {
        return $this->clientalert;
    }

    /**
     * Set clientduree
     *
     * @param integer $clientduree
     * @return RendezVous
     */
    public function setClientduree($clientduree)
    {
        $this->clientduree = $clientduree;
    
        return $this;
    }

    /**
     * Get clientduree
     *
     * @return integer 
     */
    public function getClientduree()
    {
        return $this->clientduree;
    }

    /**
     * Set clientrappel
     *
     * @param array $clientrappel
     * @return RendezVous
     */
    public function setClientrappel($clientrappel)
    {
        $this->clientrappel = $clientrappel;
    
        return $this;
    }

    /**
     * Get clientrappel
     *
     * @return array 
     */
    public function getClientrappel()
    {
        return $this->clientrappel;
    }

    /**
     * Set clientunite
     *
     * @param string $clientunite
     * @return RendezVous
     */
    public function setClientunite($clientunite)
    {
        $this->clientunite = $clientunite;
    
        return $this;
    }

    /**
     * Get clientunite
     *
     * @return string 
     */
    public function getClientunite()
    {
        return $this->clientunite;
    }

    /**
     * Set couleur
     *
     * @param string $couleur
     * @return RendezVous
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;
    
        return $this;
    }

    /**
     * Get couleur
     *
     * @return string 
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Set datedebut
     *
     * @param \DateTime $datedebut
     * @return RendezVous
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;
    
        return $this;
    }

    /**
     * Get datedebut
     *
     * @return \DateTime 
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return RendezVous
     */
    public function setEnd($end)
    {
        $this->end = $end;
    
        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set detail
     *
     * @param string $detail
     * @return RendezVous
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;
    
        return $this;
    }

    /**
     * Get detail
     *
     * @return string 
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set entete
     *
     * @param string $entete
     * @return RendezVous
     */
    public function setEntete($entete)
    {
        $this->entete = $entete;
    
        return $this;
    }

    /**
     * Get entete
     *
     * @return string 
     */
    public function getEntete()
    {
        return $this->entete;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return RendezVous
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    
        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return RendezVous
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    
        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set myalert
     *
     * @param array $myalert
     * @return RendezVous
     */
    public function setMyalert($myalert)
    {
        $this->myalert = $myalert;
    
        return $this;
    }

    /**
     * Get myalert
     *
     * @return array 
     */
    public function getMyalert()
    {
        return $this->myalert;
    }

    /**
     * Set myduree
     *
     * @param integer $myduree
     * @return RendezVous
     */
    public function setMyduree($myduree)
    {
        $this->myduree = $myduree;
    
        return $this;
    }

    /**
     * Get myduree
     *
     * @return integer 
     */
    public function getMyduree()
    {
        return $this->myduree;
    }

    /**
     * Set myrappel
     *
     * @param array $myrappel
     * @return RendezVous
     */
    public function setMyrappel($myrappel)
    {
        $this->myrappel = $myrappel;
    
        return $this;
    }

    /**
     * Get myrappel
     *
     * @return array 
     */
    public function getMyrappel()
    {
        return $this->myrappel;
    }

    /**
     * Set myunite
     *
     * @param string $myunite
     * @return RendezVous
     */
    public function setMyunite($myunite)
    {
        $this->myunite = $myunite;
    
        return $this;
    }

    /**
     * Get myunite
     *
     * @return string 
     */
    public function getMyunite()
    {
        return $this->myunite;
    }

    /**
     * Set partage
     *
     * @param integer $partage
     * @return RendezVous
     */
    public function setPartage($partage)
    {
        $this->partage = $partage;
    
        return $this;
    }

    /**
     * Get partage
     *
     * @return integer 
     */
    public function getPartage()
    {
        return $this->partage;
    }

    /**
     * Set recopie
     *
     * @param integer $recopie
     * @return RendezVous
     */
    public function setRecopie($recopie)
    {
        $this->recopie = $recopie;
    
        return $this;
    }

    /**
     * Get recopie
     *
     * @return integer 
     */
    public function getRecopie()
    {
        return $this->recopie;
    }

    /**
     * Set allDay
     *
     * @param string $allDay
     * @return RendezVous
     */
    public function setAllDay($allDay)
    {
        $this->allDay = $allDay;
    
        return $this;
    }

    /**
     * Get allDay
     *
     * @return string 
     */
    public function getAllDay()
    {
        return $this->allDay;
    }

    /**
     * Set overlap
     *
     * @param string $overlap
     * @return RendezVous
     */
    public function setOverlap($overlap)
    {
        $this->overlap = $overlap;
    
        return $this;
    }

    /**
     * Get overlap
     *
     * @return string 
     */
    public function getOverlap()
    {
        return $this->overlap;
    }

    /**
     * Set rendering
     *
     * @param string $rendering
     * @return RendezVous
     */
    public function setRendering($rendering)
    {
        $this->rendering = $rendering;
    
        return $this;
    }

    /**
     * Get rendering
     *
     * @return string 
     */
    public function getRendering()
    {
        return $this->rendering;
    }

    /**
     * Set client
     *
     * @param \bean\beanBundle\Entity\Client $client
     * @return RendezVous
     */
    public function setClient(\bean\beanBundle\Entity\Client $client = null)
    {
        $this->client = $client;
    
        return $this;
    }

    /**
     * Get client
     *
     * @return \bean\beanBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set utilisateur
     *
     * @param \bean\beanBundle\Entity\Utilisateur $utilisateur
     * @return RendezVous
     */
    public function setUtilisateur(\bean\beanBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;
    
        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \bean\beanBundle\Entity\Utilisateur 
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set agenda
     *
     * @param \bean\beanBundle\Entity\Agenda $agenda
     * @return RendezVous
     */
    public function setAgenda(\bean\beanBundle\Entity\Agenda $agenda = null)
    {
        $this->agenda = $agenda;
    
        return $this;
    }

    /**
     * Get agenda
     *
     * @return \bean\beanBundle\Entity\Agenda 
     */
    public function getAgenda()
    {
        return $this->agenda;
    }

    /**
     * Set rendezVousType
     *
     * @param \bean\beanBundle\Entity\RendezVousType $rendezVousType
     * @return RendezVous
     */
    public function setRendezVousType(\bean\beanBundle\Entity\RendezVousType $rendezVousType = null)
    {
        $this->rendezVousType = $rendezVousType;
    
        return $this;
    }

    /**
     * Get rendezVousType
     *
     * @return \bean\beanBundle\Entity\RendezVousType 
     */
    public function getRendezVousType()
    {
        return $this->rendezVousType;
    }

    /**
     * Set periodicite
     *
     * @param \bean\beanBundle\Entity\Periodicite $periodicite
     * @return RendezVous
     */
    public function setPeriodicite(\bean\beanBundle\Entity\Periodicite $periodicite = null)
    {
        $this->periodicite = $periodicite;
    
        return $this;
    }

    /**
     * Get periodicite
     *
     * @return \bean\beanBundle\Entity\Periodicite 
     */
    public function getPeriodicite()
    {
        return $this->periodicite;
    }
}
