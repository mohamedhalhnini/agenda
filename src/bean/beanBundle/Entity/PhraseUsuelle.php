<?php

namespace bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PhraseUsuelle
 *
 * @ORM\Table(name="phraseusuelle")
 * @ORM\Entity
 */
class PhraseUsuelle
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="CATEGORIE", type="integer", nullable=true)
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="TEXT", type="string", length=255, nullable=true)
     */
    private $text;
    /**
     * @ORM\OneToMany(targetEntity="Instruction",mappedBy="phraseUsuelle",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $instructions;

     /**
     * @ORM\OneToMany(targetEntity="InstructionArchivee",mappedBy="phraseUsuelle",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
     private $instructionsArchivees;
     
   /**
     * @ORM\OneToMany(targetEntity="Memo",mappedBy="phraseUsuelle",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $memos;
    
    /**
     * @ORM\OneToMany(targetEntity="Annulation",mappedBy="phraseusuelle",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $annulations;
   
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->instructions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->instructionsArchivees = new \Doctrine\Common\Collections\ArrayCollection();
        $this->memos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->annulations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categorie
     *
     * @param integer $categorie
     * @return PhraseUsuelle
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return integer 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return PhraseUsuelle
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Add instructions
     *
     * @param \bean\beanBundle\Entity\Instruction $instructions
     * @return PhraseUsuelle
     */
    public function addInstruction(\bean\beanBundle\Entity\Instruction $instructions)
    {
        $this->instructions[] = $instructions;

        return $this;
    }

    /**
     * Remove instructions
     *
     * @param \bean\beanBundle\Entity\Instruction $instructions
     */
    public function removeInstruction(\bean\beanBundle\Entity\Instruction $instructions)
    {
        $this->instructions->removeElement($instructions);
    }

    /**
     * Get instructions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * Add instructionsArchivees
     *
     * @param \bean\beanBundle\Entity\InstructionArchivee $instructionsArchivees
     * @return PhraseUsuelle
     */
    public function addInstructionsArchivee(\bean\beanBundle\Entity\InstructionArchivee $instructionsArchivees)
    {
        $this->instructionsArchivees[] = $instructionsArchivees;

        return $this;
    }

    /**
     * Remove instructionsArchivees
     *
     * @param \bean\beanBundle\Entity\InstructionArchivee $instructionsArchivees
     */
    public function removeInstructionsArchivee(\bean\beanBundle\Entity\InstructionArchivee $instructionsArchivees)
    {
        $this->instructionsArchivees->removeElement($instructionsArchivees);
    }

    /**
     * Get instructionsArchivees
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInstructionsArchivees()
    {
        return $this->instructionsArchivees;
    }

    /**
     * Add memos
     *
     * @param \bean\beanBundle\Entity\Memo $memos
     * @return PhraseUsuelle
     */
    public function addMemo(\bean\beanBundle\Entity\Memo $memos)
    {
        $this->memos[] = $memos;

        return $this;
    }

    /**
     * Remove memos
     *
     * @param \bean\beanBundle\Entity\Memo $memos
     */
    public function removeMemo(\bean\beanBundle\Entity\Memo $memos)
    {
        $this->memos->removeElement($memos);
    }

    /**
     * Get memos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMemos()
    {
        return $this->memos;
    }

    /**
     * Add annulations
     *
     * @param \bean\beanBundle\Entity\Annulation $annulations
     * @return PhraseUsuelle
     */
    public function addAnnulation(\bean\beanBundle\Entity\Annulation $annulations)
    {
        $this->annulations[] = $annulations;

        return $this;
    }

    /**
     * Remove annulations
     *
     * @param \bean\beanBundle\Entity\Annulation $annulations
     */
    public function removeAnnulation(\bean\beanBundle\Entity\Annulation $annulations)
    {
        $this->annulations->removeElement($annulations);
    }

    /**
     * Get annulations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnnulations()
    {
        return $this->annulations;
    }
}
