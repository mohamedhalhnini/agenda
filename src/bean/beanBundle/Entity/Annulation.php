<?php

namespace  bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Annulation
 *
 * @ORM\Table(name="annulation" )
 * @ORM\Entity
 */
class Annulation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="CLIENTALERT", type="array", nullable=true)
     */
    private $clientalert;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATEANNULATION", type="date", nullable=true)
     */
    private $dateannulation;

    /**
     * @var string
     *
     * @ORM\Column(name="ENTETE", type="string", length=255, nullable=true)
     */
    private $entete;

    /**
     * @var integer
     *
     * @ORM\Column(name="ETAT", type="integer", nullable=true)
     */
    private $etat;

    /**
     * @var array
     *
     * @ORM\Column(name="MYALERT", type="array", nullable=true)
     */
    private $myalert;

    /**
     * @var string
     *
     * @ORM\Column(name="MYENTETE", type="string", length=255, nullable=true)
     */
    private $myentete;

    /**
     * @var integer
     *
     * @ORM\Column(name="REPORT", type="integer", nullable=true)
     */
    private $report;

    /**
     * @var \Rendezvous
     *
     * @ORM\OneToOne(targetEntity="Rendezvous")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="RENDEZVOUS_ID", referencedColumnName="ID")
     * })
     */
    private $rendezvous;

    /**
     * @var \PhraseUsuelle
     *
     * @ORM\ManyToOne(targetEntity="PhraseUsuelle",inversedBy="annulations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PHRASEUSUELLE_ID", referencedColumnName="ID")
     * })
     */
    private $phraseusuelle;

    



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clientalert
     *
     * @param array $clientalert
     * @return Annulation
     */
    public function setClientalert($clientalert)
    {
        $this->clientalert = $clientalert;
    
        return $this;
    }

    /**
     * Get clientalert
     *
     * @return array 
     */
    public function getClientalert()
    {
        return $this->clientalert;
    }

    /**
     * Set dateannulation
     *
     * @param \DateTime $dateannulation
     * @return Annulation
     */
    public function setDateannulation($dateannulation)
    {
        $this->dateannulation = $dateannulation;
    
        return $this;
    }

    /**
     * Get dateannulation
     *
     * @return \DateTime 
     */
    public function getDateannulation()
    {
        return $this->dateannulation;
    }

    /**
     * Set entete
     *
     * @param string $entete
     * @return Annulation
     */
    public function setEntete($entete)
    {
        $this->entete = $entete;
    
        return $this;
    }

    /**
     * Get entete
     *
     * @return string 
     */
    public function getEntete()
    {
        return $this->entete;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return Annulation
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    
        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set myalert
     *
     * @param array $myalert
     * @return Annulation
     */
    public function setMyalert($myalert)
    {
        $this->myalert = $myalert;
    
        return $this;
    }

    /**
     * Get myalert
     *
     * @return array 
     */
    public function getMyalert()
    {
        return $this->myalert;
    }

    /**
     * Set myentete
     *
     * @param string $myentete
     * @return Annulation
     */
    public function setMyentete($myentete)
    {
        $this->myentete = $myentete;
    
        return $this;
    }

    /**
     * Get myentete
     *
     * @return string 
     */
    public function getMyentete()
    {
        return $this->myentete;
    }

    /**
     * Set report
     *
     * @param integer $report
     * @return Annulation
     */
    public function setReport($report)
    {
        $this->report = $report;
    
        return $this;
    }

    /**
     * Get report
     *
     * @return integer 
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set rendezvous
     *
     * @param \bean\beanBundle\Entity\Rendezvous $rendezvous
     * @return Annulation
     */
    public function setRendezvous(\bean\beanBundle\Entity\Rendezvous $rendezvous = null)
    {
        $this->rendezvous = $rendezvous;
    
        return $this;
    }

    /**
     * Get rendezvous
     *
     * @return \bean\beanBundle\Entity\Rendezvous 
     */
    public function getRendezvous()
    {
        return $this->rendezvous;
    }

    /**
     * Set phraseusuelle
     *
     * @param \bean\beanBundle\Entity\PhraseUsuelle $phraseusuelle
     * @return Annulation
     */
    public function setPhraseusuelle(\bean\beanBundle\Entity\PhraseUsuelle $phraseusuelle = null)
    {
        $this->phraseusuelle = $phraseusuelle;
    
        return $this;
    }

    /**
     * Get phraseusuelle
     *
     * @return \bean\beanBundle\Entity\PhraseUsuelle 
     */
    public function getPhraseusuelle()
    {
        return $this->phraseusuelle;
    }
}
