<?php

namespace bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Instruction
 *
 * @ORM\Table(name="instruction")
 * @ORM\Entity(repositoryClass="bean\beanBundle\Repository\InstructionRepository")
 */
class Instruction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="A", type="date", nullable=true)
     */
    private $a;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DE", type="date", nullable=true)
     */
    private $de;

    /**
     * @var string
     *
     * @ORM\Column(name="DETAIL", type="string", length=255, nullable=true)
     */
    private $detail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LE", type="date", nullable=true)
     */
    private $le;
    
  
    
     /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATENREGISTREMENT", type="date", nullable=true)
     */
    private $dateEnregistrement;
    /**
     * @var integer
     *
     * @ORM\Column(name="PRIORITE", type="integer", nullable=true)
     */
    private $priorite;

    /**
     * @var string
     *
     * @ORM\Column(name="TITRE", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur",inversedBy="instructions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="UTILISATEUR_ID", referencedColumnName="ID")
     * })
     */
    private $utilisateur;

    /**
     * @var \Periodicite
     *
     * @ORM\ManyToOne(targetEntity="Periodicite",inversedBy="instructions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERIODICITE_ID", referencedColumnName="ID")
     * })
     */
    private $periodicite;
   
    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Client",inversedBy="instructions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CLIENT_ID", referencedColumnName="ID")
     * })
     */
    private $client;


    /**
     * @var \PhraseUsuelle
     *
     * @ORM\ManyToOne(targetEntity="PhraseUsuelle",inversedBy="instructions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PHRASEUSUELLE_ID", referencedColumnName="ID")
     * })
     */
    private $phraseUsuelle;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set a
     *
     * @param \DateTime $a
     * @return Instruction
     */
    public function setA($a)
    {
        $this->a = $a;

        return $this;
    }

    /**
     * Get a
     *
     * @return \DateTime 
     */
    public function getA()
    {
        return $this->a;
    }

    /**
     * Set de
     *
     * @param \DateTime $de
     * @return Instruction
     */
    public function setDe($de)
    {
        $this->de = $de;

        return $this;
    }

    /**
     * Get de
     *
     * @return \DateTime 
     */
    public function getDe()
    {
        return $this->de;
    }

    /**
     * Set detail
     *
     * @param string $detail
     * @return Instruction
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return string 
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set le
     *
     * @param \DateTime $le
     * @return Instruction
     */
    public function setLe($le)
    {
        $this->le = $le;

        return $this;
    }

    /**
     * Get le
     *
     * @return \DateTime 
     */
    public function getLe()
    {
        return $this->le;
    }

  
 
    /**
     * Set dateEnregistrement
     *
     * @param \DateTime $dateEnregistrement
     * @return Instruction
     */
    public function setDateEnregistrement($dateEnregistrement)
    {
        $this->dateEnregistrement = $dateEnregistrement;

        return $this;
    }

    /**
     * Get dateEnregistrement
     *
     * @return \DateTime 
     */
    public function getDateEnregistrement()
    {
        return $this->dateEnregistrement;
    }

    /**
     * Set priorite
     *
     * @param integer $priorite
     * @return Instruction
     */
    public function setPriorite($priorite)
    {
        $this->priorite = $priorite;

        return $this;
    }

    /**
     * Get priorite
     *
     * @return integer 
     */
    public function getPriorite()
    {
        return $this->priorite;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Instruction
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Instruction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set utilisateur
     *
     * @param \bean\beanBundle\Entity\Utilisateur $utilisateur
     * @return Instruction
     */
    public function setUtilisateur(\bean\beanBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \bean\beanBundle\Entity\Utilisateur 
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set periodicite
     *
     * @param \bean\beanBundle\Entity\Periodicite $periodicite
     * @return Instruction
     */
    public function setPeriodicite(\bean\beanBundle\Entity\Periodicite $periodicite = null)
    {
        $this->periodicite = $periodicite;

        return $this;
    }

    /**
     * Get periodicite
     *
     * @return \bean\beanBundle\Entity\Periodicite 
     */
    public function getPeriodicite()
    {
        return $this->periodicite;
    }

    /**
     * Set client
     *
     * @param \bean\beanBundle\Entity\Client $client
     * @return Instruction
     */
    public function setClient(\bean\beanBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \bean\beanBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set phraseUsuelle
     *
     * @param \bean\beanBundle\Entity\PhraseUsuelle $phraseUsuelle
     * @return Instruction
     */
    public function setPhraseUsuelle(\bean\beanBundle\Entity\PhraseUsuelle $phraseUsuelle = null)
    {
        $this->phraseUsuelle = $phraseUsuelle;

        return $this;
    }

    /**
     * Get phraseUsuelle
     *
     * @return \bean\beanBundle\Entity\PhraseUsuelle 
     */
    public function getPhraseUsuelle()
    {
        return $this->phraseUsuelle;
    }
}
