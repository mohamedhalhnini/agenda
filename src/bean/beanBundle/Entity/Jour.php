<?php

namespace bean\beanBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Jour
 *
 * @ORM\Table(name="jour")
 * @ORM\Entity
 */
class Jour
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="NUMERO", type="integer", nullable=true)
     */
    private $numero;

    /**
     * @var \Semaine
     *
     * @ORM\ManyToOne(targetEntity="Semaine",inversedBy="jours")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SEMAINE_ID", referencedColumnName="ID")
     * })
     */
    private $semaine;

    /**
     * @ORM\OneToMany(targetEntity="Tranche",mappedBy="jour",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $mesTranches;

   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mesTranches = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     * @return Jour
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set semaine
     *
     * @param \bean\beanBundle\Entity\Semaine $semaine
     * @return Jour
     */
    public function setSemaine(\bean\beanBundle\Entity\Semaine $semaine = null)
    {
        $this->semaine = $semaine;

        return $this;
    }

    /**
     * Get semaine
     *
     * @return \bean\beanBundle\Entity\Semaine 
     */
    public function getSemaine()
    {
        return $this->semaine;
    }

    /**
     * Add mesTranches
     *
     * @param \bean\beanBundle\Entity\Tranche $mesTranches
     * @return Jour
     */
    public function addMesTranch(\bean\beanBundle\Entity\Tranche $mesTranches)
    {
        $this->mesTranches[] = $mesTranches;

        return $this;
    }

    /**
     * Remove mesTranches
     *
     * @param \bean\beanBundle\Entity\Tranche $mesTranches
     */
    public function removeMesTranch(\bean\beanBundle\Entity\Tranche $mesTranches)
    {
        $this->mesTranches->removeElement($mesTranches);
    }

    /**
     * Get mesTranches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMesTranches()
    {
        return $this->mesTranches;
    }
}
