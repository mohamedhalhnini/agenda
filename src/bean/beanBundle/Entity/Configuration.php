<?php

namespace bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Configuration
 *
 * @ORM\Table(name="configuration")
 * @ORM\Entity
 */
class Configuration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="couleur", type="string", length=20, nullable=false)
     */
    private $couleur;

    /**
     * @var string
     *
     * @ORM\Column(name="jeuIcones", type="string", length=20, nullable=false)
     */
    private $jeuicones;

    /**
     * @var string
     *
     * @ORM\Column(name="largeurTotale", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $largeurtotale;

    /**
     * @var string
     *
     * @ORM\Column(name="largeurColonneDroite", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $largeurcolonnedroite;

    /**
     * @var boolean
     *
     * @ORM\Column(name="versionTelephoneFrancaise", type="boolean", nullable=false)
     */
    private $versiontelephonefrancaise;

    /**
     * @var boolean
     *
     * @ORM\Column(name="plagesDisponiblesUniquement", type="boolean", nullable=false)
     */
    private $plagesdisponiblesuniquement;

    /**
     * @var boolean
     *
     * @ORM\Column(name="plagesIntercalaires", type="boolean", nullable=false)
     */
    private $plagesintercalaires;

    /**
     * @var boolean
     *
     * @ORM\Column(name="joursOuvresUniquement", type="boolean", nullable=false)
     */
    private $joursouvresuniquement;

    /**
     * @var boolean
     *
     * @ORM\Column(name="memosNonTransmisUniquement", type="boolean", nullable=false)
     */
    private $memosnontransmisuniquement;

    /**
     * @var boolean
     *
     * @ORM\Column(name="affichageHebdomadaireType", type="boolean", nullable=false)
     */
    private $affichagehebdomadairetype;

    /**
     * @var boolean
     *
     * @ORM\Column(name="affichageHebdomadaireModeReduit", type="boolean", nullable=false)
     */
    private $affichagehebdomadairemodereduit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="affichageHebdomadaireSeparationMijournee", type="time", nullable=false)
     */
    private $affichagehebdomadaireseparationmijournee;

    /**
     * @var string
     *
     * @ORM\Column(name="affichageHebdomadaireReduitAfficher", type="string", length=50, nullable=false)
     */
    private $affichagehebdomadairereduitafficher;

    /**
     * @var boolean
     *
     * @ORM\Column(name="vueMensuelleType", type="boolean", nullable=false)
     */
    private $vuemensuelletype;

    /**
     * @var boolean
     *
     * @ORM\Column(name="comptesGroupeUniquement", type="boolean", nullable=false)
     */
    private $comptesgroupeuniquement;

    /**
     * @var boolean
     *
     * @ORM\Column(name="affichageDynamique", type="boolean", nullable=false)
     */
    private $affichagedynamique;

    /**
     * @var boolean
     *
     * @ORM\Column(name="accesPriseMemo", type="boolean", nullable=false)
     */
    private $accesprisememo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="memosJourUniquement", type="boolean", nullable=false)
     */
    private $memosjouruniquement;

    /**
     * @var boolean
     *
     * @ORM\Column(name="iconeMemoNonLu", type="boolean", nullable=false)
     */
    private $iconememononlu;

    /**
     * @var boolean
     *
     * @ORM\Column(name="rayerInfoTransmies", type="boolean", nullable=false)
     */
    private $rayerinfotransmies;

    /**
     * @var boolean
     *
     * @ORM\Column(name="choixGestionRendezVous", type="boolean", nullable=false)
     */
    private $choixgestionrendezvous;

    /**
     * @var integer
     *
     * @ORM\Column(name="versJourActuelle", type="integer", nullable=false)
     */
    private $versjouractuelle;

    /**
     * @var string
     *
     * @ORM\Column(name="contenuPlanningRessources", type="string", length=50, nullable=false)
     */
    private $contenuplanningressources;

    /**
     * @var boolean
     *
     * @ORM\Column(name="permiersPasDelarrage", type="boolean", nullable=false)
     */
    private $permierspasdelarrage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="listeComplete", type="boolean", nullable=false)
     */
    private $listecomplete;

    /**
     * @var boolean
     *
     * @ORM\Column(name="methodeRecherche", type="boolean", nullable=false)
     */
    private $methoderecherche;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alerteCreationModification", type="boolean", nullable=false)
     */
    private $alertecreationmodification;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alerteTachesNonEffectueesLues", type="boolean", nullable=false)
     */
    private $alertetachesnoneffectueeslues;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alerteCreeModifieMemo", type="boolean", nullable=false)
     */
    private $alertecreemodifiememo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="trierMouvements", type="boolean", nullable=false)
     */
    private $triermouvements;

    /**
     * @var boolean
     *
     * @ORM\Column(name="notesTransmises", type="boolean", nullable=false)
     */
    private $notestransmises;

    /**
     * @var boolean
     *
     * @ORM\Column(name="notesRecurrentes", type="boolean", nullable=false)
     */
    private $notesrecurrentes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="mouvementAutreUtilisateurs", type="boolean", nullable=false)
     */
    private $mouvementautreutilisateurs;

    /**
     * @var boolean
     *
     * @ORM\Column(name="afficherLibelle", type="boolean", nullable=false)
     */
    private $afficherlibelle;

    /**
     * @var string
     *
     * @ORM\Column(name="detailLibelle", type="string", length=250, nullable=false)
     */
    private $detaillibelle;

    /**
     * @var string
     *
     * @ORM\Column(name="maxInfoAgrees", type="string", length=50, nullable=false)
     */
    private $maxinfoagrees;

    /**
     * @var integer
     *
     * @ORM\Column(name="afficherCelles", type="integer", nullable=false)
     */
    private $affichercelles;

    /**
     * @var boolean
     *
     * @ORM\Column(name="afficherNomEffectuees", type="boolean", nullable=false)
     */
    private $affichernomeffectuees;

    /**
     * @var boolean
     *
     * @ORM\Column(name="afficherNomLues", type="boolean", nullable=false)
     */
    private $affichernomlues;

    /**
     * @var boolean
     *
     * @ORM\Column(name="memoDejaTransmis", type="boolean", nullable=false)
     */
    private $memodejatransmis;

    /**
     * @var boolean
     *
     * @ORM\Column(name="positionnerCuseur", type="boolean", nullable=false)
     */
    private $positionnercuseur;

    /**
     * @var boolean
     *
     * @ORM\Column(name="motifAnnulationRV", type="boolean", nullable=false)
     */
    private $motifannulationrv;

    /**
     * @var boolean
     *
     * @ORM\Column(name="memoAnnulationRV", type="boolean", nullable=false)
     */
    private $memoannulationrv;

    /**
     * @var boolean
     *
     * @ORM\Column(name="memoClientInformation", type="boolean", nullable=false)
     */
    private $memoclientinformation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tacheComptePrincipal", type="boolean", nullable=false)
     */
    private $tachecompteprincipal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tacheVisibleUtilisateurConcerne", type="boolean", nullable=false)
     */
    private $tachevisibleutilisateurconcerne;

    /**
     * @var boolean
     *
     * @ORM\Column(name="conserverInfoRVcopie", type="boolean", nullable=false)
     */
    private $conserverinforvcopie;

    /**
     * @var string
     *
     * @ORM\Column(name="rendezVousAfficher", type="string", length=255, nullable=false)
     */
    private $rendezvousafficher;

    /**
     * @var boolean
     *
     * @ORM\Column(name="inclureDelimiteurs", type="boolean", nullable=false)
     */
    private $incluredelimiteurs;

   


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set couleur
     *
     * @param string $couleur
     * @return Configuration
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get couleur
     *
     * @return string 
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Set jeuicones
     *
     * @param string $jeuicones
     * @return Configuration
     */
    public function setJeuicones($jeuicones)
    {
        $this->jeuicones = $jeuicones;

        return $this;
    }

    /**
     * Get jeuicones
     *
     * @return string 
     */
    public function getJeuicones()
    {
        return $this->jeuicones;
    }

    /**
     * Set largeurtotale
     *
     * @param string $largeurtotale
     * @return Configuration
     */
    public function setLargeurtotale($largeurtotale)
    {
        $this->largeurtotale = $largeurtotale;

        return $this;
    }

    /**
     * Get largeurtotale
     *
     * @return string 
     */
    public function getLargeurtotale()
    {
        return $this->largeurtotale;
    }

    /**
     * Set largeurcolonnedroite
     *
     * @param string $largeurcolonnedroite
     * @return Configuration
     */
    public function setLargeurcolonnedroite($largeurcolonnedroite)
    {
        $this->largeurcolonnedroite = $largeurcolonnedroite;

        return $this;
    }

    /**
     * Get largeurcolonnedroite
     *
     * @return string 
     */
    public function getLargeurcolonnedroite()
    {
        return $this->largeurcolonnedroite;
    }

    /**
     * Set versiontelephonefrancaise
     *
     * @param boolean $versiontelephonefrancaise
     * @return Configuration
     */
    public function setVersiontelephonefrancaise($versiontelephonefrancaise)
    {
        $this->versiontelephonefrancaise = $versiontelephonefrancaise;

        return $this;
    }

    /**
     * Get versiontelephonefrancaise
     *
     * @return boolean 
     */
    public function getVersiontelephonefrancaise()
    {
        return $this->versiontelephonefrancaise;
    }

    /**
     * Set plagesdisponiblesuniquement
     *
     * @param boolean $plagesdisponiblesuniquement
     * @return Configuration
     */
    public function setPlagesdisponiblesuniquement($plagesdisponiblesuniquement)
    {
        $this->plagesdisponiblesuniquement = $plagesdisponiblesuniquement;

        return $this;
    }

    /**
     * Get plagesdisponiblesuniquement
     *
     * @return boolean 
     */
    public function getPlagesdisponiblesuniquement()
    {
        return $this->plagesdisponiblesuniquement;
    }

    /**
     * Set plagesintercalaires
     *
     * @param boolean $plagesintercalaires
     * @return Configuration
     */
    public function setPlagesintercalaires($plagesintercalaires)
    {
        $this->plagesintercalaires = $plagesintercalaires;

        return $this;
    }

    /**
     * Get plagesintercalaires
     *
     * @return boolean 
     */
    public function getPlagesintercalaires()
    {
        return $this->plagesintercalaires;
    }

    /**
     * Set joursouvresuniquement
     *
     * @param boolean $joursouvresuniquement
     * @return Configuration
     */
    public function setJoursouvresuniquement($joursouvresuniquement)
    {
        $this->joursouvresuniquement = $joursouvresuniquement;

        return $this;
    }

    /**
     * Get joursouvresuniquement
     *
     * @return boolean 
     */
    public function getJoursouvresuniquement()
    {
        return $this->joursouvresuniquement;
    }

    /**
     * Set memosnontransmisuniquement
     *
     * @param boolean $memosnontransmisuniquement
     * @return Configuration
     */
    public function setMemosnontransmisuniquement($memosnontransmisuniquement)
    {
        $this->memosnontransmisuniquement = $memosnontransmisuniquement;

        return $this;
    }

    /**
     * Get memosnontransmisuniquement
     *
     * @return boolean 
     */
    public function getMemosnontransmisuniquement()
    {
        return $this->memosnontransmisuniquement;
    }

    /**
     * Set affichagehebdomadairetype
     *
     * @param boolean $affichagehebdomadairetype
     * @return Configuration
     */
    public function setAffichagehebdomadairetype($affichagehebdomadairetype)
    {
        $this->affichagehebdomadairetype = $affichagehebdomadairetype;

        return $this;
    }

    /**
     * Get affichagehebdomadairetype
     *
     * @return boolean 
     */
    public function getAffichagehebdomadairetype()
    {
        return $this->affichagehebdomadairetype;
    }

    /**
     * Set affichagehebdomadairemodereduit
     *
     * @param boolean $affichagehebdomadairemodereduit
     * @return Configuration
     */
    public function setAffichagehebdomadairemodereduit($affichagehebdomadairemodereduit)
    {
        $this->affichagehebdomadairemodereduit = $affichagehebdomadairemodereduit;

        return $this;
    }

    /**
     * Get affichagehebdomadairemodereduit
     *
     * @return boolean 
     */
    public function getAffichagehebdomadairemodereduit()
    {
        return $this->affichagehebdomadairemodereduit;
    }

    /**
     * Set affichagehebdomadaireseparationmijournee
     *
     * @param \DateTime $affichagehebdomadaireseparationmijournee
     * @return Configuration
     */
    public function setAffichagehebdomadaireseparationmijournee($affichagehebdomadaireseparationmijournee)
    {
        $this->affichagehebdomadaireseparationmijournee = $affichagehebdomadaireseparationmijournee;

        return $this;
    }

    /**
     * Get affichagehebdomadaireseparationmijournee
     *
     * @return \DateTime 
     */
    public function getAffichagehebdomadaireseparationmijournee()
    {
        return $this->affichagehebdomadaireseparationmijournee;
    }

    /**
     * Set affichagehebdomadairereduitafficher
     *
     * @param string $affichagehebdomadairereduitafficher
     * @return Configuration
     */
    public function setAffichagehebdomadairereduitafficher($affichagehebdomadairereduitafficher)
    {
        $this->affichagehebdomadairereduitafficher = $affichagehebdomadairereduitafficher;

        return $this;
    }

    /**
     * Get affichagehebdomadairereduitafficher
     *
     * @return string 
     */
    public function getAffichagehebdomadairereduitafficher()
    {
        return $this->affichagehebdomadairereduitafficher;
    }

    /**
     * Set vuemensuelletype
     *
     * @param boolean $vuemensuelletype
     * @return Configuration
     */
    public function setVuemensuelletype($vuemensuelletype)
    {
        $this->vuemensuelletype = $vuemensuelletype;

        return $this;
    }

    /**
     * Get vuemensuelletype
     *
     * @return boolean 
     */
    public function getVuemensuelletype()
    {
        return $this->vuemensuelletype;
    }

    /**
     * Set comptesgroupeuniquement
     *
     * @param boolean $comptesgroupeuniquement
     * @return Configuration
     */
    public function setComptesgroupeuniquement($comptesgroupeuniquement)
    {
        $this->comptesgroupeuniquement = $comptesgroupeuniquement;

        return $this;
    }

    /**
     * Get comptesgroupeuniquement
     *
     * @return boolean 
     */
    public function getComptesgroupeuniquement()
    {
        return $this->comptesgroupeuniquement;
    }

    /**
     * Set affichagedynamique
     *
     * @param boolean $affichagedynamique
     * @return Configuration
     */
    public function setAffichagedynamique($affichagedynamique)
    {
        $this->affichagedynamique = $affichagedynamique;

        return $this;
    }

    /**
     * Get affichagedynamique
     *
     * @return boolean 
     */
    public function getAffichagedynamique()
    {
        return $this->affichagedynamique;
    }

    /**
     * Set accesprisememo
     *
     * @param boolean $accesprisememo
     * @return Configuration
     */
    public function setAccesprisememo($accesprisememo)
    {
        $this->accesprisememo = $accesprisememo;

        return $this;
    }

    /**
     * Get accesprisememo
     *
     * @return boolean 
     */
    public function getAccesprisememo()
    {
        return $this->accesprisememo;
    }

    /**
     * Set memosjouruniquement
     *
     * @param boolean $memosjouruniquement
     * @return Configuration
     */
    public function setMemosjouruniquement($memosjouruniquement)
    {
        $this->memosjouruniquement = $memosjouruniquement;

        return $this;
    }

    /**
     * Get memosjouruniquement
     *
     * @return boolean 
     */
    public function getMemosjouruniquement()
    {
        return $this->memosjouruniquement;
    }

    /**
     * Set iconememononlu
     *
     * @param boolean $iconememononlu
     * @return Configuration
     */
    public function setIconememononlu($iconememononlu)
    {
        $this->iconememononlu = $iconememononlu;

        return $this;
    }

    /**
     * Get iconememononlu
     *
     * @return boolean 
     */
    public function getIconememononlu()
    {
        return $this->iconememononlu;
    }

    /**
     * Set rayerinfotransmies
     *
     * @param boolean $rayerinfotransmies
     * @return Configuration
     */
    public function setRayerinfotransmies($rayerinfotransmies)
    {
        $this->rayerinfotransmies = $rayerinfotransmies;

        return $this;
    }

    /**
     * Get rayerinfotransmies
     *
     * @return boolean 
     */
    public function getRayerinfotransmies()
    {
        return $this->rayerinfotransmies;
    }

    /**
     * Set choixgestionrendezvous
     *
     * @param boolean $choixgestionrendezvous
     * @return Configuration
     */
    public function setChoixgestionrendezvous($choixgestionrendezvous)
    {
        $this->choixgestionrendezvous = $choixgestionrendezvous;

        return $this;
    }

    /**
     * Get choixgestionrendezvous
     *
     * @return boolean 
     */
    public function getChoixgestionrendezvous()
    {
        return $this->choixgestionrendezvous;
    }

    /**
     * Set versjouractuelle
     *
     * @param integer $versjouractuelle
     * @return Configuration
     */
    public function setVersjouractuelle($versjouractuelle)
    {
        $this->versjouractuelle = $versjouractuelle;

        return $this;
    }

    /**
     * Get versjouractuelle
     *
     * @return integer 
     */
    public function getVersjouractuelle()
    {
        return $this->versjouractuelle;
    }

    /**
     * Set contenuplanningressources
     *
     * @param string $contenuplanningressources
     * @return Configuration
     */
    public function setContenuplanningressources($contenuplanningressources)
    {
        $this->contenuplanningressources = $contenuplanningressources;

        return $this;
    }

    /**
     * Get contenuplanningressources
     *
     * @return string 
     */
    public function getContenuplanningressources()
    {
        return $this->contenuplanningressources;
    }

    /**
     * Set permierspasdelarrage
     *
     * @param boolean $permierspasdelarrage
     * @return Configuration
     */
    public function setPermierspasdelarrage($permierspasdelarrage)
    {
        $this->permierspasdelarrage = $permierspasdelarrage;

        return $this;
    }

    /**
     * Get permierspasdelarrage
     *
     * @return boolean 
     */
    public function getPermierspasdelarrage()
    {
        return $this->permierspasdelarrage;
    }

    /**
     * Set listecomplete
     *
     * @param boolean $listecomplete
     * @return Configuration
     */
    public function setListecomplete($listecomplete)
    {
        $this->listecomplete = $listecomplete;

        return $this;
    }

    /**
     * Get listecomplete
     *
     * @return boolean 
     */
    public function getListecomplete()
    {
        return $this->listecomplete;
    }

    /**
     * Set methoderecherche
     *
     * @param boolean $methoderecherche
     * @return Configuration
     */
    public function setMethoderecherche($methoderecherche)
    {
        $this->methoderecherche = $methoderecherche;

        return $this;
    }

    /**
     * Get methoderecherche
     *
     * @return boolean 
     */
    public function getMethoderecherche()
    {
        return $this->methoderecherche;
    }

    /**
     * Set alertecreationmodification
     *
     * @param boolean $alertecreationmodification
     * @return Configuration
     */
    public function setAlertecreationmodification($alertecreationmodification)
    {
        $this->alertecreationmodification = $alertecreationmodification;

        return $this;
    }

    /**
     * Get alertecreationmodification
     *
     * @return boolean 
     */
    public function getAlertecreationmodification()
    {
        return $this->alertecreationmodification;
    }

    /**
     * Set alertetachesnoneffectueeslues
     *
     * @param boolean $alertetachesnoneffectueeslues
     * @return Configuration
     */
    public function setAlertetachesnoneffectueeslues($alertetachesnoneffectueeslues)
    {
        $this->alertetachesnoneffectueeslues = $alertetachesnoneffectueeslues;

        return $this;
    }

    /**
     * Get alertetachesnoneffectueeslues
     *
     * @return boolean 
     */
    public function getAlertetachesnoneffectueeslues()
    {
        return $this->alertetachesnoneffectueeslues;
    }

    /**
     * Set alertecreemodifiememo
     *
     * @param boolean $alertecreemodifiememo
     * @return Configuration
     */
    public function setAlertecreemodifiememo($alertecreemodifiememo)
    {
        $this->alertecreemodifiememo = $alertecreemodifiememo;

        return $this;
    }

    /**
     * Get alertecreemodifiememo
     *
     * @return boolean 
     */
    public function getAlertecreemodifiememo()
    {
        return $this->alertecreemodifiememo;
    }

    /**
     * Set triermouvements
     *
     * @param boolean $triermouvements
     * @return Configuration
     */
    public function setTriermouvements($triermouvements)
    {
        $this->triermouvements = $triermouvements;

        return $this;
    }

    /**
     * Get triermouvements
     *
     * @return boolean 
     */
    public function getTriermouvements()
    {
        return $this->triermouvements;
    }

    /**
     * Set notestransmises
     *
     * @param boolean $notestransmises
     * @return Configuration
     */
    public function setNotestransmises($notestransmises)
    {
        $this->notestransmises = $notestransmises;

        return $this;
    }

    /**
     * Get notestransmises
     *
     * @return boolean 
     */
    public function getNotestransmises()
    {
        return $this->notestransmises;
    }

    /**
     * Set notesrecurrentes
     *
     * @param boolean $notesrecurrentes
     * @return Configuration
     */
    public function setNotesrecurrentes($notesrecurrentes)
    {
        $this->notesrecurrentes = $notesrecurrentes;

        return $this;
    }

    /**
     * Get notesrecurrentes
     *
     * @return boolean 
     */
    public function getNotesrecurrentes()
    {
        return $this->notesrecurrentes;
    }

    /**
     * Set mouvementautreutilisateurs
     *
     * @param boolean $mouvementautreutilisateurs
     * @return Configuration
     */
    public function setMouvementautreutilisateurs($mouvementautreutilisateurs)
    {
        $this->mouvementautreutilisateurs = $mouvementautreutilisateurs;

        return $this;
    }

    /**
     * Get mouvementautreutilisateurs
     *
     * @return boolean 
     */
    public function getMouvementautreutilisateurs()
    {
        return $this->mouvementautreutilisateurs;
    }

    /**
     * Set afficherlibelle
     *
     * @param boolean $afficherlibelle
     * @return Configuration
     */
    public function setAfficherlibelle($afficherlibelle)
    {
        $this->afficherlibelle = $afficherlibelle;

        return $this;
    }

    /**
     * Get afficherlibelle
     *
     * @return boolean 
     */
    public function getAfficherlibelle()
    {
        return $this->afficherlibelle;
    }

    /**
     * Set detaillibelle
     *
     * @param string $detaillibelle
     * @return Configuration
     */
    public function setDetaillibelle($detaillibelle)
    {
        $this->detaillibelle = $detaillibelle;

        return $this;
    }

    /**
     * Get detaillibelle
     *
     * @return string 
     */
    public function getDetaillibelle()
    {
        return $this->detaillibelle;
    }

    /**
     * Set maxinfoagrees
     *
     * @param string $maxinfoagrees
     * @return Configuration
     */
    public function setMaxinfoagrees($maxinfoagrees)
    {
        $this->maxinfoagrees = $maxinfoagrees;

        return $this;
    }

    /**
     * Get maxinfoagrees
     *
     * @return string 
     */
    public function getMaxinfoagrees()
    {
        return $this->maxinfoagrees;
    }

    /**
     * Set affichercelles
     *
     * @param integer $affichercelles
     * @return Configuration
     */
    public function setAffichercelles($affichercelles)
    {
        $this->affichercelles = $affichercelles;

        return $this;
    }

    /**
     * Get affichercelles
     *
     * @return integer 
     */
    public function getAffichercelles()
    {
        return $this->affichercelles;
    }

    /**
     * Set affichernomeffectuees
     *
     * @param boolean $affichernomeffectuees
     * @return Configuration
     */
    public function setAffichernomeffectuees($affichernomeffectuees)
    {
        $this->affichernomeffectuees = $affichernomeffectuees;

        return $this;
    }

    /**
     * Get affichernomeffectuees
     *
     * @return boolean 
     */
    public function getAffichernomeffectuees()
    {
        return $this->affichernomeffectuees;
    }

    /**
     * Set affichernomlues
     *
     * @param boolean $affichernomlues
     * @return Configuration
     */
    public function setAffichernomlues($affichernomlues)
    {
        $this->affichernomlues = $affichernomlues;

        return $this;
    }

    /**
     * Get affichernomlues
     *
     * @return boolean 
     */
    public function getAffichernomlues()
    {
        return $this->affichernomlues;
    }

    /**
     * Set memodejatransmis
     *
     * @param boolean $memodejatransmis
     * @return Configuration
     */
    public function setMemodejatransmis($memodejatransmis)
    {
        $this->memodejatransmis = $memodejatransmis;

        return $this;
    }

    /**
     * Get memodejatransmis
     *
     * @return boolean 
     */
    public function getMemodejatransmis()
    {
        return $this->memodejatransmis;
    }

    /**
     * Set positionnercuseur
     *
     * @param boolean $positionnercuseur
     * @return Configuration
     */
    public function setPositionnercuseur($positionnercuseur)
    {
        $this->positionnercuseur = $positionnercuseur;

        return $this;
    }

    /**
     * Get positionnercuseur
     *
     * @return boolean 
     */
    public function getPositionnercuseur()
    {
        return $this->positionnercuseur;
    }

    /**
     * Set motifannulationrv
     *
     * @param boolean $motifannulationrv
     * @return Configuration
     */
    public function setMotifannulationrv($motifannulationrv)
    {
        $this->motifannulationrv = $motifannulationrv;

        return $this;
    }

    /**
     * Get motifannulationrv
     *
     * @return boolean 
     */
    public function getMotifannulationrv()
    {
        return $this->motifannulationrv;
    }

    /**
     * Set memoannulationrv
     *
     * @param boolean $memoannulationrv
     * @return Configuration
     */
    public function setMemoannulationrv($memoannulationrv)
    {
        $this->memoannulationrv = $memoannulationrv;

        return $this;
    }

    /**
     * Get memoannulationrv
     *
     * @return boolean 
     */
    public function getMemoannulationrv()
    {
        return $this->memoannulationrv;
    }

    /**
     * Set memoclientinformation
     *
     * @param boolean $memoclientinformation
     * @return Configuration
     */
    public function setMemoclientinformation($memoclientinformation)
    {
        $this->memoclientinformation = $memoclientinformation;

        return $this;
    }

    /**
     * Get memoclientinformation
     *
     * @return boolean 
     */
    public function getMemoclientinformation()
    {
        return $this->memoclientinformation;
    }

    /**
     * Set tachecompteprincipal
     *
     * @param boolean $tachecompteprincipal
     * @return Configuration
     */
    public function setTachecompteprincipal($tachecompteprincipal)
    {
        $this->tachecompteprincipal = $tachecompteprincipal;

        return $this;
    }

    /**
     * Get tachecompteprincipal
     *
     * @return boolean 
     */
    public function getTachecompteprincipal()
    {
        return $this->tachecompteprincipal;
    }

    /**
     * Set tachevisibleutilisateurconcerne
     *
     * @param boolean $tachevisibleutilisateurconcerne
     * @return Configuration
     */
    public function setTachevisibleutilisateurconcerne($tachevisibleutilisateurconcerne)
    {
        $this->tachevisibleutilisateurconcerne = $tachevisibleutilisateurconcerne;

        return $this;
    }

    /**
     * Get tachevisibleutilisateurconcerne
     *
     * @return boolean 
     */
    public function getTachevisibleutilisateurconcerne()
    {
        return $this->tachevisibleutilisateurconcerne;
    }

    /**
     * Set conserverinforvcopie
     *
     * @param boolean $conserverinforvcopie
     * @return Configuration
     */
    public function setConserverinforvcopie($conserverinforvcopie)
    {
        $this->conserverinforvcopie = $conserverinforvcopie;

        return $this;
    }

    /**
     * Get conserverinforvcopie
     *
     * @return boolean 
     */
    public function getConserverinforvcopie()
    {
        return $this->conserverinforvcopie;
    }

    /**
     * Set rendezvousafficher
     *
     * @param string $rendezvousafficher
     * @return Configuration
     */
    public function setRendezvousafficher($rendezvousafficher)
    {
        $this->rendezvousafficher = $rendezvousafficher;

        return $this;
    }

    /**
     * Get rendezvousafficher
     *
     * @return string 
     */
    public function getRendezvousafficher()
    {
        return $this->rendezvousafficher;
    }

    /**
     * Set incluredelimiteurs
     *
     * @param boolean $incluredelimiteurs
     * @return Configuration
     */
    public function setIncluredelimiteurs($incluredelimiteurs)
    {
        $this->incluredelimiteurs = $incluredelimiteurs;

        return $this;
    }

    /**
     * Get incluredelimiteurs
     *
     * @return boolean 
     */
    public function getIncluredelimiteurs()
    {
        return $this->incluredelimiteurs;
    }
}
