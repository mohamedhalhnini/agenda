<?php

namespace bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Delimiteur
 *
 * @ORM\Table(name="delimiteur")
 * @ORM\Entity
 */
class Delimiteur
{

    
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     *  @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ACCESSINTERNET", type="integer", nullable=true)
     */
    private $accessinternet;

    /**
     * @var integer
     *
     * @ORM\Column(name="BLOCKAGE", type="integer", nullable=true)
     */
    private $blockage;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARTAGE", type="integer", nullable=true)
     */
    private $partage;

    /**
     * @var integer
     *
     * @ORM\Column(name="PRIORITE", type="integer", nullable=true)
     */
    private $priorite;

    /**
     * @var string
     *
     * @ORM\Column(name="TITRE", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DEBUTE", type="time", nullable=true)
     */
    private $debute;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FIN", type="time", nullable=true)
     */
    private $fin;
    
    /**
     * @var \Periodicite
     *
     * @ORM\ManyToOne(targetEntity="Periodicite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERIODICITE_ID", referencedColumnName="ID")
     * })
     */
    private $periodicite;

    /**
     * @var \Agenda
     *
     * @ORM\ManyToOne(targetEntity="Agenda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="AGENDA_ID", referencedColumnName="ID")
     * })
     */
    private $agenda;
  
     /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur",inversedBy="delimiteurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="UTILISATEUR_ID", referencedColumnName="ID")
     * })
     */
    private $utilisateur;

    

   
     /**
     * Set id
     *
     * @param integer $id
     * @return Delimiteur
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accessinternet
     *
     * @param integer $accessinternet
     * @return Delimiteur
     */
    public function setAccessinternet($accessinternet)
    {
        $this->accessinternet = $accessinternet;

        return $this;
    }

    /**
     * Get accessinternet
     *
     * @return integer 
     */
    public function getAccessinternet()
    {
        return $this->accessinternet;
    }

    /**
     * Set blockage
     *
     * @param integer $blockage
     * @return Delimiteur
     */
    public function setBlockage($blockage)
    {
        $this->blockage = $blockage;

        return $this;
    }

    /**
     * Get blockage
     *
     * @return integer 
     */
    public function getBlockage()
    {
        return $this->blockage;
    }

    /**
     * Set partage
     *
     * @param integer $partage
     * @return Delimiteur
     */
    public function setPartage($partage)
    {
        $this->partage = $partage;

        return $this;
    }

    /**
     * Get partage
     *
     * @return integer 
     */
    public function getPartage()
    {
        return $this->partage;
    }

    /**
     * Set priorite
     *
     * @param integer $priorite
     * @return Delimiteur
     */
    public function setPriorite($priorite)
    {
        $this->priorite = $priorite;

        return $this;
    }

    /**
     * Get priorite
     *
     * @return integer 
     */
    public function getPriorite()
    {
        return $this->priorite;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Delimiteur
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set datedebut
     *
     * @param \DateTime $datedebut
     * @return Delimiteur
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * Get datedebut
     *
     * @return \DateTime 
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return Delimiteur
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set periodicite
     *
     * @param \bean\beanBundle\Entity\Periodicite $periodicite
     * @return Delimiteur
     */
    public function setPeriodicite(\bean\beanBundle\Entity\Periodicite $periodicite = null)
    {
        $this->periodicite = $periodicite;

        return $this;
    }

    /**
     * Get periodicite
     *
     * @return \bean\beanBundle\Entity\Periodicite 
     */
    public function getPeriodicite()
    {
        return $this->periodicite;
    }

    /**
     * Set agenda
     *
     * @param \bean\beanBundle\Entity\Agenda $agenda
     * @return Delimiteur
     */
    public function setAgenda(\bean\beanBundle\Entity\Agenda $agenda = null)
    {
        $this->agenda = $agenda;

        return $this;
    }

    /**
     * Get agenda
     *
     * @return \bean\beanBundle\Entity\Agenda 
     */
    public function getAgenda()
    {
        return $this->agenda;
    }

    /**
     * Set utilisateur
     *
     * @param \bean\beanBundle\Entity\Utilisateur $utilisateur
     * @return Delimiteur
     */
    public function setUtilisateur(\bean\beanBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \bean\beanBundle\Entity\Utilisateur 
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }
    
    
    public function getDebute() {
        return $this->debute;
    }

    public function getFin() {
        return $this->fin;
    }

    public function setDebute(\DateTime $debute) {
        $this->debute = $debute;
    }

    public function setFin(\DateTime $fin) {
        $this->fin = $fin;
    }


}
