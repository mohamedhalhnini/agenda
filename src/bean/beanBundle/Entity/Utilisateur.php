<?php

namespace bean\beanBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur")
 * @ORM\Entity
 */
class Utilisateur extends BaseUser {

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ADRESSE", type="string", nullable=true)
     */
    private $adresse;

    /**
     * @var integer
     *
     * @ORM\Column(name="CP", type="integer", nullable=true)
     */
    private $cp;

    /**
     * @var integer
     *
     * @ORM\Column(name="MYRAPPELALERT", type="integer", nullable=true)
     */
    private $myrappelalert;

    /**
     * @var string
     *
     * @ORM\Column(name="NOM", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="NOTES", type="string", length=255, nullable=true)
     */
    private $notes;

    /**
     * @var string
     *
     * @ORM\Column(name="PRENOM", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="VILLE", type="string", nullable=true)
     */
    private $ville;

    /**
     * @var \Agenda
     *
     * @ORM\ManyToOne(targetEntity="Agenda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="AGENDA_ID", referencedColumnName="ID")
     * })
     */
    private $agenda;

    /**
     * @var \PositionActuelle
     *
     * @ORM\ManyToOne(targetEntity="PositionActuelle",inversedBy="utilisateurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="POSITIONACTUELLE_ID", referencedColumnName="ID")
     * })
     */
    private $positionActuelle;

    /**
     * @ORM\OneToMany(targetEntity="RendezVous",mappedBy="utilisateur",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $mesRendezVous;

    /**
     * @ORM\OneToMany(targetEntity="Instruction",mappedBy="utilisateur",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $instructions;

    /**
     * @ORM\OneToMany(targetEntity="InstructionArchivee",mappedBy="utilisateur",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $instructionsArchivees;

    /**
     * @ORM\OneToMany(targetEntity="Memo",mappedBy="utilisateur",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $memos;

    /**
     * @ORM\OneToMany(targetEntity="Client",mappedBy="utilisateur",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $clients;

    /**
     * @ORM\OneToMany(targetEntity="RendezVousType",mappedBy="utilisateur",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $rendezVousTypes;

    /**
     * @ORM\OneToMany(targetEntity="Delimiteur",mappedBy="utilisateur",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $delimiteurs;

    /**
     * @var \Configuration
     *
     * @ORM\OneToOne(targetEntity="Configuration")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CONFIGURATION_ID", referencedColumnName="ID")
     * })
     */
    private $configuration;

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->mesRendezVous = new \Doctrine\Common\Collections\ArrayCollection();
        $this->instructions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->instructionsArchivees = new \Doctrine\Common\Collections\ArrayCollection();
        $this->memos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clients = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rendezVousTypes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->delimiteurs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Utilisateur
     */
    public function setAdresse($adresse) {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse() {
        return $this->adresse;
    }

    /**
     * Set cp
     *
     * @param integer $cp
     * @return Utilisateur
     */
    public function setCp($cp) {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return integer 
     */
    public function getCp() {
        return $this->cp;
    }

    /**
     * Set myrappelalert
     *
     * @param integer $myrappelalert
     * @return Utilisateur
     */
    public function setMyrappelalert($myrappelalert) {
        $this->myrappelalert = $myrappelalert;

        return $this;
    }

    /**
     * Get myrappelalert
     *
     * @return integer 
     */
    public function getMyrappelalert() {
        return $this->myrappelalert;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Utilisateur
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Utilisateur
     */
    public function setNotes($notes) {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes() {
        return $this->notes;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Utilisateur
     */
    public function setPrenom($prenom) {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom() {
        return $this->prenom;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Utilisateur
     */
    public function setVille($ville) {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille() {
        return $this->ville;
    }

    /**
     * Set agenda
     *
     * @param \bean\beanBundle\Entity\Agenda $agenda
     * @return Utilisateur
     */
    public function setAgenda(\bean\beanBundle\Entity\Agenda $agenda = null) {
        $this->agenda = $agenda;

        return $this;
    }

    /**
     * Get agenda
     *
     * @return \bean\beanBundle\Entity\Agenda 
     */
    public function getAgenda() {
        return $this->agenda;
    }

    /**
     * Set positionActuelle
     *
     * @param \bean\beanBundle\Entity\PositionActuelle $positionActuelle
     * @return Utilisateur
     */
    public function setPositionActuelle(\bean\beanBundle\Entity\PositionActuelle $positionActuelle = null) {
        $this->positionActuelle = $positionActuelle;

        return $this;
    }

    /**
     * Get positionActuelle
     *
     * @return \bean\beanBundle\Entity\PositionActuelle 
     */
    public function getPositionActuelle() {
        return $this->positionActuelle;
    }

    /**
     * Add mesRendezVous
     *
     * @param \bean\beanBundle\Entity\RendezVous $mesRendezVous
     * @return Utilisateur
     */
    public function addMesRendezVous(\bean\beanBundle\Entity\RendezVous $mesRendezVous) {
        $this->mesRendezVous[] = $mesRendezVous;

        return $this;
    }

    /**
     * Remove mesRendezVous
     *
     * @param \bean\beanBundle\Entity\RendezVous $mesRendezVous
     */
    public function removeMesRendezVous(\bean\beanBundle\Entity\RendezVous $mesRendezVous) {
        $this->mesRendezVous->removeElement($mesRendezVous);
    }

    /**
     * Get mesRendezVous
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMesRendezVous() {
        return $this->mesRendezVous;
    }

    /**
     * Add instructions
     *
     * @param \bean\beanBundle\Entity\Instruction $instructions
     * @return Utilisateur
     */
    public function addInstruction(\bean\beanBundle\Entity\Instruction $instructions) {
        $this->instructions[] = $instructions;

        return $this;
    }

    /**
     * Remove instructions
     *
     * @param \bean\beanBundle\Entity\Instruction $instructions
     */
    public function removeInstruction(\bean\beanBundle\Entity\Instruction $instructions) {
        $this->instructions->removeElement($instructions);
    }

    /**
     * Get instructions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInstructions() {
        return $this->instructions;
    }

    /**
     * Add instructionsArchivees
     *
     * @param \bean\beanBundle\Entity\InstructionArchivee $instructionsArchivees
     * @return Utilisateur
     */
    public function addInstructionsArchivee(\bean\beanBundle\Entity\InstructionArchivee $instructionsArchivees) {
        $this->instructionsArchivees[] = $instructionsArchivees;

        return $this;
    }

    /**
     * Remove instructionsArchivees
     *
     * @param \bean\beanBundle\Entity\InstructionArchivee $instructionsArchivees
     */
    public function removeInstructionsArchivee(\bean\beanBundle\Entity\InstructionArchivee $instructionsArchivees) {
        $this->instructionsArchivees->removeElement($instructionsArchivees);
    }

    /**
     * Get instructionsArchivees
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInstructionsArchivees() {
        return $this->instructionsArchivees;
    }

    /**
     * Add memos
     *
     * @param \bean\beanBundle\Entity\Memo $memos
     * @return Utilisateur
     */
    public function addMemo(\bean\beanBundle\Entity\Memo $memos) {
        $this->memos[] = $memos;

        return $this;
    }

    /**
     * Remove memos
     *
     * @param \bean\beanBundle\Entity\Memo $memos
     */
    public function removeMemo(\bean\beanBundle\Entity\Memo $memos) {
        $this->memos->removeElement($memos);
    }

    /**
     * Get memos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMemos() {
        return $this->memos;
    }

    /**
     * Add clients
     *
     * @param \bean\beanBundle\Entity\Client $clients
     * @return Utilisateur
     */
    public function addClient(\bean\beanBundle\Entity\Client $clients) {
        $this->clients[] = $clients;

        return $this;
    }

    /**
     * Remove clients
     *
     * @param \bean\beanBundle\Entity\Client $clients
     */
    public function removeClient(\bean\beanBundle\Entity\Client $clients) {
        $this->clients->removeElement($clients);
    }

    /**
     * Get clients
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClients() {
        return $this->clients;
    }

    /**
     * Add rendezVousTypes
     *
     * @param \bean\beanBundle\Entity\RendezVousType $rendezVousTypes
     * @return Utilisateur
     */
    public function addRendezVousType(\bean\beanBundle\Entity\RendezVousType $rendezVousTypes) {
        $this->rendezVousTypes[] = $rendezVousTypes;

        return $this;
    }

    /**
     * Remove rendezVousTypes
     *
     * @param \bean\beanBundle\Entity\RendezVousType $rendezVousTypes
     */
    public function removeRendezVousType(\bean\beanBundle\Entity\RendezVousType $rendezVousTypes) {
        $this->rendezVousTypes->removeElement($rendezVousTypes);
    }

    /**
     * Get rendezVousTypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRendezVousTypes() {
        return $this->rendezVousTypes;
    }

    /**
     * Add delimiteurs
     *
     * @param \bean\beanBundle\Entity\Delimiteur $delimiteurs
     * @return Utilisateur
     */
    public function addDelimiteur(\bean\beanBundle\Entity\Delimiteur $delimiteurs) {
        $this->delimiteurs[] = $delimiteurs;

        return $this;
    }

    /**
     * Remove delimiteurs
     *
     * @param \bean\beanBundle\Entity\Delimiteur $delimiteurs
     */
    public function removeDelimiteur(\bean\beanBundle\Entity\Delimiteur $delimiteurs) {
        $this->delimiteurs->removeElement($delimiteurs);
    }

    /**
     * Get delimiteurs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDelimiteurs() {
        return $this->delimiteurs;
    }

    /**
     * Set configuration
     *
     * @param \bean\beanBundle\Entity\Configuration $configuration
     * @return Utilisateur
     */
    public function setConfiguration(\bean\beanBundle\Entity\Configuration $configuration = null) {
        $this->configuration = $configuration;

        return $this;
    }

    /**
     * Get configuration
     *
     * @return \bean\beanBundle\Entity\Configuration 
     */
    public function getConfiguration() {
        return $this->configuration;
    }

}
