<?php

namespace bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Periodicite
 *
 * @ORM\Table(name="periodicite")
 * @ORM\Entity
 */
class Periodicite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="COULEURALLOCCURENCE", type="string", length=255, nullable=true)
     */
    private $couleuralloccurence;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EFFET", type="datetime", nullable=true)
     */
    private $effet;

    /**
     * @var integer
     *
     * @ORM\Column(name="FINAPRESOCCURENCE", type="integer", nullable=true)
     */
    private $finapresoccurence;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="END", type="datetime", nullable=true)
     */
    private $end;//date fin

    /**
     * @var array
     *
     * @ORM\Column(name="JOURSELECTIONNE", type="array", nullable=true)
     */
    private $jourselectionne;

    /**
     * @var string
     *
     * @ORM\Column(name="LIBELLEALLOCCURENCE", type="string", length=255, nullable=true)
     */
    private $libellealloccurence;

    /**
     * @var integer
     *
     * @ORM\Column(name="TOUSLES", type="integer", nullable=true)
     */
    private $tousles;

    /**
     * @var integer
     *
     * @ORM\Column(name="TOUSLESJOURS", type="integer", nullable=true)
     */
    private $touslesjours;

    /**
     * @var integer
     *
     * @ORM\Column(name="TOUSLESJOURSOUVRABLE", type="integer", nullable=true)
     */
    private $touslesjoursouvrable;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPEPLAGEPERIODICITE", type="integer", nullable=true)
     */
    private $typeplageperiodicite;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPEQUOTIDIENT", type="integer", nullable=true)
     */
    private $typequotidient;

    
   
    /**
     * @ORM\OneToMany(targetEntity="Instruction",mappedBy="periodicite",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
     private $instructions;
     
     
    /**
     * @ORM\OneToMany(targetEntity="RendezVous", mappedBy="priodicite")
     * @ORM\JoinColumn(nullable=true)
     */
    private $rendezVous;


 
    /**
     *@ORM\ManyToOne(targetEntity="ranges", inversedBy="periodicites",cascade={"persist","remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="RANGES_ID", referencedColumnName="ID",nullable = true)
     * })
     */
    private $ranges;
   
  
  
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->instructions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rendezVous = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set couleuralloccurence
     *
     * @param string $couleuralloccurence
     * @return Periodicite
     */
    public function setCouleuralloccurence($couleuralloccurence)
    {
        $this->couleuralloccurence = $couleuralloccurence;
    
        return $this;
    }

    /**
     * Get couleuralloccurence
     *
     * @return string 
     */
    public function getCouleuralloccurence()
    {
        return $this->couleuralloccurence;
    }

    /**
     * Set effet
     *
     * @param \DateTime $effet
     * @return Periodicite
     */
    public function setEffet($effet)
    {
        $this->effet = $effet;
    
        return $this;
    }

    /**
     * Get effet
     *
     * @return \DateTime 
     */
    public function getEffet()
    {
        return $this->effet;
    }

    /**
     * Set finapresoccurence
     *
     * @param integer $finapresoccurence
     * @return Periodicite
     */
    public function setFinapresoccurence($finapresoccurence)
    {
        $this->finapresoccurence = $finapresoccurence;
    
        return $this;
    }

    /**
     * Get finapresoccurence
     *
     * @return integer 
     */
    public function getFinapresoccurence()
    {
        return $this->finapresoccurence;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return Periodicite
     */
    public function setEnd($end)
    {
        $this->end = $end;
    
        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set jourselectionne
     *
     * @param array $jourselectionne
     * @return Periodicite
     */
    public function setJourselectionne($jourselectionne)
    {
        $this->jourselectionne = $jourselectionne;
    
        return $this;
    }

    /**
     * Get jourselectionne
     *
     * @return array 
     */
    public function getJourselectionne()
    {
        return $this->jourselectionne;
    }

    /**
     * Set libellealloccurence
     *
     * @param string $libellealloccurence
     * @return Periodicite
     */
    public function setLibellealloccurence($libellealloccurence)
    {
        $this->libellealloccurence = $libellealloccurence;
    
        return $this;
    }

    /**
     * Get libellealloccurence
     *
     * @return string 
     */
    public function getLibellealloccurence()
    {
        return $this->libellealloccurence;
    }

    /**
     * Set tousles
     *
     * @param integer $tousles
     * @return Periodicite
     */
    public function setTousles($tousles)
    {
        $this->tousles = $tousles;
    
        return $this;
    }

    /**
     * Get tousles
     *
     * @return integer 
     */
    public function getTousles()
    {
        return $this->tousles;
    }

    /**
     * Set touslesjours
     *
     * @param integer $touslesjours
     * @return Periodicite
     */
    public function setTouslesjours($touslesjours)
    {
        $this->touslesjours = $touslesjours;
    
        return $this;
    }

    /**
     * Get touslesjours
     *
     * @return integer 
     */
    public function getTouslesjours()
    {
        return $this->touslesjours;
    }

    /**
     * Set touslesjoursouvrable
     *
     * @param integer $touslesjoursouvrable
     * @return Periodicite
     */
    public function setTouslesjoursouvrable($touslesjoursouvrable)
    {
        $this->touslesjoursouvrable = $touslesjoursouvrable;
    
        return $this;
    }

    /**
     * Get touslesjoursouvrable
     *
     * @return integer 
     */
    public function getTouslesjoursouvrable()
    {
        return $this->touslesjoursouvrable;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Periodicite
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set typeplageperiodicite
     *
     * @param integer $typeplageperiodicite
     * @return Periodicite
     */
    public function setTypeplageperiodicite($typeplageperiodicite)
    {
        $this->typeplageperiodicite = $typeplageperiodicite;
    
        return $this;
    }

    /**
     * Get typeplageperiodicite
     *
     * @return integer 
     */
    public function getTypeplageperiodicite()
    {
        return $this->typeplageperiodicite;
    }

    /**
     * Set typequotidient
     *
     * @param integer $typequotidient
     * @return Periodicite
     */
    public function setTypequotidient($typequotidient)
    {
        $this->typequotidient = $typequotidient;
    
        return $this;
    }

    /**
     * Get typequotidient
     *
     * @return integer 
     */
    public function getTypequotidient()
    {
        return $this->typequotidient;
    }

    /**
     * Add instructions
     *
     * @param \bean\beanBundle\Entity\Instruction $instructions
     * @return Periodicite
     */
    public function addInstruction(\bean\beanBundle\Entity\Instruction $instructions)
    {
        $this->instructions[] = $instructions;
    
        return $this;
    }

    /**
     * Remove instructions
     *
     * @param \bean\beanBundle\Entity\Instruction $instructions
     */
    public function removeInstruction(\bean\beanBundle\Entity\Instruction $instructions)
    {
        $this->instructions->removeElement($instructions);
    }

    /**
     * Get instructions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * Add rendezVous
     *
     * @param \bean\beanBundle\Entity\RendezVous $rendezVous
     * @return Periodicite
     */
    public function addRendezVous(\bean\beanBundle\Entity\RendezVous $rendezVous)
    {
        $this->rendezVous[] = $rendezVous;
    
        return $this;
    }

    /**
     * Remove rendezVous
     *
     * @param \bean\beanBundle\Entity\RendezVous $rendezVous
     */
    public function removeRendezVous(\bean\beanBundle\Entity\RendezVous $rendezVous)
    {
        $this->rendezVous->removeElement($rendezVous);
    }

    /**
     * Get rendezVous
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRendezVous()
    {
        return $this->rendezVous;
    }

    /**
     * Set ranges
     *
     * @param \bean\beanBundle\Entity\ranges $ranges
     * @return Periodicite
     */
    public function setRanges(\bean\beanBundle\Entity\ranges $ranges = null)
    {
        $this->ranges = $ranges;
    
        return $this;
    }

    /**
     * Get ranges
     *
     * @return \bean\beanBundle\Entity\ranges 
     */
    public function getRanges()
    {
        return $this->ranges;
    }
}
