<?php

namespace bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ranges
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="bean\beanBundle\Entity\rangesRepository")
 */
class ranges
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime")
     */
    private $end;
    
    /**
     * @ORM\OneToMany(targetEntity="Periodicite", mappedBy="ranges")
     * @ORM\JoinColumn(nullable=true)
     */
    private $periodicites;

    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->periodicites = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return ranges
     */
    public function setStart($start)
    {
        $this->start = $start;
    
        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return ranges
     */
    public function setEnd($end)
    {
        $this->end = $end;
    
        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Add periodicites
     *
     * @param \bean\beanBundle\Entity\Periodicite $periodicites
     * @return ranges
     */
    public function addPeriodicite(\bean\beanBundle\Entity\Periodicite $periodicites)
    {
        $this->periodicites[] = $periodicites;
    
        return $this;
    }

    /**
     * Remove periodicites
     *
     * @param \bean\beanBundle\Entity\Periodicite $periodicites
     */
    public function removePeriodicite(\bean\beanBundle\Entity\Periodicite $periodicites)
    {
        $this->periodicites->removeElement($periodicites);
    }

    /**
     * Get periodicites
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPeriodicites()
    {
        return $this->periodicites;
    }
}
