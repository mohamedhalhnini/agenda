<?php

namespace  bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Agenda
 *
 * @ORM\Table(name="memo")
 * @ORM\Entity
 */
class Memo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ALERT", type="string", nullable=true)
     */
    private $alert;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATEENREGISTREMENT", type="datetime", nullable=true)
     */
    private $dateenregistrement;
    /**
     * @var \integer
     *
     * @ORM\Column(name="VU", type="integer", nullable=true)
     */
    private $vu;

    /**
     * @var \string
     *
     * @ORM\Column(name="color", type="string", nullable=true)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="DETAIL", type="string", length=255, nullable=true)
     */
    private $detail;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERMANANTE", type="integer", nullable=true)
     */
    private $permanante;

    /**
 * @var integer
 *
 * @ORM\Column(name="PRIORITE", type="integer", nullable=true)
 */
    private $priorite;

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="allDay", type="string", length=255, nullable=true)
     */
    private $allDay;

    /**
     *@ORM\ManyToOne(targetEntity="Periodicite", inversedBy="memos",cascade={"persist","remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERIODICITE_ID", referencedColumnName="ID",nullable = true)
     * })
     */
    private $periodicite;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur",inversedBy="memos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="UTILISATEUR_ID", referencedColumnName="ID")
     * })
     */
    private $utilisateur;

    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Client",inversedBy="memos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CLIENT_ID", referencedColumnName="ID")
     * })
     */
    private $clientFrequent;
    
    /**
     * @var \PhraseUsuelle
     *
     * @ORM\ManyToOne(targetEntity="PhraseUsuelle",inversedBy="memos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PHRASEUSUELLE_ID", referencedColumnName="ID")
     * })
     */
    private $phraseUsuelle;




    /**
     * Set periodicite
     *
     * @param \bean\beanBundle\Entity\Periodicite $periodicite
     * @return Memo
     */
    public function setPeriodicite(\bean\beanBundle\Entity\Periodicite $periodicite = null)
    {
        $this->periodicite = $periodicite;
    
        return $this;
    }
        /**
     * Get periodicite
     *
     * @return \bean\beanBundle\Entity\Periodicite 
     */
    public function getPeriodicite()
    {
        return $this->periodicite;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Memo
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Set alert
     *
     * @param string $alert
     * @return Memo
     */
    public function setAlert($alert)
    {
        $this->alert = $alert;

        return $this;
    }

    /**
     * Set allDay
     *
     * @param string $allDay
     * @return Memo
     */
    public function setAllDay($allDay)
    {
        $this->allDay = $allDay;

        return $this;
    }

    /**
     * Get allDay
     *
     * @return string
     */
    public function getAllDay()
    {
        return $this->allDay;
    }

    /**
     * Get alert
     *
     * @return string 
     */
    public function getAlert()
    {
        return $this->alert;
    }

    /**
     * Set dateenregistrement
     *
     * @param \DateTime $dateenregistrement
     * @return Memo
     */
    public function setDateenregistrement($dateenregistrement)
    {
        $this->dateenregistrement = $dateenregistrement;
        return $this;
    }

    /**
     * Get dateenregistrement
     *
     * @return \DateTime 
     */
    public function getDateenregistrement()
    {
        return $this->dateenregistrement;
    }

    /**
     * Set detail
     *
     * @param string $detail
     * @return Memo
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
 * Get detail
 *
 * @return string
 */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Get vu
     *
     * @return integer
     */
    public function getVu()
    {
        return $this->vu;
    }

    /**
     * Set permanante
     *
     * @param integer $permanante
     * @return Memo
     */
    public function setPermanante($permanante)
    {
        $this->permanante = $permanante;

        return $this;
    }

    /**
     * Set vu
     *
     * @param integer $vu
     * @return Memo
     */
    public function setVu($vu)
    {
        $this->vu = $vu;

        return $this;
    }


    /**
     * Get permanante
     *
     * @return integer 
     */
    public function getPermanante()
    {
        return $this->permanante;
    }

    /**
     * Set priorite
     *
     * @param integer $priorite
     * @return Memo
     */
    public function setPriorite($priorite)
    {
        $this->priorite = $priorite;

        return $this;
    }

    /**
     * Get priorite
     *
     * @return integer 
     */
    public function getPriorite()
    {
        return $this->priorite;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Memo
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set utilisateur
     *
     * @param \bean\beanBundle\Entity\Utilisateur $utilisateur
     * @return Memo
     */
    public function setUtilisateur(\bean\beanBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \bean\beanBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set client
     *
     * @param \bean\beanBundle\Entity\Client $clientFrequent
     * @return Memo
     */
    public function setClientFrequent(\bean\beanBundle\Entity\Client $clientFrequent = null)
    {
        $this->clientFrequent = $clientFrequent;

        return $this;
    }

    /**
     * Get client
     *
     * @return \bean\beanBundle\Entity\Client
     */
    public function getClientFrequent()
    {
        return $this->clientFrequent;
    }

    /**
     * Set phraseUsuelle
     *
     * @param \bean\beanBundle\Entity\PhraseUsuelle $phraseUsuelle
     * @return Memo
     */
    public function setPhraseUsuelle( \bean\beanBundle\Entity\PhraseUsuelle $phraseUsuelle = null)
    {
        $this->phraseUsuelle = $phraseUsuelle;

        return $this;
    }

    /**
     * Get phraseUsuelle
     *
     * @return \bean\beanBundle\Entity\PhraseUsuelle
     */
    public function getPhraseUsuelle()
    {
        return $this->phraseUsuelle;
    }
}
