<?php

namespace bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tranche
 *
 * @ORM\Table(name="tranche")
 * @ORM\Entity
 */
class Tranche
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="A", type="string", length=255, nullable=true)
     */
    private $a;

    /**
     * @var string
     *
     * @ORM\Column(name="DE", type="string", length=255, nullable=true)
     */
    private $de;

    /**
     * @var integer
     *
     * @ORM\Column(name="TRANCHE", type="integer", nullable=true)
     */
    private $tranche;

    /**
     * @var \Jour
     *
     * @ORM\ManyToOne(targetEntity="Jour",inversedBy="mesTranches")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="JOUR_ID", referencedColumnName="ID")
     * })
     */
    private $jour;






    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set a
     *
     * @param string $a
     * @return Tranche
     */
    public function setA($a)
    {
        $this->a = $a;

        return $this;
    }

    /**
     * Get a
     *
     * @return string 
     */
    public function getA()
    {
        return $this->a;
    }

    /**
     * Set de
     *
     * @param string $de
     * @return Tranche
     */
    public function setDe($de)
    {
        $this->de = $de;

        return $this;
    }

    /**
     * Get de
     *
     * @return string 
     */
    public function getDe()
    {
        return $this->de;
    }

    /**
     * Set tranche
     *
     * @param integer $tranche
     * @return Tranche
     */
    public function setTranche($tranche)
    {
        $this->tranche = $tranche;

        return $this;
    }

    /**
     * Get tranche
     *
     * @return integer 
     */
    public function getTranche()
    {
        return $this->tranche;
    }

    /**
     * Set jour
     *
     * @param \bean\beanBundle\Entity\Jour $jour
     * @return Tranche
     */
    public function setJour(\bean\beanBundle\Entity\Jour $jour = null)
    {
        $this->jour = $jour;

        return $this;
    }

    /**
     * Get jour
     *
     * @return \bean\beanBundle\Entity\Jour 
     */
    public function getJour()
    {
        return $this->jour;
    }
}
