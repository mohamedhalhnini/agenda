<?php

namespace bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use bean\beanBundle\Entity\RendezVous;
/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity
 */
class Client {

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="CP", type="integer", nullable=true)
     */
    private $cp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATENAISSANCE", type="date", nullable=true)
     */
    private $datenaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="EMAILPRO", type="string", length=255, nullable=true)
     */
    private $emailpro;

    /**
     * @var string
     *
     * @ORM\Column(name="FAX", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="NOM", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="NOTE", type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARTAGE", type="integer", nullable=true)
     */
    private $partage;

    /**
     * @var string
     *
     * @ORM\Column(name="PAYS", type="string", length=255, nullable=true)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="PRENOM", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="SOCIETE", type="string", length=255, nullable=true)
     */
    private $societe;

    /**
     * @var string
     *
     * @ORM\Column(name="TEL", type="string", length=255, nullable=true)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="TELBOX", type="string", length=255, nullable=true)
     */
    private $telbox;

    /**
     * @var string
     *
     * @ORM\Column(name="TELMOB", type="string", length=255, nullable=true)
     */
    private $telmob;

    /**
     * @var string
     *
     * @ORM\Column(name="TELPRO", type="string", length=255, nullable=true)
     */
    private $telpro;

    /**
     * @var string
     *
     * @ORM\Column(name="VILLE", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur",inversedBy="clients")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="UTILISATEUR_ID", referencedColumnName="ID")
     * })
     */
    private $utilisateur;

    /**
     * @ORM\OneToMany(targetEntity="Instruction",mappedBy="client",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $instructions;
 /**
     * @ORM\OneToMany(targetEntity="InstructionArchivee",mappedBy="client",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
     private $instructionsArchivees;
    /**
     * @ORM\OneToMany(targetEntity="RendezVous",mappedBy="client")
     * @ORM\JoinColumn(nullable=true)
     */
    private $mesRendezVous;

    /**
     * @ORM\OneToMany(targetEntity="Memo",mappedBy="client",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $memos;
    
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->instructions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->instructionsArchivees = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mesRendezVous = new \Doctrine\Common\Collections\ArrayCollection();
        $this->memos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cp
     *
     * @param integer $cp
     * @return Client
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return integer 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set datenaissance
     *
     * @param \DateTime $datenaissance
     * @return Client
     */
    public function setDatenaissance($datenaissance)
    {
        $this->datenaissance = $datenaissance;

        return $this;
    }

    /**
     * Get datenaissance
     *
     * @return \DateTime 
     */
    public function getDatenaissance()
    {
        return $this->datenaissance;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set emailpro
     *
     * @param string $emailpro
     * @return Client
     */
    public function setEmailpro($emailpro)
    {
        $this->emailpro = $emailpro;

        return $this;
    }

    /**
     * Get emailpro
     *
     * @return string 
     */
    public function getEmailpro()
    {
        return $this->emailpro;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Client
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Client
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Client
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set partage
     *
     * @param integer $partage
     * @return Client
     */
    public function setPartage($partage)
    {
        $this->partage = $partage;

        return $this;
    }

    /**
     * Get partage
     *
     * @return integer 
     */
    public function getPartage()
    {
        return $this->partage;
    }

    /**
     * Set pays
     *
     * @param string $pays
     * @return Client
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Client
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set societe
     *
     * @param string $societe
     * @return Client
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;

        return $this;
    }

    /**
     * Get societe
     *
     * @return string 
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return Client
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set telbox
     *
     * @param string $telbox
     * @return Client
     */
    public function setTelbox($telbox)
    {
        $this->telbox = $telbox;

        return $this;
    }

    /**
     * Get telbox
     *
     * @return string 
     */
    public function getTelbox()
    {
        return $this->telbox;
    }

    /**
     * Set telmob
     *
     * @param string $telmob
     * @return Client
     */
    public function setTelmob($telmob)
    {
        $this->telmob = $telmob;

        return $this;
    }

    /**
     * Get telmob
     *
     * @return string 
     */
    public function getTelmob()
    {
        return $this->telmob;
    }

    /**
     * Set telpro
     *
     * @param string $telpro
     * @return Client
     */
    public function setTelpro($telpro)
    {
        $this->telpro = $telpro;

        return $this;
    }

    /**
     * Get telpro
     *
     * @return string 
     */
    public function getTelpro()
    {
        return $this->telpro;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Client
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set utilisateur
     *
     * @param \bean\beanBundle\Entity\Utilisateur $utilisateur
     * @return Client
     */
    public function setUtilisateur(\bean\beanBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \bean\beanBundle\Entity\Utilisateur 
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Add instructions
     *
     * @param \bean\beanBundle\Entity\Instruction $instructions
     * @return Client
     */
    public function addInstruction(\bean\beanBundle\Entity\Instruction $instructions)
    {
        $this->instructions[] = $instructions;

        return $this;
    }

    /**
     * Remove instructions
     *
     * @param \bean\beanBundle\Entity\Instruction $instructions
     */
    public function removeInstruction(\bean\beanBundle\Entity\Instruction $instructions)
    {
        $this->instructions->removeElement($instructions);
    }

    /**
     * Get instructions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * Add instructionsArchivees
     *
     * @param \bean\beanBundle\Entity\InstructionArchivee $instructionsArchivees
     * @return Client
     */
    public function addInstructionsArchivee(\bean\beanBundle\Entity\InstructionArchivee $instructionsArchivees)
    {
        $this->instructionsArchivees[] = $instructionsArchivees;

        return $this;
    }

    /**
     * Remove instructionsArchivees
     *
     * @param \bean\beanBundle\Entity\InstructionArchivee $instructionsArchivees
     */
    public function removeInstructionsArchivee(\bean\beanBundle\Entity\InstructionArchivee $instructionsArchivees)
    {
        $this->instructionsArchivees->removeElement($instructionsArchivees);
    }

    /**
     * Get instructionsArchivees
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInstructionsArchivees()
    {
        return $this->instructionsArchivees;
    }

    /**
     * Add mesRendezVous
     *
     * @param \bean\beanBundle\Entity\RendezVous $mesRendezVous
     * @return Client
     */
    public function addMesRendezVous(\bean\beanBundle\Entity\RendezVous $mesRendezVous)
    {
        $this->mesRendezVous[] = $mesRendezVous;

        return $this;
    }

    /**
     * Remove mesRendezVous
     *
     * @param \bean\beanBundle\Entity\RendezVous $mesRendezVous
     */
    public function removeMesRendezVous(\bean\beanBundle\Entity\RendezVous $mesRendezVous)
    {
        $this->mesRendezVous->removeElement($mesRendezVous);
    }

    /**
     * Get mesRendezVous
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMesRendezVous()
    {
        return $this->mesRendezVous;
    }

    /**
     * Add memos
     *
     * @param \bean\beanBundle\Entity\Memo $memos
     * @return Client
     */
    public function addMemo(\bean\beanBundle\Entity\Memo $memos)
    {
        $this->memos[] = $memos;

        return $this;
    }

    /**
     * Remove memos
     *
     * @param \bean\beanBundle\Entity\Memo $memos
     */
    public function removeMemo(\bean\beanBundle\Entity\Memo $memos)
    {
        $this->memos->removeElement($memos);
    }

    /**
     * Get memos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMemos()
    {
        return $this->memos;
    }
}
