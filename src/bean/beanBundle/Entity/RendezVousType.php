<?php

namespace bean\beanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rendezvoustype
 *
 * @ORM\Table(name="rendezvoustype")
 * @ORM\Entity
 */
class RendezVousType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="COULEUR", type="string", length=255, nullable=true)
     */
    private $couleur;

    /**
     * @var time
     *
     * @ORM\Column(name="DUREEE", type="time", nullable=true)
     */
    private $dureee;

    /**
     * @var string
     *
     * @ORM\Column(name="INFORMATION", type="string", length=255, nullable=true)
     */
    private $information;

    /**
     * @var string
     *
     * @ORM\Column(name="LIBELLE", type="string", length=255, nullable=true)
     */
    private $libelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="telDomicile", type="integer", nullable=true)
     */
    private $telDomicie;

    /**
     * @var integer
     *
     * @ORM\Column(name="telTravail", type="integer", nullable=true)
     */

    private $telTravail;

    /**
     * @var integer
     *
     * @ORM\Column(name="telMobile", type="integer", nullable=true)
     */

    private $telMobile;


    /**
     * @var integer
     *
     * @ORM\Column(name="telBox", type="integer", nullable=true)
     */

    private $telBox;

    /**
     * @var integer
     *
     * @ORM\Column(name="adresse", type="integer", nullable=true)
     */

    private $adresse;

    /**
     * @var integer
     *
     * @ORM\Column(name="email", type="integer", nullable=true)
     */

    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="notes", type="integer", nullable=true)
     */

    private $notes;

    /**
     * @var integer
     *
     * @ORM\Column(name="dateNaissance", type="integer", nullable=true)
     */

    private $dateNaissance;

    /**
     * @var integer
     *
     * @ORM\Column(name="nomJF", type="integer", nullable=true)
     */

    private $nomJF;

    /**
     * @var integer
     *
     * @ORM\Column(name="lastVisit", type="integer", nullable=true)
     */

    private $lastVisit;

    /**
     * @var integer
     *
     * @ORM\Column(name="selectionnable", type="integer", nullable=true)
     */

    private $selectionnable;

    /**
     * @var integer
     *
     * @ORM\Column(name="champsClient", type="integer", nullable=true)
     */

    private $champsClient;


    /**
     * @var integer
     *
     * @ORM\Column(name="infoSup", type="integer", nullable=true)
     */

    private $infoSup;

    /**
     * @var integer
     *
     * @ORM\Column(name="selectParDef", type="integer", nullable=true)
     */

    private $selectParDef;

    /**
     * @var integer
     *
     * @ORM\Column(name="choixAlertRappel", type="integer", nullable=true)
     */

    private $choixAlertRappel;    
    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur",inversedBy="rendezVousTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="UTILISATEUR_ID", referencedColumnName="ID", nullable = true)
     * })
     */
    private $utilisateur;

   /**
     * @ORM\OneToMany(targetEntity="RendezVous", mappedBy="rendezVousType")
     * @ORM\JoinColumn(nullable=true)
     */
    private $rendezVous;





 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rendezVous = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set couleur
     *
     * @param string $couleur
     * @return RendezVousType
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;
    
        return $this;
    }

    /**
     * Get couleur
     *
     * @return string 
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Set dureee
     *
     * @param \DateTime $dureee
     * @return RendezVousType
     */
    public function setDureee($dureee)
    {
        $this->dureee = $dureee;
    
        return $this;
    }

    /**
     * Get dureee
     *
     * @return \DateTime 
     */
    public function getDureee()
    {
        return $this->dureee;
    }

    /**
     * Set information
     *
     * @param string $information
     * @return RendezVousType
     */
    public function setInformation($information)
    {
        $this->information = $information;
    
        return $this;
    }

    /**
     * Get information
     *
     * @return string 
     */
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return RendezVousType
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    
        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set telDomicie
     *
     * @param integer $telDomicie
     * @return RendezVousType
     */
    public function setTelDomicie($telDomicie)
    {
        $this->telDomicie = $telDomicie;
    
        return $this;
    }

    /**
     * Get telDomicie
     *
     * @return integer 
     */
    public function getTelDomicie()
    {
        return $this->telDomicie;
    }

    /**
     * Set telTravail
     *
     * @param integer $telTravail
     * @return RendezVousType
     */
    public function setTelTravail($telTravail)
    {
        $this->telTravail = $telTravail;
    
        return $this;
    }

    /**
     * Get telTravail
     *
     * @return integer 
     */
    public function getTelTravail()
    {
        return $this->telTravail;
    }

    /**
     * Set telMobile
     *
     * @param integer $telMobile
     * @return RendezVousType
     */
    public function setTelMobile($telMobile)
    {
        $this->telMobile = $telMobile;
    
        return $this;
    }

    /**
     * Get telMobile
     *
     * @return integer 
     */
    public function getTelMobile()
    {
        return $this->telMobile;
    }

    /**
     * Set telBox
     *
     * @param integer $telBox
     * @return RendezVousType
     */
    public function setTelBox($telBox)
    {
        $this->telBox = $telBox;
    
        return $this;
    }

    /**
     * Get telBox
     *
     * @return integer 
     */
    public function getTelBox()
    {
        return $this->telBox;
    }

    /**
     * Set adresse
     *
     * @param integer $adresse
     * @return RendezVousType
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    
        return $this;
    }

    /**
     * Get adresse
     *
     * @return integer 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set email
     *
     * @param integer $email
     * @return RendezVousType
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return integer 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set notes
     *
     * @param integer $notes
     * @return RendezVousType
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return integer 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set dateNaissance
     *
     * @param integer $dateNaissance
     * @return RendezVousType
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;
    
        return $this;
    }

    /**
     * Get dateNaissance
     *
     * @return integer 
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Set nomJF
     *
     * @param integer $nomJF
     * @return RendezVousType
     */
    public function setNomJF($nomJF)
    {
        $this->nomJF = $nomJF;
    
        return $this;
    }

    /**
     * Get nomJF
     *
     * @return integer 
     */
    public function getNomJF()
    {
        return $this->nomJF;
    }

    /**
     * Set lastVisit
     *
     * @param integer $lastVisit
     * @return RendezVousType
     */
    public function setLastVisit($lastVisit)
    {
        $this->lastVisit = $lastVisit;
    
        return $this;
    }

    /**
     * Get lastVisit
     *
     * @return integer 
     */
    public function getLastVisit()
    {
        return $this->lastVisit;
    }

    /**
     * Set selectionnable
     *
     * @param integer $selectionnable
     * @return RendezVousType
     */
    public function setSelectionnable($selectionnable)
    {
        $this->selectionnable = $selectionnable;
    
        return $this;
    }

    /**
     * Get selectionnable
     *
     * @return integer 
     */
    public function getSelectionnable()
    {
        return $this->selectionnable;
    }

    /**
     * Set champsClient
     *
     * @param integer $champsClient
     * @return RendezVousType
     */
    public function setChampsClient($champsClient)
    {
        $this->champsClient = $champsClient;
    
        return $this;
    }

    /**
     * Get champsClient
     *
     * @return integer 
     */
    public function getChampsClient()
    {
        return $this->champsClient;
    }

    /**
     * Set infoSup
     *
     * @param integer $infoSup
     * @return RendezVousType
     */
    public function setInfoSup($infoSup)
    {
        $this->infoSup = $infoSup;
    
        return $this;
    }

    /**
     * Get infoSup
     *
     * @return integer 
     */
    public function getInfoSup()
    {
        return $this->infoSup;
    }

    /**
     * Set selectParDef
     *
     * @param integer $selectParDef
     * @return RendezVousType
     */
    public function setSelectParDef($selectParDef)
    {
        $this->selectParDef = $selectParDef;
    
        return $this;
    }

    /**
     * Get selectParDef
     *
     * @return integer 
     */
    public function getSelectParDef()
    {
        return $this->selectParDef;
    }

    /**
     * Set choixAlertRappel
     *
     * @param integer $choixAlertRappel
     * @return RendezVousType
     */
    public function setChoixAlertRappel($choixAlertRappel)
    {
        $this->choixAlertRappel = $choixAlertRappel;
    
        return $this;
    }

    /**
     * Get choixAlertRappel
     *
     * @return integer 
     */
    public function getChoixAlertRappel()
    {
        return $this->choixAlertRappel;
    }

    /**
     * Set utilisateur
     *
     * @param \bean\beanBundle\Entity\Utilisateur $utilisateur
     * @return RendezVousType
     */
    public function setUtilisateur(\bean\beanBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;
    
        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \bean\beanBundle\Entity\Utilisateur 
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Add rendezVous
     *
     * @param \bean\beanBundle\Entity\RendezVous $rendezVous
     * @return RendezVousType
     */
    public function addRendezVous(\bean\beanBundle\Entity\RendezVous $rendezVous)
    {
        $this->rendezVous[] = $rendezVous;
    
        return $this;
    }

    /**
     * Remove rendezVous
     *
     * @param \bean\beanBundle\Entity\RendezVous $rendezVous
     */
    public function removeRendezVous(\bean\beanBundle\Entity\RendezVous $rendezVous)
    {
        $this->rendezVous->removeElement($rendezVous);
    }

    /**
     * Get rendezVous
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRendezVous()
    {
        return $this->rendezVous;
    }
}
