<?php

namespace Memo\MemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use bean\beanBundle\Entity\Memo;
use Memo\MemoBundle\Form\MemoType;
use Symfony\Component\HttpFoundation\Request;

class MemoController extends Controller
{
    public function indexAction($name)
{
    return $this->render('MemoMemoBundle:Memo:index.html.twig', array('name' => $name));
}
    public function addAction(Request $request)
    {
        $memo = new Memo();
        $form = $this->createForm(new MemoType(), $memo);
        $form->handleRequest($request);
        if( $this->get('request')->getMethod() == 'POST' ){
        $somme=0;
        foreach($_POST['alerte'] as $valeur)
        {
            $somme+=$valeur;
        }
        $memo->setAlert($somme);
        $memo->setPermanante(1);
        }
        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($memo);
            $em->flush();
            return $this->redirect(($this->generateUrl("agenda_memo_listMemo")));
        }
        return $this->render('MemoMemoBundle:Memo:index.html.twig', array(
        'form' => $form->createView(),
    ));
    }
    public function modifierAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $memo = $em->getRepository("beanBundle:Memo")->findOneById($id);
        if (!$memo) {
            throw $this->createNotFoundException('Aucune mémo ne correspond à l ID'.$id);
        }
        $form = $this->createForm(new MemoType(), $memo);
        $form->handleRequest($request);
        if( $this->get('request')->getMethod() == 'POST' ){
            $somme=0;
            foreach($_POST['alerte'] as $valeur)
            {
                $somme+=$valeur;
            }
            $memo->setAlert($somme);
            $memo->setPermanante(1);
        }
        if ($form->isValid()) {
            $em->flush();
            return $this->redirect( $this->generateUrl('agenda_memo_listMemo') );
        }
        return $this->render('MemoMemoBundle:Memo:modifier.html.twig', array(
            'form' => $form->createView(),
            'm' => $memo,
        ));
    }
    public function listMemoAction()
    {
        $memos = $this->getDoctrine()->getRepository("beanBundle:Memo")->findAll();
        //return array('memos' => $memos);
        return $this->render('MemoMemoBundle:Memo:list.html.twig', array(
            'memos' => $memos));
    }
    public function supprimerAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $memo = $em->getRepository("beanBundle:Memo")->findOneById($id);
        if (!$memo) {
            throw $this->createNotFoundException('No guest found for id '.$id);
        }
        $em->remove($memo);
        $em->flush();
        return $this->redirect( $this->generateUrl('agenda_memo_listMemo') );
    }
}
