<?php

namespace Memo\MemoBundle\Controller;

use bean\beanBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ClientController extends Controller {

    public function createAction() {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("MemoMemoBundle:Client");
        $request = $this->getRequest('request');
//        extract($_POST);
        $client = new Client();
        if ($request->getMethod() == 'POST') {
            $client->setNom($_POST['nom']);
            //$client->setPrenom($prenom);
            $em->persist($client);
            $em->flush();
//            return new Response(array('client'=>$client));
        }else{
        return $this->render('MemoMemoBundle:Client:create.html.twig', array());}
    }

    public function deleteAction() {
        return $this->render('MemoMemoBundle:Client:delete.html.twig', array(
                        // ...
        ));
    }

    private function getParames($request) {
        
    }

}
