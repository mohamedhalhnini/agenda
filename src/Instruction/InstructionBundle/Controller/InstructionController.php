<?php

/**
 * Created by PhpStorm.
 * User: kamal
 * Date: 20/05/2015
 * Time: 09:18
 */

namespace Instruction\InstructionBundle\Controller;

use DateTime;
use bean\beanBundle\Entity\Instruction;
use bean\beanBundle\Entity\InstructionArchivee;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response as Response2;
use Symfony\Component\HttpFoundation\Response;

class InstructionController extends Controller {

    public function indexAction($name) {

        return $this->render('InstructionBundle:Instruction:index.html.twig', array('name' => $this->getDateTime("12/10/2015")));
    }

    /**
     * @return Response
     */
    public function addAction() {
        $em = $this->getDoctrine()->getManager();
        $phrases = $em->getRepository("beanBundle:PhraseUsuelle")->findAll();
        $clients = $em->getRepository("beanBundle:Client")->findAll();
        $instructions = $em->getRepository("beanBundle:Instruction")->findAllDesc();
        $request = $this->getRequest('request');
        $instruction = new Instruction();
        if ($request->getMethod() == 'POST') {
            //recuperation des données depuis le formulaire
            $idPhraseUsuelle = $_POST['phraseUsuelle'];
            $idContact = $_POST['client'];
            $type = $_POST['type'];
            $phrase = $em->getRepository("beanBundle:PhraseUsuelle")->find($idPhraseUsuelle);
            $contact = $em->getRepository("beanBundle:Client")->find($idContact);
            //remlire la onstrution
            $instruction->setPhraseUsuelle($phrase);
            $instruction->setClient($contact);
            $instruction->setDetail($_POST['detail']);
            $instruction->setTitre($_POST['titre']);
            if ($type == "0") {
                $type = 0;
            } else if ($type == "1") {
                $type = 1;
                $dateLe = $this->getTransformedDate($_POST['dateLe']);
                $instruction->setLe($dateLe);
            } else if ($type == "2") {
                $type = 2;
                $dateDe = $this->getTransformedDate($_POST['dateDe']);
                $dateA = $this->getTransformedDate($_POST['dateA']);
                $instruction->setDe($dateDe);
                $instruction->setA($dateA);
            }
            $instruction->setType($type);
            $instruction->setPriorite($_POST['priorite']);
            $instruction->setArchived(0);
            $instruction->setDateEnregistrement(new DateTime());
            $em->persist($instruction);
            $em->flush();
            return $this->render('InstructionBundle:Instruction:add.html.twig', array('phrases' => $phrases, 'clients' => $clients, 'instructions' => $instructions, 'instructioninsred' => $instruction));
        } else {
            return $this->render('InstructionBundle:Instruction:add.html.twig', array('phrases' => $phrases, 'clients' => $clients, 'instructions' => $instructions));
        }
    }

    public function listInstructionsAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest('request');
        if ($request->getMethod() == 'GET' and isset($_GET['type'])) {
            $type = $_GET['type'];
            if ($type == "1") {
                $instructions = $em->getRepository("beanBundle:Instruction")->findAllPermanentes();
            } else if ($type == "2") {
                $instructions = $em->getRepository("beanBundle:Instruction")->findAllPunctuals();
            } else {
                $instructions = $em->getRepository("beanBundle:Instruction")->findAllPeriodiques();
            }
            $serializer = $this->get('jms_serializer');
            $instructions = $serializer->serialize($instructions, 'json');
            return new Response($instructions);
        } else if ($request->getMethod() == 'POST') {
            $type = $_POST['type'];
            $type = $_POST['type'];
            //typeArchived == "1" ==> non archivées
            //typeArchived == "2" ==> archivées
            //typeArchived == "2" ==> tous
            $typeArchived = $_POST['typeArchived'];
            if ($type == "1") {// instruction permanentes
                if ($typeArchived == "1") {
                    $archived = 0;
                    $instructions = $em->getRepository("beanBundle:Instruction")->findAllPermanentes();
                    $titre = 'liste des instructions permanents non archiveés';
                } else if ($typeArchived == "2") {
                    $instructions = $em->getRepository("beanBundle:InstructionArchivee")->findArchivedPermanentes();
                    $titre = 'liste des instructions permanents archivés';
                    $archived = 1;
                } else if ($typeArchived == "3") {
                    $instructions = $em->getRepository("beanBundle:InstructionArchivee")->findDeletedPermanentes();
                    $titre = 'liste des instructions permanents supprimées';
                    $archived = 0;
                }
                return $this->render('InstructionBundle:Instruction:list.html.twig', array('permanentInstructions' => $instructions, 'titre' => $titre, 'archived' => $archived));
            } else if ($type == "2") {//instructions Ponctuels
                if ($typeArchived == "1") {
                    $instructions = $em->getRepository("beanBundle:Instruction")->findAllPunctuals();
                    $titre = 'liste des instructions ponctuels non archiveés';
                    $archived = 0;
                } else if ($typeArchived == "2") {
                    $instructions = $em->getRepository("beanBundle:InstructionArchivee")->findArchivedPunctuals();
                    $titre = 'liste des instructions ponctuels  archiveés';
                    $archived = 1;
                } else if ($typeArchived == "3") {
                    $instructions = $em->getRepository("beanBundle:InstructionArchivee")->findDeletedPunctuals();
                    $titre = 'liste des instructions ponctuels supprimées';
                    $archived = 0;
                }
                return $this->render('InstructionBundle:Instruction:list.html.twig', array('ponctualInstructions' => $instructions, 'titre' => $titre, 'archived' => $archived));
            } else {//instructions entre deux dates
                if ($typeArchived == "1") {
                    $instructions = $em->getRepository("beanBundle:Instruction")->findAllPeriodiques();
                    $titre = 'liste des instructions d une periode non archiveés';
                    $archived = 0;
                } else if ($typeArchived == "2") {
                    $instructions = $em->getRepository("beanBundle:InstructionArchivee")->findArchivedPeriodiques();
                    $titre = 'liste des instructions d une periode   archiveés';
                    $archived = 1;
                } else if ($typeArchived == "3") {
                    $instructions = $em->getRepository("beanBundle:InstructionArchivee")->findDeletedPeriodiques();
                    $titre = 'liste des instructions d une periode  supprimées ';
                    $archived = 0;
                }
                return $this->render('InstructionBundle:Instruction:list.html.twig', array('periodiqueInstructions' => $instructions, 'titre' => $titre, 'archived' => $archived));
            }
        }
        $instructions = $em->getRepository("beanBundle:Instruction")->findAllPermanentes();
        $titre = 'liste des instructions permanents';
        return $this->render('InstructionBundle:Instruction:list.html.twig', array('permanentInstructions' => $instructions, 'titre' => $titre, 'archived' => 0));
    }

//    public function periodiqueInstructionsAction() {
//        $em = $this->getDoctrine()->getManager();
//        $instructions = $em->getRepository("InstructionBundle:Instruction")->findAllPeriodiques();
//        return $this->render('InstructionBundle:Instruction:listPeriodique.html.twig', array('instructions' => $instructions));
//    }
//    public function punctualInstructionsAction() {
//        $em = $this->getDoctrine()->getManager();
//        $instructions = $em->getRepository("InstructionBundle:Instruction")->findAllPunctuals();
//        return $this->render('InstructionBundle:Instruction:listPunctual.html.twig', array('ponctualInstructions' => $instructions));
//    }

    public function updateAction($id) {
        //        $id=5;
        $em = $this->getDoctrine()->getManager();
        $phrases = $em->getRepository("beanBundle:PhraseUsuelle")->findAll();
        $clients = $em->getRepository("beanBundle:Client")->findAll();
        $request = $this->getRequest();
        $instruction = new Instruction();
        $instruction = $em->getRepository("beanBundle:Instruction")->find($id);
        if ($request->getMethod() == 'POST') {
            //recuperation des données depuis le formulaire
            $idPhraseUsuelle = $_POST['phraseUsuelle'];
            $idContact = $_POST['client'];
            $type = $_POST['type'];
            $phrase = $em->getRepository("beanBundle:PhraseUsuelle")->find($idPhraseUsuelle);
            $contact = $em->getRepository("beanBundle:Client")->find($idContact);
            //remlire la onstrution
            $instruction->setPhraseUsuelle($phrase);
            $instruction->setClient($contact);
            $instruction->setDetail($_POST['detail']);
            $instruction->setTitre($_POST['titre']);
            if ($type == "0") {
                $type = 0;
            } else if ($type == "1") {
                $type = 1;
                $dateLe = $this->getTransformedDate($_POST['dateLe']);
                $instruction->setLe($dateLe);
            } else {
                $type = 2;
                $dateDe = $this->getTransformedDate($_POST['dateDe']);
                $dateA = $this->getTransformedDate($_POST['dateA']);
                $instruction->setDe($dateDe);
                $instruction->setA($dateA);
            }
            $instruction->setType($type);
            $instruction->setPriorite($_POST['priorite']);
            $em->flush();
        }
        return $this->render('InstructionBundle:Instruction:update.html.twig', array('instruction' => $instruction, 'phrases' => $phrases, 'clients' => $clients));
    }

    /* in this function we move the instruction from the table 'Instruction' to the table 'InstructionArchivee' 
     * so we archivate the instruction in another table 
     * the table InstructionArchivee hase to type of instruction : the archived one and the deleted one
     */

    public function archivateAction($id) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest('request');
        $instructionFind = $em->getRepository("beanBundle:Instruction")->find($id);
        $instruction = new Instruction();
        $instruction = $instructionFind;
        $em->remove($instruction); // delete from table instruction
        $archivedInstruction = new InstructionArchivee();
        $archivedInstruction = $this->newArchivedInstruction($instructionFind);
        $archivedInstruction->setArchived(1); //  1==>archived  instruction  , 0==> deleted instruction 
        $em->persist($archivedInstruction); // insert into table InstructionArchivee
        $em->flush();
        return new Response($id);
    }
   

    public function deleteAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest('request');
        if ($request->getMethod() == 'GET') {
            $id = $_GET['id'];
            $instruction = $em->getRepository("beanBundle:Instruction")->find($id);
            $em->remove($instruction);// delete from table instruction
            $archivedInstruction = new InstructionArchivee();
            $archivedInstruction = $this->newArchivedInstruction($instruction);
            $archivedInstruction->setArchived(0); // 0==> deleted instruction , 1==>archived  instruction  
            $em->persist($archivedInstruction); // insert into table InstructionArchivee
            $em->flush();
            return new Response($id);
        }
    }

    //************************ helper functions *********************
    private function getParames($request) {
        $instruction = new Instruction();
    }

    private function newArchivedInstruction(Instruction $instruction) {
        $archivedInstruction = new InstructionArchivee();
        $archivedInstruction->setTitre($instruction->getTitre())
                ->setDetail($instruction->getDetail())
                ->setType($instruction->getType())
                ->setPriorite($instruction->getPriorite())
                ->setUtilisateur($instruction->getUtilisateur())
                ->setDe($instruction->getDe())
                ->setLe($instruction->getLe())
                ->setA($instruction->getA())
                ->setClient($instruction->getClient())
                ->setUtilisateur($instruction->getUtilisateur())
                ->setPhraseUsuelle($instruction->getPhraseUsuelle())
                ->setPeriodicite($instruction->getPeriodicite())
                ->setDateArchivation(new DateTime());
        return $archivedInstruction;
    }

    private function getTransformedDate($date) {
        $tab = explode("/", $date);
        if (strlen($tab[0] . "") == 1) {
            $tab[0] = '0' . $tab[0];
        }
        if (strlen($tab[1] . "") == 1) {
            $tab[1] = '0' . $tab[1];
        }
        return new DateTime($tab[1] . "/" . $tab[0] . "/" . $tab[2]);
    }

}
