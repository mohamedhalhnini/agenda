<?php

namespace Instruction\InstructionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use bean\beanBundle\Entity\Client;

class ClientController extends Controller {
    
             /*   utilised by ajax      */
    public function createAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest('request');
        $client = new Client();
        if ($request->getMethod() == 'POST') {
            $client = $this->getParams($request);
            $em->persist($client);
            //  $em->flush();
            $id = $client->getId();
            $nom = $client->getNom();
            $prenom = $client->getPrenom();
            $response = new \Symfony\Component\HttpFoundation\JsonResponse();
            return $response->setData(array('id' => $id, 'nom' => $nom, 'prenom'=>$prenom));
        } else {
            return $this->render('InstructionBundle:Client:create.html.twig', array());
        }
    }

    function deleteAction() {
        return $this->render('InstructionBundle:Client:delete.html.twig', array(
// ...
        ));
    }

    // ************* helper functions *********************
    //   ******************* get params ***************
    private function getParams($request) {
        $client = new Client();
        $client->setNom($request->get('nom'));
        $client->setPrenom($request->get('prenom'));
        $client->setTel(($request->get('tel')));
        $client->setTelmob($request->get('telMob'));
        $client->setTelpro($request->get('telPro'));
        $client->setTelbox($request->get('telBox'));
        $client->setEmail($request->get('email'));
        $client->setNote($request->get('note'));
        //   $client->setPartage($request->get('partage'));
        // $clent-> cicvilite    must be added to  the entity
        //   $client-> adresse;  must be added to  the entity
        // $client-> password;  must be added to the entity
        return $client;
    }

}
