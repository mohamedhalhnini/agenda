<?php

namespace Instruction\InstructionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('InstructionBundle:Default:index.html.twig', array('name' => $name));
    }
    public function ajouterAction()
    {
        return $this->render('InstructionBundle:Default:ajouter.html.twig');
    }
}
