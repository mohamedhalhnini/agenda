<?php

namespace Instruction\InstructionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InstructionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('a','date')
            ->add('de','date')
            ->add('detail','textarea')
            ->add('le','date')
            ->add('titre','text')
            ->add('type','radio')
            ->add('client',"entity",array(
                "class"=>"Instruction\InstructionBundle\Entity\Client","property"=>"nom"))
            ->add('phraseUsuelle',"entity",array(
                "class"=>"Instruction\InstructionBundle\Entity\PhraseUsuelle","property"=>"text")
                               
      );

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Instruction\InstructionBundle\Entity\Instruction'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'instruction_instructionbundle_instruction';
    }
}
