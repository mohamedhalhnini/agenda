<?php

namespace Agenda\MemoBundle\Controller;

use bean\beanBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ClientController extends Controller {

    public function createAction() {
            $em = $this->getDoctrine()->getManager();
            $request = $this->getRequest('request');
            $client = new Client();
            if ($request->getMethod() == 'POST') {
                $client = $this->getParams($request);
                $em->persist($client);
                $em->flush();
                $id = $client->getId();
                $nom = $client->getNom();
                $prenom = $client->getPrenom();
                $response = new \Symfony\Component\HttpFoundation\JsonResponse();
                return $response->setData(array('id' => $id, 'nom' => $nom, 'prenom'=>$prenom));
            } else {
                return $this->render('AgendaMemoBundle:Client:create.html.twig', array());
            }
    }
    public function getClientAction($id){
        $em = $this->getDoctrine()->getManager();
        $memo = $em->getRepository('beanBundle:Memo')->find($id);
        $nom = $memo->getClientFrequent()->getNom();
        $prenom= $memo->getClientFrequent()->getPrenom();
        $email = $memo->getClientFrequent()->getEmail();
        $tel = $memo->getClientFrequent()->getTelpro();
        $detail = $memo->getDetail();
        $response = new \Symfony\Component\HttpFoundation\JsonResponse();
        return $response->setData(array('nom'=>$nom,'prenom'=>$prenom,'email'=>$email,'tel'=>$tel,'detail'=>$detail));

    }
    public function modifierAction($id) {

            $request = $this->get('request');
            $em = $this->getDoctrine()->getManager();
            $client = $em->getRepository('beanBundle:Client')->find($id);

            $id = $client->getId();
            $name = $client->getNom();
            $prenom = $client->getPrenom();
            $teldom = $client->getTel();
            $telbox = $client->getTelbox();
            $telpro = $client->getTelpro();
            $telmob = $client->getTelmob();
            $email = $client->getEmail();
            $note = $client->getNote();
        if (($request->getMethod() == 'GET')) {
            $response = new \Symfony\Component\HttpFoundation\JsonResponse();
            return $response->setData(array('id' => $id, 'nom' => $name, 'prenom' => $prenom, 'teldom' => $teldom,
                'telbox' => $telbox,
                'telpro' => $telpro,
                'telmob' => $telmob,
                'email' => $email,
                'note' => $note,
            ));
        }
        else if (($request->getMethod() == 'POST')) {
                $client->setNom($_POST['nom']);
                $client->setPrenom($_POST['prenom']);
                $client->setNote($_POST['note']);
                $client->setTel($_POST['telDomicile']);
                $client->setTelbox($_POST['telBox']);
                $client->setTelmob($_POST['telMob']);
                $client->setTelpro($_POST['telPro']);
                $client->setEmail($_POST['email']);
                $client->setNote($_POST['note']);
                $em->flush();
                $id = $client->getId();
                $nom = $client->getNom();
                $prenom = $client->getPrenom();
                $response = new \Symfony\Component\HttpFoundation\JsonResponse();
                return $response->setData(array('id' => $id, 'nom' => $nom, 'prenom'=>$prenom));
        }
            //$client->setPrenom($_POST['prenom']);
            //$client->setTel($_POST['telDomicile']);
            //$client->setTel($_POST['telDomicile']);
            //$em->persist($client);
            //$em->flush();
//            return new Response(array('client'=>$client));
    }
    public function imprimerPdfAction($id){
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('beanBundle:Client')->find($id);
        $pw = $request->get('password');
        $email = $request->get('email');
        $nom = $request->get('nom');
        if (($request->getMethod() == 'POST')) {
                if(!empty($pw))
                {
                    $pw = $pw;
                }
                else
                {
                    $pw="Attention, vous n'avez pas encore de mot de passe associé à votre compte.
Lors de votre première connexion, il vous sera demandé d'en spécifier un.";
                }
                //$filename = "Agenda" . $client->getNom();
                $html = $this->renderView(
                    'AgendaMemoBundle:Client:details.pdf.twig',
                    array(
                        'pw'=>$pw,
                        'email'=>$email,
                        'nom'=>$nom
                    )
                );

                // __DIR__ . '/../../../../web/download/' . $filename . '.pdf'
                //return $this->redirect($this->generateUrl('agenda_client_details',
                //    array('id' => $id)
                //));
                return new Response(
                    $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                    200,
                    array(
                        'Content-Type'          => 'application/pdf',
                        'Content-Disposition'   => 'attachment; filename="Agenda_Rappel_Identifiants_Client_'.$nom.'.pdf"'
                    )
                );

            }

    }
    public function envoyerEmailAction($id){
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('beanBundle:Client')->find($id);
        $id = $client->getId();
        $name = $client->getNom();
        $prenom = $client->getPrenom();
        $teldom = $client->getTel();
        $telbox = $client->getTelbox();
        $telpro = $client->getTelpro();
        $telmob = $client->getTelmob();
        $email = $client->getEmail();
        $note = $client->getNote();
        if (($request->getMethod() == 'POST')) {
                $pw = $_POST['password'];
                $email = $_POST['email'];
                $nom = $client->getNom();
                //$mailer = $this->container->get('mailer');
                //$transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com',465,'ssl');
                //  ->setUsername('mohamedelmehditioutchi@gmail.com')
                //   ->setPassword('067486725');
                //$mailer = \Swift_Mailer::newInstance($transport);
                $message = \Swift_Message::newInstance()
                    ->setSubject('Rappel de vos identifiants')
                    ->setFrom('mohamedelmehditioutchi@gmail.com')
                    ->setTo($email)
                    ->setBody($this->renderView(
                        'AgendaMemoBundle:Client:details.html.twig',
                        array(
                            'pw'=>$pw,
                            'email'=>$email,
                            'nom'=>$nom
                        )
                    ))
                    ->setContentType('text/html');
                $this->get('mailer')->send($message);
                return new Response("email envoyé");
        }
    }
    public function detailsAction(Client $client, $format)
    {
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $pw = $_POST['password'];
            $email = $_POST['email'];
            $nom = $client->getNom();
            $filename = "Agenda" . $client->getNom();
            switch ($format) {
                case 'pdf':
                    $this->get('knp_snappy.pdf')->generateFromHtml(
                        $this->renderView(
                            'AgendaMemoBundle:Client:details.pdf.twig',
                            array(
                                'pw'=>$pw,
                                'email'=>$email,
                                'nom'=>$nom
                            )
                        ),
                        __DIR__ . '/../../../../web/download/' . $filename . '.pdf'
                    );
                    break;
                case 'html':
                default:
                    return $this->render('AgendaMemoBundle:Client:details.html.twig', array(
                        'client' => $client,
                    ));
                    break;
            }
        }
    }

    public function deleteAction() {
        return $this->render('AgendaMemoBundle:Client:delete.html.twig', array(
                        // ...
        ));
    }

    private function getParames($request) {
        
    }
    function genererMDP ($longueur = 8){
        // initialiser la variable $mdp
        $mdp = "";

        // Définir tout les caractères possibles dans le mot de passe,
        // Il est possible de rajouter des voyelles ou bien des caractères spéciaux
        $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";

        // obtenir le nombre de caractères dans la chaîne précédente
        // cette valeur sera utilisé plus tard
        $longueurMax = strlen($possible);

        if ($longueur > $longueurMax) {
            $longueur = $longueurMax;
        }

        // initialiser le compteur
        $i = 0;

        // ajouter un caractère aléatoire à $mdp jusqu'à ce que $longueur soit atteint
        while ($i < $longueur) {
            // prendre un caractère aléatoire
            $caractere = substr($possible, mt_rand(0, $longueurMax-1), 1);

            // vérifier si le caractère est déjà utilisé dans $mdp
            if (!strstr($mdp, $caractere)) {
                // Si non, ajouter le caractère à $mdp et augmenter le compteur
                $mdp .= $caractere;
                $i++;
            }
        }

        // retourner le résultat final
        return $mdp;
    }
    public function generatepwAction() {
        $motdepasse = $this->genererMDP();
        return $this->render('AgendaMemoBundle:Client:modifier.html.twig', array(
            'pw'=>$motdepasse
        ));
    }
    private function getParams($request) {
        $client = new Client();
        $client->setNom($request->get('nom'));
        $client->setPrenom($request->get('prenom'));
        $client->setTel(($request->get('tel')));
        $client->setTelmob($request->get('telMob'));
        $client->setTelpro($request->get('telPro'));
        $client->setTelbox($request->get('telBox'));
        $client->setEmail($request->get('email'));
        $client->setNote($request->get('note'));
        //   $client->setPartage($request->get('partage'));
        // $clent-> cicvilite    must be added to  the entity
        //   $client-> adresse;  must be added to  the entity
        // $client-> password;  must be added to the entity
        return $client;
    }

}
