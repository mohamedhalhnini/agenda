<?php

namespace Agenda\MemoBundle\Controller;

use bean\beanBundle\Entity\Client;
use bean\beanBundle\Entity\Memo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class CalendrierController extends Controller {
    public function memoAction(){
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('select m.id as id,m.dateenregistrement as start,m.detail as title,m.allDay,m.color from beanBundle:Memo m ');
        $memos = $query->getResult();
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($memos, 'json');
        return $this->render('AgendaMemoBundle:Memo:fullCalendar.html.twig', array('memos' => $data, 'content_type' => 'application/json'));
}
}