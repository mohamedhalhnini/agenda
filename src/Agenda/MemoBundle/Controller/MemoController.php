<?php

namespace Agenda\MemoBundle\Controller;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use bean\beanBundle\Entity\Memo;
use bean\beanBundle\Entity\PhraseUsuelle;
use bean\beanBundle\Entity\Client;
use bean\beanBundle\Entity\Periodicite;
use Agenda\MemoBundle\Form\MemoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MemoController extends Controller
{
    private function getTransformedDate($date) {
        $tab = explode("/", $date);
        if (strlen($tab[0] . "") == 1) {
            $tab[0] = '0' . $tab[0];
        }
        if (strlen($tab[1] . "") == 1) {
            $tab[1] = '0' . $tab[1];
        }
        return new DateTime($tab[1] . "/" . $tab[0] . "/" . $tab[2]);
    }
    public function indexAction($name)
{
    return $this->render(':index.html.twig', array('name' => $name));
}
    public function addAction(Request $request)
    {
        $memo = new Memo();
        $form = $this->createForm(new MemoType(), $memo);
        $form->handleRequest($request);
        if( $this->get('request')->getMethod() == 'POST' ){
        $somme=0;
        foreach($_POST['alerte'] as $valeur)
        {
            $somme+=$valeur;
        }
            $memo->setAlert($somme);
            $memo->setAllDay('true');
            $memo->setPermanante(1);
            $d = $this->getTransformedDate($_POST['dateenregistrement']);
            $memo->setDateenregistrement($d);
            $periodicite = new Periodicite();
        $quotidien = $request->get('quotidien');



        $ttLes = $request->get('ttLes');
        $jours = $request->get('jours');
        $jourSelect = $request->get('jourSelect');
        $jourOuvrable = $request->get('jourOuvrable');
        $uneFois = $request->get('uneFois');



        if ($uneFois == "0" && isset($uneFois)) {
            $periodicite->setType($uneFois);
        }

        if ($uneFois == "1" && isset($ttLes)) {

            $periodicite->setType($uneFois);
            if ($ttLes == "1") {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setTouslesjours($jours);
            }
            if ($ttLes == "2") {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setTouslesjoursouvrable(1);
            }
            if ($ttLes == "3") {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setJourselectionne($request->get('quotid'));
            }
        }


        if ($uneFois == "2" && isset($uneFois)) {
            $periodicite->setType($uneFois);
            //$periodicite->setTousles($request->get('daySelect'));
            $periodicite->setJourselectionne($request->get('daySelect'));
        }


//            ->setTouslesjours($request->get(''))
        $finApres = $request->get('finApres');
        $datePickerFinLe = $request->get('datePickerFinLe');
        $periodicite->setEnd(new DateTime($datePickerFinLe));

//        $myRange = array('{start:' . $request->get('datepicker1'), 'end:' . $datePickerFinLe . '}');
//        $periodicite->setRanges('{start:' . $request->get('datepicker1'), 'end:' . $datePickerFinLe . '}');
        //***********************Ranges******************************
        /*
        $ranges = new ranges();
        $ranges->setStart(new DateTime($request->get('datepicker1')));
        $ranges->setEnd(new DateTime($datePickerFinLe));*/
        $memo->setPeriodicite($periodicite);
        //$periodicite->setRanges($ranges);
        //***********************************************************
        $occurence = $request->get('occurence');
        if ($finApres == 1) {
            $periodicite->setFinapresoccurence($occurence);
        }
//            if($finApres==2)
//           $periodicite ->setFinapresoccurence($finApres);
        //****************************************************************************************************** 
        }
        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($periodicite);
            $em->persist($memo);
            $em->flush();
            $query = $em->createQuery('select m.id as id,m.dateenregistrement as start,m.detail as title,m.allDay,m.color from  m ');
            $memos = $query->getResult();
            $serializer = $this->get('jms_serializer');
            $data = $serializer->serialize($memos, 'json');
            return $this->render('AgendaMemoBundle:Memo:fullCalendar.html.twig', array('memos' => $data, 'content_type' => 'application/json'));
            //return $this->redirect(($this->generateUrl("agenda_memo_listMemo")));
        }
        return $this->render('AgendaMemoBundle:Memo:index.html.twig', array(
        'form' => $form->createView(),
    ));
    }
    public function dropAction($id,$start1,$startH){
            $em = $this->getDoctrine()->getManager();
            $event = $em->getRepository("beanBundle:Memo")->find($id);
            $event->setDateenregistrement(new DateTime($start1.' '.$startH));
            $em->flush();
    }
    public function modifierAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $memo = $em->getRepository("beanBundle:Memo")->findOneById($id);
        if (!$memo) {
            throw $this->createNotFoundException('Aucune mémo ne correspond à l ID'.$id);
        }
        $form = $this->createForm(new MemoType(), $memo);

        $form->handleRequest($request);
        if( $this->get('request')->getMethod() == 'POST' ){
            $somme=0;
            foreach($_POST['alerte'] as $valeur)
            {
                $somme+=$valeur;
            }
            $memo->setAlert($somme);
            $memo->setPermanante(1);
            $d = $this->getTransformedDate($_POST['dateenregistrement']);
            $memo->setDateenregistrement($d);
        }
        if ($form->isValid()) {
            $memo->setVu(2);
            $memo->setColor('#006600');
            $em->flush();
            return $this->redirect(($this->generateUrl("agenda_memo_calendrier")));
        }
        return $this->render('AgendaMemoBundle:Memo:modifier.html.twig', array(
            'form' => $form->createView(),
            'm' => $memo,
        ));
    }
    public function listMemoAction()
    {
        $memos = $this->getDoctrine()->getRepository("beanBundle:Memo")->findAll();
        //return array('memos' => $memos);
        return $this->render('AgendaMemoBundle:Memo:list.html.twig', array(
            'memos' => $memos));
    }
    public function supprimerAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $memo = $em->getRepository("beanBundle:Memo")->findOneById($id);
        if (!$memo) {
            throw $this->createNotFoundException('No guest found for id '.$id);
        }
        $em->remove($memo);
        $em->flush();
        return $this->redirect( $this->generateUrl('agenda_memo_listMemo') );
    }
    public function startEndAction($startD,Request $request) {
        $memo = new Memo();
        $form = $this->createForm(new MemoType(), $memo);
        $form->handleRequest($request);
        if( $this->get('request')->getMethod() == 'POST' ){
            $somme=0;
            foreach($_POST['alerte'] as $valeur)
            {
                $somme+=$valeur;
            }
            $memo->setAlert($somme);
            $memo->setAllDay('true');
            $memo->setPermanante(1);
            $d = $this->getTransformedDate($_POST['dateenregistrement']);
            $memo->setDateenregistrement($d);
            $periodicite = new Periodicite();
        $quotidien = $request->get('quotidien');



        $ttLes = $request->get('ttLes');
        $jours = $request->get('jours');
        $jourSelect = $request->get('jourSelect');
        $jourOuvrable = $request->get('jourOuvrable');
        $uneFois = $request->get('uneFois');



        if ($uneFois == "0" && isset($uneFois)) {
            $periodicite->setType($uneFois);
        }

        if ($uneFois == "1" && isset($ttLes)) {

            $periodicite->setType($uneFois);
            if ($ttLes == "1") {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setTouslesjours($jours);
            }
            if ($ttLes == "2") {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setTouslesjoursouvrable(1);
            }
            if ($ttLes == "3") {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setJourselectionne($request->get('quotid'));
            }
        }


        if ($uneFois == "2" && isset($uneFois)) {
            $periodicite->setType($uneFois);
            //$periodicite->setTousles($request->get('daySelect'));
            $periodicite->setJourselectionne($request->get('daySelect'));
        }


//            ->setTouslesjours($request->get(''))
        $finApres = $request->get('finApres');
        $datePickerFinLe = $request->get('datePickerFinLe');
        $periodicite->setEnd(new DateTime($datePickerFinLe));

//        $myRange = array('{start:' . $request->get('datepicker1'), 'end:' . $datePickerFinLe . '}');
//        $periodicite->setRanges('{start:' . $request->get('datepicker1'), 'end:' . $datePickerFinLe . '}');
        //***********************Ranges******************************
        /*
        $ranges = new ranges();
        $ranges->setStart(new DateTime($request->get('datepicker1')));
        $ranges->setEnd(new DateTime($datePickerFinLe));*/
        $memo->setPeriodicite($periodicite);
        //$periodicite->setRanges($ranges);
        //***********************************************************
        $occurence = $request->get('occurence');
        if ($finApres == 1) {
            $periodicite->setFinapresoccurence($occurence);
        }
        }
        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $memo->setVu(1);
            $memo->setColor('#000066');
            $em->persist($periodicite);
            $em->persist($memo);
            $em->flush();
            return $this->redirect(($this->generateUrl("agenda_memo_calendrier")));
        }
        return $this->render('AgendaMemoBundle:Memo:index.html.twig', array(
            'form' => $form->createView(),'startD' => $startD
        ));
    }
    public function editColor($id,$color)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $memo = $em->getRepository("beanBundle:Memo")->findOneById($id);
        $memo->setColor($color);
        $em->persist($memo);
        $em->flush();
        return $this->redirect( $this->generateUrl('agenda_memo_calendrier') );
    }
}
