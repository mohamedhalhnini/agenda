<?php
namespace Agenda\MemoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MemoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('priorite', 'choice', array(
                'choices'  => array('1' => 'Oui','0' =>  'Non'),
                'multiple' => false,
                'expanded' => true
            ))
            /*->add('alert', 'choice', array(
                'choices'  => array('1' => 'par Mail','2' => 'par SMS', '4' => 'par FAX'),
                'multiple' => false,
                'expanded' => true
            ))*/
            ->add('status', 'choice', array(
                'choices'  => array('0' => 'Auto', '1' => 'Oui'),
                'multiple' => false,
                'expanded' => true
            ))
            /*->add('clientFrequent','choice',array(
            'attr' => array('class' => 'select'),
            'choices'  => array('1' =>'mohamed el mehdi',  '2' =>'kamal'),
            'multiple' => false,
            'expanded' => false
        ))*/
            ->add('clientFrequent','entity',array(
            'attr' => array('class' => 'select',
            'data-select-options' =>'{"searchIfMoreThan":1,"searchText":"Rechercher"}',
            'onChange' => 'recup1()'
            ),
            'class'  => 'bean\beanBundle\Entity\Client',
            'property' => 'nom'))

                       ->add('phraseUsuelle','entity',array(
                'attr' => array('class' => 'select',
                'data-select-options' =>'{"searchIfMoreThan":1,"searchText":"Rechercher"}',
                'onChange' => 'recup2()'
                ),
                'class'  => 'bean\beanBundle\Entity\PhraseUsuelle',
                'property' => 'text')
        )
            ->add('detail','textarea',array(
            'attr' => array('class' => 'input full-width')))
            ->add('Enregistrer','submit',array(
            'attr' => array('class' => 'button icon-replay-all')))
        ;
    }

    public function getName()
    {
        return 'agenda_memobundle_general_type';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'bean\beanBundle\Entity\Agenda',
        );
    }
}
