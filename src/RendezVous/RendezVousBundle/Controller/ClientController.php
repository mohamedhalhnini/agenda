<?php

namespace RendezVous\RendezVousBundle\Controller;

use bean\beanBundle\Entity\Client as Client2;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Client;
//use bean\beanBundle\Repository\RendezvousRepository;


class ClientController extends Controller
{
 

    public function getClientAction($id){
     $em = $this->getDoctrine()->getManager();
        $rdv = $em->getRepository('beanBundle:RendezVous')->findOneBy(array('id'=>$id));
        $nom = $rdv->getClient()->getNom();
        $prenom= $rdv->getClient()->getPrenom();
        $email = $rdv->getClient()->getEmail();
        $tel = $rdv->getClient()->getTelpro();
        
        $response = new JsonResponse();
        return $response->setData(array('nom'=>$nom,'prenom'=>$prenom,'email'=>$email,'tel'=>$tel)); 
        
}
   

    public function createAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest('request');
        $client = new Client2();
       
            $client = $this->getParams($request);
            $em->persist($client);
            $em->flush();
            $id = $client->getId();
            $nom = $client->getNom();
            $prenom = $client->getPrenom();
            $response = new JsonResponse();
            return $response->setData(array('id' => $id, 'nom' => $nom, 'prenom'=>$prenom));
         
    }
    function deleteAction() {
        return $this->render('InstructionBundle:Client:delete.html.twig', array(
// ...
        ));
    }
    //   ******************* get params ***************
    private function getParams($request) {
        $client = new Client2();
        $client->setNom($request->get('nom'));
        $client->setPrenom($request->get('prenom'));
        $client->setTel(($request->get('tel')));
        $client->setTelmob($request->get('telMob'));
        $client->setTelpro($request->get('telPro'));
        $client->setTelbox($request->get('telBox'));
        $client->setEmail($request->get('email'));
        $client->setNote($request->get('note'));
       
        //   $client->setPartage($request->get('partage'));
        // $clent-> cicvilite    must be added to  the entity
        //   $client-> adresse;  must be added to  the entity
        // $client-> password;  must be added to the entity
        return $client;
    }

   public function checkAction($idClient,$idRdvType)
    {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('beanBundle:Client')->find($idClient);
        $rdvType = $em->getRepository('beanBundle:Rendezvoustype')->find($idRdvType);
        //*************client**********************
        $telMobCli = $client->getTelmob();
        $emailCli = $client->getEmail();
        $noteCli = $client->getNote();
        $telBoxCli = $client->getTelbox();
        $dateNaissCli = $client->getDatenaissance();
        //*******************************************
        //***************rdvType*********************
        $telMobRdv = $rdvType->getTelMobile();
        $telBoxRdv = $rdvType->getTelBox();
        $email = $rdvType->getEmail();
        $note = $rdvType->getNotes();
        $dateNaissRdv =  $rdvType->getNotes();
        
        //******************************************
        $response = new JsonResponse();
//        $serializer= $this->get('jms_serializer');
//        $data1=$serializer->serialize($client,'json');
//        $data2=$serializer->serialize($rdvType,'json');
//        var_dump($data1);
//        var_dump($data2);
       return $response->setData(array('telmobcli'=>$telMobCli,'emailcli'=> $emailCli,'notecli'=>$noteCli,'telboxcli'=>$telBoxCli,'datenaisscli'=>$dateNaissCli,
                                        'telmobrdv'=> $telMobRdv,'telboxrdv'=>$telBoxRdv,'emailrdv'=>$email,'noterdv'=>$note,'datenaissrdv'=>$dateNaissRdv)); 

    }
 

}
