<?php

namespace RendezVous\RendezVousBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use bean\beanBundle\Repository\RendezvousRepository;


class RdvAnnulerController extends Controller
{
 
  public function createAction(){
      return $this->render('RendezVousBundle:Default:annulerRdvv.html.twig');
  }

 public function modifierAction($id) {
        $em = $this->getDoctrine()->getManager();
        $rdv = $em->getRepository('beanBundle:RendezVous')->find($id);
        $phrases = $em->getRepository('beanBundle:PhraseUsuelle')->findAll();
        return $this->render('RendezVousBundle:Default:annulerRdvv.html.twig', array('event' => $rdv, 'phrases' => $phrases));
    }
    
    public function annulerAction($id) {
        $request = $this->get('request');
         $annuler = new \bean\beanBundle\Entity\Annulation();
        $em = $this->getDoctrine()->getManager();
        $rdv = $em->getRepository('beanBundle:RendezVous')->find($id);
        //**********************************************
       $clientalert = $request->get('clientAlert');
//       $dateannulation= $request->get('dateannulation');
       $entete = $request->get('entete');
       $etat =  $request->get('etat');
       $myalert = $request->get('myAlert');
       $myentete = $request->get('myEntete');//default text
       $phraseusuelle = $request->get('phraseusuelle');
       //*******************************************************
       $phrase = $em->getRepository('beanBundle:PhraseUsuelle')->find($phraseusuelle);
       $report = $request->get('report');//memo
       //************************************************
       $annuler->setClientalert($clientalert);
       $annuler->setDateannulation(new \DateTime());
       $annuler->setEntete($entete);
       $annuler->setEtat($etat);
       $annuler->setMyalert($myalert);
       $annuler->setMyentete($myentete);
       $annuler->setPhraseUsuelle($phrase);
       $annuler->setRendezvous($rdv);
       $annuler->setReport($report);
       $rdv->setAnnuler(1);
       
       $em->persist($annuler);
       $em->flush();

      
//        $query = $em->createQuery('select r.id as id,r.end, r.libelle, r.detail as description, r.datedebut as start, r.couleur as color,
//                                p.jourselectionne as dow, c.nom as title   from beanBundle:RendezVous r inner join r.periodicite p inner join r.client c');
//        $evenements = $query->getResult();
//        $serializer = $this->get('jms_serializer');
//        $data = $serializer->serialize($evenements, 'json');
//
//       
//        return $this->render('RendezVousBundle:Default:fullCalendar.html.twig', array('events' => $data));
    }
    
    


    


}
