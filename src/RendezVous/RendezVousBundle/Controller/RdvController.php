<?php

namespace RendezVous\RendezVousBundle\Controller;

use bean\beanBundle\Entity\Periodicite;
use bean\beanBundle\Entity\ranges;
use bean\beanBundle\Entity\RendezVous;
use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class RdvController extends Controller {

    public function addAction() {

        $em = $this->getDoctrine()->getManager();

        $clients = $em->getRepository('beanBundle:Client')->findAll();

        $rdvType = $em->getRepository('beanBundle:Rendezvoustype')->findAll();
        return $this->render('RendezVousBundle:Default:addRendezVous.html.twig', array('clients' => $clients, 'rdvType' => $rdvType));
    }

    public function createAction() {

        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();



        $client = $em->getRepository('beanBundle:Client')->find($request->get('client'));
        $rdvType = $em->getRepository('beanBundle:RendezVousType')->find($request->get('rdvType'));
        $uneFois = $request->get('uneFois');

        $rdv = new RendezVous();
        $rdv->setLibelle($request->get('libelle'))
                ->setTypePeriodicite($uneFois)
                ->setEntete($request->get('entete'))
                ->setClient($client)
                ->setDetail($request->get('detail'))
//                ->setDatedebut(new DateTime($request->get('datepicker1') . ' ' . $request->get('timeDebut')))
//                ->setEnd(new DateTime($request->get('datepicker1') . ' ' . $request->get('timeFin')))
                ->setCouleur($request->get('color'))
                ->setPartage($request->get('partage'))
                ->setClientunite('')
                ->setAnnuler(0)
                ->setMyunite('');
        $jrEntiere = $request->get('jrEntiere');
        if (isset($jrEntiere)) {
            $rdv->setDatedebut(new DateTime($request->get('datepicker1') . ' ' . '08:00:00'));
            $rdv->setEnd(new DateTime($request->get('datepicker1') . ' ' . '18:00:00'));
        } else {

            $rdv->setDatedebut(new DateTime($request->get('datepicker1') . ' ' . $request->get('timeDebut')));
            $rdv->setEnd(new DateTime($request->get('datepicker1') . ' ' . $request->get('timeFin')));
        }
//***************myAlert/***********
        $tabMyAlert = $request->get('alert');
        if (isset($tabMyAlert)) {
            $rdv->setMyAlert($tabMyAlert);
        } else
            $rdv->setMyAlert('');
//********************alertClient*****************
        $tabAlertCli = $request->get('alertCli');
        if (isset($tabAlertCli)) {
            $rdv->setClientalert($tabAlertCli);
        } else
            $rdv->setClientalert('');
//**************************************************
        $etat = $request->get('etat');
        $rdv->setEtat($etat);
        $myDuree = $request->get('myRappel');
        $duree1 = $request->get('duree1');
        if ($request->get('rappel') == "0") {
            $rdv->setMyrappel('');
        } else {
            $tabRap = $request->get('rap');
            if ($duree1 == "1") {
                $rdv->setMyduree($myDuree);
            } else {
                $rdv->setMyduree($myDuree * 60);
            }
            if (isset($tabRap)) {
                $rdv->setMyrappel($tabRap);
            }
        }

        $dureeClient = $request->get('rappelCli');
        $duree2 = $request->get('duree2');

        if ($request->get('rapCli') == "0") {
            $rdv->setClientrappel('');
        } else {
            $tabRapCli = $request->get('cli');
            if ($duree2 == "1") {
                $rdv->setClientduree($dureeClient);
            } else {
                $rdv->setClientduree($dureeClient * 60);
            }
            if (isset($tabRapCli)) {
                $rdv->setClientrappel($tabRapCli);
            }
        }

        $recopie = $request->get('recopie');
        if (isset($recopie)) {
            $rdv->setRecopie($recopie);
        }
        //****************************************Periodicite**************************************************
        $periodicite = new Periodicite();
        $quotidien = $request->get('quotidien');



        $ttLes = $request->get('ttLes');
        $jours = $request->get('jours');
        $jourSelect = $request->get('jourSelect');
        $jourOuvrable = $request->get('jourOuvrable');
        $uneFois = $request->get('uneFois');



        if ($uneFois == "0" && isset($uneFois)) {
            $periodicite->setType($uneFois);
        }

        if ($uneFois == "1" && isset($ttLes)) {

            $periodicite->setType($uneFois);
            if ($ttLes == "1") {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setTouslesjours($jours);
            }
            if ($ttLes == "2") {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setTouslesjoursouvrable(1);
            }
            if ($ttLes == "3") {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setJourselectionne($request->get('quotid'));
            }
        }


        if ($uneFois == "2" && isset($uneFois)) {
            $periodicite->setType($uneFois);
            //$periodicite->setTousles($request->get('daySelect'));
            $periodicite->setJourselectionne($request->get('daySelect'));
        }


//            ->setTouslesjours($request->get(''))
        $finApres = $request->get('finApres');
        $datePickerFinLe = $request->get('datePickerFinLe');
        $periodicite->setEnd(new DateTime($datePickerFinLe));

//        $myRange = array('{start:' . $request->get('datepicker1'), 'end:' . $datePickerFinLe . '}');
//        $periodicite->setRanges('{start:' . $request->get('datepicker1'), 'end:' . $datePickerFinLe . '}');
        //***********************Ranges******************************
        $ranges = new ranges();
        $ranges->setStart(new DateTime($request->get('datepicker1')));
        $ranges->setEnd(new DateTime($datePickerFinLe));
        //***********************************************************
        $occurence = $request->get('occurence');
        if ($finApres == 1) {
            $periodicite->setFinapresoccurence($occurence);
        }
//            if($finApres==2)
//           $periodicite ->setFinapresoccurence($finApres);
        //****************************************************************************************************** 
        $rdv->setRendezVousType($rdvType);
        $rdv->setPeriodicite($periodicite);
        $periodicite->setRanges($ranges);

        $em->persist($periodicite);
        $em->persist($ranges);
        $em->persist($rdv);
        var_dump($rdv);
        $em->flush();
        $phrases = $em->getRepository('beanBundle:PhraseUsuelle')->findAll();
        $query = $em->createQuery('select r.id as id,r.end,r.allDay, r.libelle as title, r.datedebut as start, r.couleur as color
                                   from beanBundle:RendezVous r ');
        $evenements = $query->getResult();
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($evenements, 'json');



        //$clients = $em->getRepository('beanBundle:Client')->findAll();
        return $this->render('RendezVousBundle:Default:fullCalendar.html.twig', array('events' => $data, 'phrases' => $phrases, 'content_type' => 'application/json'));
    }

    private function createPeriodiciteAction() {
//        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $periodicite = new Periodicite();
        $periodicite->setEffet($request->get(''))
                ->setEnd($request->get(''))
                ->setCouleuralloccurence($request->get(''));
        $uneFois = $request->get('uneFois');
        $quotidien = $request->get('quotidien');
        $ttLes = $request->get('ttles');
        $jours = $request->get('jours');
        $jourSelect = $request->get('jourSelect');
        $jourOuvrable = $request->get('jourOuvrable');


        if (isset($uneFois)) {
            $periodicite->setType($uneFois);
        }

        if (isset($uneFois)) {
            $periodicite->setType($uneFois);
            if (isset($ttLes)) {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setTouslesjours($request->get($jours));
            }

            if (isset($ttles)) {
                //  $periodicite ->setTypequotidient($ttLes);
                $periodicite->setTouslesjoursouvrable($ttLes);
            } else {
                $periodicite->setTouslesjours(1);
                $periodicite->setTouslesjoursouvrable(0);
            }


            if (isset($ttles)) {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setJourselectionne($request->get('quotid'));
            } else
                $periodicite->setTouslesJours(1);
        }


        if (isset($uneFois)) {
            $periodicite->setType($uneFois);
            // $periodicite->setTousles($request->get('daySelect'));
            $periodicite->setJourselectionne($request->get('daySelect'));
        } else
            $periodicite->setTouslesJours(1);

        $periodicite->setFinapresoccurence($request->get(''));
//
//
//            ->setTouslesjours($request->get(''))
        $finApres = $request->get('finApres');

        if (isset($finApres))
            $periodicite->setLibellealloccurence($request->get('finApres'));


        return $periodicite;
    }

    public function updateAction($id) {

        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();

        $client = $em->getRepository('beanBundle:Client')->find($request->get('client'));
        $rdv = $em->getRepository('beanBundle:Rendezvous')->find($id);

        $rdv->setLibelle($request->get('libelle'))
                ->setEntete($request->get('entete'))
                ->setClient($client)
                ->setDetail($request->get('detail'))
                ->setDatedebut(new DateTime($request->get('datepicker1') . ' ' . $request->get('timeDebut')))
                ->setEnd(new DateTime($request->get('datepicker1') . ' ' . $request->get('timeFin')))
                ->setCouleur($request->get('color'))
                ->setPartage($request->get('partage'))
                ->setClientunite('')
                ->setMyduree(0)
                ->setClientduree(0)
                ->setMyunite('');



//***************myAlert/***********
        $tabMyAlert = $request->get('alert');
        $string3 = "";
        if (isset($tabMyAlert)) {
            //foreach($tabMyAlert as $myAlert)// $string3.=$myAlert.' ';
//        $myInt3 = intval($string3);
            $rdv->setMyAlert($tabMyAlert);
        } else
            $rdv->setMyAlert('');
        //*******************************
        //$rdv->setMyAlertType(10);
//********************alertClient*****************
        $tabAlertCli = $request->get('alertCli');
        // $string2="";
        if (isset($tabAlertCli)) {
            // foreach($tabAlertCli as $alertCli) $string2.=$alertCli;
            // $myInt2 = intval($string2);
            $rdv->setClientalert($tabMyAlert);
        } else
            $rdv->setClientalert('');
//**************************************************

        $etat = $request->get('etat');
//    if(isset($etat))
        $rdv->setEtat($etat);

        if ($request->get('rappel') == "0") {
            $rdv->setMyrappel('');
        } else {
            $tabRap = $request->get('rap');
            // $string1="";
            if (isset($tabRap)) {
                // foreach($tabRap as $rap) $string1.=$rap.' ';
//        $myInt1 = intval($string1);
                $rdv->setMyrappel($tabRap);
            }
        }

        //************Type de rappel************************
//    $tabRap = $request->get('rap');
//    $string1="";
//    if (isset($tabRap)) {
//        foreach($tabRap as $rap) $string1.=$rap.' ';
////        $myInt1 = intval($string1);
//        $rdv->setMyrappel($string1);
//    }else $rdv->setMyrappel('');
//
////    }
        //***************************************


        if ($request->get('rapCli') == 0) {
            $rdv->setClientrappel('');
        } else {
            $tabRapCli = $request->get('cli');
            $rappels = "";
            if (isset($tabRapCli)) {
                //foreach ($tabRapCli as $rapCli) $rappels .= $rapCli.' ';
                $rdv->setClientrappel($tabRapCli);
            }
        }

        //**************************
//    $tabRapCli = $request->get('cli');
//    $rappels = "";
//    if (isset($tabRapCli)) {
//    foreach ($tabRapCli as $rapCli) $rappels .= $rapCli;
//       $myInt = intval($rappels);
//        $rdv->setRappelClientType($myInt);
//   }else{
//        $rdv->setRappelClientType(0);
//    }
        //****************************
//    $periodicite = new Periodicite();
//    $periodicite->setFinApresOccurence(1)
//        ->setFinLe(new \DateTime())
//        ->setNoteLier(2)
//        ->setRdv($rdv)
//        ->setType(3)
//        ->setTousLes(4);
//    $periodicite = new Periodicite();
//
//    $rdv->setPeriodicite($periodicite);

        $recopie = $request->get('recopie');
        if (isset($recopie))
            $rdv->setRecopie($recopie);



        $em->flush();
//    $evenements = $em->getRepository('beanBundle:RendezVous')->findEvents();
//    $serializer= $this->get('jms_serializer');
//    $data=$serializer->serialize($evenements,'json');
        //array('events'=>$data,'content_type'=>'application/json')



        $evenements = $em->getRepository('beanBundle:RendezVous')->findEvents();
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($evenements, 'json');
        return $this->render('RendezVousBundle:Default:fullCalendar.html.twig', array('events' => $data, 'content_type' => 'application/json'));
    }

    public function fullAction() {
        // $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        $phrases = $em->getRepository('beanBundle:PhraseUsuelle')->findAll();
        $dateTimeDebut = date_format(new \DateTime(),'Y-m-d');
        $dateTimeEnd = $this->calculDate($dateTimeDebut);
        $query = $em->createQuery("select r.id as id,r.end, r.allDay , r.libelle as title , r.detail ,  r.datedebut as start,p.jourselectionne as dow, r.couleur as color,
        c.nom as description  
        from beanBundle:RendezVous r 
        inner join r.periodicite p 
        inner join r.client c 
        where r.datedebut >= :dateDebut
        and r.datedebut <= :dateFin 
        and r.annuler=0 ")
                ->setParameter('dateDebut', $dateTimeDebut)
                ->setParameter('dateFin', $dateTimeEnd);

//        $query = $em->createQueryBuilder('r')
//            ->select('r.id as id , r.libelle as title , r.datedebut as start,r.end , r.couleur color ')
//            ->from('beanBundle:RendezVous', 'r')
//            ->getQuery();
        $evenements = $query->getResult();

$delimiteur = $em->createQuery('select r.id as id, r.titre as title,p.effet as start, p.end , p.couleuralloccurence as color '
        . 'from beanBundle:Delimiteur r join r.periodicite p');
$delimiteurs = $delimiteur->getResult();

        //  return array('events'=>$evenements);
        // $delimiteurs = $em->getRepository('RendezVousBundle:Periodicite')->findDelimiteurs();
        // }//        array_push($tab,$evenements);
//        array_push($tab,$delimiteurs);

        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($evenements, 'json');
        $data2 = $serializer->serialize($delimiteurs, 'json');


        print '<pre>';
        print_r($data);
        print '\n';
        print '</pre>';
        print '*****************************************************';
        print $dateTimeEnd;
        print '*****************************************************';

        //return $this->render(array('events'=>$data));
        return $this->render('RendezVousBundle:Default:fullCalendar.html.twig', array('events' => $data,'delimiteur'=>$data2, 'content_type' => 'application/json', 'phrases' => $phrases));
    }

    public function modifierAction($id) {
        $em = $this->getDoctrine()->getManager();
        $rdv = $em->getRepository('beanBundle:RendezVous')->find($id);
        $clients = $em->getRepository('beanBundle:Client')->findAll();
        $rdvTypes = $em->getRepository('beanBundle:Rendezvoustype')->findAll();
        return $this->render('RendezVousBundle:Default:updateRendezVous.html.twig', array('event' => $rdv, 'clients' => $clients, 'rdvTypes' => $rdvTypes));
    }

    public function rdvTypeAction() {
        $em = $this->getDoctrine()->getManager();
        $rdv = $em->getRepository('beanBundle:RendezVous')->find($id);
        $clients = $em->getRepository('beanBundle:Client')->findAll();
        return $this->render('RendezVousBundle:Default:updateRendezVous.html.twig', array('event' => $rdv, 'clients' => $clients));
    }

    public function startEndAction($startD, $startH, $endH) {
        $em = $this->getDoctrine()->getManager();
// print $start.'  ';
//        print '  '.$end;
//        $debut = split('T', $start);
//        $fin = split('T', $end);
//        $var1 =join(" ", $debut);
//        $var2 =join(" ", $fin);
        $clients = $em->getRepository('beanBundle:Client')->findAll();
        $rdvType = $em->getRepository('beanBundle:Rendezvoustype')->findAll();
        return $this->render('RendezVousBundle:Default:addRendezVous.html.twig', array('startD' => $startD, 'endH' => $endH, 'startH' => $startH, 'clients' => $clients, 'rdvType' => $rdvType));
    }

    public function removeAction($id) {
        $em = $this->getDoctrine()->getManager();
        $rdv = $em->getRepository('beanBundle:RendezVous')->find($id);
        //$periodicite = $em->getRepository('beanBundle:Periodicite')->find($rdv->getPeriodicite());
        $em->remove($rdv);
        //$em->remove($periodicite);
        $em->flush();
        //return $this->render('RendezVousBundle:Default:updateRendezVous.html.twig', array('event' => $rdv, 'clients' => $clients));
    }

    public function calculDate($date) {
        $date = new DateTime($date);
        $date->add(new DateInterval('P7D'));
        return $date->format('Y-m-d');
    }

    public function myCalendarAction() {
        $em = $this->getDoctrine()->getManager();
        $phrases = $em->getRepository('beanBundle:PhraseUsuelle')->findAll();
        return $this->render('RendezVousBundle:Default:fullCalendar.html.twig', array('phrases' => $phrases));
    }

    public function calendarTestAction() {
        $em = $this->getDoctrine()->getManager();
//        $query = $em->createQuery("select r,c
//                                from beanBundle:RendezVous r , beanBundle:Client c where r.client.id = c.id");
//        $evenements = $query->getResult();
        $query = $em->createQuery('SELECT  r FROM beanBundle:RendezVous r where r.id=31 ');

$serializer = $this->get('serializer');

$entity = $this->get('doctrine')
               ->getRepository('beanBundle:RendezVous')
               ->find(31);


 $lol = $query->getResult();


$toEncode = array(
    'response' => array(
        'entity' => $serializer->normalize($entity)
       
    ),
);
return new Response(json_encode($toEncode));
//     
       
//$serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new 
//JsonEncoder()));
//$json = $serializer->serialize($lol, 'json');
////         
//       print $json;
    }

    
    
}
