<?php

namespace RendezVous\RendezVousBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

//use bean\beanBundle\Repository\RendezvousRepository;

class ClientRdvController extends Controller {

    public function clientRdvAction() {

        $session = new Session();



        $em = $this->getDoctrine()->getManager();
        $utilisateurs = $em->getRepository('beanBundle:Utilisateur')->findAll();
        $rdvTypes = $em->getRepository('beanBundle:RendezVousType')->findAll();
        $query = $em->createQuery("select r.id as id,r.end , r.detail, r.overlap  , r.rendering  ,  r.datedebut as start,p.jourselectionne as dow, 
        c.nom as description  
        from beanBundle:RendezVous r 
        inner join r.periodicite p 
        inner join r.client c 
        where r.annuler=0 ");


        $evenements = $query->getResult();

        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($evenements, 'json');

        $request = $this->get('request');
        $login = $request->get('login');
        $pass = $request->get('pass');
        $clients = $em->getRepository('beanBundle:Client')->findAll();
        foreach ($clients as $client) {
            if (($client->getEmail() == $login) && ($client->getNom() == $pass)) {
                $session->set('name', $client->getNom());
                $session->set('prenom', $client->getPrenom());
                $session->set('id', $client->getId());
                $dateTime = date_format(new \DateTime(), 'Y-m-d');
                $passe = $em->createQuery("select r.id,r.libelle,r.datedebut,r.annuler,r.end from beanBundle:RendezVous r  join r.client c  where r.datedebut <= :myDate and c.id = :clientId ")
                        ->setParameter('myDate', $dateTime)
                        ->setParameter('clientId', $client->getId());
                        
                $clientEventsPasse = $passe->getResult();
                
                $futur = $em->createQuery("select r.id,r.libelle,r.datedebut,r.annuler,r.end from beanBundle:RendezVous r  join r.client c  where r.datedebut >= :myDate and c.id = :clientId ")
                        ->setParameter('myDate', $dateTime)
                        ->setParameter('clientId', $client->getId());
                        
                $clientEventsFutur = $futur->getResult();

                $query = $em->createQuery('SELECT COUNT(r.id) FROM beanBundle:RendezVous r  join r.client c  where r.datedebut <= :myDate and c.id = :clientId ')
                          ->setParameter('myDate', $dateTime)
                        ->setParameter('clientId', $client->getId());
            $countPasse = $query->getSingleScalarResult();
            
             $query1 = $em->createQuery('SELECT COUNT(r.id) FROM beanBundle:RendezVous r  join r.client c  where r.datedebut >= :myDate and c.id = :clientId ')
                          ->setParameter('myDate', $dateTime)
                        ->setParameter('clientId', $client->getId());
            $countFutur = $query1->getSingleScalarResult();
            
            $query3 = $em->createQuery("select r.id,r.libelle,r.datedebut,r.annuler,r.end from beanBundle:RendezVous r  join r.client c  where c.id = :clientId ")
                        ->setParameter('clientId', $client->getId());
            $findAll = $query3->getResult();
           
            $tout =  $countPasse+$countFutur;
                return $this->render('RendezVousBundle:RdvClient:fullCalendar.html.twig', array('events' => $data, 'utilisateurs' => $utilisateurs, 'rdvTypes' => $rdvTypes,'clientEventsPasse'=>$clientEventsPasse,'countPasse'=>$countPasse,'clientEventsFutur'=>$clientEventsFutur,'countFutur'=>$countFutur,'findAll'=>$findAll,'tout'=>$tout,'clientId'=>$client->getId()));
            }
        }
        return $this->render('RendezVousBundle:RdvClient:login.html.twig');
    }

    public function loginAction() {
        return $this->render('RendezVousBundle:RdvClient:login.html.twig');
    }

    public function createAction() {
      

        $request = $this->get('request');

        $em = $this->getDoctrine()->getManager();



        $client = $em->getRepository('beanBundle:Client')->find($request->get('client'));
        $rdvType = $em->getRepository('beanBundle:RendezVousType')->find($request->get('myRdvType'));
        // $uneFois = $request->get('uneFois');

        $rdv = new RendezVous();
        $rdv->setLibelle($rdvType->getLibelle().''.$client->getNom())
                ->setTypePeriodicite('0')
                ->setEntete($rdvType->getLibelle())
                ->setClient($client)
                ->setDetail($client->getEmail())
//                ->setDatedebut(new DateTime($request->get('datepicker1') . ' ' . $request->get('timeDebut')))
//                ->setEnd(new DateTime($request->get('datepicker1') . ' ' . $request->get('timeFin')))
                ->setCouleur($rdvType->getCouleur())
                ->setPartage('')
                ->setClientunite('')
                ->setAnnuler(0)
                ->setMyunite('');
        // $jrEntiere = $request->get('jrEntiere');
        // if (isset($jrEntiere)) {
        // $rdv->setDatedebut(new DateTime($request->get('datepicker1') . ' ' . '08:00:00'));
        // $rdv->setEnd(new DateTime($request->get('datepicker1') . ' ' . '18:00:00'));
        // } else {

        $rdv->setDatedebut(new DateTime($request->get('myDate') . ' ' . $request->get('myStart')));
        $rdv->setEnd(new DateTime($request->get('myDate') . ' ' . $request->get('myEnd')));
        //}
//***************myAlert/***********
        $tabMyAlert = $request->get('alert');
        if (isset($tabMyAlert)) {
            $rdv->setMyAlert($tabMyAlert);
        } else
            $rdv->setMyAlert('');
//********************alertClient*****************
        $tabAlertCli = $request->get('alertCli');
        if (isset($tabAlertCli)) {
            $rdv->setClientalert($tabAlertCli);
        } else
            $rdv->setClientalert('');
//**************************************************
        $etat = $request->get('etat');
        $rdv->setEtat($etat);
        $myDuree = $request->get('myRappel');
        $duree1 = $request->get('duree1');
        if ($request->get('rappel') == "0") {
            $rdv->setMyrappel('');
        } else {
            $tabRap = $request->get('rap');
            if ($duree1 == "1") {
                $rdv->setMyduree($myDuree);
            } else {
                $rdv->setMyduree($myDuree * 60);
            }
            if (isset($tabRap)) {
                $rdv->setMyrappel($tabRap);
            }
        }

        $dureeClient = $request->get('rappelCli');
        $duree2 = $request->get('duree2');

        if ($request->get('rapCli') == "0") {
            $rdv->setClientrappel('');
        } else {
            $tabRapCli = $request->get('cli');
            if ($duree2 == "1") {
                $rdv->setClientduree($dureeClient);
            } else {
                $rdv->setClientduree($dureeClient * 60);
            }
            if (isset($tabRapCli)) {
                $rdv->setClientrappel($tabRapCli);
            }
        }

        $recopie = $request->get('recopie');
        if (isset($recopie)) {
            $rdv->setRecopie($recopie);
        }
        //****************************************Periodicite**************************************************
        $periodicite = new Periodicite();
        $quotidien = $request->get('quotidien');



        $ttLes = $request->get('ttLes');
        $jours = $request->get('jours');
        $jourSelect = $request->get('jourSelect');
        $jourOuvrable = $request->get('jourOuvrable');
        $uneFois = $request->get('uneFois');



       // if ($uneFois == "0" && isset($uneFois)) {
            $periodicite->setType(0);
       // }

        if ($uneFois == "1" && isset($ttLes)) {

            $periodicite->setType($uneFois);
            if ($ttLes == "1") {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setTouslesjours($jours);
            }
            if ($ttLes == "2") {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setTouslesjoursouvrable(1);
            }
            if ($ttLes == "3") {
                $periodicite->setTypequotidient($ttLes);
                $periodicite->setJourselectionne($request->get('quotid'));
            }
        }


        if ($uneFois == "2" && isset($uneFois)) {
            $periodicite->setType($uneFois);
            //$periodicite->setTousles($request->get('daySelect'));
            $periodicite->setJourselectionne($request->get('daySelect'));
        }


//            ->setTouslesjours($request->get(''))
        $finApres = $request->get('finApres');
        $datePickerFinLe = $request->get('datePickerFinLe');
        $periodicite->setEnd(new DateTime($datePickerFinLe));

//        $myRange = array('{start:' . $request->get('datepicker1'), 'end:' . $datePickerFinLe . '}');
//        $periodicite->setRanges('{start:' . $request->get('datepicker1'), 'end:' . $datePickerFinLe . '}');
        //***********************Ranges******************************
        $ranges = new ranges();
        $ranges->setStart(new DateTime($request->get('myDate')));
        $ranges->setEnd(new DateTime($request->get('myDate')));
        //***********************************************************
        $occurence = $request->get('occurence');
        if ($finApres == 1) {
            $periodicite->setFinapresoccurence($occurence);
        }
//            if($finApres==2)
//           $periodicite ->setFinapresoccurence($finApres);
        //****************************************************************************************************** 
        $rdv->setRendezVousType($rdvType);
        $rdv->setPeriodicite($periodicite);
        $periodicite->setRanges($ranges);

        $em->persist($periodicite);
        $em->persist($ranges);
        $em->persist($rdv);
        var_dump($rdv);
        $em->flush();
//        $phrases = $em->getRepository('beanBundle:PhraseUsuelle')->findAll();
//        $query = $em->createQuery('select r.id as id,r.end,r.allDay, r.libelle as title, r.datedebut as start, r.couleur as color
//                                   from beanBundle:RendezVous r ');
//        $evenements = $query->getResult();
//        $serializer = $this->get('jms_serializer');
//        $data = $serializer->serialize($evenements, 'json');



        //$clients = $em->getRepository('beanBundle:Client')->findAll();
        //return $this->render('RendezVousBundle:Default:fullCalendar.html.twig', array('events' => $data, 'phrases' => $phrases, 'content_type' => 'application/json'));
    }

}
