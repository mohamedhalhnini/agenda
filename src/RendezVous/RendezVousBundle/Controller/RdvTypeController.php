<?php

namespace RendezVous\RendezVousBundle\Controller;

use bean\beanBundle\Entity\RendezVousType;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
//use bean\beanBundle\Repository\RendezvousRepository;


class RdvTypeController extends Controller
{
 public function createAction(){
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        $typeRdv = new RendezVousType();
        $adr=$request->get('adresse');
        if(isset($adr)){
        $typeRdv->setAdresse($adr);}
        else  {$typeRdv->setAdresse(0);}

        $champClient = $request->get('champClient');
//        if(isset($champClient))
        $typeRdv->setChampsClient($champClient);
//        else  $typeRdv->setChampsClient(0);

        $typeRdv->setCouleur($request->get('couleur'));

        $dateNaissance = $request->get('dateNaissance');
        if(isset($dateNaissance))
            $typeRdv ->setDateNaissance($dateNaissance);
        else  $typeRdv ->setDateNaissance(0);

        $nomJF=$request->get('nomJF');
        if(isset($nomJF))
            $typeRdv ->setNomJF($nomJF);
        else   $typeRdv ->setNomJF(0);


        $typeRdv->setDureee(new DateTime($request->get('duree')));

        $email = $request->get('email');
        if(isset($email))
             $typeRdv->setEmail($email);
        else $typeRdv->setEmail(0);

        $infoSup=$request->get('infoSup');
        if(isset($infoSup))
             $typeRdv->setInfoSup($infoSup);
        else  $typeRdv->setInfoSup(0);

        $lastVisit = $request->get('lastVisit');
        if(isset($lastVisit))
             $typeRdv->setLastVisit($lastVisit);
        else $typeRdv->setLastVisit(0);

             $typeRdv->setLibelle($request->get('libelle').' '.$request->get('concat'));
        $telBox = $request->get('telBox');
        if(isset($telBox))
             $typeRdv->setTelBox($telBox);
        else  $typeRdv->setTelBox(0);

        $telMobile = $request->get('telMobile');
            if(isset($telMobile))
             $typeRdv->setTelMobile($telMobile);
        else   $typeRdv->setTelMobile(0);

        $telTravail=$request->get('telTravail');
        if(isset($telTravail))
             $typeRdv->setTelTravail($telTravail);
        else  $typeRdv->setTelTravail(0);

        $telDomicile =$request->get('telDomicile');
        if(isset($telDomicile))
             $typeRdv->setTelDomicie($telDomicile);
        else  $typeRdv->setTelDomicie(0);

        $notes =$request->get('notes');
        if(isset($notes))
            $typeRdv->setNotes($notes);
        else  $typeRdv->setNotes(0);


        $typeRdv->setInformation($request->get('information'));

             $typeRdv->setSelectParDef($request->get('selectParDef'));
             $typeRdv->setSelectionnable($request->get('selectionnable'));
             $typeRdv->setChoixAlertRappel($request->get('choixAlertRappel'));

        $em->persist($typeRdv);
        $em->flush();

        return $this->render('RendezVousBundle:Default:rdvType.html.twig');

    }

    
  public function modifierAction($id) {
        $em = $this->getDoctrine()->getManager();
        $rdvType = $em->getRepository('beanBundle:RendezVousType')->find($id);
        return $this->render('RendezVousBundle:Default:updateRendezVousType.html.twig', array('rdvType' => $rdvType));
    }
 public function updateAction($id){
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        
        $typeRdv = $em->getRepository('beanBundle:RendezVousType')->find($id);
        $adr=$request->get('adresse');
        if(isset($adr)){
        $typeRdv->setAdresse($adr);}
        else  {$typeRdv->setAdresse(0);}

        $champClient = $request->get('champClient');
//        if(isset($champClient))
        $typeRdv->setChampsClient($champClient);
//        else  $typeRdv->setChampsClient(0);

        $typeRdv->setCouleur($request->get('couleur'));

        $dateNaissance = $request->get('dateNaissance');
        if(isset($dateNaissance))
            $typeRdv ->setDateNaissance($dateNaissance);
        else  $typeRdv ->setDateNaissance(0);

        $nomJF=$request->get('nomJF');
        if(isset($nomJF))
            $typeRdv ->setNomJF($nomJF);
        else   $typeRdv ->setNomJF(0);


        $typeRdv->setDureee(new DateTime($request->get('duree')));

        $email = $request->get('email');
        if(isset($email))
             $typeRdv->setEmail($email);
        else $typeRdv->setEmail(0);

        $infoSup=$request->get('infoSup');
        if(isset($infoSup))
             $typeRdv->setInfoSup($infoSup);
        else  $typeRdv->setInfoSup(0);

        $lastVisit = $request->get('lastVisit');
        if(isset($lastVisit))
             $typeRdv->setLastVisit($lastVisit);
        else $typeRdv->setLastVisit(0);

             $typeRdv->setLibelle($request->get('libelle').' '.$request->get('concat'));
        $telBox = $request->get('telBox');
        if(isset($telBox))
             $typeRdv->setTelBox($telBox);
        else  $typeRdv->setTelBox(0);

        $telMobile = $request->get('telMobile');
            if(isset($telMobile))
             $typeRdv->setTelMobile($telMobile);
        else   $typeRdv->setTelMobile(0);

        $telTravail=$request->get('telTravail');
        if(isset($telTravail))
             $typeRdv->setTelTravail($telTravail);
        else  $typeRdv->setTelTravail(0);

        $telDomicile =$request->get('telDomicile');
        if(isset($telDomicile))
             $typeRdv->setTelDomicie($telDomicile);
        else  $typeRdv->setTelDomicie(0);

        $notes =$request->get('notes');
        if(isset($notes))
            $typeRdv->setNotes($notes);
        else  $typeRdv->setNotes(0);


        $typeRdv->setInformation($request->get('information'));

             $typeRdv->setSelectParDef($request->get('selectParDef'));
             $typeRdv->setSelectionnable($request->get('selectionnable'));
             $typeRdv->setChoixAlertRappel($request->get('choixAlertRappel'));

      
        $em->flush();

        return $this->render('RendezVousBundle:Default:rdvType.html.twig');

    }   
    
    
    
    
    
    public function rdvTypeAction(){

        return $this->render('RendezVousBundle:Default:rdvType.html.twig');
    }

   
    public function dropAction($id,$start,$startH,$endH){
     
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('beanBundle:RendezVous')->find($id);
        $event->setDatedebut(new DateTime($start.' '.$startH))
            ->setEnd(new DateTime($start.' '.$endH));
        $em->flush();

    }
    
     public function rdvTypeAjaxAction( $rdvType1,$temps){
            $temp_string = explode(":", $temps);
        $temp_string1 = explode(":", $rdvType1);
//************************************************
            $totalHours = $temp_string[0];
            $totalMinutes = $temp_string[1];
        $totalHours1 = $temp_string1[0];
        $totalMinutes1 = $temp_string1[1];
//************************************************
        $M1 = $totalHours1*60+ $totalMinutes1;
        $M2 = $totalHours*60+ $totalMinutes;
//************************************************

        $TM = $M1+$M2;

        $MinTotal = $TM % 60;
        $HeureTotal =($TM-$MinTotal)/60;

        if( $MinTotal < 10 ) {
            $MinTotal = "0" . $MinTotal;
        }
        if( $HeureTotal < 10 ) {
            $HeureTotal = "0" . $HeureTotal;
        }
            $myTime = $HeureTotal . ":" . $MinTotal ;


        $response = new JsonResponse();
        return $response->setData(array('idiot'=>$myTime));
     //  die($myTime);

    }

  public function rdvTypeDureeAction($id){
        $em = $this->getDoctrine()->getManager();
        $rdvType1 = $em->getRepository('beanBundle:RendezVousType')->findOneBy(array('id'=>$id));
        if($rdvType1){
            $pff = date_format($rdvType1->getDureee(),'H:i');
            $color = $rdvType1->getCouleur();
        }else{
            $pff  = null;
            $color = null;
        }
        $response = new JsonResponse();
        return $response->setData(array('myTime'=>$pff,'color'=>$color));
    }

    
    public function listRdvTypeAction(){
            $em = $this->getDoctrine()->getManager();
        $rdvTypes = $em->getRepository('beanBundle:RendezVousType')->findAll();
        return $this->render('RendezVousBundle:Default:listRdvType.html.twig',array('rdvTypes' => $rdvTypes));
    }
    
    public function deleteTypeAction($id){
       
        $em = $this->getDoctrine()->getManager();
        $rdvType = $em->getRepository('beanBundle:RendezVousType')->find($id);
        $em->remove($rdvType);
        $em->flush();
        
        
    }
        
}
