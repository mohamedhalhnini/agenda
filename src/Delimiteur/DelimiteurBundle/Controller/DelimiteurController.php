<?php

namespace Delimiteur\DelimiteurBundle\Controller;

use bean\beanBundle\Entity\Delimiteur;
use bean\beanBundle\Entity\Periodicite;
use bean\beanBundle\Entity\typeDelimiteur;
use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;



class DelimiteurController extends Controller
{
    
    public function indexAction($name)
    {
        return $this->render('DelimiteurBundle:Default:index.html.twig', array('name' => $name));
    }
    
    
    public function delimiteurAction()
    {
         $em=$this->getDoctrine()->getManager();
         $typeDelimiteurs=$em->getRepository('beanBundle:typeDelimiteur')->findAll();
        return $this->render('DelimiteurBundle:Default:delimiteur.html.twig',array('typeDelimiteurs'=>$typeDelimiteurs));
    }
    
    //récuperer les événements
    private function getAllEvents(){
         $em = $this->getDoctrine()->getManager();
       
        $query = $em->createQuery('select r.id as id , r.titre as title , p.end ,p.jourselectionne as dow,p.effet as start, p.couleuralloccurence as color 
                                  from beanBundle:Delimiteur r  join r.periodicite p');

        $evenements= $query->getResult();

   $serializer= $this->get('jms_serializer');
   $data=$serializer->serialize($evenements,'json');
   return $data;
    }
    //récuperer les rdvs
    private function getAllRdvs(){
         $em = $this->getDoctrine()->getManager();
       
       $query = $em->createQuery('select r.id as id,r.end,r.allDay, r.libelle as title, r.datedebut as start, r.couleur as color
                                   from beanBundle:RendezVous r ');

        $evenements= $query->getResult();

   $serializer= $this->get('jms_serializer');
   $data2=$serializer->serialize($evenements,'json');
   return $data2;
    }
    //récuperer l'id maximal
    private function getMax(){
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('select max(r.id) from beanBundle:Delimiteur r');
        $max= $query->getSingleScalarResult();
        
        return $max;
    }
    
    
    //trasformation de la date
   private function getTransformedDate($date) {
        $tab = explode("/", $date);
        if (strlen($tab[0] . "") == 1) {
            $tab[0] = '0' . $tab[0];
        }
        if (strlen($tab[1] . "") == 1) {
            $tab[1] = '0' . $tab[1];
        }   
        $d = $tab[1] . "/" . $tab[0] . "/" . $tab[2];
        return $d;
    }
    // ajouter un delimiteur
    public function addDelimiteurAction(){
        $session = $this->getRequest();
        $delimiteur = new Delimiteur();
        $periodicite = new Periodicite();
        $delimiteur->setUtilisateur($this->get('security.context')->getToken()->getUser());
        
        $periodicite->setType($session->get('type'));
        
        
        if($session->get('type')==2){
            $periodicite->setTypequotidient($session->get('quotidienne'));
        if($session->get('quotidienne')==1){
            $periodicite->setTouslesjours($session->get('jours'));
        }
        if($session->get('quotidienne')==2){
            $periodicite->setTouslesjoursouvrable(2);
        }
        if($session->get('quotidienne')==3){
            $periodicite->setJourselectionne($session->get('checked'));
        }
        }
        if($session->get('type')==3){
            $periodicite->setTousles($session->get('hebdomadaire'));
        }
        $prendEffet= $this->getTransformedDate($session->get('prendEffet'));
        $periodicite->setEffet(new DateTime($prendEffet."".$session->get('debute')));
        
        if($session->get('prendFin')==1){
            $periodicite->setFinapresoccurence($session->get('occurences'));
            $occurence=7*$session->get('occurences');
            $interval=new DateInterval('P'.$occurence.'D');
            $finle=$this->getTransformedDate($session->get('prendEffet'));
            $periodicite->setEnd((new DateTime($prendEffet.''.$session->get('fin')))->add($interval));
        }
        if($session->get('prendFin')==2){
            $finle=$this->getTransformedDate($session->get('dateFin'));
            $periodicite->setEnd(new DateTime($finle."".$session->get('debute')));
        }
            $periodicite->setCouleuralloccurence($session->get('couleur'));
        
            
        $delimiteur->setTitre($session->get('titre'));
        $delimiteur->setDebute(new DateTime($session->get('debute')));
        $delimiteur->setFin(new DateTime($session->get('fin')));
        $delimiteur->setAccessinternet($session->get('accessInternet'));
        $delimiteur->setBlockage($session->get('blockage'));
        $delimiteur->setPartage($session->get('partage'));
        $delimiteur->setPriorite($session->get('priorite'));
        $delimiteur->setPeriodicite($periodicite);
        
        $em= $this->getDoctrine()->getManager();
        $em->persist($periodicite);
        $em->persist($delimiteur);
        $em->flush();
            $typeDelimiteurs=$em->getRepository('beanBundle:typeDelimiteur')->findAll();
        $data=$this->getAllEvents();
        $data2=$this->getAllRdvs();
        return $this->render('DelimiteurBundle:Default:fullCalendar.html.twig',array('events'=>$data,'rdvs'=>$data2,'content_type'=>'application/json','typeDelimiteurs'=>$typeDelimiteurs));
    }
    // modifier un délimiteur
     public function updateAction($id)
    {
                $em=$this->getDoctrine()->getManager();
                $delimiteur=$em->getRepository('beanBundle:Delimiteur')->find($id);
                $typeDelimiteurs=$em->getRepository('beanBundle:typeDelimiteur')->findAll();
                $jours=array('lundi'=>1,'mardi'=>2,'mercredi'=>3,'jeudi'=>4,'vendredi'=>5,'samedi'=>6,'dimanche'=>7);
                 return $this->render('DelimiteurBundle:Default:edit.html.twig',array('delimiteur'=>$delimiteur,'jours'=>$jours,'typeDelimiteurs'=>$typeDelimiteurs));
    }
    public function listDelimiteurAction()
            {
        $em=$this->getDoctrine()->getManager();
        $delimiteurs=$em->getRepository('beanBundle:Delimiteur')->findAll();
        
         return $this->render('DelimiteurBundle:Default:list.html.twig',array('delimiteurs'=>$delimiteurs));
       
            }
      // supprimer un délimiteur       
    public function deleteAction($id){
        $em=$this->getDoctrine()->getManager();
        
        $delimiteur=$em->getRepository('beanBundle:Delimiteur')->find($id);
       // echo $delimiteur->getBlockage();              
        $em->remove($delimiteur);
        $em->flush();
        $delimiteurs=$em->getRepository('beanBundle:Delimiteur')->findAll();
         $typeDelimiteurs=$em->getRepository('beanBundle:typeDelimiteur')->findAll();
        $data=$this->getAllEvents();
        $data2=$this->getAllRdvs();
        return $this->render('DelimiteurBundle:Default:fullCalendar.html.twig',array('delimiteurs'=>$delimiteurs,'events'=>$data,'rdvs'=>$data2,'typeDelimiteurs'=>$typeDelimiteurs));
    }           
    public function findAction(){
//        $date=new DateTime('2015-05-19 8:15:00');
//        $date->add(new DateInterval('P21D'));
       // $periodicite=$em->getRepository('DelimiteurBundle:Periodicite')->find(13);
        $date = date("Y-m-d");
        $date = strtotime(date("Y-m-d", strtotime($date)) . " +1 day");
        return $this->render('DelimiteurBundle:Default:test.html.twig',array('date'=>$date));
    }
    // modifier le délimiteur
    public function editAction($id){
        $em=$this->getDoctrine()->getManager();
        $session = $this->getRequest();
        $delimiteur =$em->getRepository('beanBundle:Delimiteur')->find($id);
        $periodicite = $em->getRepository('beanBundle:Periodicite')->find($delimiteur->getPeriodicite()->getId());
        
        
        
        $periodicite->setType($session->get('type'));
        
        
        if($session->get('type')==2){
            $periodicite->setTypequotidient($session->get('quotidienne'));
        if($session->get('quotidienne')==1){
            $periodicite->setTouslesjours($session->get('jours'));
        }
        if($session->get('quotidienne')==2){
            $periodicite->setTouslesjoursouvrable(2);
        }
        if($session->get('quotidienne')==3){
            $periodicite->setJourselectionne($session->get('checked'));
        }
        }
        if($session->get('type')==3){
            $periodicite->setTousles($session->get('hebdomadaire'));
        }
         $prendEffet= $this->getTransformedDate($session->get('prendEffet'));
        $periodicite->setEffet(new DateTime($prendEffet."".$session->get('debute')));
        
        if($session->get('prendFin')==1){
            $periodicite->setFinapresoccurence($session->get('occurences'));
            $occurence=7*$session->get('occurences');
            $interval=new DateInterval('P'.$occurence.'D');
            $finle=$this->getTransformedDate($session->get('prendEffet'));
            $periodicite->setEnd((new DateTime($finle.''.$session->get('debute')))->add($interval));
        }
        if($session->get('prendFin')==2){
             $finle=$this->getTransformedDate($session->get('dateFin'));
            $periodicite->setEnd(new DateTime($finle."".$session->get('debute')));
        }
            $periodicite->setCouleuralloccurence($session->get('couleur'));
        
            
        $delimiteur->setTitre($session->get('titre'));
        $delimiteur->setDebute(new DateTime($session->get('debute')));
        $delimiteur->setFin(new DateTime($session->get('fin')));
        $delimiteur->setAccessinternet($session->get('accessInternet'));
        $delimiteur->setBlockage($session->get('blockage'));
        $delimiteur->setPartage($session->get('partage'));
        $delimiteur->setPriorite($session->get('priorite'));
        $delimiteur->setPeriodicite($periodicite);
        
        $em->flush();
        $typeDelimiteurs=$em->getRepository('beanBundle:typeDelimiteur')->findAll();
        $data=$this->getAllEvents();
        return $this->render('DelimiteurBundle:Default:fullCalendar.html.twig',array('events'=>$data,'typeDelimiteurs'=>$typeDelimiteurs));
    }
    //selectionner un type de délimiteur pour récupérer son nom et son couleur
    public function selectDelimiteurAction($id){
        $em=$this->getDoctrine()->getManager();
        $typeDelimiteur=$em->getRepository('beanBundle:typeDelimiteur')->find($id);
        $nom =  $typeDelimiteur->getNom();
        $couleur =  $typeDelimiteur->getCouleur();
        $response= new JsonResponse();
        return $response->setData(array('nom'=>$nom,'couleur'=>$couleur));
    }
    //vérifier si un délimiteur est blocké ou pas
    public function findDelimiteurAction($id){
        $em=$this->getDoctrine()->getManager();
        $delimiteur=$em->getRepository('beanBundle:Delimiteur')->find($id);
        $blockage=$delimiteur->getBlockage();
         $response= new JsonResponse();
        return $response->setData(array('blockage'=>$blockage));
    }
    // ajouter un type de délimiteur
    public function ajouterTypeDelimiteurAction(){
        extract($_POST);
        extract($_GET);
        $em=$this->getDoctrine()->getManager();
        $typeDelimiteur=new typeDelimiteur();
        $typeDelimiteur->setNom($nom);
        $typeDelimiteur->setCouleur($couleur);
        $em->persist($typeDelimiteur);
        $em->flush();
        $typeDelimiteurs=$em->getRepository('beanBundle:typeDelimiteur')->findAll();
        return $this->render('DelimiteurBundle:Default:ajoutTypeDelimiteur.html.twig',array('typedelimiteurs'=>$typeDelimiteurs));
    }
    public function typeDelimiteurAction(){
         $em=$this->getDoctrine()->getManager();
          $typeDelimiteurs=$em->getRepository('beanBundle:typeDelimiteur')->findAll();
        return $this->render('DelimiteurBundle:Default:ajoutTypeDelimiteur.html.twig',array('typedelimiteurs'=>$typeDelimiteurs));
    }
    public function updateTypeAction($id){
        $em=$this->getDoctrine()->getManager();
        $typedelimiteur =$em->getRepository('beanBundle:typeDelimiteur')->find($id);
        
        return $this->render('DelimiteurBundle:Default:editType.html.twig',array('type'=>$typedelimiteur));
    }
    public function editTypeAction($id){
         extract($_POST);
        extract($_GET);
        $em=$this->getDoctrine()->getManager();
        $post=$this->getRequest();
        $typedelimiteur =$em->getRepository('beanBundle:typeDelimiteur')->find($id);
        $typedelimiteur->setNom($post->get('nom'));
        $typedelimiteur->setCouleur($post->get('couleur'));
        $em->flush();
         $typeDelimiteurs=$em->getRepository('beanBundle:typeDelimiteur')->findAll();
        return $this->render('DelimiteurBundle:Default:ajoutTypeDelimiteur.html.twig',array('typedelimiteurs'=>$typeDelimiteurs));
        
    }
    
    //afficher les délimiteurs et les rdvs
    public function fullAction(){
        $em = $this->getDoctrine()->getManager();
   $data=$this->getAllEvents();
   $typeDelimiteurs=$em->getRepository('beanBundle:typeDelimiteur')->findAll();
   $data2=$this->getAllRdvs();
   return $this->render('DelimiteurBundle:Default:fullCalendar.html.twig',array('events'=>$data,'rdvs'=>$data2,'typeDelimiteurs'=>$typeDelimiteurs,'content_type'=>'application/json'));
    }
    //récupérer les la date de début et la date defin du calendrier
    public function ajoutdelCalAction($startD, $startH, $endH) {
        $em = $this->getDoctrine()->getManager();

        $typedelimiteurs = $em->getRepository('beanBundle:typeDelimiteur')->findAll();
        return $this->render('DelimiteurBundle:Default:delimiteur.html.twig', array('startD' => $startD, 'endH' => $endH, 'startH' => $startH, 'typeDelimiteurs' => $typedelimiteurs));
    }
    //drag un délimiteur
    public function dropDelAction($id,$startD,$startH,$end,$endH){
        echo $startD + $startH;
        var_dump($startD);
        $em = $this->getDoctrine()->getManager();
        $delimiteur = $em->getRepository('beanBundle:Delimiteur')->find($id);
        $periodicite=$em->getRepository('beanBundle:Periodicite')->find($delimiteur->getPeriodicite()->getId());
        //$nstartD= $this->getTransformedDate($startD);
        //$nend= $this->getTransformedDate($end);
        $periodicite->setEffet(new DateTime($startD." ".$startH));
        $periodicite->setEnd(new DateTime($end." ".$startH));
        $delimiteur->setDebute(new DateTime($startH));
         //$interval=new DateInterval('P3H');
        $delimiteur->setFin(new DateTime($endH));
        $delimiteur->setPeriodicite($periodicite);
      // $em->persist($delimiteur);
        $em->flush();
        
        
       
    }
    
    // ajouter un délimiteur simple(en spécifiant juste les champs du périodicité)
    public function ajoutDelimiteurDirectAction($startD,$startH,$endH,$type){
         $em = $this->getDoctrine()->getManager();
         $delimiteur= new Delimiteur();
         $periodicite=new Periodicite();
         $typedelimiteur = $em->getRepository('beanBundle:typeDelimiteur')->find($type);
         $periodicite->setCouleuralloccurence($typedelimiteur->getCouleur());
         $periodicite->setEffet(new DateTime($startD." ".$startH));
         $periodicite->setEnd(new DateTime($startD." ".$endH));
         $delimiteur->setDebute(new DateTime($startH));
         $delimiteur->setFin(new DateTime($endH));
         $delimiteur->setId($this->getMax()+2);
         $delimiteur->setTitre($typedelimiteur->getNom());
         $em->persist($periodicite);
         $delimiteur->setPeriodicite($periodicite);
         $em->persist($delimiteur);
         $em->flush();
         $data=$this->getAllEvents();
         $data2=$this->getAllRdvs();
   $typeDelimiteurs=$em->getRepository('beanBundle:typeDelimiteur')->findAll();
   return $this->render('DelimiteurBundle:Default:fullCalendar.html.twig',array('events'=>$data,'rdvs'=>$data2,'typeDelimiteurs'=>$typeDelimiteurs,'content_type'=>'application/json'));
    }
    public function calculerAction(){
       
       return $this->render('DelimiteurBundle:Default:projet.html.twig');
    }
}            
    